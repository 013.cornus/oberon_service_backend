var FormValidation = function () {

    return {
        //main function to initiate the module
        init: function () {

            // for more info visit the official plugin documentation: 
            // http://docs.jquery.com/Plugins/Validation

            var form1 = $('#submitform');
            var error1 = $('.alert-error', form1);
            var success1 = $('.alert-success', form1);

            form1.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-inline', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",
                
                rules: {
                    productid: {
                        required: true
                    },

                    stockvalue: {
                        required: true
                    },

                    startday: {
                        required: true
                    },

                    endday: {
                        required: true
                    },

                },

                 messages: { // custom messages for radio buttons and checkboxes

                    productid: {
                        required: "此欄位必填"
                    },

                    stockvalue: {
                        required: "此欄位必填"
                    },

                    startday: {
                        required: "此欄位必填"
                    },

                    endday: {
                        required: "此欄位必填"
                    },
                },

                invalidHandler: function (event, validator) { //display error alert on form submit              
                    success1.hide();
                    error1.show();
                    App.scrollTo(error1, -200);
                },

                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.help-inline').removeClass('ok'); // display OK icon
                        $(element)
                        .closest('.control-group').removeClass('success').addClass('error'); // set error class to the control group
                 },

                unhighlight: function (element) { // revert the change dony by hightlight
                    $(element)
                        .closest('.control-group').removeClass('error'); // set error class to the control group
                },

                success: function (label,element) {
                    
                    if($(element).attr('name') == 'startday' || $(element).attr('name') == 'endday'){
                        label.next().next()
                        .addClass('valid').addClass('help-inline ok') // mark the current input as valid and display OK icon
                    .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                    }
                    else{
                        label
                         .addClass('valid').addClass('help-inline ok') // mark the current input as valid and display OK icon
                    .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group

                    }

             
                },

                submitHandler: function (form) {
                    error1.hide();
                    document.getElementById("submitform").submit();
                }
            });

         }

     };

 }();