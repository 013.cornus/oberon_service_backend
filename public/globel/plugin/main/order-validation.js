var FormValidation = function () {


    return {
        //main function to initiate the module
        init: function () {

            // for more info visit the official plugin documentation: 
            // http://docs.jquery.com/Plugins/Validation

            var form3 = $('#orderform');
            var error3 = $('.alert-error', form3);
            var success3 = $('.alert-success', form3);

            form3.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-inline', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",

                rules: {
                    contractnum: {
                        required: true
                    },

                    organization: {
                        required: true
                    },

                    customername: {
                        required: true
                    },

                    employeename: {
                        required: true
                    },

                    buydate: {
                        required: true
                    },

                    activedate: {
                        required: true
                    },

                    totalitem: {
                        required: true,
                        min :1
                    },

                },

                messages: { // custom messages for radio buttons and checkboxes
                    contractnum: {
                        required: "此欄位必填"
                    },

                    organization: {
                        required: "此欄位必填"
                    },

                    customername: {
                        required: "此欄位必填"
                    },

                    employeename: {
                        required: "此欄位必填"
                    },

                    buydate: {
                        required: "此欄位必填"
                    },

                    activedate: {
                        required: "此欄位必填"
                    },

                    totalitem: {
                        required: "商品數量不可小於1",
                        min : "商品數量不可小於1"
                    },

                },

                invalidHandler: function (event, validator) { //display error alert on form submit              
                    success3.hide();
                    error3.show();
                    App.scrollTo(error3, -200);
                },

                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.help-inline').removeClass('ok'); // display OK icon
                        $(element)
                        .closest('.control-group').removeClass('success').addClass('error'); // set error class to the control group
                    },

                unhighlight: function (element) { // revert the change dony by hightlight
                    $(element)
                        .closest('.control-group').removeClass('error'); // set error class to the control group
                    },

                    success: function (label) {
                        label
                        .addClass('valid').addClass('help-inline ok') // mark the current input as valid and display OK icon
                    .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                },

                submitHandler: function (form) {
                    error3.hide();
                    document.getElementById("orderform").submit();
                }
            });

        }

    };

}();