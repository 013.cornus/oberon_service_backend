var FormValidation = function () {


    return {
        //main function to initiate the module
        init: function () {

            // for more info visit the official plugin documentation: 
            // http://docs.jquery.com/Plugins/Validation

            var form1 = $('#customerform');
            var error1 = $('.alert-error', form1);
            var success1 = $('.alert-success', form1);

            form1.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-inline', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",
                rules: {
                    name: {
                        required: true
                    },

                    idcard: {
                        required: true
                    },

                    paddressdetail: {
                        required: true
                    },

                    raddressdetail: {
                        required: true
                    },

                    cellphone: {
                        required: true,
                    },

                    organization: {
                        required: true
                    },

                    referrer: {
                        required: true
                    },

                    supervisor: {
                        required: true
                    },

                    occupation: {
                       required: true
                    },

                    bank: {
                        required: true
                    },

                    branch: {
                        required: true
                    },

                    account: {
                        required: true
                    },

                    accountname: {
                        required: true
                    },
                },

                 messages: { // custom messages for radio buttons and checkboxes

                    name: {
                        required: "此欄位必輸"
                    },

                    idcard: {
                        required: "此欄位必輸"
                    },

                    paddressdetail: {
                        required: "此欄位必輸"
                    },

                    raddressdetail: {
                        required: "此欄位必輸"
                    },

                    cellphone: {
                        required: "此欄位必輸"
                    },

                    organization: {
                        required: "此欄位必輸"
                    },

                    referrer: {
                        required: "此欄位必輸"
                    },

                    supervisor: {
                        required: "此欄位必輸"
                    },

                    occupation: {
                       required: "此欄位必輸"
                    },

                    bank: {
                        required: "此欄位必輸"
                    },

                    branch: {
                        required: "此欄位必輸"
                    },

                    account: {
                       required: "此欄位必輸"
                    },

                    accountname: {
                        required: "此欄位必輸"
                    },
                },

                invalidHandler: function (event, validator) { //display error alert on form submit              
                    success1.hide();
                    error1.show();
                    App.scrollTo(error1, -200);
                },

                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.help-inline').removeClass('ok'); // display OK icon
                        $(element)
                        .closest('.control-group').removeClass('success').addClass('error'); // set error class to the control group
                    },

                unhighlight: function (element) { // revert the change dony by hightlight
                    $(element)
                        .closest('.control-group').removeClass('error'); // set error class to the control group
                    },

                    success: function (label) {
                        label
                        .addClass('valid').addClass('help-inline ok') // mark the current input as valid and display OK icon
                    .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                },

                submitHandler: function (form) {
                    error1.hide();
                    document.getElementById("customerform").submit();
                }
            });

            //Sample 2
            $('#form_2_select2').select2({
                placeholder: "Select an Option",
                allowClear: true
            });

            var form2 = $('#employeeform');
            var error2 = $('.alert-error', form2);
            var success2 = $('.alert-success', form2);

            form2.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-inline', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",
                rules: {
                    name: {
                        required: true
                    },

                    idcard: {
                        required: true
                    },

                    cellphone: {
                        required: true,
                    },

                    organization: {
                        required: true
                    },

                    referrer: {
                        required: true
                    },

                    supervisor: {
                        required: true
                    },

                    occupation: {
                       required: true
                    },

                    bank: {
                        required: true
                    },

                    branch: {
                        required: true
                    },

                    account: {
                        required: true
                    },
                },

                messages: { // custom messages for radio buttons and checkboxes

                     name: {
                        required: "此欄位必輸"
                    },

                    idcard: {
                       required: "此欄位必輸"
                    },

                    cellphone: {
                        required: "此欄位必輸"
                    },

                    organization: {
                       required: "此欄位必輸"
                    },

                    referrer: {
                        required: "此欄位必輸"
                    },

                    supervisor: {
                        required: "此欄位必輸"
                    },

                    occupation: {
                       required: "此欄位必輸"
                    },

                    bank: {
                        required: "此欄位必輸"
                    },

                    branch: {
                        required: "此欄位必輸"
                    },

                    account: {
                        required: "此欄位必輸"
                    },
                },

                errorPlacement: function (error, element) { // render error placement for each input type
                    if (element.attr("name") == "education") { // for chosen elements, need to insert the error after the chosen container
                        error.insertAfter("#form_2_education_chzn");
                    } else if (element.attr("name") == "membership") { // for uniform radio buttons, insert the after the given container
                        error.addClass("no-left-padding").insertAfter("#form_2_membership_error");
                    } else if (element.attr("name") == "service") { // for uniform checkboxes, insert the after the given container
                        error.addClass("no-left-padding").insertAfter("#form_2_service_error");
                    } else {
                        error.insertAfter(element); // for other inputs, just perform default behavoir
                    }
                },

                invalidHandler: function (event, validator) { //display error alert on form submit   
                    success2.hide();
                    error2.show();
                    App.scrollTo(error2, -200);
                },

                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.help-inline').removeClass('ok'); // display OK icon
                        $(element)
                        .closest('.control-group').removeClass('success').addClass('error'); // set error class to the control group
                    },

                unhighlight: function (element) { // revert the change dony by hightlight
                    $(element)
                        .closest('.control-group').removeClass('error'); // set error class to the control group
                    },

                    success: function (label) {
                    if (label.attr("for") == "service" || label.attr("for") == "membership") { // for checkboxes and radip buttons, no need to show OK icon
                        label
                        .closest('.control-group').removeClass('error').addClass('success');
                        label.remove(); // remove error label here
                    } else { // display success icon for other inputs
                        label
                            .addClass('valid').addClass('help-inline ok') // mark the current input as valid and display OK icon
                        .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                    }
                },

                submitHandler: function (form) {
                    document.getElementById("employeeform").submit();
                }

            });

            //apply validation on chosen dropdown value change, this only needed for chosen dropdown integration.
            $('.chosen, .chosen-with-diselect', form2).change(function () {
                form2.validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
            });

             //apply validation on select2 dropdown value change, this only needed for chosen dropdown integration.
             $('.select2', form2).change(function () {
                form2.validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
            });

            var form3 = $('#orderform');
            var error3 = $('.alert-error', form3);
            var success3 = $('.alert-success', form3);

            form3.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-inline', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",

                rules: {
                    contractnum: {
                        required: true
                    },

                    customername: {
                        required: true
                    },

                    employeename: {
                        required: true
                    },

                    paytype: {
                        required: true
                    },

                    activedate: {
                        required: true,
                    },

                    signdate: {
                        required: true
                    },
                },

                messages: { // custom messages for radio buttons and checkboxes

                     name: {
                        required: "此欄位必輸"
                    },

                   contractnum: {
                        required: "此欄位必輸"
                    },

                    customername: {
                        required: "此欄位必輸"
                    },

                    employeename: {
                        required: "此欄位必輸"
                    },

                    paytype: {
                        required: "此欄位必輸"
                    },

                    activedate: {
                        required: "此欄位必輸"
                    },

                    signdate: {
                        required: "此欄位必輸"
                    },
                },

                invalidHandler: function (event, validator) { //display error alert on form submit              
                    success3.hide();
                    error3.show();
                    App.scrollTo(error3, -200);
                },

                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.help-inline').removeClass('ok'); // display OK icon
                        $(element)
                        .closest('.control-group').removeClass('success').addClass('error'); // set error class to the control group
                    },

                unhighlight: function (element) { // revert the change dony by hightlight
                    $(element)
                        .closest('.control-group').removeClass('error'); // set error class to the control group
                    },

                    success: function (label) {
                        label
                        .addClass('valid').addClass('help-inline ok') // mark the current input as valid and display OK icon
                    .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                },

                submitHandler: function (form) {
                    error3.hide();
                    document.getElementById("orderform").submit();
                }
            });

         }

     };

 }();