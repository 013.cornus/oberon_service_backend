
$( "#submitform" ).validate({
		// 參數設定
		rules:{
			password_1:{
				required:true,
			},
			password_2:{
				required: true,
				equalTo: '#password_1', 
				rangelength: [6, 20], 
			}
		},
		messages:{
			password_1: {
				required: "<span style='color:red;'>必填欄位！請輸入密碼</span>",
			},
			password_2: { 
				required: "<span style='color:red;'>必填欄位！請輸入密碼</span>",
				equalTo: "<span style='color:red;'>密碼不一致</span>", 
				rangelength: "<span style='color:red;'>密碼長度必須介於 {0} 至 {1} 之間！</span>", 
			} 
		},
		errorPlacement: function( error, element ) {
			error.appendTo(element.siblings("#errorMsg"));
			return true;
		}
	});

