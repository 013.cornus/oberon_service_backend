<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Exportexcel_model extends CI_model{

    //  報表模板設置
    public function __construct(){
        parent::__construct();
        $this->load->library('excel');
    }

    //  客戶報表輸出
    public function customers($data_arr){

       if (!is_array($data_arr)) {
            echo '參數格式不符合！';
            exit;
        }

        $objPHPExcel = new PHPExcel();

        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->setTitle('客戶清冊');
        $objPHPExcel->getActiveSheet()->mergeCells('A1:P1');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
        $objPHPExcel->getActiveSheet()->getStyle('A2:P2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true)->setSize(20);
        $objPHPExcel->getActiveSheet()->setCellValue('A1', '客戶清冊');
        $objPHPExcel->getActiveSheet()->setCellValue('A2', '姓名');
        $objPHPExcel->getActiveSheet()->setCellValue('B2', '出生年月日');
        $objPHPExcel->getActiveSheet()->setCellValue('C2', '身分證字號');
        $objPHPExcel->getActiveSheet()->setCellValue('D2', '通訊地址');
        $objPHPExcel->getActiveSheet()->setCellValue('E2', '戶籍地址');
        $objPHPExcel->getActiveSheet()->setCellValue('F2', '手機');
        $objPHPExcel->getActiveSheet()->setCellValue('G2', '聯絡電話(公)');
        $objPHPExcel->getActiveSheet()->setCellValue('H2', '聯絡電話(宅)');
        $objPHPExcel->getActiveSheet()->setCellValue('I2', '電子郵件信箱');
        $objPHPExcel->getActiveSheet()->setCellValue('J2', '戶名');
        $objPHPExcel->getActiveSheet()->setCellValue('K2', '匯款銀行');
        $objPHPExcel->getActiveSheet()->setCellValue('L2', '匯款帳號');
        $objPHPExcel->getActiveSheet()->setCellValue('M2', '經辦人員');
        $objPHPExcel->getActiveSheet()->setCellValue('N2', '營業處');
        $objPHPExcel->getActiveSheet()->setCellValue('O2', '備註');
        $objPHPExcel->getActiveSheet()->setCellValue('P2', '狀態');
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(50);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(50);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(30);

        $row = 3;
        foreach($data_arr as $customer){

            $objPHPExcel->getActiveSheet()->getStyle('I'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT); 
            //姓名
            $objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $customer['customer_name']);
            //出生年月日
            $objPHPExcel->getActiveSheet()->setCellValue('B'.$row, $customer['customer_birthday']);
            //身份證字號
            $objPHPExcel->getActiveSheet()->setCellValue('C'.$row, $customer['customer_id_card']);
            //通訊地址
            $objPHPExcel->getActiveSheet()->setCellValue('D'.$row, $customer['customer_permanent_address']);
            //戶籍地址
            $objPHPExcel->getActiveSheet()->setCellValue('E'.$row, $customer['customer_residential_address']);
            //手機
            $objPHPExcel->getActiveSheet()->setCellValue('F'.$row, $customer['customer_phone']);
            //聯絡電話(公)
            $objPHPExcel->getActiveSheet()->setCellValue('G'.$row, $customer['customer_company_tel']);
            //聯絡電話(宅)
            $objPHPExcel->getActiveSheet()->setCellValue('H'.$row, $customer['customer_residential_tel']);
            //電子郵件信箱
            $objPHPExcel->getActiveSheet()->setCellValue('I'.$row, $customer['customer_email']);
            //戶名
            $objPHPExcel->getActiveSheet()->setCellValue('J'.$row, $customer['customer_account_name']);
            //匯款銀行
            $objPHPExcel->getActiveSheet()->setCellValue('K'.$row, $customer['branch']);
            //匯款帳號
            $objPHPExcel->getActiveSheet()->setCellValue('L'.$row, $customer['customer_account']);
            //經辦人員
            $objPHPExcel->getActiveSheet()->setCellValue('M'.$row, $customer['employee_name']);
            //營業處
            $objPHPExcel->getActiveSheet()->setCellValue('N'.$row, $customer['organization_name']);
            //備註
            $objPHPExcel->getActiveSheet()->setCellValue('O'.$row, $customer['customer_note']);
            //狀態
            $objPHPExcel->getActiveSheet()->setCellValue('P'.$row, $customer['status']);

            $row++;
        }

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        ob_end_clean();
        $filename = date("Ymd")."客戶清冊.xls";
        //  DOWNLOAD FILE
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename='.$filename.'');
        header('Cache-Control: max-age=0');

        $objWriter->save('php://output');
    }

    //  業務承攬人報表輸出
    public function employee($data_arr){

        if (!is_array($data_arr)) {
            echo '參數格式不符合！';
            exit;
        }

        $objPHPExcel = new PHPExcel();

        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->setTitle('業務承攬人清冊');
        $objPHPExcel->getActiveSheet()->mergeCells('A1:K1');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
        $objPHPExcel->getActiveSheet()->getStyle('A2:K2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true)->setSize(20);
        $objPHPExcel->getActiveSheet()->setCellValue('A1', '業務承攬人清冊');
        $objPHPExcel->getActiveSheet()->setCellValue('A2', '姓名');
        $objPHPExcel->getActiveSheet()->setCellValue('B2', '身分證字號');
        $objPHPExcel->getActiveSheet()->setCellValue('C2', '營業處');
        $objPHPExcel->getActiveSheet()->setCellValue('D2', '職稱');
        $objPHPExcel->getActiveSheet()->setCellValue('E2', '直屬經理');
        $objPHPExcel->getActiveSheet()->setCellValue('F2', '推薦人');
        $objPHPExcel->getActiveSheet()->setCellValue('G2', '戶名');
        $objPHPExcel->getActiveSheet()->setCellValue('H2', '銀行名稱');
        $objPHPExcel->getActiveSheet()->setCellValue('I2', '分行代號');
        $objPHPExcel->getActiveSheet()->setCellValue('J2', '匯款帳號');
        $objPHPExcel->getActiveSheet()->setCellValue('K2', '備註');
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(30);

        $row = 3;
        foreach($data_arr as $value){

            $objPHPExcel->getActiveSheet()->getStyle('I'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT); 

            //姓名
            $objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $value['employee_name']);
            //身分證字號
            $objPHPExcel->getActiveSheet()->setCellValue('B'.$row, $value['employee_id_card']);
            //營業處
            $objPHPExcel->getActiveSheet()->setCellValue('C'.$row, $value['organization_name']);
            //職稱
            $objPHPExcel->getActiveSheet()->setCellValue('D'.$row, $value['occupation_name']);
            //直屬經理
            $objPHPExcel->getActiveSheet()->setCellValue('E'.$row, $value['supervisor_name']);
            //推薦人
            $objPHPExcel->getActiveSheet()->setCellValue('F'.$row, $value['referrer_name']);
            //戶名
            $objPHPExcel->getActiveSheet()->setCellValue('G'.$row, $value['employee_account_name']);
            //銀行名稱
            $objPHPExcel->getActiveSheet()->setCellValue('H'.$row, $value['bank']);
            //分行代號
            $objPHPExcel->getActiveSheet()->setCellValue('I'.$row, $value['bank_branch_code']);
            //匯款帳號
            $objPHPExcel->getActiveSheet()->setCellValue('J'.$row, $value['employee_bank_account']);
            //備註
            $objPHPExcel->getActiveSheet()->setCellValue('K'.$row, $value['employee_note']);
            //狀態
            $objPHPExcel->getActiveSheet()->setCellValue('K'.$row, $value['status']);

            $row++;
        }

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        ob_end_clean();
        $filename = date("Ymd")."業務承攬人清冊.xls";
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename='.$filename.'');
        header('Cache-Control: max-age=0');

        $objWriter->save('php://output');
    }

    //  獎金異動報表輸出
    public function bonusChange($data_arr){

        if (!is_array($data_arr)) {
            echo '參數格式不符合！';
            exit;
        }

        $objPHPExcel = new PHPExcel();

        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->setTitle('獎金清冊');
        $objPHPExcel->getActiveSheet()->mergeCells('A1:H1');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
        $objPHPExcel->getActiveSheet()->getStyle('A2:H2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true)->setSize(20);
        $objPHPExcel->getActiveSheet()->setCellValue('A1', '獎金清冊');
        $objPHPExcel->getActiveSheet()->setCellValue('A2', '獎金發放狀態');
        $objPHPExcel->getActiveSheet()->setCellValue('B2', '合約書編號');
        $objPHPExcel->getActiveSheet()->setCellValue('C2', '業務承攬人');
        $objPHPExcel->getActiveSheet()->setCellValue('D2', '所屬單位');
        $objPHPExcel->getActiveSheet()->setCellValue('E2', '職稱');
        $objPHPExcel->getActiveSheet()->setCellValue('F2', '獎金名稱');
        $objPHPExcel->getActiveSheet()->setCellValue('G2', '獎金金額');
        $objPHPExcel->getActiveSheet()->setCellValue('H2', '合約生效日');
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(25);

        $row = 3;
        foreach($data_arr as $value){
            //獎金發放狀態
            $objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $value['users_bonus_is_check']);
            //合約書編號
            $objPHPExcel->getActiveSheet()->setCellValue('B'.$row, $value['contract_id']);
            //業務承攬人
            $objPHPExcel->getActiveSheet()->setCellValue('C'.$row, $value['employee_name']);
            //所屬單位
            $objPHPExcel->getActiveSheet()->setCellValue('D'.$row, $value['organization_name']);
            //職稱
            $objPHPExcel->getActiveSheet()->setCellValue('E'.$row, $value['occupation_name']);
            //獎金名稱
            $objPHPExcel->getActiveSheet()->setCellValue('F'.$row, $value['bonus_type_name']);
            //獎金金額
            $objPHPExcel->getActiveSheet()->setCellValue('G'.$row, $value['users_bonus_money']);
            //合約生效日
            $objPHPExcel->getActiveSheet()->setCellValue('H'.$row, $value['order_active_date']);

            $row++;
        }

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        ob_end_clean();
        $filename = date("Ymd")."-".date("m")."月獎金清冊.xls";
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename='.$filename.'');
        header('Cache-Control: max-age=0');

        $objWriter->save('php://output');
    }

    //  股權待售報表輸出
    public function stocksold($data_arr){
        
        if (!is_array($data_arr)) {
            echo '參數格式不符合！';
            exit;
        }

        $objPHPExcel = new PHPExcel();

        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->setTitle('股權代售清冊');
        $objPHPExcel->getActiveSheet()->mergeCells('A1:H1');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
        $objPHPExcel->getActiveSheet()->getStyle('A2:H2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true)->setSize(20);
        $objPHPExcel->getActiveSheet()->setCellValue('A1', '股權代售清冊');
        $objPHPExcel->getActiveSheet()->setCellValue('A2', '合約書編號');
        $objPHPExcel->getActiveSheet()->setCellValue('B2', '客戶姓名');
        $objPHPExcel->getActiveSheet()->setCellValue('C2', '承購時間');
        $objPHPExcel->getActiveSheet()->setCellValue('D2', '承購股數');
        $objPHPExcel->getActiveSheet()->setCellValue('E2', '本月股價');
        $objPHPExcel->getActiveSheet()->setCellValue('F2', '狀態');
        $objPHPExcel->getActiveSheet()->setCellValue('G2', '經辦人員');
        $objPHPExcel->getActiveSheet()->setCellValue('H2', '營業處');
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);

        $row = 3;
        foreach($data_arr as $value){
            //合約書編號
            $objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $value['contract_id']);
            //客戶姓名
            $objPHPExcel->getActiveSheet()->setCellValue('B'.$row, $value['customer_name']);
            //承購時間
            $objPHPExcel->getActiveSheet()->setCellValue('C'.$row, substr($value['order_active_date'],0,10));
            //承購股數
            $objPHPExcel->getActiveSheet()->setCellValue('D'.$row, $value['order_stock']);
            //本月股價
            $objPHPExcel->getActiveSheet()->setCellValue('E'.$row, $value['stock_value']);
            //狀態
            $objPHPExcel->getActiveSheet()->setCellValue('F'.$row, $value['status']);
            //經辦人員
            $objPHPExcel->getActiveSheet()->setCellValue('G'.$row, $value['employee_name']);
            //營業處
            $objPHPExcel->getActiveSheet()->setCellValue('H'.$row, $value['organization_name']);

            $row++;
        }

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        ob_end_clean();
        $filename = date("Ymd")."-".date("m")."月股權代售清冊.xls";
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename='.$filename.'');
        header('Cache-Control: max-age=0');

        $objWriter->save('php://output');
    }

    //  訂單報表輸出
    public function order($data_arr,$order_date){
       
       if (!is_array($data_arr)) {
            echo '參數格式不符合！';
            exit;
        }

        $objPHPExcel = new PHPExcel();

        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->setTitle('訂單清冊');
        $objPHPExcel->getActiveSheet()->mergeCells('A1:P1');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
        $objPHPExcel->getActiveSheet()->getStyle('A2:P2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true)->setSize(20);
        $objPHPExcel->getActiveSheet()->setCellValue('A1', '訂單清冊');
        $objPHPExcel->getActiveSheet()->setCellValue('A2', '審核狀態');
        $objPHPExcel->getActiveSheet()->setCellValue('B2', '承購日期');
        $objPHPExcel->getActiveSheet()->setCellValue('C2', '生效日');
        $objPHPExcel->getActiveSheet()->setCellValue('D2', '合約書編號');
        $objPHPExcel->getActiveSheet()->setCellValue('E2', '承購人姓名');
        $objPHPExcel->getActiveSheet()->setCellValue('F2', '承購商品');
        $objPHPExcel->getActiveSheet()->setCellValue('G2', '承購單位');
        $objPHPExcel->getActiveSheet()->setCellValue('H2', '承購商品');
        $objPHPExcel->getActiveSheet()->setCellValue('I2', '承購單位');
        $objPHPExcel->getActiveSheet()->setCellValue('J2', '合計金額');
        $objPHPExcel->getActiveSheet()->setCellValue('K2', '付款別');
        $objPHPExcel->getActiveSheet()->setCellValue('L2', '簽收日');
        $objPHPExcel->getActiveSheet()->setCellValue('M2', '經辦人員');
        $objPHPExcel->getActiveSheet()->setCellValue('N2', '直屬經理');
        $objPHPExcel->getActiveSheet()->setCellValue('O2', '備註1');
        $objPHPExcel->getActiveSheet()->setCellValue('P2', '備註2');
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(40);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(40);
        $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(30);

        $row = 3;
        foreach($data_arr as $value){
            //訂單審核狀態
            $objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $value['order_is_check']);
            //承購日期
            $objPHPExcel->getActiveSheet()->setCellValue('B'.$row, substr($value['order_created_date'],0,10));
            //生效日
            $objPHPExcel->getActiveSheet()->setCellValue('C'.$row, substr($value['order_active_date'],0,10));
            //合約書編號
            $objPHPExcel->getActiveSheet()->setCellValue('D'.$row, $value['contract_id']);
            //承購人姓名
            $objPHPExcel->getActiveSheet()->setCellValue('E'.$row, $value['customer_name']);
            //基金
            $objPHPExcel->getActiveSheet()->setCellValue('F'.$row, $value['product_name']);
            //基金
            $objPHPExcel->getActiveSheet()->setCellValue('G'.$row, $value['order_info_count']);
            //股權
            $objPHPExcel->getActiveSheet()->setCellValue('H'.$row, "茂云實業投資有限公司股權");
            //股權
            $objPHPExcel->getActiveSheet()->setCellValue('I'.$row, $value['order_info_count_1']);
            //合計金額
            $objPHPExcel->getActiveSheet()->setCellValue('J'.$row, $value['totalprice']);
            //付款別
            $objPHPExcel->getActiveSheet()->setCellValue('K'.$row, $value['order_pay_type']);
            //到期日
            // $objPHPExcel->getActiveSheet()->setCellValue('L'.$row, substr($order['order_over_date'],0,10));
            //簽收日
            $objPHPExcel->getActiveSheet()->setCellValue('L'.$row, substr($value['order_sign_date'],0,10));
            //經辦人員
            $objPHPExcel->getActiveSheet()->setCellValue('M'.$row, $value['employee_name']);
            //直屬經理
            $objPHPExcel->getActiveSheet()->setCellValue('N'.$row, $value['supervisor_name']);
            //備註1
            $objPHPExcel->getActiveSheet()->setCellValue('O'.$row, $value['order_note_1']);
            //備註2
            $objPHPExcel->getActiveSheet()->setCellValue('P'.$row, $value['order_note_2']);

            $row++;
        }

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        ob_end_clean();
        if ($this->input->get('id') != '') {
            $orginfo = $this->organization_model->getData($this->input->get('id'));
            $orgname = $orginfo['organization_name'];
            $filename = $order_date['year'].$order_date['selectmonth'].$order_date['day']."-".$order_date['selectmonth']."月訂單清冊"."-".$orgname.".xls";
        }
        else{
            $filename = $order_date['year'].$order_date['selectmonth'].$order_date['day']."-".$order_date['selectmonth']."月訂單清冊.xls";
        }
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename='.$filename.'');
        header('Cache-Control: max-age=0');

        $objWriter->save('php://output');
    }

    //  利得發放報表輸出
    public function interest($data_arr,$product_info){
        
        if (!is_array($data_arr)) {
            echo '參數格式不符合！';
            exit;
        }

        $row = 2;
        $acsnum = 69;
        $productrow = 0;
        foreach($product_info as $productlist){
            $productrow++;
        }
        $lastrow = $acsnum + $productrow + 9;

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->setTitle('利得發放清冊');
        $objPHPExcel->getActiveSheet()->mergeCells('A1:'.$this->exportexcel_model->row_code($lastrow).'1');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
        $objPHPExcel->getActiveSheet()->getStyle('A2:'.$this->exportexcel_model->row_code($lastrow).'2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true)->setSize(20);
        $objPHPExcel->getActiveSheet()->setCellValue('A1', '利得發放清冊');
        $objPHPExcel->getActiveSheet()->setCellValue('A2', '序號');
        $objPHPExcel->getActiveSheet()->setCellValue('B2', '合約書編號');
        $objPHPExcel->getActiveSheet()->setCellValue('C2', '生效日');
        $objPHPExcel->getActiveSheet()->setCellValue('D2', '客戶姓名');
        foreach($product_info as $productlist){
            $objPHPExcel->getActiveSheet()->setCellValue($this->exportexcel_model->row_code($acsnum).$row, $productlist['product_name']);
            $objPHPExcel->getActiveSheet()->getColumnDimension($this->exportexcel_model->row_code($acsnum))->setWidth(40);
            $acsnum++;
        }
        $objPHPExcel->getActiveSheet()->setCellValue($this->exportexcel_model->row_code($acsnum).$row, '總單位');
        $objPHPExcel->getActiveSheet()->setCellValue($this->exportexcel_model->row_code($acsnum+1).$row, '建置期利息補貼');
        $objPHPExcel->getActiveSheet()->setCellValue($this->exportexcel_model->row_code($acsnum+2).$row, '季利得');
        $objPHPExcel->getActiveSheet()->setCellValue($this->exportexcel_model->row_code($acsnum+3).$row, '合計');
        $objPHPExcel->getActiveSheet()->setCellValue($this->exportexcel_model->row_code($acsnum+4).$row, '業務姓名');
        $objPHPExcel->getActiveSheet()->setCellValue($this->exportexcel_model->row_code($acsnum+5).$row, '直屬經理');
        $objPHPExcel->getActiveSheet()->setCellValue($this->exportexcel_model->row_code($acsnum+6).$row, '營業處');
        $objPHPExcel->getActiveSheet()->setCellValue($this->exportexcel_model->row_code($acsnum+7).$row, '分行');
        $objPHPExcel->getActiveSheet()->setCellValue($this->exportexcel_model->row_code($acsnum+8).$row, '銀行代號');
        $objPHPExcel->getActiveSheet()->setCellValue($this->exportexcel_model->row_code($acsnum+9).$row, '帳號');
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension($this->exportexcel_model->row_code($acsnum+1))->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension($this->exportexcel_model->row_code($acsnum+3))->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension($this->exportexcel_model->row_code($acsnum+4))->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension($this->exportexcel_model->row_code($acsnum+5))->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension($this->exportexcel_model->row_code($acsnum+6))->setWidth(40);
        $objPHPExcel->getActiveSheet()->getColumnDimension($this->exportexcel_model->row_code($acsnum+7))->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension($this->exportexcel_model->row_code($acsnum+8))->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension($this->exportexcel_model->row_code($acsnum+9))->setWidth(30);

        $rowcount = 3;
        $num = 1;

        foreach($data_arr as $key => $value){
            $acsnum = 69;
            $objPHPExcel->getActiveSheet()->getStyle('R'.$rowcount)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

            //序號
            $objPHPExcel->getActiveSheet()->setCellValue('A'.$rowcount, $num);
            //合約書編號
            $objPHPExcel->getActiveSheet()->setCellValue('B'.$rowcount, $value['contract_id']);
            //生效日
            $objPHPExcel->getActiveSheet()->setCellValue('C'.$rowcount, substr($value['order_active_date'],0,10));
            //客戶姓名
            $objPHPExcel->getActiveSheet()->setCellValue('D'.$rowcount, $value['customer_name']);

            //商品項目
            foreach($product_info as $productlist){
                $objPHPExcel->getActiveSheet()->setCellValue($this->exportexcel_model->row_code($acsnum).$rowcount,$productlist[$key]['product_num']);
                $acsnum++;
            }

            //總單位
           $objPHPExcel->getActiveSheet()->setCellValue($this->exportexcel_model->row_code($acsnum).$rowcount, $value['total']);
            //建置期利息補貼
            $objPHPExcel->getActiveSheet()->setCellValue($this->exportexcel_model->row_code($acsnum+1).$rowcount, $value['interest']);
            //季利得
            $objPHPExcel->getActiveSheet()->setCellValue($this->exportexcel_model->row_code($acsnum+2).$rowcount, $value['qinterest']);
            //合計
            $objPHPExcel->getActiveSheet()->setCellValue($this->exportexcel_model->row_code($acsnum+3).$rowcount,$value['interest_total']);
            //業務姓名
            $objPHPExcel->getActiveSheet()->setCellValue($this->exportexcel_model->row_code($acsnum+4).$rowcount,$value['employee_name']);
            //直屬經理
            $objPHPExcel->getActiveSheet()->setCellValue($this->exportexcel_model->row_code($acsnum+5).$rowcount,$value['supervisor_name']);
            //營業處
            $objPHPExcel->getActiveSheet()->setCellValue($this->exportexcel_model->row_code($acsnum+6).$rowcount,$value['organization_name']);
            //分行
            $objPHPExcel->getActiveSheet()->setCellValue($this->exportexcel_model->row_code($acsnum+7).$rowcount,$value['branch']);
            //銀行代號
            $objPHPExcel->getActiveSheet()->setCellValue($this->exportexcel_model->row_code($acsnum+8).$rowcount,$value['branchcode']);
            //帳號
            $objPHPExcel->getActiveSheet()->setCellValue($this->exportexcel_model->row_code($acsnum+9).$rowcount,$value['customer_account']);

            $num++;
            $rowcount++;
        }

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        ob_end_clean();
        $filename = date("Ymd")."-".date("m")."月利得發放清冊.xls";
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename='.$filename.'');
        header('Cache-Control: max-age=0');

        $objWriter->save('php://output');
    }

    //  業務人員獎金總表輸出
    public function employeeBouns($data_arr,$product_info){
        
        if (!is_array($data_arr)) {
            echo '參數格式不符合！';
            exit;
        }
        $row = 2;
        $acsnum = 67;
        $productrow = 0;
        foreach($product_info as $productlist){
            $productrow++;
        }
        $lastrow = $acsnum + $productrow + 9;

         $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true)->setSize(20);
        $objPHPExcel->getActiveSheet()->setTitle('營業處獎金總表');
        $objPHPExcel->getActiveSheet()->mergeCells('A1:'.$this->exportexcel_model->row_code($lastrow).'1');
        $objPHPExcel->getActiveSheet()->setCellValue('A1', '營業處獎金總表');
        $objPHPExcel->getActiveSheet()->setCellValue('A2', '營業處');
        $objPHPExcel->getActiveSheet()->setCellValue('B2', '業務姓名');
        foreach($product_info as $productlist){
            $objPHPExcel->getActiveSheet()->setCellValue($this->exportexcel_model->row_code($acsnum).$row, $productlist['product_name']);
            $objPHPExcel->getActiveSheet()->getColumnDimension($this->exportexcel_model->row_code($acsnum))->setWidth(40);
            $acsnum++;
        }
        $objPHPExcel->getActiveSheet()->setCellValue($this->exportexcel_model->row_code($acsnum).$row, '總單位');
        $objPHPExcel->getActiveSheet()->setCellValue($this->exportexcel_model->row_code($acsnum+1).$row, '業績獎金');
        $objPHPExcel->getActiveSheet()->setCellValue($this->exportexcel_model->row_code($acsnum+2).$row, '差額獎金');
        $objPHPExcel->getActiveSheet()->setCellValue($this->exportexcel_model->row_code($acsnum+3).$row, '同階獎金');
        $objPHPExcel->getActiveSheet()->setCellValue($this->exportexcel_model->row_code($acsnum+4).$row, '服務津貼');
        $objPHPExcel->getActiveSheet()->setCellValue($this->exportexcel_model->row_code($acsnum+5).$row, '其他加項');
        $objPHPExcel->getActiveSheet()->setCellValue($this->exportexcel_model->row_code($acsnum+6).$row, '說明');
        $objPHPExcel->getActiveSheet()->setCellValue($this->exportexcel_model->row_code($acsnum+7).$row, '其他扣項');
        $objPHPExcel->getActiveSheet()->setCellValue($this->exportexcel_model->row_code($acsnum+8).$row, '說明');
        $objPHPExcel->getActiveSheet()->setCellValue($this->exportexcel_model->row_code($acsnum+9).$row, '總計');
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension($this->exportexcel_model->row_code($acsnum+9))->setWidth(30);

        $rowcount = 3;      
        foreach($data_arr as $key => $value){

           $acsnum = 67;
            //營業處名稱
            $objPHPExcel->getActiveSheet()->setCellValue('A'.$rowcount,$value['orgname']);
            //業務姓名
            $objPHPExcel->getActiveSheet()->setCellValue('B'.$rowcount,$value['employeename']);
            foreach($product_info as $productlist){
                $objPHPExcel->getActiveSheet()->setCellValue($this->exportexcel_model->row_code($acsnum).$rowcount, $productlist[$key]['product_num']);
                $acsnum++;

            }
            //總單位
            $objPHPExcel->getActiveSheet()->setCellValue($this->exportexcel_model->row_code($acsnum).$rowcount,$value['totalpdnum']);
            //業績獎金
            $objPHPExcel->getActiveSheet()->setCellValue($this->exportexcel_model->row_code($acsnum+1).$rowcount,$value['count']);
            //差額獎金
            $objPHPExcel->getActiveSheet()->setCellValue($this->exportexcel_model->row_code($acsnum+2).$rowcount,$value['allowance']);
            //同階獎金
            $objPHPExcel->getActiveSheet()->setCellValue($this->exportexcel_model->row_code($acsnum+3).$rowcount,$value['samelvallowance']);
            //服務津貼
            $objPHPExcel->getActiveSheet()->setCellValue($this->exportexcel_model->row_code($acsnum+4).$rowcount,round($value['totalallowance']));
            //其他加項
            $objPHPExcel->getActiveSheet()->setCellValue($this->exportexcel_model->row_code($acsnum+5).$rowcount,$value['addmoney']);
            //說明
            $objPHPExcel->getActiveSheet()->setCellValue($this->exportexcel_model->row_code($acsnum+6).$rowcount,$value['addtxt']);
            //其他減項
            $objPHPExcel->getActiveSheet()->setCellValue($this->exportexcel_model->row_code($acsnum+7).$rowcount,$value['lessmoney']);
            //說明
            $objPHPExcel->getActiveSheet()->setCellValue($this->exportexcel_model->row_code($acsnum+8).$rowcount,$value['lesstxt']);
            //總計
            $objPHPExcel->getActiveSheet()->setCellValue($this->exportexcel_model->row_code($acsnum+9).$rowcount,$value['total']);

            $rowcount++;
        }

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        ob_end_clean();
        $filename = date("Ymd").'-'.date("m")."月營業處獎金總表.xls";
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename='.$filename.'');
        header('Cache-Control: max-age=0');

        $objWriter->save('php://output');
    }

    //  營業處總獎金總表輸出
    public function organizationBouns($data_arr,$product_info,$date){
        
        if (!is_array($data_arr)) {
            echo '參數格式不符合！';
            exit;
        }

        $row = 2;
        $acsnum = 66;
        $productrow = 0;
        foreach($product_info as $productlist){
            $productrow++;
        }
        $lastrow = $acsnum + $productrow + 13;

        $this->load->library('excel');
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->setTitle('獎金核發總表');
        $objPHPExcel->getActiveSheet()->mergeCells('A1:'.$this->exportexcel_model->row_code($lastrow).'1');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
        $objPHPExcel->getActiveSheet()->getStyle('A2:'.$this->exportexcel_model->row_code($lastrow).'2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true)->setSize(18);
        $objPHPExcel->getActiveSheet()->getStyle('A2:'.$this->exportexcel_model->row_code($lastrow).'2')->getFont()->setBold(true)->setSize(14);
        $objPHPExcel->getActiveSheet()->setCellValue('A1', '獎金核發總表');
        $objPHPExcel->getActiveSheet()->setCellValue('A2', '營業處名稱');
        foreach($product_info as $productlist){
            $objPHPExcel->getActiveSheet()->setCellValue($this->exportexcel_model->row_code($acsnum).$row, $productlist['product_name']);
            $objPHPExcel->getActiveSheet()->getColumnDimension($this->exportexcel_model->row_code($acsnum))->setWidth(40);
            $acsnum++;
        }
        $objPHPExcel->getActiveSheet()->setCellValue($this->exportexcel_model->row_code($acsnum).$row, '總單位');
        $objPHPExcel->getActiveSheet()->setCellValue($this->exportexcel_model->row_code($acsnum+1).$row, '總PV');
        $objPHPExcel->getActiveSheet()->setCellValue($this->exportexcel_model->row_code($acsnum+2).$row, '業績獎金');
        $objPHPExcel->getActiveSheet()->setCellValue($this->exportexcel_model->row_code($acsnum+3).$row, '同階獎金');
        $objPHPExcel->getActiveSheet()->setCellValue($this->exportexcel_model->row_code($acsnum+4).$row, '全國分紅');
        $objPHPExcel->getActiveSheet()->setCellValue($this->exportexcel_model->row_code($acsnum+5).$row, '衍生營業處津貼');
        $objPHPExcel->getActiveSheet()->setCellValue($this->exportexcel_model->row_code($acsnum+6).$row, '服務津貼');
        $objPHPExcel->getActiveSheet()->setCellValue($this->exportexcel_model->row_code($acsnum+7).$row, '行政津貼');
        $objPHPExcel->getActiveSheet()->setCellValue($this->exportexcel_model->row_code($acsnum+8).$row, '差額獎金 ');
        $objPHPExcel->getActiveSheet()->setCellValue($this->exportexcel_model->row_code($acsnum+9).$row, '其他加項');
        $objPHPExcel->getActiveSheet()->setCellValue($this->exportexcel_model->row_code($acsnum+10).$row, '說明');
        $objPHPExcel->getActiveSheet()->setCellValue($this->exportexcel_model->row_code($acsnum+11).$row, '其他扣項');
        $objPHPExcel->getActiveSheet()->setCellValue($this->exportexcel_model->row_code($acsnum+12).$row, '說明');
        $objPHPExcel->getActiveSheet()->setCellValue($this->exportexcel_model->row_code($acsnum+13).$row, '總計');
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension($this->exportexcel_model->row_code($acsnum+1))->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension($this->exportexcel_model->row_code($acsnum+2))->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension($this->exportexcel_model->row_code($acsnum+3))->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension($this->exportexcel_model->row_code($acsnum+4))->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension($this->exportexcel_model->row_code($acsnum+5))->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension($this->exportexcel_model->row_code($acsnum+6))->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension($this->exportexcel_model->row_code($acsnum+7))->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension($this->exportexcel_model->row_code($acsnum+8))->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension($this->exportexcel_model->row_code($acsnum+9))->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension($this->exportexcel_model->row_code($acsnum+10))->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension($this->exportexcel_model->row_code($acsnum+11))->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension($this->exportexcel_model->row_code($acsnum+12))->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension($this->exportexcel_model->row_code($acsnum+13))->setWidth(20);

        $rowcount = 3;
        foreach($data_arr as $key => $value){
            $acsnum = 66;
            $productrow = 0;
            foreach($product_info as $productlist){
                $productrow++;
            }
            $lastrow = $acsnum + $productrow + 13;

            $objPHPExcel->getActiveSheet()->getStyle('A'.$rowcount.':'.$this->exportexcel_model->row_code($lastrow).$rowcount)->getFont()->setSize(12);
            $objPHPExcel->getActiveSheet()->setCellValue('A'.$rowcount, $value['organization_name']);
            foreach($product_info as $productlist){
                $objPHPExcel->getActiveSheet()->setCellValue($this->exportexcel_model->row_code($acsnum).$rowcount, $productlist[$key]['product_num']);
                $acsnum++;
            }

            //總單位
            $objPHPExcel->getActiveSheet()->setCellValue($this->exportexcel_model->row_code($acsnum).$rowcount, $value['ordernum']);
            //總PV
            $objPHPExcel->getActiveSheet()->setCellValue($this->exportexcel_model->row_code($acsnum+1).$rowcount,$value['orgpv']);
            //業績獎金
            $objPHPExcel->getActiveSheet()->setCellValue($this->exportexcel_model->row_code($acsnum+2).$rowcount, $value['performance']);
            //同階獎金
            $objPHPExcel->getActiveSheet()->setCellValue($this->exportexcel_model->row_code($acsnum+3).$rowcount, $value['samelvallowance']);
            //全國分紅
            $objPHPExcel->getActiveSheet()->setCellValue($this->exportexcel_model->row_code($acsnum+4).$rowcount, $value['dividend']);
            //衍生營業處津貼
            $objPHPExcel->getActiveSheet()->setCellValue($this->exportexcel_model->row_code($acsnum+5).$rowcount, $value['boAllowance']);
            //服務津貼
            $objPHPExcel->getActiveSheet()->setCellValue($this->exportexcel_model->row_code($acsnum+6).$rowcount, round($value['totalallowance']));
            //行政津貼
            $objPHPExcel->getActiveSheet()->setCellValue($this->exportexcel_model->row_code($acsnum+7).$rowcount, $value['subsidypv']);
            //差額獎金
            $objPHPExcel->getActiveSheet()->setCellValue($this->exportexcel_model->row_code($acsnum+8).$rowcount,$value['allowance']);
            //其他加項
            $objPHPExcel->getActiveSheet()->setCellValue($this->exportexcel_model->row_code($acsnum+9).$rowcount,$value['addmoney']);
            //加項說明
            $objPHPExcel->getActiveSheet()->setCellValue($this->exportexcel_model->row_code($acsnum+10).$rowcount,$value['addtxt']);
            //其他減項
            $objPHPExcel->getActiveSheet()->setCellValue($this->exportexcel_model->row_code($acsnum+11).$rowcount,$value['lessmoney']);
            //減項說明
            $objPHPExcel->getActiveSheet()->setCellValue($this->exportexcel_model->row_code($acsnum+12).$rowcount,$value['lesstxt']);
            //合計
            $objPHPExcel->getActiveSheet()->setCellValue($this->exportexcel_model->row_code($acsnum+13).$rowcount,$value['totalbonusnum']);

            $rowcount++;
        }

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        ob_end_clean();
        if($this->session->userdata('year') != false && $this->session->userdata('month') != false){
            $filename = $date['year'].'-'.$date['month']."月獎金核發總表.xls";
        }
        else{
            $filename = date("Ymd")."-".date("m")."月獎金核發總表.xls";
        }
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename='.$filename.'');
        header('Cache-Control: max-age=0');

        $objWriter->save('php://output');
    }

    //當excel欄位數超過Z的處理
    public function row_code($number){
        $result = '';
        if($number <= 90){
            $result = chr($number);
        }
        else{
            $number = $number-90+64;
            $result = 'A'.chr($number);
        }

        return $result;
    }
}
