<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Supplier_model extends CI_model{

    public function getidData($id){
        $this->db->select('*');
        $this->db->from('supplier');
        $this->db->where('supplier_id',$id);
        $this->db->where('supplier_is_del',0);

        $query = $this->db->get();
        return $query->row_array();
    }

    public function getList($keyword='',$limit=''){
        $this->db->select('*');
        $this->db->from('supplier');
        $this->db->where('supplier_is_del',0);
        if (isset($keyword['fullname']) && $keyword['fullname'] != '') {
            $this->db->like('supplier_name',$keyword['fullname']);
        }
        if (isset($keyword['status']) && $keyword['status'] != '') {
            $this->db->where('supplier_status',$keyword['status']);
        }

        if ($limit) {
            $this->db->limit($limit);
        }

        $query = $this->db->get();
        return $query->result_array();
    }

    //新增產品
    public function add_supplier($supplier){

        $this->db->insert('supplier',$supplier);

    }

    //修改產品
    public function update_supplier($supplier,$id){
        $this->db->update('supplier',$supplier,array('supplier_id' => $id));
    }

    //撈出最新商品id
    public function get_latest_id(){
        $this->db->select_max('supplier_id','maxid');
        $this->db->from('supplier');

        $query = $this->db->get();
        return $query->row_array();
    }
}
