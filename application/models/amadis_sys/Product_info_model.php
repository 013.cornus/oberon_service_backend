<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product_info_model extends CI_model{

    public function getidData($id){
        $this->db->select('*');
        $this->db->from('product_info');
        $this->db->where('product_info_id',$id);
        $this->db->where('product_info_is_del',0);

        $query = $this->db->get();
        return $query->row_array();
    }

    public function getList($keyword='',$limit=''){
        $this->db->select('*');
        $this->db->from('product_info');
        $this->db->where('product_info_is_del',0);
        if (isset($keyword['fullname']) && $keyword['fullname'] != '') {
            $this->db->like('product_info_name',$keyword['fullname']);
        }
        if (isset($keyword['status']) && $keyword['status'] != '') {
            $this->db->where('product_info_status',$keyword['status']);
        }

        if (isset($keyword['product_id']) && $keyword['product_id'] != '') {
            $this->db->where('product_id',$keyword['product_id']);
        }

        if ($limit) {
            $this->db->limit($limit);
        }

        $query = $this->db->get();
        return $query->result_array();
    }

    //新增產品
    public function add_product_info($product_info){

        $this->db->insert('product_info',$product_info);

    }

    //修改產品
    public function update_product_info($product_info,$id){
        $this->db->update('product_info',$product_info,array('product_info_id' => $id));
    }

    //撈出最新商品id
    public function get_latest_id(){
        $this->db->select_max('product_info_id','maxid');
        $this->db->from('product_info');

        $query = $this->db->get();
        return $query->row_array();
    }

    //查詢單個商品詳細資訊
    public function getcertaininfo($id){
        $this->db->select('*');
        $this->db->from('product_info');
        $this->db->where('product_id',$id);
        $this->db->where('product_info_is_del',0);
        $this->db->where('product_info_status',1);
      
        $query = $this->db->get();
        return $query->result_array();
    }

    //刪除productinfo
    public function delete_product_info($id,$datetime,$user_id){
        $this->db->where('product_info_id',$id);
        $this->db->set('product_info_is_del',1); 
        $this->db->set('product_info_updated_date',$datetime);
        $this->db->set('product_info_updated_user',$user_id);
        $this->db->update('product_info');
    }
}
