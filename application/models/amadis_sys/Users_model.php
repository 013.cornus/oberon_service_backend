<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users_model extends CI_model{

    public function register_user($user){
        $this->db->insert('user', $user);
    }

    public function login_user($account,$pass){
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('users_account',$account);
        $this->db->where('users_password',$pass);
        $this->db->where('users_status',1);
        $this->db->where('users_is_del',0);
        $this->db->order_by('competence_id','asc');

        if($query = $this->db->get()){
            return $query->row_array();
        }else{
            return false;
        }
    }

    public function login_customer($account,$pass){
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('users_account',$account);
        $this->db->where('users_password',$pass);
        $this->db->where('competence_id',5);
        $this->db->where('users_status',1);
        $this->db->where('users_is_del',0);
        
        if($query = $this->db->get()){
            return $query->row_array();
        }else{
            return false;
        }
    }

    public function account_check($admin_account){
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('users_account',$admin_account);
        $this->db->where('users_status',1);
        $this->db->where('users_is_del',0);

        $query = $this->db->get();

        if($query->num_rows() > 0){
            return false;
        }else{
            return true;
        }
    }

    public function getdata(){
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('users_status',1);
        $this->db->where('users_is_del',0);

        $query = $this->db->get();
        return $query->result_array();
    }

    public function updatelastlogin($id){
        $data = array(
            'users_last_login' => date('Y-m-d H:i:s'),
        );
        $this->db->where('users_id', $id);
        $this->db->update('users',$data);
    }

    public function updatepassword($data){
        $this->db->where('users_id', $data['users_id']);
        $this->db->update('users',$data);
    }

    public function getuseridData($userid){
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('users_id',$userid);
        $this->db->where('users_status',1);
        $this->db->where('users_is_del',0);
        
        $query = $this->db->get();
        return $query->row_array();
    }

    //查詢是否該客戶已有帳號
    public function get_customers_account($id){
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('customer_id',$id);
        $this->db->where('users_status',1);
        $this->db->where('users_is_del',0);
        
        $query = $this->db->get();
        return $query->row_array();
    }

    //查詢是否該業務已有帳號
    public function get_employee_account($id){
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('employee_id',$id);
        $this->db->where('users_status',1);
        $this->db->where('users_is_del',0);
        
        $query = $this->db->get();
        return $query->row_array();
    }

    //查詢登入者是否有多重身分
    public function check_account_num($account){
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('users_account',$account);
        $this->db->where('users_status',1);
        $this->db->where('users_is_del',0);

        $query = $this->db->get();

        if($query->num_rows() > 1){
            return true;
        }else{
            return false;
        }
    }
}
