<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report_template_model extends CI_model{

    public function getidData($id){
        $this->db->select('*');
        $this->db->from('report_template');
        $this->db->where('report_template_id',$id);
        $this->db->where('report_template_is_del',0);

        $query = $this->db->get();
        return $query->row_array();
    }

    public function getKeyData($key,$value){
        $this->db->select('*')
                 ->from('report_template')
                 ->where($key,$value);
        $result = $this->db->get();
        $result = $result->row_array();
        return $result;
    }

    public function getList($keyword='',$limit=''){
        $this->db->select('*');
        $this->db->from('report_template');
        $this->db->where('report_template_is_del',0);
        if (isset($keyword['fullname']) && $keyword['fullname'] != '') {
            $this->db->like('report_template_name',$keyword['fullname']);
        }
        if (isset($keyword['status']) && $keyword['status'] != '') {
            $this->db->where('report_template_status',$keyword['status']);
        }

        if ($limit) {
            $this->db->limit($limit);
        }

        $query = $this->db->get();
        return $query->result_array();
    }

    //新增產品
    public function add_report_template($report_template){

        $this->db->insert('report_template',$report_template);

    }

    //修改產品
    public function update_report_template($report_template,$id){
        $this->db->update('report_template',$report_template,array('report_template_id' => $id));
    }

    //撈出最新商品id
    public function get_latest_id(){
        $this->db->select_max('report_template_id','maxid');
        $this->db->from('report_template');

        $query = $this->db->get();
        return $query->row_array();
    }
}
