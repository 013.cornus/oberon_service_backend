<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Organization_model extends CI_model{

    //  查詢單一營業處
    public function getData($id){
        $this->db->select('*');
        $this->db->from('organization');
        $this->db->where('organization_id',$id);
        $this->db->where('organization_is_del',0);
        // $this->db->where('organization_status',1);

        $query = $this->db->get();
        return $query->row_array();
    }

    //  查詢所有營業處
    public function getList($keyword='',$limit=''){
        $this->db->select('*');
        $this->db->from('organization');
        $this->db->where('organization_is_del',0);
        // $this->db->where('organization_status',1);

        if (isset($keyword['organization_id']) && $keyword['organization_id'] != '') {
            $this->db->where('organization_id',$keyword['organization_id']);
        }

        if (isset($keyword['status']) && $keyword['status'] != '') {
            $this->db->where('organization_status',$keyword['status']);
        }

        if (isset($keyword['organization_name']) && $keyword['organization_name'] != '') {
            $this->db->like('organization_name',$keyword['organization_name']);
        }

        if ($limit) {
            $this->db->limit($limit);
        }
        
        $result = $this->db->get();
        $result = $result->result_array();
        return $result;
    }

    //  新增組織
    public function add_organization($organization){
        $this->db->insert('organization',$organization);
    }

    //  修改會員
    public function update_organization($organizationupdate,$id){
        $this->db->update('organization',$organizationupdate  ,array('organization_id' => $id));
    }
}
