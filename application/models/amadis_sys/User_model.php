<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_model{

    public function register_user($user){
        $this->db->insert('user', $user);
    }

    public function login_user($account,$pass){
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('users_account',$account);
        $this->db->where('users_password',$pass);

        if($query = $this->db->get()){
            return $query->row_array();
        }else{
            return false;
        }
    }

    public function account_check($admin_account){
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('users_account',$admin_account);

        $query = $this->db->get();

        if($query->num_rows() > 0){
            return false;
        }else{
            return true;
        }
    }

    public function getdata(){
        $this->db->select('*');
        $this->db->from('user');

        $query = $this->db->get();
        return $query->result_array();
    }
}
