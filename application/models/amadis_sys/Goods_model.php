<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Goods_model extends CI_model{

    public function getidData($id){
        $this->db->select('*');
        $this->db->from('goods');
        $this->db->where('goods_id',$id);
        $this->db->where('goods_is_del',0);

        $query = $this->db->get();
        return $query->row_array();
    }

    public function getList($keyword='',$limit=''){
        $this->db->select('*');
        $this->db->from('goods');
        $this->db->where('goods_is_del',0);
        if (isset($keyword['fullname']) && $keyword['fullname'] != '') {
            $this->db->like('goods_name',$keyword['fullname']);
        }
        if (isset($keyword['status']) && $keyword['status'] != '') {
            $this->db->where('goods_status',$keyword['status']);
        }

        if ($limit) {
            $this->db->limit($limit);
        }

        $query = $this->db->get();
        return $query->result_array();
    }

    //新增產品
    public function add_goods($goods){

        $this->db->insert('goods',$goods);

    }

    //修改產品
    public function update_goods($goods,$id){
        $this->db->update('goods',$goods,array('goods_id' => $id));
    }

    //撈出最新商品id
    public function get_latest_id(){
        $this->db->select_max('goods_id','maxid');
        $this->db->from('goods');

        $query = $this->db->get();
        return $query->row_array();
    }
}
