<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users_log_model extends CI_model{

    //查詢單筆
    public function getdata($keyword){
        $this->db->select('*');
        $this->db->from('users_log');
        $this->db->where('users_log_is_del',0);

        if (isset($keyword) && $keyword != '') {
            $this->db->like('users_log_desc',$keyword);
            // $this->db->or_like('users_account',$keyword);
        }
        
        $query = $this->db->get();
        return $query->row_array();
    }

    //  查詢所有
    public function getList($keyword,$limit){
        $this->db->select('*');
        $this->db->from('users_log');
        $this->db->where('users_log_is_del',0);
        // $this->db->where('users_log_status',1);

        if (isset($keyword['users_log_desc']) && $keyword['users_log_desc'] != '') {
            $this->db->like('users_log_desc',$keyword['users_log_desc']);
        }
        if (isset($keyword['users_id']) && $keyword['users_id'] != '') {
            $this->db->where('users_id',$keyword['users_id']);
        }

        if ($limit) {
            $this->db->limit($limit);
        }

        $result = $this->db->get();
        $result = $result->result_array();
        return $result;
    }

    //新增
    public function insertData($type){
        $ip = $this->input->ip_address();
        if ( !$this->input->valid_ip($ip)){
            $ip = 'Not Valid';
        }
        switch ($type) {
            case '1':
            $log = array(
                'users_id'               => $this->session->userdata('bms_users_id'),
                'users_log_type_id'      => 1,
                'users_log_desc'         => '成功！登入系統操作成功！',
                'users_log_ip'           => $ip,
                'users_log_browser'      => $this->agent->browser().' '.$this->agent->version(),
                'users_log_platform'     => $this->agent->platform(),
                'users_log_mobile'       => ($this->agent->is_mobile()) ? $this->agent->mobile() : NULL,
                'users_log_created_date' => date('Y-m-d H:i:s'),
                'users_log_created_user' => $this->session->userdata('bms_users_id'),
            );
            break;
            case '2':
            $log = array(
                'users_id'               => $this->session->userdata('bms_users_id'),
                'users_log_type_id'      => 1,
                'users_log_desc'         => '成功！登出系統操作成功！',
                'users_log_ip'           => $ip,
                'users_log_browser'      => $this->agent->browser().' '.$this->agent->version(),
                'users_log_platform'     => $this->agent->platform(),
                'users_log_mobile'       => ($this->agent->is_mobile()) ? $this->agent->mobile() : NULL,
                'users_log_created_date' => date('Y-m-d H:i:s'),
                'users_log_created_user' => $this->session->userdata('bms_users_id'),
            );
            break;
            default:
                # code...
            break;
        }
        $this->db->insert('users_log',$log);
        // return  $this->db->insert_id();
    }

    //修改
    public function updateData($data){
        $this->db->where('users_log_id', $data['users_log_id']);
        $this->db->update('users_log',$data);
    }

}
