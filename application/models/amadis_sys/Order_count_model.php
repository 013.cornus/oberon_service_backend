<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order_count_model extends CI_model{

    // 新增利得
    public function create_profit($order_id,$product_num,$product_id,$product_price,$givemonth,$giveday){

        //寫入利得
        $product_rate_info = $this->bonus_rate_model->getonedata($product_id);
        $profitnum = $product_rate_info['bonus_rate_give'];

        $interestnum = $product_num;
        $interestrate = $product_rate_info['bonus_rate_interest_allowance'] / 100;
        $qinterestrate = $product_rate_info['bonus_rate_quarterly'] / 100;
        $build_period = $product_rate_info['bonus_rate_build_period'];

        //發放建置期利息補貼
        if($build_period != 0){
            $month = date("Y-m",strtotime("+1month",strtotime($givemonth)));
            if(date("m",strtotime($month)) == '02' && $giveday > 20){
                $date = $month.'-'.'28';
            }
            else{
                $date = $month.'-'.$giveday;
            }
            $profit = array(
                'order_id'            => $order_id,
                'profit_type'         => 0,
                'profit_name'         => "利息補貼",
                'profit_bouns_momey'  => $product_price * $interestnum * $interestrate * $build_period / 12,
                'profit_created_date' => $date,
                'profit_created_user' => $this->session->userdata('users_id')
            );
            $this->profit_model->add_profit($profit);
        }

        //季利得發放起始月
        $qinterest_begin_date = $build_period + 1;

        //發放季利得
        for($i = 1;$i <= $profitnum;$i++){
            if($i == 1 or $i == 5 or $i == 9){
                switch($i){
                    case "1":
                    $month = date("Y-m",strtotime("+".($qinterest_begin_date)."month",strtotime($givemonth)));
                    if(date("m",strtotime($month)) == '02' && $giveday > 20){
                        $date = $month.'-'.'28';
                    }
                    else{
                        $date = $month.'-'.$giveday;
                    }
                    break;
                    case "5":
                    $month = date("Y-m",strtotime("+".($qinterest_begin_date+12)."month",strtotime($givemonth)));
                    if(date("m",strtotime($month)) == '02' && $giveday > 20){
                        $date = $month.'-'.'28';
                    }
                    else{
                        $date = $month.'-'.$giveday;
                    }
                    break;
                    case "9":
                    $month = date("Y-m",strtotime("+".($qinterest_begin_date+24)."month",strtotime($givemonth)));
                    if(date("m",strtotime($month)) == '02' && $giveday > 20){
                        $date = $month.'-'.'28';
                    }
                    else{
                        $date = $month.'-'.$giveday;
                    }
                    break;
                }
                $profit = array(
                    'order_id'            => $order_id,
                    'profit_type'         => 1,
                    'profit_name'         => "第一季利得",
                    'profit_bouns_momey'  => $product_price * $interestnum * $qinterestrate / 4,
                    'profit_created_date' => $date,
                    'profit_created_user' => $this->session->userdata('users_id')
                );
                $this->profit_model->add_profit($profit);
            }

            if($i == 2 or $i == 6 or $i == 10){
                switch($i){
                    case "2":
                    $month = date("Y-m",strtotime("+".($qinterest_begin_date+3)."month",strtotime($givemonth)));
                    if(date("m",strtotime($month)) == '02' && $giveday > 20){
                        $date = $month.'-'.'28';
                    }
                    else{
                        $date = $month.'-'.$giveday;
                    }
                    break;
                    case "6":
                    $month = date("Y-m",strtotime("+".($qinterest_begin_date+15)."month",strtotime($givemonth)));
                    if(date("m",strtotime($month)) == '02' && $giveday > 20){
                        $date = $month.'-'.'28';
                    }
                    else{
                        $date = $month.'-'.$giveday;
                    }
                    break;
                    case "10":
                    $month = date("Y-m",strtotime("+".($qinterest_begin_date+27)."month",strtotime($givemonth)));
                    if(date("m",strtotime($month)) == '02' && $giveday > 20){
                        $date = $month.'-'.'28';
                    }
                    else{
                        $date = $month.'-'.$giveday;
                    }
                    break;
                }
                $profit = array(
                    'order_id'            => $order_id,
                    'profit_type'         => 2,
                    'profit_name'         => "第二季利得",
                    'profit_bouns_momey'  => $product_price * $interestnum * $qinterestrate / 4,
                    'profit_created_date' => $date,
                    'profit_created_user' => $this->session->userdata('users_id')
                );
                $this->profit_model->add_profit($profit);
            }

            if($i == 3 or $i == 7 or $i == 11){
                switch($i){
                    case "3":
                    $month = date("Y-m",strtotime("+".($qinterest_begin_date+6)."month",strtotime($givemonth)));
                    if(date("m",strtotime($month)) == '02' && $giveday > 20){
                        $date = $month.'-'.'28';
                    }
                    else{
                        $date = $month.'-'.$giveday;
                    }
                    break;
                    case "7":
                    $month = date("Y-m",strtotime("+".($qinterest_begin_date+18)."month",strtotime($givemonth)));
                    if(date("m",strtotime($month)) == '02' && $giveday > 20){
                        $date = $month.'-'.'28';
                    }
                    else{
                        $date = $month.'-'.$giveday;
                    }
                    break;
                    case "11":
                    $month = date("Y-m",strtotime("+".($qinterest_begin_date+30)."month",strtotime($givemonth)));
                    if(date("m",strtotime($month)) == '02' && $giveday > 20){
                        $date = $month.'-'.'28';
                    }
                    else{
                        $date = $month.'-'.$giveday;
                    }
                    break;
                }
                $profit = array(
                    'order_id'            => $order_id,
                    'profit_type'         => 3,
                    'profit_name'         => "第三季利得",
                    'profit_bouns_momey'  => $product_price * $interestnum * $qinterestrate / 4,
                    'profit_created_date' => $date,
                    'profit_created_user' => $this->session->userdata('users_id')
                );
                $this->profit_model->add_profit($profit);
            }

            if($i == 4 or $i == 8 or $i == 12){
                switch($i){
                    case "4":
                    $month = date("Y-m",strtotime("+".($qinterest_begin_date+9)."month",strtotime($givemonth)));
                    if(date("m",strtotime($month)) == '02' && $giveday > 20){
                        $date = $month.'-'.'28';
                    }
                    else{
                        $date = $month.'-'.$giveday;
                    }
                    break;
                    case "8":
                    $month = date("Y-m",strtotime("+".($qinterest_begin_date+21)."month",strtotime($givemonth)));
                    if(date("m",strtotime($month)) == '02' && $giveday > 20){
                        $date = $month.'-'.'28';
                    }
                    else{
                        $date = $month.'-'.$giveday;
                    }
                    break;
                    case "12":
                    $month = date("Y-m",strtotime("+".($qinterest_begin_date+33)."month",strtotime($givemonth)));
                    if(date("m",strtotime($month)) == '02' && $giveday > 20){
                        $date = $month.'-'.'28';
                    }
                    else{
                        $date = $month.'-'.$giveday;
                    }
                    break;
                }
                $profit = array(
                    'order_id'            => $order_id,
                    'profit_type'         => 4,
                    'profit_name'         => "第四季利得",
                    'profit_bouns_momey'  => $product_price * $interestnum * $qinterestrate / 4,
                    'profit_created_date' => $date,
                    'profit_created_user' => $this->session->userdata('users_id')
                );
                $this->profit_model->add_profit($profit);
            }
        }
    }

    // 新增業績獎金
    public function performance($order,$id,$orderequitypv,$orderfundpv){
        //建立業績獎金
        $employee = $this->employee_model->getidData($order['employee_id']);
        $bonus = $this->bonus_model->getidData($employee['occupation_id']);
        if($employee['occupation_id'] == 0){
            $bonus['bouns_id'] = 0;
            $bonus['bouns_equity'] = 10;
            $bonus['bouns_fund'] = 5;
            $bonus['bouns_id'] = 5;
        }
        $performancesum = $orderequitypv * $bonus['bouns_equity'] + $orderfundpv * $bonus['bouns_fund'];
        $performance = array(
            'employee_id'              => $order['employee_id'],
            'bonus_id'                 => $bonus['bouns_id'],
            'order_id'                 => $id,
            'organization_id'          => $order['organization_id'],
            'bonus_type_id'            => 1,
            'users_bonus_money'        => $performancesum/100,
            'users_bonus_created_date' => $order['order_active_date'],
            'users_bonus_created_user' => $this->session->userdata('users_id')
        );
        $this->bonus_model->add_performance($performance);
        //建立業績獎金
    }

    // 新增差額獎金
    public function difference($order,$id,$orderequitypv,$orderfundpv){
        //建立差額獎金(OK)
        //差額獎金總和%數
        $fundrate = 11;
        $equityrate = 16;

        $employee = $this->employee_model->getidData($order['employee_id']);
        $originem = $this->employee_model->getidData($order['employee_id']);
        $originbonus = $this->bonus_model->getidData($originem['occupation_id']);
        $remainfund = $fundrate - $originbonus['bouns_fund'];
        $remainequity = $equityrate - $originbonus['bouns_equity'];
        $funddifference = 0;
        $equitydifference = 0;
        $base_occupation = $originem['occupation_id'];

        for($i = 1;$i <= 8;$i++){
            $bonus = $this->bonus_model->getidData($employee['occupation_id']);

            if($employee['occupation_id'] == 0){
                $bonus['bouns_id'] = 0;
                $bonus['bouns_equity'] = 10;
                $bonus['bouns_fund'] = 5;
                $bonus['bouns_id'] = 5;
            }

            $referrer = $this->employee_model->getidData($employee['users_id']);
            $referrerbonus = $this->bonus_model->getidData($referrer['occupation_id']);
            if($referrer['employee_id'] == $employee['employee_id'] || $employee['users_id'] == 0){
                break;
            }

            if($referrer['occupation_id'] < $base_occupation && $referrer['occupation_id'] != $originem['occupation_id']){
                $base_occupation = $referrer['occupation_id'];
                if($remainfund > 0){
                    $givefundrate = $referrerbonus['bouns_fund'] - $bonus['bouns_fund'];
                    $ifovergivefund = $remainfund - $givefundrate;
                    if($ifovergivefund >= 0){
                        $funddifference = $orderfundpv * $givefundrate/100;
                        $remainfund = $ifovergivefund;
                    }
                    else{
                        $funddifference = $orderfundpv * $remainfund/100;
                        $remainfund = 0;
                    }
                }

                if($remainequity > 0){
                    $giveequityrate = $referrerbonus['bouns_fund'] - $bonus['bouns_fund'];
                    $ifovergiveequity = $remainequity - $giveequityrate;
                    if($ifovergivefund >= 0){
                        $equitydifference = $orderequitypv * $giveequityrate/100;
                        $remainequity = $ifovergiveequity;
                    }
                    else{
                        $equitydifference = $orderequitypv * $remainequity/100;
                        $remainequity = 0;
                    }
                }

                $totaldifference = round($equitydifference + $funddifference);
                if($totaldifference != 0){
                    $difference = array(
                        'employee_id'              => $referrer['employee_id'],
                        'bonus_id'                 => $referrerbonus['bouns_id'],
                        'order_id'                 => $id,
                        'organization_id'          => $referrer['organization_id'],
                        'bonus_type_id'            => 3,
                        'users_bonus_money'        => $totaldifference,
                        'users_bonus_created_date' => $order['order_active_date'],
                        'users_bonus_created_user' => $this->session->userdata('users_id')
                    );
                    $this->bonus_model->add_difference($difference);
                }
            }

            $employee = $this->employee_model->getidData($employee['users_id']);
            $bonus = $this->bonus_model->getidData($employee['occupation_id']);
        }
        //建立差額獎金
    }

    // 新增同階獎金
    public function samelevel($order,$id,$orderequitypv,$orderfundpv){
       //建立同階獎金
        $originem = $this->employee_model->getidData($order['employee_id']);
        $employee = $this->employee_model->getidData($order['employee_id']);
        $givenum = 0;
        $firstmanageid = 0;
        $firstdirectorid = 0;
        $samelevelbonus = ($orderequitypv + $orderfundpv) * 0.005;
        for($i=1;$i<=8;$i++){
            $referrer = $this->employee_model->getidData($employee['users_id']);
            if($referrer['employee_id'] == $employee['employee_id'] || $employee['users_id'] == 0){
                break;
            }
            $referrerbonus = $this->bonus_model->getidData($referrer['occupation_id']);
            if($referrerbonus['bouns_id'] == null){
                $referrerbonus['bouns_id'] = 5;
            }
            if($originem['occupation_id'] == 3 && $referrer['employee_id'] != $employee['employee_id']){
                if($originem['occupation_id'] == $referrer['occupation_id']){
                    $difference = array(
                        'employee_id'              => $referrer['employee_id'],
                        'bonus_id'                 => $referrerbonus['bouns_id'],
                        'order_id'                 => $id,
                        'organization_id'          => $referrer['organization_id'],
                        'bonus_type_id'            => 4,
                        'users_bonus_money'        => round($samelevelbonus),
                        'users_bonus_created_date' => $order['order_active_date'],
                        'users_bonus_created_user' => $this->session->userdata('users_id')
                    );
                    $this->bonus_model->add_difference($difference);
                    break;
                }
            }
            else if($originem['occupation_id'] == 4 && $referrer['employee_id'] != $employee['employee_id'] ){
                if($referrer['occupation_id'] == 4){
                    $difference = array(
                        'employee_id'              => $referrer['employee_id'],
                        'bonus_id'                 => $referrerbonus['bouns_id'],
                        'order_id'                 => $id,
                        'organization_id'          => $referrer['organization_id'],
                        'bonus_type_id'            => 4,
                        'users_bonus_money'        => round($samelevelbonus),
                        'users_bonus_created_date' => $order['order_active_date'],
                        'users_bonus_created_user' => $this->session->userdata('users_id')
                    );
                    $this->bonus_model->add_difference($difference);
                    break;
                }
                else if($referrer['occupation_id'] == 3){
                    if($firstmanageid == 0){
                        $firstmanageid = $referrer['employee_id'];
                    }
                    else{
                        $difference = array(
                            'employee_id'              => $referrer['employee_id'],
                            'bonus_id'                 => $referrerbonus['bouns_id'],
                            'order_id'                 => $id,
                            'organization_id'          => $referrer['organization_id'],
                            'bonus_type_id'            => 4,
                            'users_bonus_money'        => round($samelevelbonus),
                            'users_bonus_created_date' => $order['order_active_date'],
                            'users_bonus_created_user' => $this->session->userdata('users_id')
                        );
                        $this->bonus_model->add_difference($difference);
                        break;
                    }
                }
            }
            else if($referrer['employee_id'] != $employee['employee_id']){
                if($referrer['occupation_id'] == 3){
                    if($firstmanageid == 0){
                        $firstmanageid = $referrer['employee_id'];
                    }
                    else{
                        $difference = array(
                            'employee_id'              => $referrer['employee_id'],
                            'bonus_id'                 => $referrerbonus['bouns_id'],
                            'order_id'                 => $id,
                            'organization_id'          => $referrer['organization_id'],
                            'bonus_type_id'            => 4,
                            'users_bonus_money'        => round($samelevelbonus),
                            'users_bonus_created_date' => $order['order_active_date'],
                            'users_bonus_created_user' => $this->session->userdata('users_id')
                        );
                        $this->bonus_model->add_difference($difference);
                        break;
                    }
                }

                if($referrer['occupation_id'] == 4){
                    if($firstdirectorid == 0){
                        $firstdirectorid = $referrer['employee_id'];
                    }
                    else{
                        $difference = array(
                            'employee_id'              => $referrer['employee_id'],
                            'bonus_id'                 => $referrerbonus['bouns_id'],
                            'order_id'                 => $id,
                            'organization_id'          => $referrer['organization_id'],
                            'bonus_type_id'            => 4,
                            'users_bonus_money'        => round($samelevelbonus),
                            'users_bonus_created_date' => $order['order_active_date'],
                            'users_bonus_created_user' => $this->session->userdata('users_id')
                        );
                        $this->bonus_model->add_difference($difference);
                        break;
                    }
                }
            }
            $employee = $this->employee_model->getidData($referrer['employee_id']);
        }
        //建立同階獎金
    }

    // 新增行政津貼
    public function general_subsidy($order,$id,$orderequitypv,$orderfundpv){
        //建立行政津貼
        $organizationid = $order['organization_id'];
        $organization = $this->organization_model->getData($organizationid);
        $employee = $this->employee_model->getidData($organization['employee_id']);
        $bonus = $this->bonus_model->getidData($employee['occupation_id']);

        if($employee['occupation_id'] == 0){
            $bonus['bouns_id'] == 0;
            $bonus['bouns_equity'] = 10;
            $bonus['bouns_fund'] = 5;
            $bonus['bouns_id'] = 5;
        }

        $performance = array(
            'employee_id'              => $organization['employee_id'],
            'bonus_id'                 => $bonus['bouns_id'],
            'order_id'                 => $id,
            'organization_id'          => $order['organization_id'],
            'bonus_type_id'            => 6,
            'users_bonus_money'        => round(($orderequitypv + $orderfundpv)*0.03),
            'users_bonus_created_date' => $order['order_active_date'],
            'users_bonus_created_user' => $this->session->userdata('users_id')
        );
        $this->bonus_model->add_performance($performance);
        //建立行政津貼
    }

    // 新增衍生營業處津貼
    public function organizbonus($order,$id,$orderequitypv,$orderfundpv){
       //建立衍生營業處津貼
        $organizationid = $order['organization_id'];
        $organization = $this->organization_model->getData($organizationid);
        $orgreferrer = $this->organization_model->getData($organization['organization_referrer']);
        $referrerprinciple = $this->employee_model->getidData($orgreferrer['employee_id']);
        $orgparent = $this->organization_model->getData($organization['organization_parent_unit']);
        $parentprinciple = $this->employee_model->getidData($orgparent['employee_id']);
        $refbonus = $this->bonus_model->getidData($referrerprinciple['occupation_id']);
        $parentbonus = $this->bonus_model->getidData($parentprinciple['occupation_id']);

        //推薦人獎金
        if($referrerprinciple['employee_id'] != null){
            $referrerbonus = array(
                'employee_id'              => $referrerprinciple['employee_id'],
                'bonus_id'                 => $refbonus['bouns_id'],
                'order_id'                 => $id,
                'organization_id'          => $orgreferrer['organization_id'],
                'bonus_type_id'            => 7,
                'users_bonus_money'        => round($orderequitypv + $orderfundpv)*0.006,
                'users_bonus_created_date' => $order['order_active_date'],
                'users_bonus_created_user' => $this->session->userdata('users_id')
            );
            $this->users_bonus_model->add_userbonus($referrerbonus);
        }

        //母單位獎金
        if($parentprinciple['employee_id'] != null){
            $parentbonus = array(
                'employee_id'              => $parentprinciple['employee_id'],
                'bonus_id'                 => $parentbonus['bouns_id'],
                'order_id'                 => $id,
                'organization_id'          => $orgparent['organization_id'],
                'bonus_type_id'            => 7,
                'users_bonus_money'        => round($orderequitypv + $orderfundpv)*0.004,
                'users_bonus_created_date' => $order['order_active_date'],
                'users_bonus_created_user' => $this->session->userdata('users_id')
            );
            $this->users_bonus_model->add_userbonus($parentbonus);
        }
        //建立衍生營業處津貼
    }
}
