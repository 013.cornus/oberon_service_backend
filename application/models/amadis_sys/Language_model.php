<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Language_model extends CI_model{

	public function __construct(){
		parent::__construct();
		$this->lang->load('system_lang', 'zh-CN');
	}

	public function getzhcnList(){
		$result[0] = $this->getcommondata();
		$result[1] = $this->getAdata();
		$result[2] = $this->getBdata();
		$result[3] = $this->getCdata();
		$result[4] = $this->getDdata();
		$result[5] = $this->getEdata();
		$result[6] = $this->getFdata();
		$result[7] = $this->getGdata();
		$result[8] = $this->getIdata();
		$result[9] = $this->getJdata();
		$result[10] = $this->getKdata();

		return $result;
	}

    //  查詢全域語系
	public function getGlobeldata($lang,$keyword){
		$this->lang->load('system_lang', $lang);
		$result = $this->lang->line($keyword);

		return $result;
	}

    //  查詢簡體語系
	public function getdata($keyword){
		$result = $this->lang->line($keyword);

		return $result;
	}

    //  共用語系
	public function getcommondata(){
        //	系統操作
		$result['new']            = $this->lang->line('new');        
		$result['edit']           = $this->lang->line('edit');       
		$result['disable']        = $this->lang->line('disable');    
		$result['active']         = $this->lang->line('active');
		$result['notenabled']     = $this->lang->line('notenabled');     
		$result['shelf']          = $this->lang->line('shelf');      
		$result['obtained']       = $this->lang->line('obtained');   
		$result['delete']         = $this->lang->line('delete');     
		$result['view']           = $this->lang->line('view');       
		$result['search']         = $this->lang->line('search');     
		$result['update_date']    = $this->lang->line('update_date');
		$result['active_date']    = $this->lang->line('active_date');
		$result['action']         = $this->lang->line('action');  

		$result['home']           = $this->lang->line('home');     
		//	警告訊息
		$result['warning']        = $this->lang->line('warning');
		$result['delete_alert']   = $this->lang->line('delete_alert');
		$result['stop_alert']     = $this->lang->line('stop_alert');
		$result['active_alert']   = $this->lang->line('active_alert');
		$result['expost_alert']   = $this->lang->line('expost_alert');

		$result['required_alert'] = $this->lang->line('required_alert');
		$result['success_alert']  = $this->lang->line('success_alert');
		
		$result['delete_btn']     = $this->lang->line('delete_btn');
		$result['stop_btn']   	  = $this->lang->line('stop_btn');
		$result['active_btn']     = $this->lang->line('active_btn');
		$result['expost_btn']     = $this->lang->line('expost_btn');
		//	搜尋列
		$result['search_bar']     = $this->lang->line('search_bar');    
		$result['search_keyword'] = $this->lang->line('search_keyword');
		//	select2
		$result['select2_key_1']  = $this->lang->line('select2_key_1');
		//	工具列
		$result['toolbar']     	  = $this->lang->line('toolbar'); 
		$result['expost_excel']   = $this->lang->line('expost_excel');
		//	表格資訊
		$result['item']           = $this->lang->line('item'); 
		$result['name']           = $this->lang->line('name');        
		$result['status']         = $this->lang->line('status');
		$result['phone']          = $this->lang->line('phone');      
		$result['work_unit']   	  = $this->lang->line('work_unit');
		$result['expost']         = $this->lang->line('expost');   
		$result['job']         	  = $this->lang->line('job');     
		//	狀態列
		$result['check_off']      = $this->lang->line('check_off');
		$result['check_on']       = $this->lang->line('check_on');
		$result['check_ok']       = $this->lang->line('check_ok'); 
		$result['check_not']      = $this->lang->line('check_not');
		$result['yes_sale']       = $this->lang->line('yes_sale'); 
		$result['no_sale']        = $this->lang->line('no_sale');  

		//	記錄資訊
		$result['save']           = $this->lang->line('save');  
		$result['back']           = $this->lang->line('back');  
		$result['cancel']         = $this->lang->line('cancel');

		return $result;
	}

	//  首頁語系
	public function getAdata(){
		// $result = $this->getcommondata();

		$result['dashboard'] = $this->lang->line('dashboard');
		$result['dashboard_1'] = $this->lang->line('dashboard_1');
		$result['dashboard_2'] = $this->lang->line('dashboard_2');
		$result['dashboard_3'] = $this->lang->line('dashboard_3');
		$result['dashboard_4'] = $this->lang->line('dashboard_4');

		return $result;
	}

	//  人事管理語系
	public function getBdata(){
		// $result = $this->getcommondata();

		//	業務承攬人列表
		$result['employee_name'] = $this->lang->line('employee_name');
		$result['organization'] = $this->lang->line('organization');
		//	搜尋列
		$result['employee_keyword'] = $this->lang->line('employee_keyword');
		//	新增/編輯業務承攬人
		$result['new_employee'] = $this->lang->line('new_employee');
		$result['edit_employee'] = $this->lang->line('edit_employee');
		$result['employee_list'] = $this->lang->line('employee_list');
		$result['view_employee'] = $this->lang->line('view_employee');
		//	基本數據
		$result['employee_1_data'] = $this->lang->line('employee_1_data');
		$result['employee_1_data_1'] = $this->lang->line('employee_1_data_1');
		$result['employee_1_data_2'] = $this->lang->line('employee_1_data_2');
		$result['employee_1_data_3'] = $this->lang->line('employee_1_data_3');
		$result['employee_1_data_4'] = $this->lang->line('employee_1_data_4');
		$result['employee_1_data_5'] = $this->lang->line('employee_1_data_5');
		$result['employee_1_data_6'] = $this->lang->line('employee_1_data_6');
		//	帳戶資料
		$result['employee_2_data'] = $this->lang->line('employee_2_data');
		$result['employee_2_data_1'] = $this->lang->line('employee_2_data_1');
		$result['employee_2_data_2'] = $this->lang->line('employee_2_data_2');
		$result['employee_2_data_3'] = $this->lang->line('employee_2_data_3');
		$result['employee_2_data_4'] = $this->lang->line('employee_2_data_4');
		$result['employee_2_data_5'] = $this->lang->line('employee_2_data_5');
		$result['employee_2_data_6'] = $this->lang->line('employee_2_data_6');
		$result['employee_2_data_7'] = $this->lang->line('employee_2_data_7');
		//	帳戶資料
		$result['employee_3_data'] = $this->lang->line('employee_3_data');
		$result['employee_3_data_1'] = $this->lang->line('employee_3_data_1');
		$result['employee_3_data_2'] = $this->lang->line('employee_3_data_2');
		$result['employee_3_data_3'] = $this->lang->line('employee_3_data_3');

		return $result;
	}

	//  客戶管理語系
	public function getCdata(){
		// $result = $this->getcommondata();

		//	客戶管理
		$result['customers'] = $this->lang->line('customers');
		$result['customers_list'] = $this->lang->line('customers_list');
		$result['customers_keyword'] = $this->lang->line('customers_keyword');

		//	客戶列表
		$result['customers_job'] = '职称';
		//	编辑 / 新增客户
		$result['customers_new'] = $this->lang->line('customers_new');
		$result['customers_edit'] = $this->lang->line('customers_edit');
		
		$result['customers_1_data']    = $this->lang->line('customers_1_data');
		$result['customers_1_data_1']  = $this->lang->line('customers_1_data_1');
		$result['customers_1_data_2']  = $this->lang->line('customers_1_data_2');
		$result['customers_1_data_3']  = $this->lang->line('customers_1_data_3');
		$result['customers_1_data_4']  = $this->lang->line('customers_1_data_4');
		$result['customers_1_data_5']  = $this->lang->line('customers_1_data_5');
		$result['customers_1_data_6']  = $this->lang->line('customers_1_data_6');
		$result['customers_1_data_7']  = $this->lang->line('customers_1_data_7');
		$result['customers_1_data_8']  = $this->lang->line('customers_1_data_8');
		$result['customers_1_data_9']  = $this->lang->line('customers_1_data_9');
		$result['customers_1_data_10'] = $this->lang->line('customers_1_data_10');
		//	客户帐户资料
		$result['customers_2_data']    = $this->lang->line('customers_2_data');
		$result['customers_2_data_1']  = $this->lang->line('customers_2_data_1');
		$result['customers_2_data_2']  = $this->lang->line('customers_2_data_2');
		$result['customers_2_data_3']  = $this->lang->line('customers_2_data_3');
		$result['customers_2_data_4']  = $this->lang->line('customers_2_data_4');
		$result['customers_2_data_5']  = $this->lang->line('customers_2_data_5');
		$result['customers_2_data_6']  = $this->lang->line('customers_2_data_6');
		$result['customers_2_data_7']  = $this->lang->line('customers_2_data_7');
		$result['customers_2_data_8']  = $this->lang->line('customers_2_data_8');
		$result['customers_2_data_9']  = $this->lang->line('customers_2_data_9');
		$result['customers_2_data_10']  = $this->lang->line('customers_2_data_10');
		$result['customers_2_data_11']  = $this->lang->line('customers_2_data_11');

		return $result;
	}

	//	利得发放、股权代售
	public function getDdata(){
		$result['interest_list']    = $this->lang->line('interest_list');   
		$result['interest_keyword'] = $this->lang->line('interest_keyword');
		$result['interest_1']       = $this->lang->line('interest_1');      
		$result['interest_2']       = $this->lang->line('interest_2');      
		$result['interest_3']       = $this->lang->line('interest_3');      
		$result['interest_4']       = $this->lang->line('interest_4');      
		$result['interest_5']       = $this->lang->line('interest_5');      
		
		//	股权代售
		$result['stocksold']   = $this->lang->line('stocksold');  
		$result['stocksold_1'] = $this->lang->line('stocksold_1');
		$result['stocksold_2'] = $this->lang->line('stocksold_2');
		$result['stocksold_3'] = $this->lang->line('stocksold_3');
		$result['stocksold_4'] = $this->lang->line('stocksold_4');
		$result['stocksold_5'] = $this->lang->line('stocksold_5');
		$result['stocksold_6'] = $this->lang->line('stocksold_6');
		$result['stocksold_7'] = $this->lang->line('stocksold_7');
		$result['stocksold_8'] = $this->lang->line('stocksold_8');

		return $result;
	}

	//	商品管理
	public function getEdata(){
		$result['goods']         = $this->lang->line('goods');        
		$result['goods_list']    = $this->lang->line('goods_list');  
		$result['goods_keyword'] = $this->lang->line('goods_keyword');
		$result['goods_new']     = $this->lang->line('goods_new');    
		$result['goods_1']       = $this->lang->line('goods_1');     
		$result['goods_2']       = $this->lang->line('goods_2');     
		$result['goods_3']       = $this->lang->line('goods_3');     
		$result['goods_4']       = $this->lang->line('goods_4');     
		$result['goods_5']       = $this->lang->line('goods_5');     
		$result['goods_6']       = $this->lang->line('goods_6');     
		$result['goods_7']       = $this->lang->line('goods_7');     
		$result['goods_8']       = $this->lang->line('goods_8');     
		$result['goods_9']       = $this->lang->line('goods_9');     
		$result['goods_10']      = $this->lang->line('goods_10');     
		$result['goods_11']      = $this->lang->line('goods_11');     

		return $result;
	}

	//	訂單管理
	public function getFdata(){
		$result['order']      = $this->lang->line('order');     
		$result['order_list'] = $this->lang->line('order_list');
		$result['order_1']    = $this->lang->line('order_1');   
		$result['order_2']    = $this->lang->line('order_2');   
		$result['order_3']    = $this->lang->line('order_3');   
		$result['order_4']    = $this->lang->line('order_4');   
		$result['order_5']    = $this->lang->line('order_5');   
		$result['order_6']    = $this->lang->line('order_6');   
		$result['order_new']  = $this->lang->line('order_new'); 
		$result['order_1']    = $this->lang->line('order_1');   
		$result['order_2']    = $this->lang->line('order_2');   
		$result['order_3']    = $this->lang->line('order_3');   
		$result['order_4']    = $this->lang->line('order_4');   
		$result['order_5']    = $this->lang->line('order_5');   
		$result['order_6']    = $this->lang->line('order_6');   
		$result['order_7']    = $this->lang->line('order_7');   
		$result['order_8']    = $this->lang->line('order_8');   
		$result['order_9']    = $this->lang->line('order_9');   
		$result['order_10']   = $this->lang->line('order_10');  
		$result['order_11']   = $this->lang->line('order_11');  
		$result['order_12']   = $this->lang->line('order_12');  
		$result['order_13']   = $this->lang->line('order_13');  
		$result['order_14']   = $this->lang->line('order_14');  
		$result['order_15']   = $this->lang->line('order_15');  
		$result['order_16']   = $this->lang->line('order_16');  
		$result['order_17']   = $this->lang->line('order_17');  
		$result['order_18']   = $this->lang->line('order_18');  
		$result['order_19']   = $this->lang->line('order_19');  

		return $result;
	}

	//	組織管理
	public function getGdata(){
		$result['organization']         = $this->lang->line('organization');        
		$result['organization_unit']    = $this->lang->line('organization_unit');   
		$result['organization_keyword'] = $this->lang->line('organization_keyword');
		$result['organization_list']    = $this->lang->line('organization_list');   
		$result['organization_1']       = $this->lang->line('organization_1');      
		$result['organization_2']       = $this->lang->line('organization_2');      
		$result['organization_3']       = $this->lang->line('organization_3');      
		$result['organization_4']       = $this->lang->line('organization_4');      
		$result['organization_5']       = $this->lang->line('organization_5');      
		$result['organization_6']       = $this->lang->line('organization_6');      
		$result['organization_7']       = $this->lang->line('organization_7');      
		$result['organization_8']       = $this->lang->line('organization_8');      
		$result['organization_9']       = $this->lang->line('organization_9');      
		$result['occupation']           = $this->lang->line('occupation');          
		$result['occupation_keyword']   = $this->lang->line('occupation_keyword');  
		$result['occupation_list']      = $this->lang->line('occupation_list');     
		$result['occupation_1']         = $this->lang->line('occupation_1');        

		return $result;
	}

	//	獎金核發管理
	public function getIdata(){
		$result['bouns']      = $this->lang->line('bouns');     
		$result['bouns_1']    = $this->lang->line('bouns_1');   
		$result['bouns_2']    = $this->lang->line('bouns_2');   
		$result['bouns_3']    = $this->lang->line('bouns_3');   
		$result['bouns_4']    = $this->lang->line('bouns_4');   
		$result['bouns_5']    = $this->lang->line('bouns_5');   
		$result['bouns_6']    = $this->lang->line('bouns_6');   
		$result['bouns_7']    = $this->lang->line('bouns_7');   
		$result['bouns_1_1']  = $this->lang->line('bouns_1_1'); 
		$result['bouns_1_2']  = $this->lang->line('bouns_1_2'); 
		$result['bouns_1_3']  = $this->lang->line('bouns_1_3'); 
		$result['bouns_1_4']  = $this->lang->line('bouns_1_4'); 
		$result['bouns_1_5']  = $this->lang->line('bouns_1_5'); 
		$result['bouns_1_6']  = $this->lang->line('bouns_1_6'); 
		$result['bouns_1_7']  = $this->lang->line('bouns_1_7'); 
		$result['bouns_1_8']  = $this->lang->line('bouns_1_8'); 
		$result['bouns_1_9']  = $this->lang->line('bouns_1_9'); 
		$result['bouns_1_10'] = $this->lang->line('bouns_1_10');
		$result['bouns_1_11'] = $this->lang->line('bouns_1_11');
		$result['bouns_1_12'] = $this->lang->line('bouns_1_12');
		$result['bouns_1_13'] = $this->lang->line('bouns_1_13');
		$result['bouns_1_14'] = $this->lang->line('bouns_1_14');
		$result['bouns_2_1']  = $this->lang->line('bouns_2_1'); 
		$result['bouns_2_2']  = $this->lang->line('bouns_2_2'); 
		$result['bouns_2_3']  = $this->lang->line('bouns_2_3'); 
		$result['bouns_2_4']  = $this->lang->line('bouns_2_4'); 
		$result['bouns_2_5']  = $this->lang->line('bouns_2_5'); 
		$result['bouns_2_6']  = $this->lang->line('bouns_2_6'); 
		$result['bouns_2_7']  = $this->lang->line('bouns_2_7'); 
		$result['bouns_2_8']  = $this->lang->line('bouns_2_8'); 
		$result['bouns_2_9']  = $this->lang->line('bouns_2_9'); 
		$result['bouns_2_10'] = $this->lang->line('bouns_2_10');
		$result['bouns_2_11'] = $this->lang->line('bouns_2_11');
		$result['bouns_2_12'] = $this->lang->line('bouns_2_12');
		$result['bouns_2_13'] = $this->lang->line('bouns_2_13');
		$result['bouns_2_14'] = $this->lang->line('bouns_2_14');
		$result['bouns_3_1']  = $this->lang->line('bouns_3_1'); 
		$result['bouns_3_2']  = $this->lang->line('bouns_3_2'); 
		$result['bouns_3_3']  = $this->lang->line('bouns_3_3'); 
		$result['bouns_3_4']  = $this->lang->line('bouns_3_4'); 
		$result['bouns_3_5']  = $this->lang->line('bouns_3_5'); 
		$result['bouns_3_6']  = $this->lang->line('bouns_3_6'); 
		$result['bouns_3_7']  = $this->lang->line('bouns_3_7'); 
		$result['bouns_3_8']  = $this->lang->line('bouns_3_8'); 
		$result['bouns_3_9']  = $this->lang->line('bouns_3_9'); 
		$result['bouns_3_10'] = $this->lang->line('bouns_3_10');
		$result['bouns_3_11'] = $this->lang->line('bouns_3_11');
		$result['bouns_3_12'] = $this->lang->line('bouns_3_12');
		$result['bouns_3_13'] = $this->lang->line('bouns_3_13');
		$result['bouns_3_14'] = $this->lang->line('bouns_3_14');
		$result['bouns_4_1']  = $this->lang->line('bouns_4_1'); 
		$result['bouns_4_2']  = $this->lang->line('bouns_4_2'); 
		$result['bouns_4_3']  = $this->lang->line('bouns_4_3'); 
		$result['bouns_4_4']  = $this->lang->line('bouns_4_4'); 
		$result['bouns_4_5']  = $this->lang->line('bouns_4_5'); 
		$result['bouns_4_6']  = $this->lang->line('bouns_4_6'); 
		$result['bouns_4_7']  = $this->lang->line('bouns_4_7'); 
		$result['bouns_4_8']  = $this->lang->line('bouns_4_8'); 
		$result['bouns_4_9']  = $this->lang->line('bouns_4_9'); 
		$result['bouns_4_10'] = $this->lang->line('bouns_4_10');
		$result['bouns_4_11'] = $this->lang->line('bouns_4_11');
		$result['bouns_4_12'] = $this->lang->line('bouns_4_12');
		$result['bouns_4_13'] = $this->lang->line('bouns_4_13');
		$result['bouns_4_14'] = $this->lang->line('bouns_4_14');
		$result['bouns_4_15'] = $this->lang->line('bouns_4_15');
		$result['bouns_4_16'] = $this->lang->line('bouns_4_16');
		$result['bouns_4_17'] = $this->lang->line('bouns_4_17');
		$result['bouns_4_18'] = $this->lang->line('bouns_4_18');
		$result['bouns_5_1']  = $this->lang->line('bouns_5_1'); 
		$result['bouns_5_2']  = $this->lang->line('bouns_5_2'); 
		$result['bouns_5_3']  = $this->lang->line('bouns_5_3'); 
		$result['bouns_5_4']  = $this->lang->line('bouns_5_4'); 
		$result['bouns_5_5']  = $this->lang->line('bouns_5_5'); 
		$result['bouns_5_6']  = $this->lang->line('bouns_5_6'); 
		$result['bouns_5_7']  = $this->lang->line('bouns_5_7'); 
		$result['bouns_5_8']  = $this->lang->line('bouns_5_8'); 
		$result['bouns_5_9']  = $this->lang->line('bouns_5_9'); 
		$result['bouns_5_10'] = $this->lang->line('bouns_5_10');
		$result['bouns_5_11'] = $this->lang->line('bouns_5_11');
		$result['bouns_5_12'] = $this->lang->line('bouns_5_12');
		$result['bouns_6_1']  = $this->lang->line('bouns_6_1'); 
		$result['bouns_6_2']  = $this->lang->line('bouns_6_2'); 
		$result['bouns_6_3']  = $this->lang->line('bouns_6_3'); 
		$result['bouns_6_4']  = $this->lang->line('bouns_6_4'); 
		$result['bouns_6_5']  = $this->lang->line('bouns_6_5'); 
		$result['bouns_6_6']  = $this->lang->line('bouns_6_6'); 
		$result['bouns_6_7']  = $this->lang->line('bouns_6_7'); 
		$result['bouns_6_8']  = $this->lang->line('bouns_6_8'); 
		$result['bouns_6_9']  = $this->lang->line('bouns_6_9'); 
		$result['bouns_6_10'] = $this->lang->line('bouns_6_10');
		$result['bouns_6_11'] = $this->lang->line('bouns_6_11');
		$result['bouns_6_12'] = $this->lang->line('bouns_6_12');
		$result['bouns_6_13'] = $this->lang->line('bouns_6_13');
		$result['bouns_6_14'] = $this->lang->line('bouns_6_14');
		$result['bouns_6_15'] = $this->lang->line('bouns_6_15');
		$result['bouns_6_16'] = $this->lang->line('bouns_6_16');
		$result['bouns_6_17'] = $this->lang->line('bouns_6_17');
		$result['bouns_6_18'] = $this->lang->line('bouns_6_18');
		$result['bouns_6_19'] = $this->lang->line('bouns_6_19');
		$result['bouns_6_20'] = $this->lang->line('bouns_6_20');
		$result['bouns_6_21'] = $this->lang->line('bouns_6_21');

		return $result;
	}

	//	獎金報表管理	
	public function getJdata(){
		$result['bouns_export']    =  $this->lang->line('bouns_export');
		$result['bouns_export_1']  =  $this->lang->line('bouns_export_1');
		$result['bouns_export_2']  =  $this->lang->line('bouns_export_2');
		$result['bouns_export_3']  =  $this->lang->line('bouns_export_3');
		$result['bouns_export_4']  =  $this->lang->line('bouns_export_4');
		$result['bouns_export_5']  =  $this->lang->line('bouns_export_5');
		$result['bouns_export_6']  =  $this->lang->line('bouns_export_6');
		$result['bouns_export_7']  =  $this->lang->line('bouns_export_7');
		$result['bouns_export_8']  =  $this->lang->line('bouns_export_8');
		$result['bouns_export_9']  =  $this->lang->line('bouns_export_9');
		$result['bouns_export_10'] =  $this->lang->line('bouns_export_10');
		$result['bouns_export_11'] =  $this->lang->line('bouns_export_11');
		$result['bouns_export_12'] =  $this->lang->line('bouns_export_12');
		$result['bouns_export_13'] =  $this->lang->line('bouns_export_13');
		$result['bouns_export_14'] =  $this->lang->line('bouns_export_14');
		$result['bouns_export_15'] =  $this->lang->line('bouns_export_15');
		$result['bouns_export_16'] =  $this->lang->line('bouns_export_16');
		$result['bouns_export_17'] =  $this->lang->line('bouns_export_17');
		$result['bouns_export_18'] =  $this->lang->line('bouns_export_18');
		$result['bouns_export_19'] =  $this->lang->line('bouns_export_19');
		$result['bouns_export_20'] =  $this->lang->line('bouns_export_20');
		$result['bouns_export_21'] =  $this->lang->line('bouns_export_21');
		$result['bouns_export_22'] =  $this->lang->line('bouns_export_22');
		$result['bouns_export_23'] =  $this->lang->line('bouns_export_23');
		$result['bouns_export_24'] =  $this->lang->line('bouns_export_24');
		$result['bouns_export_25'] =  $this->lang->line('bouns_export_25');
		$result['bouns_export_26'] =  $this->lang->line('bouns_export_26');
		$result['bouns_export_27'] =  $this->lang->line('bouns_export_27');
		$result['bouns_export_28'] =  $this->lang->line('bouns_export_28');
		$result['bouns_export_29'] =  $this->lang->line('bouns_export_29');
		$result['bouns_export_30'] =  $this->lang->line('bouns_export_30');
		$result['bouns_export_31'] =  $this->lang->line('bouns_export_31');
		$result['bouns_export_32'] =  $this->lang->line('bouns_export_32');
		$result['bouns_export_33'] =  $this->lang->line('bouns_export_33');
		$result['bouns_export_34'] =  $this->lang->line('bouns_export_34');
		$result['bouns_export_35'] =  $this->lang->line('bouns_export_35');
		$result['bouns_export_36'] =  $this->lang->line('bouns_export_36');

		return $result;
	}

	//	帳號管理	
	public function getKdata(){
		$result['account']         = $this->lang->line('account');        
		$result['account_list']    = $this->lang->line('account_list');   
		$result['account_bar']     = $this->lang->line('account_bar');    
		$result['account_keyword'] = $this->lang->line('account_keyword');
		$result['account_1']       = $this->lang->line('account_1');      
		$result['account_2']       = $this->lang->line('account_2');      
		$result['account_3']       = $this->lang->line('account_3');      
		$result['account_4']       = $this->lang->line('account_4');      
		$result['account_5']       = $this->lang->line('account_5');      
		$result['account_6']       = $this->lang->line('account_6');      
		$result['account_7']       = $this->lang->line('account_7');      
		$result['account_8']       = $this->lang->line('account_8');      

		return $result;
	}

	public function getMdata(){

	}


}
