<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customers_model extends CI_model{

    public function getiddata($id){
        $this->db->select('*');
        $this->db->from('customer');
        $this->db->where('customer_id',$id);
        // $this->db->where('customer_status',1);
        $this->db->where('customer_is_del',0);

        $query = $this->db->get();
        return $query->row_array();
    }

    //查詢所有客戶
    public function getList($keyword='',$limit=''){
        $this->db->select('*');
        $this->db->from('customer');
        $this->db->where('customer_is_del',0);
        // $this->db->where('customer_status',1);

        if (isset($keyword['customer_name']) && $keyword['customer_name'] != '') {
            $this->db->like('customer_name',$keyword['customer_name']);
            // $this->db->like('contract_id',$keyword['contract_id']);
        }
        if (isset($keyword['employee_id']) && $keyword['employee_id'] != '') {
            $this->db->where('employee_id',$keyword['employee_id']);
        }
        if (isset($keyword['organization_id']) && $keyword['organization_id'] != '') {
            $this->db->where('organization_id',$keyword['organization_id']);
        }

         if (isset($keyword['status']) && $keyword['status'] != '') {
            $this->db->where('customer_status',$keyword['status']);
        }

        if ($limit) {
            $this->db->limit($limit);
        }

        $result = $this->db->get();
        $result = $result->result_array();
        return $result;
    }

    //新增客戶
    public function add_customers($customers){

        $this->db->insert('customer',$customers);

    }

    //修改客戶
    public function update_customers($customersupdate,$id){
        $this->db->update('customer',$customersupdate,array('customer_id' => $id));
    }

    //查詢最後一筆客戶ID
    public function getlatestid(){
        $this->db->select_Max('customer_id','maxid');  
        $query = $this->db->get('customer');
        return $query->result_array();
    }

    //查詢新客戶
    public function get_new_customer($date,$selection){
        $this->db->select('*');
        $this->db->from('customer');
        $this->db->where('year(customer_created_date)',date('Y',strtotime($date)));
        $this->db->where('month(customer_created_date)',date('m',strtotime($date)));
        if (isset($selection['organization_id']) && $selection['organization_id'] != '') {
            $this->db->where('organization_id',$selection['organization_id']);
        }
        $this->db->where('customer_status',1);
        $this->db->where('customer_is_del',0); 

        $query = $this->db->count_all_results();
        return $query;
    }

}
