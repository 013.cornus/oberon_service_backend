<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pagintion_model extends CI_model{

    public function setPager($data_arr,$page,$url_suffix = '') {

        // 分頁設置
        $config['full_tag_open']        = '<div class="pagination" style="float: right;"><ul>';
        $config['full_tag_close']       = '</ul></div>';
        
        $config['query_string_segment'] = 'page';
        $config['prev_link']            = ' <i class="fa fa-angle-left"></i> 上頁 ';
        $config['prev_tag_open']        = '<li class="prev">';
        $config['prev_tag_close']       = '</li>';
        $config['next_link']            = '下頁 <i class="fa fa-angle-right"></i> ';
        $config['next_tag_open']        = '<li class="next">';
        $config['next_tag_close']       = '</li>';
        $config['cur_tag_open']         = '<li class="active"><a>';
        $config['cur_tag_close']        = '</a></li>';
        $config['num_tag_open']         = '<li>';
        $config['num_tag_close']        = '</li>';
        
        $config['suffix']               = $url_suffix;

        $config['first_tag_open']       = '<li>';
        $config['first_link']           = '<i class="fa fa-angle-double-left"></i> 第一頁';
        $config['first_tag_close']      = '</li>';
        $config['last_tag_open']        = '<li>';
        $config['last_link']            = '最後頁 <i class="fa fa-angle-double-right"></i>';
        $config['last_tag_close']       = '</li>';

        //url位址
        $config['base_url']             = site_url($this->router->class.'/list/'); 
        $config['first_url']            = site_url($this->router->class.'/list/').$url_suffix;
        $config['total_rows']           = count($data_arr);
        $config['num_links']            = 2;            // 顯示分頁數量
        $config['per_page']             = 20;           // 每頁顯示數量
        $config['use_page_numbers']     = TRUE;         // 使用頁碼方式而非偏移量方式傳值

        // 分頁初始化
        $this->pagination->initialize($config);

        // DB查詢
        $page = (int)$this->uri->segment(3); // 讀取頁碼

        // 計算偏移量
        $offset = ($page == false) ? 0 : ($config['per_page'] * ($page - 1)); 

        $this->db->limit($config['per_page'], $offset); // 每頁顯示數量，偏移量
        
        // 讀取DB
        $pager['rows'] = '總筆數：'.$config['total_rows'];
        $pager['list'] = $config['per_page']; 
        $pager['page'] = $this->pagination->create_links();

        return $pager;
    }

    public function setBplanPager($total_rows) {
        // $this->load->library('pagination');

        $config['base_url']             = site_url($this->router->class);
        $config['total_rows']           = $total_rows;
        $config['per_page']             = 10;
        $config['enable_query_strings'] = true;
        $config['page_query_string']    = true;
        $config['reuse_query_string']   = true;
        $config['display_pages']        = false;
        $config['use_page_numbers']     = true;
        $config['prev_link']            = '上一頁';
        $config['prev_tag_open']        = '<li class="previous">';
        $config['prev_tag_close']       = '</li>';
        $config['next_link']            = '下一頁';
        $config['next_tag_open']        = '<li class="next">';
        $config['next_tag_close']       = '</li>';
        $config['first_link']           = '';
        $config['last_link']            = '';

        $pager['per_page'] = $config['per_page'];
        // 從 URI 中 讀取頁碼參數
        $pager['curpage'] = $this->input->get('per_page') ? intval($this->input->get('per_page')) : 1;
        // 頁碼x每頁數量 = 查詢從第幾條數據查詢
        $pager['start_page'] = ($pager['curpage'] && is_numeric($pager['curpage']) && $pager['curpage'] > 1) ? ($pager['curpage'] - 1) * $pager['per_page'] : 0;

        $this->pagination->initialize($config);
        
        return $pager;
    }
}
