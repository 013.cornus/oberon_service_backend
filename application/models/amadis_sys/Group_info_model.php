<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Group_info_model extends CI_model{
    public function __construct(){
        parent::__construct();
        $this->db = $this->load->database('system_db',true); 
    }

    public function getidData($id){
        $this->db->select('*');
        $this->db->from('group_info');
        $this->db->where('group_info_id',$id);
        $this->db->where('group_info_is_del',0);

        $query = $this->db->get();
        return $query->row_array();
    }

    public function getDataByType($type){
        $this->db->select('*');
        $this->db->from('group_info');
        $this->db->where('group_info_type',$type);
        $this->db->where('group_info_is_del',0);

        $query = $this->db->get();
        return $query->row_array();
    }

    public function getList($keyword='',$limit=''){
        $this->db->select('*');
        $this->db->from('group_info');
        $this->db->where('group_info_is_del',0);
        if (isset($keyword['fullname']) && $keyword['fullname'] != '') {
            $this->db->like('group_info_name',$keyword['fullname']);
        }
        if (isset($keyword['status']) && $keyword['status'] != '') {
            $this->db->where('group_info_status',$keyword['status']);
        }
        if (isset($keyword['type']) && $keyword['type'] != '') {
            $this->db->where('group_info_type',$keyword['type']);
        }

        if ($limit) {
            $this->db->limit($limit);
        }

        $query = $this->db->get();
        return $query->result_array();
    }

    //新增產品
    public function add_group_info($group_info){

        $this->db->insert('group_info',$group_info);

    }

    //修改產品
    public function update_group_info($group_info,$id){
        $this->db->update('group_info',$group_info,array('group_info_id' => $id));
    }

    //撈出最新商品id
    public function get_latest_id(){
        $this->db->select_max('group_info_id','maxid');
        $this->db->from('group_info');

        $query = $this->db->get();
        return $query->row_array();
    }
}
