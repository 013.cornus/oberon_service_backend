<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Competence_model extends CI_model{
    //查詢所有權限資料
    public function getdata($keyword = ''){
        $this->db->select('*');
        $this->db->from('competence');
        $this->db->like('competence_name',$keyword);
        $this->db->where('competence_is_del',0);

        $query = $this->db->get();
        return $query->result_array();
    }

    //查詢單個權限資料
    public function getidData($id){
        $this->db->select('*');
        $this->db->from('competence');
        $this->db->where('competence_id',$id);

        $query = $this->db->get();
        return $query->row_array();
    }

    //查詢所有除系統管理員外權限資料
    public function get_certain_competence(){
        $this->db->select('*');
        $this->db->from('competence');
        $this->db->where('competence_status',1);
        $this->db->where('competence_is_del',0);
        $this->db->where('competence_id !=',1);

        $query = $this->db->get();
        return $query->result_array();
    }

    //查詢所有權限資料
    public function getsuitableright($id){
        $this->db->select('*');
        $this->db->from('competence');
        $this->db->where('competence_id >=',$id);
        $this->db->where('competence_status',1);
        $this->db->where('competence_is_del',0);

        $query = $this->db->get();
        return $query->result_array();
    }

    //新增權限
    public function add_competence($competence){
        $this->db->insert('competence',$competence);
    }

    //新增權限動作
    public function add_actions($newright){
        $this->db->insert('actions',$newright);
    }

    //修改權限
    public function update_competence($competence,$competence_id){
        $this->db->update('competence',$competence,array('competence_id' => $competence_id));
    }

    //修改權限可執行動作
    public function update_actions($editright,$actionsid){
        $this->db->update('actions',$editright,array('actions_id' => $actionsid));
    }

    //撈出可新增/編輯權限
    public function EditableCompetence($competence){
        $this->db->select('*');
        $this->db->from('competence');
        $this->db->where('competence_id >=',$competence);
        $this->db->where('competence_is_del',0);

        $query = $this->db->get();
        return $query->result_array();
    }

    //撈出最新一筆權限
    public function getlatest(){
        $this->db->select_Max('competence_id','maxid');  
        $query = $this->db->get('competence');
        return $query->result_array();
    }

    //權限執行動作
    public function getaction($competence_id){
        $this->db->select('*');
        $this->db->from('actions');
        $this->db->join('competence','competence_id');
        $this->db->where('competence_id',$competence_id);

        $query = $this->db->get();
        return $query->result_array();
    }
}
