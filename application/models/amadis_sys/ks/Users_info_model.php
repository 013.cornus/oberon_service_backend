<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users_info_model extends CI_Model {

	public function __construct(){
		parent::__construct();
		$this->from = 'users_info';
		$this->db_connect2 = $this->load->database('ks_db',true); 
	}

	public function getList($queryData){
		$this->db_connect2->select('*')
				 ->from($this->from)
				 ->where($this->from.'_is_del',0,false);

		if ( isset($queryData['row']) && $queryData['row'] && isset($queryData['key']) && $queryData['key']) {
		 	$this->db_connect2->where($queryData['row'],$queryData['key']);
		} 

		$result = $this->db_connect2->get();
		$result = $result->result_array();
		return $result;
	}

	//新增帳號群
    public function add_users_info($users_info){

        $this->db_connect2->insert('users_info',$users_info);
        return $this->db_connect2->insert_id();
    }

    //修改帳號群
    public function update_users_info($users_info,$id){
        $this->db_connect2->update('users_info',$users_info,array('users_id' => $id));
    }

	public function checkUsersPhone($phone){
		$this->db_connect2->from($this->from)
				 ->where($this->from.'_phone', $phone)
				 ->where($this->from.'_is_del', 0, false)
				 ->where($this->from.'_status', 1, false);

		$result = $this->db_connect2->count_all_results();
		return $result;
	}

}
