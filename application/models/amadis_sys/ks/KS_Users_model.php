<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class KS_Users_model extends CI_Model {

	public function __construct(){
		parent::__construct();
		$this->from = 'users';
		$this->db_connect2 = $this->load->database('ks_db',true); 
	}

	public function getIdData($id){
		$this->db_connect2->select('*')
				 ->from($this->from)
				 ->where($this->from.'_status',1,false)
				 ->where($this->from.'_is_del',0,false)
				 ->where($this->from.'_id',$id,false);
		$result = $this->db_connect2->get();
		$result = $result->row_array();
		return $result;
	}

	//新增帳號
    public function add_users($users){

        $this->db_connect2->insert('users',$users);

    }

    //修改帳號
    public function update_users($users,$id){
        $this->db_connect2->update('users',$users,array('users_id' => $id));
    }

	public function getList($queryData){
		$this->db_connect2->select('*')
				 ->from($this->from)
				 ->where($this->from.'_status',1,false)
				 ->where($this->from.'_is_del',0,false);
		if ( isset($queryData['row']) && $queryData['row'] && isset($queryData['key']) && $queryData['key']) {
		 	$this->db_connect2->where($queryData['row'], $queryData['key']);
		} 
		if ( isset($queryData['users_main']) && $queryData['users_main'] ) {
		 	$this->db_connect2->where('users_main', $queryData['users_main']);
		} 
		if ( isset($queryData['group']) && $queryData['group'] ) {
		 	$this->db_connect2->where('group_id', $queryData['group']);
		} 
		if ( isset($queryData['users_id']) && $queryData['users_id'] ) {
		 	$this->db_connect2->where('users_id', $queryData['users_id']);
		} 
		if ( isset($queryData['not_in']) && $queryData['not_in'] ) {
		 	$this->db_connect2->where_not_in('users_id',$queryData['not_in']);
		}

		$result = $this->db_connect2->get();
		$result = $result->result_array();
		return $result;
	}
}
