<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register_key_model extends CI_model{
	public function __construct(){
		parent::__construct();
		$this->from = 'register_key';
		$this->db = $this->load->database('ks_db', true);
	}

	public function getIdData($id){
		$this->db->select('*')
				 ->from($this->from)
				 ->where($this->from.'_status',1,false)
				 ->where($this->from.'_is_del',0,false)
				 ->where($this->from.'_id',$id,false);
		$result = $this->db->get();
		$result = $result->row_array();
		return $result;
	}

	public function getList($keyword='', $limit=''){
		$this->db->select('*');
		$this->db->from('register_key');
		$this->db->where('register_key_is_del', 0);
		$this->db->where('register_key_status', 1);
		if (isset($keyword['group_type']) && $keyword['group_type'] != '') {
		    $this->db->where('group_type',$keyword['group_type']);
		}
		$query = $this->db->get();
		return $query->result_array();
	}

	//update register key use
	public function update_register_key($register_key_id, $register_key_data){
		$this->db->update('register_key', $register_key_data, array('register_key_id' => $register_key_id));
	}
}