<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Detection_model extends CI_model{
    public function __construct(){
        parent::__construct();
        $this->db_connect2 = $this->load->database('ks_db',true); 
    }

    public function getidData($id){
        $this->db_connect2->select('*');
        $this->db_connect2->from('detection');
        $this->db_connect2->where('detection_id',$id);
        $this->db_connect2->where('detection_is_del',0);

        $query = $this->db_connect2->get();
        return $query->row_array();
    }

    public function getList($keyword='',$limit=''){
        $this->db_connect2->select('*');
        $this->db_connect2->from('detection');
        $this->db_connect2->where('detection_is_del',0);
        if (isset($keyword['fullname']) && $keyword['fullname'] != '') {
            $this->db_connect2->like('detection_name',$keyword['fullname']);
        }
        if (isset($keyword['status']) && $keyword['status'] != '') {
            $this->db_connect2->where('detection_status',$keyword['status']);
        }
        if (isset($keyword['user']) && $keyword['user'] != '') {
            $this->db_connect2->where('users_id',$keyword['user']);
        }

        if ($limit) {
            $this->db_connect2->limit($limit);
        }

        $query = $this->db_connect2->get();
        return $query->result_array();
    }

    //新增產品
    public function add_detection($detection){

        $this->db_connect2->insert('detection',$detection);

    }

    //修改產品
    public function update_detection($detection,$id){
        $this->db_connect2->update('detection',$detection,array('detection_id' => $id));
    }

    //撈出最新商品id
    public function get_latest_id(){
        $this->db_connect2->select_max('detection_id','maxid');
        $this->db_connect2->from('detection');

        $query = $this->db_connect2->get();
        return $query->row_array();
    }

    //檢測數量
    public function detectionNumber(){
        $this->db_connect2->select('*');
        $this->db_connect2->from('detection');
        $this->db_connect2->where('detection_is_del',0);
        return $this->db_connect2->count_all_results();
    }
}
