<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Group_model extends CI_Model {

	public function __construct(){
		parent::__construct();
		$this->from = 'group';
		$this->db_connect2 = $this->load->database('ks_db',true); 
	}

	public function getIdData($id){
		$this->db_connect2->select('*')
				 ->from($this->from)
				 ->where($this->from.'_status',1,false)
				 ->where($this->from.'_is_del',0,false)
				 ->where($this->from.'_id',$id,false);
		$result = $this->db_connect2->get();
		$result = $result->row_array();
		return $result;
	}

	public function getKeyData($key,$value){
		$this->db_connect2->select('*')
				 ->from($this->from)
				 ->where($key,$value);
		$result = $this->db_connect2->get();
		$result = $result->row_array();
		return $result;
	}

	public function getList($queryData){
		$this->db_connect2->select('*')
				 ->from($this->from)
				 // ->where($this->from.'_status',1,false)
				 ->where($this->from.'_is_del',0,false);

		if ( isset($queryData['type']) && $queryData['type'] ) {
		 	$this->db_connect2->where($this->from.'_type',$queryData['type']);
		} 
		if ( isset($queryData['id']) && $queryData['id'] ) {
		 	$this->db_connect2->where($this->from.'_id',$queryData['id']);
		}
		if ( isset($queryData['groupList']) && $queryData['groupList'] ) {
		 	$this->db_connect2->where_in($this->from.'_id', $queryData['groupList']);
		}

		$result = $this->db_connect2->get();
		$result = $result->result_array();
		return $result;
	}

	//獲取所有帳號群
	public function getGroupType(){
		$this->db_connect2->select('group_type')
				 ->from($this->from)
				 ->where($this->from.'_status',1,false)
				 ->where($this->from.'_is_del',0,false);
		$this->db_connect2->distinct();
		$result = $this->db_connect2->get();
		$result = $result->result_array();
		return $result;
	}

	//新增帳號群
    public function add_group($group){

        $this->db_connect2->insert('group',$group);
        return $this->db_connect2->insert_id();
    }

    //修改帳號群
    public function update_group($group,$id){
        $this->db_connect2->update('group',$group,array('group_id' => $id));
    }
	
	public function checkUsersPhone($phone){
		$this->db_connect2->from($this->from)
				 ->where($this->from.'_phone', $phone)
				 ->where($this->from.'_is_del', 0, false)
				 ->where($this->from.'_status', 1, false);

		$result = $this->db_connect2->count_all_results();
		return $result;
	}

}