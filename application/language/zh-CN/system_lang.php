<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * 簡體中文 語言包
 * 
 */

//	登入頁
$lang['system_name'] = '茂云业务管理系统';
$lang['remember_account'] = '记住帐号';
$lang['account'] = '请输入帐号';
$lang['password'] = '请输入密码';
$lang['login'] = '登入';

$lang['home']        	= '首页';
//	系統操作
$lang['new']            = '新增';
$lang['edit']           = '编辑';
$lang['disable']        = '停权';
$lang['active']         = '启用';
$lang['notenabled']     = '停用';
$lang['shelf']          = '上架';
$lang['obtained']       = '下架';
$lang['delete']         = '删除';
$lang['view']           = '检视';
$lang['search']         = '搜寻';

//	警告訊息
$lang['warning']       	= '警告';
$lang['delete_alert']   = '确定要删除该笔资料吗?';
$lang['stop_alert']   	= '确定要停用这笔资料吗?';
$lang['active_alert']   = '确定要启用这笔资料吗?';
$lang['expost_alert']   = '确定要汇出报表 (依照页面搜寻条件)?';

$lang['required_alert'] = '错误！请完成所有必填项目';
$lang['success_alert']  = '您的表单验证成功！';

$lang['delete_btn']   	= '确定删除';
$lang['stop_btn']   	= '确定停用';
$lang['active_btn']   	= '确定启用';
$lang['expost_btn']     = '确定要汇出';

//	搜尋列
$lang['search_bar']     = '搜寻列';
$lang['search_keyword'] = '搜寻关键词';

//	select2
$lang['select2_key_1']  = '请选择';
$lang['select2_key_2']  = '搜寻关键词';

//	工具列
$lang['toolbar']     	= '工具列';
$lang['expost_excel'] 	= '输出成 Excel';

//	表格資訊
$lang['item']           = '项次';
$lang['name']           = '姓名';
$lang['status']         = '状态';
$lang['phone']          = '手机';
$lang['work_unit']   	= '所属单位';
$lang['expost']         = '报表输出';
$lang['job'] 			= '职称';
$lang['update_date']    = '更新日期';
$lang['active_date']    = '生效日';
$lang['action']         = '动作';

$lang['check_off']      = '未发放';
$lang['check_on']       = '已发放';
$lang['check_ok']       = '审核通过';
$lang['check_not']      = '审核否决';
$lang['yes_sale']       = '可售出';
$lang['no_sale']        = '不可售出';

//	記錄資訊
$lang['save']           = '储存';
$lang['back']           = '返回';
$lang['cancel']         = '取消';
//	=================================================
//	1.首頁
$lang['dashboard']   = '资讯总览';
$lang['dashboard_1'] = '本月奖金总额';
$lang['dashboard_2'] = '本月销售总额';
$lang['dashboard_3'] = '本月新客户';
$lang['dashboard_4'] = '上月销售总额';
$lang['account_setting'] = '帐户设定';
$lang['pws_change'] = '密码变更';
$lang['dashboard_4'] = '上月销售总额';
$lang['users_name'] = '用户名称';
$lang['users_account'] = '用户帐号';
$lang['users_psw'] = '新用户密码';
$lang['users_check_psw'] = '再次确认密码';

//	2.人事管理
$lang['menu2'] = '人事管理';

//	2.1	業務承攬人列表
$lang['employee_name'] = '业务姓名';
$lang['organization'] = '营业处';
//	2.1.1	搜尋列
$lang['employee_keyword'] = '请输入关键词 / 业务姓名';
//	2.1.1	業務列表

//	2.2	新增/編輯業務承攬人
$lang['new_employee'] = '新增-业务承揽人';
$lang['edit_employee'] = '编辑-业务承揽人';
$lang['employee_list'] = '业务承揽人列表';
$lang['view_employee'] = '检视-业务承揽人';

//	2.2.1	基本數據
$lang['employee_1_data'] = '基本数据';
$lang['employee_1_data_1'] = '姓名';
$lang['employee_1_data_2'] = '身分证字号';
$lang['employee_1_data_3'] = '所属单位';
$lang['employee_1_data_4'] = '手机';
$lang['employee_1_data_5'] = '联络电话';
$lang['employee_1_data_6'] = '联络地址';

//	2.2.1	帳戶資料
$lang['employee_2_data'] = '帐户资料';
$lang['employee_2_data_1'] = '银行/邮局';
$lang['employee_2_data_2'] = '银行';
$lang['employee_2_data_3'] = '邮局';
$lang['employee_2_data_4'] = '汇款银行';
$lang['employee_2_data_5'] = '分行名称';
$lang['employee_2_data_6'] = '汇款账号';
$lang['employee_2_data_7'] = '备注';

//	2.2.1	帳戶資料
$lang['employee_3_data'] = '公司资料';
$lang['employee_3_data_1'] = '职称';
$lang['employee_3_data_2'] = '推荐人';
$lang['employee_3_data_3'] = '直属经理';

//	3.客戶管理
$lang['customers'] = '客户管理';
$lang['customers_list'] = '客户列表';

//	3.1.1 搜尋列
$lang['customers_keyword'] = '请输入关键词 / 客户姓名';

//	3.1.2 客戶列表
$lang['customers_job'] = '职称';
//	3.2 编辑 / 新增客户
$lang['customers_new'] = '新增客户';
$lang['customers_edit'] = '编辑客户';
//	3.2.1 编辑 / 新增客户
$lang['customers_1_data']    = '客户基本数据';
$lang['customers_1_data_1']  = '姓名';
$lang['customers_1_data_2']  = '性别';
$lang['customers_1_data_3']  = '男';
$lang['customers_1_data_4']  = '女';
$lang['customers_1_data_5']  = '出生年月日';
$lang['customers_1_data_6']  = '身分证字号';
$lang['customers_1_data_7']  = '户籍地址';
$lang['customers_1_data_8']  = '通讯地址';
$lang['customers_1_data_9']  = '电子信箱';
$lang['customers_1_data_10'] = 'WeChat ID';
//	3.2.2 客户帐户资料
$lang['customers_2_data']    = '客户帐户资料';
$lang['customers_2_data_1']  = '公司电话';
$lang['customers_2_data_2']  = '住宅电话';
$lang['customers_2_data_3']  = '手机';
$lang['customers_2_data_4'] = '银行/邮局';
$lang['customers_2_data_5'] = '银行';
$lang['customers_2_data_6'] = '邮局';
$lang['customers_2_data_7']  = '汇款银行';
$lang['customers_2_data_8']  = '分行名称';
$lang['customers_2_data_9']  = '汇款账号';
$lang['customers_2_data_10']  = '户名';
$lang['customers_2_data_11']  = '备注';

//	3.3 利得发放
$lang['interest_list'] = '利得发放列表';
//	3.3.1
$lang['interest_keyword'] = '请输入关键词 / 合约书编号';
//	3.3.2
$lang['interest_1'] = '合约书编号';
$lang['interest_2'] = '生效日';
$lang['interest_3'] = '客户姓名';
$lang['interest_4'] = '业务承揽人';
$lang['interest_5'] = '奖金种类';

//	3.4	股权代售
$lang['stocksold'] = '股权代售';
// 	3.4.1
$lang['stocksold_1'] = '业务姓名';
$lang['stocksold_2'] = '客户姓名';
$lang['stocksold_3'] = '营业处';
$lang['stocksold_4'] = '请输入关键词 / 合约书编号';
$lang['stocksold_5'] = '每股价格';
$lang['stocksold_6'] = '年';
$lang['stocksold_7'] = '月';
//	3.4.2
$lang['stocksold_8'] = '每股价格';

//	4.商品管理
$lang['goods'] = '商品管理';
//	4.1
$lang['goods_list'] = '商品列表';
//	4.1.1
$lang['goods_keyword'] = '请输入关键词 / 商品名称';
//	4.1.2
$lang['goods_new'] = '新增商品';
//	4.2
$lang['goods_1'] = '商品名称';
$lang['goods_2'] = '商品单价';
$lang['goods_3'] = '业绩奖金PV';

$lang['goods_4'] = '股价维护';
$lang['goods_5'] = '请输入关键词 / 商品名称';
$lang['goods_6'] = '起算日期';

$lang['goods_7'] = '商品名称';
$lang['goods_8'] = '股价';
$lang['goods_9'] = '每单位股数';
$lang['goods_10'] = '起算日期';
$lang['goods_11'] = '结束日期';

//	訂單管理
$lang['order'] = '订单管理';
$lang['order_list'] = '订单列表';
$lang['order_1'] = '业务姓名';
$lang['order_2'] = '客户姓名';
$lang['order_3'] = '营业处';
$lang['order_4'] = '年';
$lang['order_5'] = '月';
$lang['order_6'] = '请输入关键词 / 合约书编号';

$lang['order_new'] = '新增订单';
$lang['order_1'] = '订单编号';
$lang['order_2'] = '合约书编号';
$lang['order_3'] = '营业处';
$lang['order_4'] = '客户姓名';
$lang['order_5'] = '商品信息';
$lang['order_6'] = '增加订单商品';
$lang['order_7'] = '商品项目';
$lang['order_8'] = '承购单位';
$lang['order_9'] = '规格';
$lang['order_10'] = '合计金额';
$lang['order_11'] = '承购股数';
$lang['order_12'] = '经办人';
$lang['order_13'] = '付款方式';
$lang['order_14'] = '现金';
$lang['order_15'] = '转账';
$lang['order_16'] = '承购日期';
$lang['order_17'] = '合约生效日';
$lang['order_18'] = '合约到期日';
$lang['order_19'] = '合约签收日';

//	組織管理
$lang['organization'] = '组织管理';
$lang['organization_unit'] = '营业处管理';
$lang['organization_keyword'] = '请输入关键词 / 组织名称';
$lang['organization_list'] = '营业处列表';
$lang['organization_1'] = '营业处名称';
$lang['organization_2'] = '连络电话';

$lang['organization_3'] = '营业处名称';
$lang['organization_4'] = '副总经理';
$lang['organization_5'] = '母单位';
$lang['organization_6'] = '推荐人';
$lang['organization_7'] = '地址';
$lang['organization_8'] = '电话';
$lang['organization_9'] = '传真';

$lang['occupation'] = '职级管理';
$lang['occupation_keyword'] = '请输入关键词 / 职级名称';
$lang['occupation_list'] = '职级列表';
$lang['occupation_1'] = '职级名称';

//	獎金核發管理
$lang['bouns'] = '奖金核发管理';
$lang['bouns_1'] = '业务姓名';
$lang['bouns_2'] = '营业处';
$lang['bouns_3'] = '请输入关键词 / 业务姓名';
$lang['bouns_4'] = '奖金核发管理列表';
$lang['bouns_5'] = '所属单位';
$lang['bouns_6'] = '职称';
$lang['bouns_7'] = '本月PV';

$lang['bouns_1_1'] = '业绩奖金';
$lang['bouns_1_2'] = '员工信息';
$lang['bouns_1_3'] = '员工姓名';
$lang['bouns_1_4'] ='所属单位';
$lang['bouns_1_5'] ='职称';
$lang['bouns_1_6'] ='未结算个人PV';
$lang['bouns_1_7'] ='未发放津贴';
$lang['bouns_1_8'] ='推荐人';
$lang['bouns_1_9'] ='直属长官';

$lang['bouns_1_10'] = '业绩一览';
$lang['bouns_1_11'] = '合约书编号';
$lang['bouns_1_12'] = '付款状态';
$lang['bouns_1_13'] = '确认审核';
$lang['bouns_1_14'] = '金额';

$lang['bouns_2_1'] = '服务津贴';
$lang['bouns_2_2'] = '员工信息';
$lang['bouns_2_3'] = '员工姓名';
$lang['bouns_2_4'] = '所属单位';
$lang['bouns_2_5'] = '职称';
$lang['bouns_2_6'] = '当月PV';
$lang['bouns_2_7'] = '本月未发放服务津贴';
$lang['bouns_2_8'] = '推荐人';
$lang['bouns_2_9'] = '直属长官';

$lang['bouns_2_10'] = '业务承揽人当月订单信息';
$lang['bouns_2_11'] = '合约书编号';
$lang['bouns_2_12'] = '付款状态';
$lang['bouns_2_13'] = '确认审核';
$lang['bouns_2_14'] = '金额';

$lang['bouns_3_1'] = '差额奖金';
$lang['bouns_3_2'] = '员工信息';
$lang['bouns_3_3'] = '员工姓名';
$lang['bouns_3_4'] = '所属单位';
$lang['bouns_3_5'] = '职称';
$lang['bouns_3_6'] = '未结算个人PV';
$lang['bouns_3_7'] = '未发放差额奖金';
$lang['bouns_3_8'] = '推荐人';
$lang['bouns_3_9'] = '直属长官';
$lang['bouns_3_10'] = '业务承揽人订单信息';
$lang['bouns_3_11'] = '合约书编号';
$lang['bouns_3_12'] = '付款状态';
$lang['bouns_3_13'] = '确认审核';
$lang['bouns_3_14'] = '金额';

$lang['bouns_4_1'] = '同阶奖金';
$lang['bouns_4_2'] = '员工信息';
$lang['bouns_4_3'] = '员工姓名';
$lang['bouns_4_4'] = '所属单位';
$lang['bouns_4_5'] = '职称';
$lang['bouns_4_6'] = '个人PV';
$lang['bouns_4_7'] = '同阶奖金';
$lang['bouns_4_8'] = '推荐人';
$lang['bouns_4_9'] = '直属长官';
$lang['bouns_4_10'] = '业务承揽人订单信息';
$lang['bouns_4_11'] = '合约书编号';
$lang['bouns_4_12'] = '付款状态';
$lang['bouns_4_13'] = '确认审核';
$lang['bouns_4_14'] = '金额';

$lang['bouns_4_15'] = '请输入关键词 / 营业处名称';
$lang['bouns_4_16'] = '营业处名称';
$lang['bouns_4_17'] = '连络电话';
$lang['bouns_4_18'] = '本月PV';

$lang['bouns_5_1'] = '全国分红';
$lang['bouns_5_2'] = '营业处信息';
$lang['bouns_5_3'] = '营业处名称';
$lang['bouns_5_4'] = '负责人';
$lang['bouns_5_5'] = '职称';
$lang['bouns_5_6'] = '本月营业处PV';
$lang['bouns_5_7'] = '推荐人';
$lang['bouns_5_8'] = '母单位';
$lang['bouns_5_9'] = '当月津贴';
$lang['bouns_5_10'] = '全国当月符合营业处单数';
$lang['bouns_5_11'] = '全国当月总PV';
$lang['bouns_5_12'] = '营业处当月单数';

$lang['bouns_6_1'] = '行政津贴';
$lang['bouns_6_2'] = '营业处信息';
$lang['bouns_6_3'] = '营业处名称';
$lang['bouns_6_4'] = '负责人';
$lang['bouns_6_5'] = '职称';
$lang['bouns_6_6'] = '本月营业处PV';
$lang['bouns_6_7'] = '推荐人';
$lang['bouns_6_8'] = '母单位';
$lang['bouns_6_9'] = '行政津贴';
$lang['bouns_6_10'] = '本月津贴';
$lang['bouns_6_11'] = '本月营业处营业额';
$lang['bouns_6_12'] = '本月营业处单数';
$lang['bouns_6_13'] = '衍生营业处津贴';
$lang['bouns_6_14'] = '营业处信息';
$lang['bouns_6_15'] = '营业处名称';
$lang['bouns_6_16'] = '负责人';
$lang['bouns_6_17'] = '职称';
$lang['bouns_6_18'] = '本月营业处PV';
$lang['bouns_6_19'] = '推荐人';
$lang['bouns_6_20'] = '母单位';
$lang['bouns_6_21'] = '衍生营业处津贴';

//	獎金報表管理	
$lang['bouns_export'] = '奖金报表管理';
$lang['bouns_export_1'] = '营业处总奖金';

$lang['bouns_export_2'] = '请输入关键词 / 营业处名称';
$lang['bouns_export_3'] = '营业处名称';
$lang['bouns_export_4'] = '连络电话';
$lang['bouns_export_5'] = '本月PV';
$lang['bouns_export_6'] = '营业处信息';
$lang['bouns_export_7'] = '营业处名称';
$lang['bouns_export_8'] = '负责人';
$lang['bouns_export_9'] = '职称';
$lang['bouns_export_10'] = '总单位';
$lang['bouns_export_11'] = '业绩奖金';
$lang['bouns_export_12'] = '差额奖金';
$lang['bouns_export_13'] = '同阶奖金';
$lang['bouns_export_14'] = '全国分红';
$lang['bouns_export_15'] = '衍生营业处津贴';
$lang['bouns_export_16'] = '服务津贴';
$lang['bouns_export_17'] = '行政津贴';
$lang['bouns_export_18'] = '总计';
$lang['bouns_export_19'] = '业务人员奖金';

$lang['bouns_export_20'] = '业务姓名';
$lang['bouns_export_21'] = '营业处';
$lang['bouns_export_22'] = '请输入关键词 / 业务姓名';
$lang['bouns_export_23'] = '本月PV';
$lang['bouns_export_24'] = '员工信息';
$lang['bouns_export_25'] = '员工姓名';
$lang['bouns_export_26'] = '所属营业处';
$lang['bouns_export_27'] = '总单位';
$lang['bouns_export_28'] = '业绩奖金';
$lang['bouns_export_29'] = '差额奖金';
$lang['bouns_export_30'] = '同阶奖金';
$lang['bouns_export_31'] = '服务津贴';
$lang['bouns_export_32'] = '总计';
$lang['bouns_export_33'] = '其他加项';
$lang['bouns_export_34'] = '说明';
$lang['bouns_export_35'] = '其他扣项';
$lang['bouns_export_36'] = '说明';

$lang['account'] = '账号管理';
$lang['account_list'] = '用户列表';
$lang['account_bar'] = '搜寻列';
$lang['account_keyword'] = '搜寻关键词';
$lang['account_1'] = '请输入关键词 / 用户名称';
$lang['account_2'] = '用户列表';
$lang['account_3'] = '密码变更';
$lang['account_4'] = '用户名称';
$lang['account_5'] = '用户账号';
$lang['account_6'] = '新用户密码';
$lang['account_7'] = '再次确认密码';
$lang['account_8'] = '注销';

