		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">    
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3 class="page-title">
							<?php echo $title; ?> <small><?php echo $title_small; ?></small>
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="index.html">首頁</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li><a href="#"><?php echo $title_small; ?></a></li>
							<li class="pull-right no-text-shadow" style="display: none;">
								<div id="dashboard-report-range" class="dashboard-date-range tooltips no-tooltip-on-touch-device responsive" data-tablet="" data-desktop="tooltips" data-placement="top" data-original-title="Change dashboard date range">
									<i class="icon-calendar"></i>
									<span></span>
									<i class="icon-angle-down"></i>
								</div>
							</li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN VALIDATION STATES-->
						<div class="portlet box green">
							<div class="portlet-title">
								<div class="caption"><i class="icon-reorder"></i><?php echo $title_small; ?></div>
								<div class="tools">
									<a href="javascript:;" class="collapse"></a>
									<!-- <a href="#portlet-config" data-toggle="modal" class="config"></a>
									<a href="javascript:;" class="reload"></a>
									<a href="javascript:;" class="remove"></a> -->
								</div>
							</div>
							<div class="portlet-body form">
								<!-- BEGIN FORM-->
								<h3></h3>
								<?php foreach($latestid as $r){$id = $r['maxid']+1;?>
								<form action="<?php echo base_url('customers/create?id=').$id;?>" class="form-horizontal"  method="post" name="submitform" id="submitform">
								<?php }?>
								<div class="alert alert-error hide">
									<button class="close" data-dismiss="alert"></button>
									You have some form errors. Please check below.
								</div>
								<div class="alert alert-success hide">
									<button class="close" data-dismiss="alert"></button>
									Your form validation is successful!
								</div>
								<div class="control-group">
									<label class="control-label">姓名<span class="required">*</span></label>
									<div class="controls">
										<input type="text" id="name" name="name"  data-required="1" class="span6 m-wrap"/>
										<span id="namesMsg"></span>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">性別<span class="required">*</span></label>
									<div class="controls">
										<label class="radio">
											<input type="radio" name="optionsRadios1" value="1" />男
										</label>
										<label class="radio">
											<input type="radio" name="optionsRadios1" value="2" />女
										</label>  
											<!-- <select class="span6 m-wrap" name="sex" id="sex">
												<?php foreach($result as $row){?>
													<option value="1" <?php if($row['customer_sex']==1){echo "selected='selected'";} ?>>男</option>
													<option value="2" <?php if($row['customer_sex']==2){echo "selected='selected'";} ?>>女</option>
												<?php } ?>
											</select> -->
											<span id="sex"></span>
										</div>
									</div>
									<div class="control-group">

										<label class="control-label">出生年月日</label>

										<div class="controls">

											<div class="input-append date date-picker"  data-date-format="yyyy-mm-dd" data-date-viewmode="years">

												<input class="m-wrap m-ctrl-medium date-picker" name="birthday" id="birthday" readonly size="16" type="text"/><span class="add-on"><i class="icon-calendar"></i></span><span id="birthdayMsg"></span>

											</div>

										</div>

									</div>

									<div class="control-group">
										<label class="control-label">身分證字號<span class="required">*</span></label>
										<div class="controls">
											<input name="idcard" id="idcard" type="text" class="span6 m-wrap" maxlength="10"/><span id="idMsg"></span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">戶籍地址<span class="required">*</span></label>
										<div class="controls">
											<select class="span6 m-wrap" name="permanentaddress" style="width: auto;">
												<?php foreach ($result as $r) { ?>
													<option value="<?php echo $r["address_id"];?>"><?php echo $r["address_zip"].$r["address_city"].$r["address_area"];?></option>
												<?php }?>
											</select>
											<input name="paddressdetail" id="paddressdetail" type="text" class="span6 m-wrap" style="width: 30%;"/>
											<span id="paddressMsg"></span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">通訊地址<span class="required">*</span></label>
										<div class="controls">
											<select class="span6 m-wrap" name="residentialaddress" style="width: auto;">
												<?php foreach ($result as $r) { ?>
													<option value="<?php echo $r["address_id"];?>"><?php echo $r["address_zip"].$r["address_city"].$r["address_area"];?></option>
												<?php }?>
											</select>
											<input name="raddressdetail" id="raddressdetail" type="text" class="span6 m-wrap" style="width: 30%;"/>
											<span id="raddressMsg"></span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">電子信箱<span class="required">*</span></label>
										<div class="controls">
											<input name="email" id="email" type="text" class="span6 m-wrap"/>
											<span id="emailMsg"></span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Line ID<span class="required">*</span></label>
										<div class="controls">
											<input name="line" id="line" type="text" class="span6 m-wrap"/>
											<span id="lineMsg"></span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">公司電話<span class="required">*</span></label>
										<div class="controls">
											<input name="ctelarea" id="ctelarea" type="text" class="span1 m-wrap" value=""/>
											<input name="companytel" id="companytel" type="text" class="span5 m-wrap"/>
											<span id="ctelephoneMsg"></span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">住宅電話<span class="required">*</span></label>
										<div class="controls">
											<input name="rtelarea" id="rtelarea" type="text" class="span1 m-wrap" value=""/>
											<input name="residentialtel" id="residentialtel" type="text" class="span5 m-wrap"/>
											<span id="rtelephoneMsg"></span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">手機<span class="required">*</span></label>
										<div class="controls">
											<input name="cellphone" id="cellphone" type="text" class="span6 m-wrap" maxlength="10" />
											<span id="cellphoneMsg"></span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">匯款銀行<span class="required">*</span></label>
										<div class="controls">
											<input name="bank" id="bank" type="text" class="span6 m-wrap" maxlength="10" />
											<span id="bankMsg"></span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">分行代號<span class="required">*</span></label>
										<div class="controls">
											<input name="branch" id="branch" type="text" class="span6 m-wrap" maxlength="10" />
											<span id="branchMsg"></span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">匯款帳號<span class="required">*</span></label>
										<div class="controls">
											<input name="account" id="account" type="text" class="span6 m-wrap" maxlength="10" />
											<span id="accountMsg"></span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">戶名<span class="required">*</span></label>
										<div class="controls">
											<input name="accountname" id="accountname" type="text" class="span6 m-wrap" maxlength="10" />
											<span id="accountnameMsg"></span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">備註<span class="required">*</span></label>
										<div class="controls">
											<input name="note" id="note" type="text" class="span6 m-wrap" maxlength="10" />
											<span id="noteMsg"></span>
										</div>
									</div>
									<div class="form-actions">
										<button type="submit" class="btn green" onclick="check()">
											新增 <div class="icon-save"></div>
										</button>
										<a href="<?php echo base_url('customers');?>">
											<button type="button" class="btn">取消 <div class="icon-undo"></div></button>
										</a>
									</div>
								</form>
								<!-- END FORM-->
							</div>
						</div>
						<!-- END VALIDATION STATES-->
					</div>
				</div>
				<!-- END DASHBOARD STATS -->
				<div class="clearfix"></div>
			</div>
		</div>
		<!-- END PAGE CONTAINER-->    
	</div>
	<!-- END PAGE -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<div class="footer">
	<?php require_once(dirname(__FILE__).'/../common/footer.php'); ?>
</div>
<!-- END FOOTER -->
<?php require_once(dirname(__FILE__).'/../common/script.php'); ?>
<!-- BEGIN RULE -->
<!-- <?php require_once(dirname(__FILE__).'/../customers/rule.php'); ?> -->
<!-- END RULE -->
</body>
<!-- END BODY -->
</html>