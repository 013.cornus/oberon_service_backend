		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">    
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3 class="page-title">
							<?php echo $title; ?> <small><?php echo $title_small; ?></small>
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="<?php echo base_url('home'); ?>">首頁</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li><a href="<?php echo $this->router->fetch_class();?>"><?php echo $title_small; ?></a></li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<div id="dashboard">
					<!-- BEGIN ALTER -->
					<?php echo $this->session->flashdata('messagediv');?>
					<!-- END ALTER -->
					<!-- BEGIN DASHBOARD STATS -->
					<!-- BEGIN PAGE CONTENT-->
					<div class="row-fluid">
						<div class="span12">
							<div class="portlet box blue" id="form_wizard_1">
							</div>
						</div>
					</div>
					<!-- END PAGE CONTENT-->
					<div class="row-fluid">

						<div class="span12">
							<!-- BEGIN EXAMPLE TABLE PORTLET-->
							<div class="portlet box light-grey">
								<div class="portlet-title">
									<div class="caption"><i class="icon-globe"></i><?php echo $title_small; ?></div>
									<div class="tools">
										<a href="javascript:;" class="collapse"></a>
										<a href="#portlet-config" data-toggle="modal" class="config" style="display: none;"></a>
										<a href="javascript:;" class="reload" style="display: none;"></a>
										<a href="javascript:;" class="remove" style="display: none;"></a>
									</div>
								</div>
								<div class="portlet-body">
									<div class="clearfix">
										<div class="btn-group">
												<button id="" class="btn green" onclick="batch_create()">
													批次產生
												</button>
										</div>
									</div>
									<div style="overflow:scroll; overflow:auto;">
										<table class="table table-striped table-bordered table-hover" id="sample_1" style="min-width: 768px;">
											<thead>
												<tr>
													<th style="width:8px;">
														<input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" />
													</th>
													<th>項次</th>
													<th>受測者</th>
													<th>受測日期</th>
													<th>動作</th>
												</tr>
											</thead>
											<tbody>
												<?php $itemNum = 0; foreach ($result as $key => $row) {?>
													<tr class="odd">
														<td><input type="checkbox" name="case_id[]" value="<?php echo $row['case']?>"/></td>
														<td><?php $itemNum++; echo $itemNum;?></td>
														<td><?php echo $row['name'];?></td>
														<td><?php echo date('Y-m-d H:i:s',$row['date']);?></td>
														<td>
															<a href="<?php echo $link[$key];?>" target='_blank' class="btn green mini">產生報告
															</a>
														</td>
													</tr>
												<?php } ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>

							<!-- 警告視窗 -->
							<!-- BEGIN CONFIRM WINDOW -->
							<div id="confirm_window" class="modal hide fade " role="dialog" data-keyboard="false" style="top:20%">
								<div class="modal-header red">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
									<h4><div class="icon-warning-sign"></div> 警告</h4>
								</div>
								<form>
									<div class="modal-body">
										<p id='confirm_text'>警告文字</p>
									</div>
									<div class="modal-footer">
										<button type="button" data-dismiss="modal" class="btn">取消 <div class="icon-undo"></div></button>
										<button id="confirm_btn" type="button" data-dismiss="modal" class="btn red" >確認按鈕<div class="icon-check"></div></button>
									</div>
								</form>
							</div>
							<!-- END CONFIRM WINDOW -->


							<!-- END EXAMPLE TABLE PORTLET-->
						</div>
					</div>
					<!-- END DASHBOARD STATS -->
					<div class="clearfix"></div>
				</div>
			</div>
			<!-- END PAGE CONTAINER-->    
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
	<div class="footer">
		<?php echo $footer; ?>
	</div>

	<!-- END FOOTER -->
	<?php echo $script; ?>
</body>
<script type="text/javascript">
	//$link為欲前往連結
	//$confirm_text為警告視窗文字
	//$btn_text為確認按鈕文字
	function alert_window($link,$confirm_text,$btn_text){
		$("#confirm_text").text($confirm_text);
		$("#confirm_btn").text($btn_text);
		$("#confirm_btn").attr('onclick',"javascript:location.href='"+$link+"'");
	}

	function batch_create(){
		var case_list = new Array();
		$('input:checkbox:checked[name="case_id[]"]').each(function(i) { 
			case_list[i] = this.value;
		});
		window.location = 'http://34.80.127.28/bms_backend/report_create/batch_create?case_list=' + case_list;
	}
</script>
<!-- END BODY -->
</html>