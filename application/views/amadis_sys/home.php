		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3 class="page-title">
							<?php echo $title; ?> <small><?php echo $title_small; ?></small>
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="<?php echo base_url('home'); ?>"><?php echo $title; ?></a> 
								<i class="icon-angle-right"></i>
							</li>
							<li><a href="#"><?php echo $title_small; ?></a></li>
							<li class="pull-right no-text-shadow" style="display: none;">
								<div id="dashboard-report-range" class="dashboard-date-range tooltips no-tooltip-on-touch-device responsive" data-tablet="" data-desktop="tooltips" data-placement="top" data-original-title="Change dashboard date range">
									<i class="icon-calendar"></i>
									<span></span>
									<i class="icon-angle-down"></i>
								</div>
							</li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<div id="dashboard">
					<!-- BEGIN ALTER -->
					<?php echo $this->session->flashdata('success_msg');?>
					<?php echo $this->session->flashdata('messagediv');?>
					<?php echo $alert;?>
					<!-- END ALTER -->
					<!-- BEGIN DASHBOARD STATS -->
					<div class="row-fluid">
						<canvas id="myChart"></canvas>
						<div class="span3 responsive" data-tablet="span6" data-desktop="span3" <?php echo $detectionTotalView;?> >
							<div class="dashboard-stat purple">
								<div class="visual">
									<i class="icon-globe"></i>
								</div>
								<div class="details">
									<div class="number" ><?php echo $detectionNumber;?></div>
									<div class="desc">總檢測樣本</div>
								</div>
								<a class="more" href="<?php echo $link0?>">
									瀏覽報告 <i class="m-icon-swapright m-icon-white"></i>
								</a>                
							</div>
						</div>
						<?php foreach($typeList as $key => $row){?>
						<div class="span3 responsive" data-tablet="span6" data-desktop="span3">
							<div class="dashboard-stat purple">
								<div class="visual">
									<i class="icon-globe"></i>
								</div>
								<div class="details">
									<div class="number" ><?php echo $groupCaseNum[$key];?></div>
									<div class="desc"><?php echo $row['group_type_name'].'檢測數';?></div>
								</div>
								<a class="more" href="<?php echo ${'link'.$row['group_type_id']}?>">
									瀏覽報告 <i class="m-icon-swapright m-icon-white"></i>
								</a>               
							</div>
						</div><?php }?>
						<div class="span3 responsive" data-tablet="span6" data-desktop="span3">
							<div class="dashboard-stat green">
								<div class="visual">
									<i class="icon-globe"></i>
								</div>
								<div class="details">
									<div class="number" ><?php echo $users_num;?></div>
									<div class="desc">總使用者數量</div>
								</div>
								<a class="more" href="javascript:;">
									檢視使用者 <i class="m-icon-swapright m-icon-white"></i>
								</a>                
							</div>
						</div>
					</div>
					<!-- END DASHBOARD STATS -->
					<div class="clearfix"></div>
					<div class="row-fluid hidden">
						<div class="span6">
							<!-- BEGIN PORTLET-->
							<div class="portlet solid bordered light-grey">
								<div class="portlet-title">
									<div class="caption"><i class="icon-bar-chart"></i>本月業績總覽</div>
									<div class="tools">
										<div class="btn-group pull-right" data-toggle="buttons-radio">
											<a href="" class="btn mini">Users</a>
											<a href="" class="btn mini active">Feedbacks</a>
										</div>
									</div>
								</div>
								<div class="portlet-body">
									<div id="site_statistics_loading">
										<img src="./public/media/image/loading.gif" alt="loading" />
									</div>
									<div id="site_statistics_content" class="hide">

										<div id="site_statistics" class="chart"></div>
									</div>
								</div>
							</div>
							<!-- END PORTLET-->
						</div>
						<div class="span6 hidden">
							<!-- BEGIN PORTLET-->
							<div class="portlet paddingless">
								<div class="portlet-title line">
									<div class="caption"><i class="icon-bell"></i>待辦事項</div>
									<div class="tools">
										<a href="" class="collapse"></a>
										<!-- <a href="#portlet-config" data-toggle="modal" class="config"></a>
										<a href="" class="reload"></a>
										<a href="" class="remove"></a> -->
									</div>
								</div>
								<div class="portlet-body">
									<!--BEGIN TABS-->
									<div class="tabbable tabbable-custom">
										<ul class="nav nav-tabs">
											<li class="active"><a href="#tab_1_1" data-toggle="tab">系統日誌</a></li>
											<li><a href="#tab_1_2" data-toggle="tab">最新訂單</a></li>
											<!-- <li><a href="#tab_1_3" data-toggle="tab">客戶備忘錄</a></li> -->
										</ul>
										<div class="tab-content">
											<div class="tab-pane active" id="tab_1_1">
												<div class="scroller" data-height="290px" data-always-visible="1" data-rail-visible="0">
													<ul class="feeds">
														<li>
															<div class="col1">
																<div class="cont">
																	<div class="cont-col1">
																		<div class="label label-success">           
																			<i class="icon-bell"></i>
																		</div>
																	</div>
																	<div class="cont-col2">
																		<div class="desc">
																			You have 4 pending tasks.
																			<span class="label label-important label-mini">
																				Take action 
																				<i class="icon-share-alt"></i>
																			</span>
																		</div>
																	</div>
																</div>
															</div>
															<div class="col2">
																<div class="date">
																	Just now
																</div>
															</div>
														</li>
														<li>
															<a href="#">
																<div class="col1">
																	<div class="cont">
																		<div class="cont-col1">
																			<div class="label label-success">
																				<i class="icon-bell"></i>
																			</div>
																		</div>
																		<div class="cont-col2">
																			<div class="desc">
																				New version v1.4 just lunched!   
																			</div>
																		</div>
																	</div>
																</div>
																<div class="col2">
																	<div class="date">
																		20 mins
																	</div>
																</div>
															</a>
														</li>
														<li>
															<div class="col1">
																<div class="cont">
																	<div class="cont-col1">
																		<div class="label label-important">
																			<i class="icon-bolt"></i>
																		</div>
																	</div>
																	<div class="cont-col2">
																		<div class="desc">
																			Database server #12 overloaded. Please fix the issue.
																		</div>
																	</div>
																</div>
															</div>
															<div class="col2">
																<div class="date">
																	24 mins
																</div>
															</div>
														</li>
														<li>
															<div class="col1">
																<div class="cont">
																	<div class="cont-col1">
																		<div class="label label-info">                    
																			<i class="icon-bullhorn"></i>
																		</div>
																	</div>
																	<div class="cont-col2">
																		<div class="desc">
																			New order received. Please take care of it.                 
																		</div>
																	</div>
																</div>
															</div>
															<div class="col2">
																<div class="date">
																	30 mins
																</div>

															</div>
														</li>
														<li>
															<div class="col1">
																<div class="cont">
																	<div class="cont-col1">
																		<div class="label label-success">                       

																			<i class="icon-bullhorn"></i>
																		</div>
																	</div>
																	<div class="cont-col2">
																		<div class="desc">
																			New order received. Please take care of it.                 
																		</div>
																	</div>
																</div>
															</div>
															<div class="col2">
																<div class="date">
																	40 mins
																</div>
															</div>
														</li>
														<li>
															<div class="col1">
																<div class="cont">
																	<div class="cont-col1">
																		<div class="label label-warning">
																			<i class="icon-plus"></i>
																		</div>
																	</div>
																	<div class="cont-col2">
																		<div class="desc">
																			New user registered.           
																		</div>
																	</div>
																</div>
															</div>
															<div class="col2">
																<div class="date">
																	1.5 hours
																</div>
															</div>
														</li>
														<li>
															<div class="col1">
																<div class="cont">
																	<div class="cont-col1">
																		<div class="label label-success">
																			<i class="icon-bell-alt"></i>
																		</div>
																	</div>
																	<div class="cont-col2">
																		<div class="desc">
																			Web server hardware needs to be upgraded. 
																			<span class="label label-inverse label-mini">Overdue</span>             
																		</div>
																	</div>
																</div>
															</div>
															<div class="col2">
																<div class="date">
																	2 hours
																</div>
															</div>
														</li>
														<li>
															<div class="col1">
																<div class="cont">
																	<div class="cont-col1">
																		<div class="label">                   
																			<i class="icon-bullhorn"></i>
																		</div>
																	</div>
																	<div class="cont-col2">
																		<div class="desc">
																			New order received. Please take care of it.                 
																		</div>
																	</div>
																</div>
															</div>
															<div class="col2">
																<div class="date">
																	3 hours
																</div>
															</div>
														</li>
														<li>
															<div class="col1">
																<div class="cont">
																	<div class="cont-col1">
																		<div class="label label-warning">
																			<i class="icon-bullhorn"></i>
																		</div>
																	</div>
																	<div class="cont-col2">
																		<div class="desc">
																			New order received. Please take care of it.                 
																		</div>
																	</div>
																</div>
															</div>
															<div class="col2">
																<div class="date">
																	5 hours
																</div>
															</div>
														</li>
														<li>
															<div class="col1">
																<div class="cont">
																	<div class="cont-col1">
																		<div class="label label-info">
																			<i class="icon-bullhorn"></i>
																		</div>
																	</div>
																	<div class="cont-col2">
																		<div class="desc">
																			New order received. Please take care of it.                 
																		</div>
																	</div>
																</div>
															</div>
															<div class="col2">
																<div class="date">
																	18 hours
																</div>
															</div>
														</li>
													</ul>
												</div>
											</div>
											<div class="tab-pane" id="tab_1_2">
												<div class="scroller" data-height="290px" data-always-visible="1" data-rail-visible1="1">
													<ul class="feeds">
														<li>
															<a href="#">
																<div class="col1">
																	<div class="cont">
																		<div class="cont-col1">
																			<div class="label label-success">
																				<i class="icon-bell"></i>
																			</div>
																		</div>
																		<div class="cont-col2">
																			<div class="desc">
																				New user registered
																			</div>
																		</div>
																	</div>
																</div>
																<div class="col2">
																	<div class="date">
																		Just now
																	</div>
																</div>
															</a>
														</li>
														<li>
															<a href="#">

																<div class="col1">

																	<div class="cont">

																		<div class="cont-col1">

																			<div class="label label-success">
																				<i class="icon-bell"></i>
																			</div>
																		</div>
																		<div class="cont-col2">
																			<div class="desc">
																				New order received 
																			</div>
																		</div>
																	</div>
																</div>
																<div class="col2">
																	<div class="date">
																		10 mins
																	</div>
																</div>
															</a>
														</li>
														<li>
															<div class="col1">
																<div class="cont">
																	<div class="cont-col1">
																		<div class="label label-important">
																			<i class="icon-bolt"></i>
																		</div>
																	</div>
																	<div class="cont-col2">
																		<div class="desc">
																			Order #24DOP4 has been rejected.    
																			<span class="label label-important label-mini">Take action <i class="icon-share-alt"></i></span>
																		</div>
																	</div>
																</div>
															</div>
															<div class="col2">
																<div class="date">
																	24 mins
																</div>
															</div>
														</li>
														<li>
															<a href="#">
																<div class="col1">
																	<div class="cont">
																		<div class="cont-col1">
																			<div class="label label-success">
																				<i class="icon-bell"></i>
																			</div>
																		</div>
																		<div class="cont-col2">
																			<div class="desc">
																				New user registered
																			</div>
																		</div>
																	</div>
																</div>
																<div class="col2">
																	<div class="date">
																		Just now
																	</div>
																</div>
															</a>
														</li>
														<li>
															<a href="#">
																<div class="col1">
																	<div class="cont">
																		<div class="cont-col1">
																			<div class="label label-success">                       

																				<i class="icon-bell"></i>
																			</div>
																		</div>
																		<div class="cont-col2">
																			<div class="desc">
																				New user registered
																			</div>
																		</div>
																	</div>
																</div>
																<div class="col2">
																	<div class="date">
																		Just now
																	</div>
																</div>
															</a>
														</li>
														<li>
															<a href="#">
																<div class="col1">
																	<div class="cont">
																		<div class="cont-col1">
																			<div class="label label-success">
																				<i class="icon-bell"></i>
																			</div>
																		</div>
																		<div class="cont-col2">
																			<div class="desc">
																				New user registered
																			</div>
																		</div>
																	</div>
																</div>
																<div class="col2">
																	<div class="date">
																		Just now
																	</div>
																</div>
															</a>
														</li>
														<li>
															<a href="#">
																<div class="col1">
																	<div class="cont">
																		<div class="cont-col1">
																			<div class="label label-success">
																				<i class="icon-bell"></i>
																			</div>
																		</div>
																		<div class="cont-col2">
																			<div class="desc">
																				New user registered
																			</div>
																		</div>
																	</div>
																</div>
																<div class="col2">
																	<div class="date">
																		Just now
																	</div>
																</div>
															</a>
														</li>
													</ul>
												</div>
											</div>
											<div class="tab-pane" id="tab_1_3">
												<div class="scroller" data-height="290px" data-always-visible="1" data-rail-visible1="1">
													<div class="row-fluid">
														<div class="span6 user-info">
															<img alt="" src="./public/media/image/avatar.png" />
															<div class="details">
																<div>
																	<a href="#">Robert Nilson</a>
																	<span class="label label-success">Approved</span>
																</div>
																<div>29 Jan 2013 10:45AM</div>
															</div>
														</div>
														<div class="span6 user-info">
															<img alt="" src="./public/media/image/avatar.png" />
															<div class="details">
																<div>
																	<a href="#">Lisa Miller</a> 
																	<span class="label label-info">Pending</span>
																</div>
																<div>19 Jan 2013 10:45AM</div>
															</div>
														</div>
													</div>
													<div class="row-fluid">
														<div class="span6 user-info">
															<img alt="" src="./public/media/image/avatar.png" />
															<div class="details">
																<div>
																	<a href="#">Eric Kim</a> 
																	<span class="label label-info">Pending</span>
																</div>
																<div>19 Jan 2013 12:45PM</div>
															</div>
														</div>
														<div class="span6 user-info">
															<img alt="" src="./public/media/image/avatar.png" />
															<div class="details">
																<div>
																	<a href="#">Lisa Miller</a> 
																	<span class="label label-important">In progress</span>
																</div>
																<div>19 Jan 2013 11:55PM</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<!--END TABS-->
								</div>
							</div>
							<!-- END PORTLET-->
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<!-- END PAGE CONTAINER-->    
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
	<div class="footer">
		<?php echo $footer; ?>
	</div>
	<!-- END FOOTER -->
	<?php echo $script; ?>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.js"></script>
	<script type="text/javascript">
		var ctx = document.getElementById('myChart').getContext('2d');
		var chart = new Chart(ctx, {
			type: 'bar',

			data: {
				labels: ['1'+'('+<?php echo $status_1;?>+')', '2'+'('+<?php echo $status_2;?>+')', '3'+'('+<?php echo $status_3;?>+')', '4'+'('+<?php echo $status_4;?>+')', '5'+'('+<?php echo $status_1;?>+')', '6'+'('+<?php echo $status_6;?>+')'],
				datasets: [{
					label: '點位狀態分布',
					backgroundColor: 'rgb(255, 99, 132)',
					borderColor: 'rgb(255, 99, 132)',
					data: [<?php echo $status_1;?>, <?php echo $status_2;?>, <?php echo $status_3;?>, <?php echo $status_4;?>, <?php echo $status_5;?>, <?php echo $status_6;?>]
				}]
			},
			options: {}
		});
	</script>
</body>
<!-- END BODY -->
</html>