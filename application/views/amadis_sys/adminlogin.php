<!-- <!DOCTYPE html> -->
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <!-- <html lang="en"> --> <!--<![endif]-->
<!-- BEGIN HEAD -->
<!-- <head> -->
	<!-- <?php echo require_once('common/head.php'); ?> -->
	<!-- BEGIN PAGE LEVEL STYLES -->
	<!-- <link href="<?php echo base_url('public/globel/css/login.css')?>" rel="stylesheet" type="text/css"/> -->
	<!-- END PAGE LEVEL STYLES -->
<!-- </head> -->
<!-- END HEAD -->
<!-- BEGIN BODY -->
<link href="<?php echo base_url('public/globel/css/login.css')?>" rel="stylesheet" type="text/css"/>
<body class="login">
	<!-- BEGIN LOGO -->
	<div class="logo">
		<img src="<?php echo base_url('public/globel/image/logo/system-logo-white.png')?>" alt="" /> 
		<!-- <img src="<?php echo base_url('public/globel/image/logo-big.png')?>" alt="" />  -->
	</div>
	<!-- END LOGO -->
	<!-- BEGIN LOGIN -->
	<div class="content">
		<!-- BEGIN LOGIN FORM -->
		<form class="form-vertical login-form" action="<?php echo base_url('home/logincheck'); ?>" method="post" id="submitform">
			<h3 class="form-title" style="text-align: center;"><?php echo LOGIN_TITLE; ?></h3>
			<?php echo $this->session->flashdata('error_msg');?>
			<!-- <div class="alert alert-error hide">
				<button class="close" data-dismiss="alert"></button>
				<span>請輸入帳號與密碼！</span>
			</div> -->
			<div class="control-group">
				<!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
				<label class="control-label visible-ie8 visible-ie9">帳號</label>
				<div class="controls">
					<div class="input-icon left">
						<i class="icon-user"></i>
						<input class="m-wrap placeholder-no-fix" type="text" placeholder="請輸入帳號" name="user_email" value="" required/>
					</div>
					<span id="errorMsg"></span>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label visible-ie8 visible-ie9">密碼</label>
				<div class="controls">
					<div class="input-icon left">
						<i class="icon-lock"></i>
						<input class="m-wrap placeholder-no-fix" type="password" placeholder="請輸入密碼" name="user_password" value="" required/>
					</div>
					<span id="errorMsg"></span>
				</div>
			</div>
			<div class="form-actions">
				<label class="checkbox"><!-- checked -->
					<input type="checkbox" name="remember" value="1" /> 記住帳號
				</label>
				<button type="submit" class="btn green pull-right">
					登入 <i class="icon-circle-arrow-right"></i>
				</button>            
			</div>
			<!-- <div class="forget-password" style="display: none;">
				<h4>忘記密碼 ?</h4>
				<p>
					請點擊 <a href="javascript:;" class="" id="forget-password">這裡</a>
					重置您的密碼.
				</p>
			</div>
			<div class="create-account" style="display: none;">
				<p>
					沒有帳戶 ?&nbsp; 
					<a href="javascript:;" id="register-btn" class="">Create an account</a>
				</p>
			</div> -->
		</form>
		<!-- END LOGIN FORM -->        
		<!-- BEGIN FORGOT PASSWORD FORM -->
		<!-- <form class="form-vertical forget-form" action="">
			<h3 class="">Forget Password ?</h3>
			<p>Enter your e-mail address below to reset your password.</p>
			<div class="control-group">
				<div class="controls">
					<div class="input-icon left">
						<i class="icon-envelope"></i>
						<input class="m-wrap placeholder-no-fix" type="text" placeholder="Email" name="email" />
					</div>
				</div>
			</div>
			<div class="form-actions">
				<button type="button" id="back-btn" class="btn">
				<i class="m-icon-swapleft"></i> Back
				</button>
				<button type="submit" class="btn green pull-right">
				Submit <i class="m-icon-swapright m-icon-white"></i>
				</button>            
			</div>
		</form> -->
		<!-- END FORGOT PASSWORD FORM -->
		<!-- BEGIN REGISTRATION FORM -->
		<!-- <form class="form-vertical register-form" action="">
			<h3 class="">Sign Up</h3>
			<p>Enter your account details below:</p>
			<div class="control-group">
				<label class="control-label visible-ie8 visible-ie9">Username</label>
				<div class="controls">
					<div class="input-icon left">
						<i class="icon-user"></i>
						<input class="m-wrap placeholder-no-fix" type="text" placeholder="Username" name="username"/>
					</div>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label visible-ie8 visible-ie9">Password</label>
				<div class="controls">
					<div class="input-icon left">
						<i class="icon-lock"></i>
						<input class="m-wrap placeholder-no-fix" type="password" id="register_password" placeholder="Password" name="password"/>
					</div>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label visible-ie8 visible-ie9">Re-type Your Password</label>
				<div class="controls">
					<div class="input-icon left">
						<i class="icon-ok"></i>
						<input class="m-wrap placeholder-no-fix" type="password" placeholder="Re-type Your Password" name="rpassword"/>
					</div>
				</div>
			</div> -->
			<!-- <div class="control-group"> -->
				<!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
				<!-- <label class="control-label visible-ie8 visible-ie9">Email</label>
				<div class="controls">
					<div class="input-icon left">
						<i class="icon-envelope"></i>
						<input class="m-wrap placeholder-no-fix" type="text" placeholder="Email" name="email"/>
					</div>
				</div>
			</div>
			<div class="control-group">
				<div class="controls">
					<label class="checkbox">
					<input type="checkbox" name="tnc"/> I agree to the <a href="#">Terms of Service</a> and <a href="#">Privacy Policy</a>
					</label>  
					<div id="register_tnc_error"></div>
				</div>
			</div>
			<div class="form-actions">
				<button id="register-back-btn" type="button" class="btn">
				<i class="m-icon-swapleft"></i>  Back
				</button>
				<button type="submit" id="register-submit-btn" class="btn green pull-right">
				Sign Up <i class="m-icon-swapright m-icon-white"></i>
				</button>            
			</div>
		</form> -->
		<!-- END REGISTRATION FORM -->
	</div>
	<!-- END LOGIN -->
	<!-- BEGIN COPYRIGHT -->
	<div class="copyright">
		<?php echo FOOTER_YEAR.FOOTER_COPY.FOOTER_COMPANY.FOOTER_COPY_TEXT; ?>
	</div>
	<!-- END COPYRIGHT -->
</body>
<!-- END BODY -->
<?php echo require_once('common/script.php'); ?>

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url('public/plugin/main/login.js'); ?>" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->

</html>