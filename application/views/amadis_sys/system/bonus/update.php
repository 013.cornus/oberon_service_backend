		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3 class="page-title">
							<?php echo $title; ?> <small><?php echo $title_small; ?></small>
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="<?php echo base_url('home');?>">首頁</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li>
								<a href="<?php echo base_url($this->router->fetch_class()); ?>"><?php echo $title; ?></a>
								<i class="icon-angle-right"></i>
							</li>
							<li><a href="#"><?php echo $title_small; ?></a></li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN VALIDATION STATES-->
						<div class="portlet box green">
							<div class="portlet-title">
								<div class="caption"><i class="icon-user"></i><?php echo $title_small; ?></div>
								<div class="tools">
									<a href="javascript:;" class="collapse"></a>
									<!-- <a href="#portlet-config" data-toggle="modal" class="config"></a>
									<a href="javascript:;" class="reload"></a>
									<a href="javascript:;" class="remove"></a> -->
								</div>
							</div>
							<div class="portlet-body form">
								<!-- BEGIN FORM-->
								<h3></h3>
								<form action="<?php echo base_url('bonus/update?occupid=').$occupation_info['occupation_id'];?>" class="form-horizontal"  method="post" name="submitform" id="submitform">
									<div class="alert alert-error hide">
										<button class="close" data-dismiss="alert"></button>
										錯誤！請完成所有必填項目
									</div>
									<div class="alert alert-success hide">
										<button class="close" data-dismiss="alert"></button>
										Your form validation is successful!
									</div>
									<div class="alert alert-info">
										<i class="icon-warning-sign"></i> 注意！各項獎金參數比例均為<span style="color: red;">百分比％</span>
									</div>
									<div class="control-group">
										<label class="control-label">職級名稱<span class="required">*</span></label>
										<div class="controls">
											<select class="span6 select2_category" data-placeholder="請選擇" name="occupationname" id="occupationname">
												<option value=""></option>
												<?php foreach($occupation as $row){?>
													<option value="<?php echo $row['occupation_id'];?>" <?php if($occupation_info['occupation_id'] == $row['occupation_id']){echo "selected";}?>><?php echo $row['occupation_name'];?></option>
												<?php }?>		
											</select>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">基金<span class="required">*</span></label>
										<div class="controls">
											<input name="fund" id="fund" type="text" class="span6 m-wrap" value="<?php echo $result['bouns_fund'];?>" /><span id="fundMsg"></span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">股權<span class="required">*</span></label>
										<div class="controls">
											<input name="equity" id="equity" type="text" class="span6 m-wrap" value="<?php echo $result['bouns_equity'];?>"/><span id="equityMsg"></span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">服務津貼<span class="required">*</span></label>
										<div class="controls">
											<input name="service" id="service" type="text" class="span6 m-wrap" value="<?php echo $result['bouns_service_subsidy'];?>"/><span id="serviceMsg"></span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">差額獎金<span class="required">*</span></label>
										<div class="controls">
											<input name="difference" id="difference" type="text" class="span6 m-wrap"  value="<?php echo $result['bouns_difference'];?>"/><span id="differenceMsg"></span>
										</div>
									</div>	
									<div class="control-group">
										<label class="control-label">同階獎金<span class="required">*</span></label>
										<div class="controls">
											<input name="samelevel" id="samelevel" type="text" class="span6 m-wrap" value="<?php echo $result['bouns_same_level'];?>"/><span id="passwordcheckMsg"></span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">全國分紅<span class="required">*</span></label>
										<div class="controls">
											<input name="dividend" id="dividend" type="text" class="span6 m-wrap" value="<?php echo $result['bouns_national_dividend'];?>"/><span id="dividendMsg"></span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">營業處行政津貼<span class="required">*</span></label>
										<div class="controls">
											<input name="general" id="general" type="text" class="span6 m-wrap" value="<?php echo $result['bouns_general_subsidy'];?>"/><span id="generalMsg"></span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">是否適用同階獎金<span class="required">*</span></label>
										<div class="controls">
											<select class="span6 m-wrap" name="samecheck" id="samecheck">
												<option value="0" <?php if($result['bouns_same_is_check'] == 0){echo "selected='selected'";}?>>無</option>
												<option value="1" <?php if($result['bouns_same_is_check'] == 1){echo "selected='selected'";}?>>是</option>
											</select>
											<span id="passwordcheckMsg"></span>
										</div>
									</div>					
									<div class="form-actions">
										<button type="submit" class="btn green" name="id" value="<?php echo $upid;?>" onclick="check()">
											儲存 <div class="icon-save"></div>
										</button>
										<a href="<?php echo base_url('bonus');?>">
											<button type="button" class="btn">取消 <div class="icon-undo"></div></button>
										</a>
									</div>
								</form>
								<!-- END FORM-->
							</div>
						</div>
						<!-- END VALIDATION STATES-->
					</div>
				</div>
				<!-- END DASHBOARD STATS -->
				<div class="clearfix"></div>
			</div>
		</div>
		<!-- END PAGE CONTAINER-->    
	</div>
	<!-- END PAGE -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<div class="footer">
	<?php echo $footer; ?>
</div>
<!-- END FOOTER -->
<?php echo $script; ?>
<!-- BEGIN PAGE LEVEL PLUGINS -->

<script src="<?php echo base_url('public/globel/plugin/main/bonus-validation.js')?>" type="text/javascript"></script>
<script>
	jQuery(document).ready(function() {   
		// initiate layout and plugins
		FormValidation.init();
	});
</script>	
</body>
<!-- END BODY -->
</html>