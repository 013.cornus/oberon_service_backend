<script type="text/javascript">
	var rule1=/./;
	var rule2=/.@./;
	var rule3=/^[A-Z]{1}[0-9]{9}$/;
	var rule4=/^09\d{8}/;
	var rule5=/^0\d{1,2}-?(\d{6,8})(#\d{1,5}){0,1}/;
	var rule6=/[\u4e00-\u9fa5]{0,2}縣|市[\u4e00-\u9fa5]{2,4}鄉|鎮|市|區[\u4e00-\u9fa5]{2,4}路|街|大道.*\d*號./;
	//名稱
	$("#mainname").blur(function(){
		if(rule1.test($(this).val())){	
			$(this).css("border-color","green")
			mainnameMsg.innerHTML="OK";
			mainnameMsg.style.color="green";
		}else{
			$(this).css("border-color","red")
			mainnameMsg.innerHTML="名稱不可為空";
			mainnameMsg.style.color="red";
		}
	})

	//圖示
	$("#icon").blur(function(){
		if(rule1.test($(this).val())){	
			$(this).css("border-color","green")
			iconMsg.innerHTML="OK";
			iconMsg.style.color="green";
		}else{
			$(this).css("border-color","red")
			iconMsg.innerHTML="圖示不可為空";
			iconMsg.style.color="red";
		}
	})

	//代碼
	$("#code").blur(function(){
		if(rule1.test($(this).val())){	
			$(this).css("border-color","green")
			codeMsg.innerHTML="OK";
			codeMsg.style.color="green";
		}else{
			$(this).css("border-color","red")
			codeMsg.innerHTML="代碼不可為空";
			codeMsg.style.color="red";
		}
	})

	//前台連結
	$("#frontlink").blur(function(){
		if(rule1.test($(this).val())){	
			$(this).css("border-color","green")
			frontlinkMsg.innerHTML="OK";
			frontlinkMsg.style.color="green";
		}else{
			$(this).css("border-color","red")
			frontlinkMsg.innerHTML="前台連結不可為空";
			frontlinkMsg.style.color="red";
		}
	})

	//後台連結
	$("#backlink").blur(function(){
		if(rule1.test($(this).val())){	
			$(this).css("border-color","green")
			backlinkMsg.innerHTML="OK";
			backlinkMsg.style.color="green";
		}else{
			$(this).css("border-color","red")
			backlinkMsg.innerHTML="後台連結不可為空";
			backlinkMsg.style.color="red";
		}
	})
	function check() {
		if (rule1.test($("#mainname").val()) && rule1.test($("#icon").val()) && rule1.test($("#code").val()) && rule1.test($("#frontlink").val()) && rule1.test($("#backlink").val())){
			document.getElementById("submitform").submit();	
		} else {
			alert("請輸入完整資訊");

			if(rule1.test($("#mainname").val()) == false){
				$("#mainname").css("border-color","red")
				mainnameMsg.innerHTML="名稱不可為空";
				mainnameMsg.style.color="red";
			}

			if(rule1.test($("#icon").val()) == false){
				$("#icon").css("border-color","red")
				iconMsg.innerHTML="圖示不可為空";
				iconMsg.style.color="red";
			}

			if(rule1.test($("#code").val()) == false){
				$("#code").css("border-color","red")
				codeMsg.innerHTML="代碼不可為空";
				codeMsg.style.color="red";
			}

			if(rule1.test($("#frontlink").val()) == false){
				$("#frontlink").css("border-color","red")
				frontlinkMsg.innerHTML="前台連結不可為空";
				frontlinkMsg.style.color="red";
			}

			if(rule1.test($("#backlink").val()) == false){
				$("#backlink").css("border-color","red")
				backlinkMsg.innerHTML="後台連結不可為空";
				backlinkMsg.style.color="red";
			}
		}
	}
</script>
</script>