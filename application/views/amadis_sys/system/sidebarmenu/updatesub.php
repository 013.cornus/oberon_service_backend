		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3 class="page-title">
							<?php echo $title; ?> <small><?php echo $title_small; ?></small>
						</h3>
						<?php $mainid=$sub['sidebar_main_id'];?>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="<?php echo base_url('home');?>">首頁</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li>
								<a href="<?php echo base_url('sidebarmenu');?>"><?php echo $title;?></a> 
								<i class="icon-angle-right"></i>
							</li>
							<li>
								<a href="<?php echo base_url('sidebarmenu/sublist?id=').$mainid;?>"><?php echo $unit_title;?></a> 
								<i class="icon-angle-right"></i>
							</li>
							<li><a href="#"><?php echo $title_small; ?></a></li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN VALIDATION STATES-->
						<div class="portlet box green">
							<div class="portlet-title">
								<div class="caption"><i class="icon-user"></i><?php echo $title_small; ?></div>
								<div class="tools">
									<a href="javascript:;" class="collapse"></a>
									<!-- <a href="#portlet-config" data-toggle="modal" class="config"></a>
									<a href="javascript:;" class="reload"></a>
									<a href="javascript:;" class="remove"></a> -->
								</div>
							</div>
							<div class="portlet-body form">
								<!-- BEGIN FORM-->
								<h3></h3>
								
								<form action="<?php echo base_url('sidebarmenu/updatesub?id=').$upid.'&mainid='.$mainid;?>" class="form-horizontal"  method="post" name="submitform" id="submitform">
									<div class="alert alert-error hide">
										<button class="close" data-dismiss="alert"></button>
										錯誤！請完成所有必填項目
									</div>
									<div class="alert alert-success hide">
										<button class="close" data-dismiss="alert"></button>
										Your form validation is successful!
									</div>
									<!-- <div class="control-group">
										<label class="control-label">權限<span class="required">*</span></label>
										<div class="controls">
											<select class="span6 m-wrap" name="competence" id="competence">
												<?php foreach ($competence as $r) { foreach($sub as $d){?>
													<option value="<?php echo $r["competence_id"];?>" <?php if($r["competence_id"]==$d["competence_id"])
													{echo "selected='selected'";}?>><?php echo $r["competence_name"];?></option>
												<?php }}?>
											</select>
											<span id="competenceMsg"></span>
										</div>
									</div> -->
									<div class="control-group">
										<label class="control-label">主選單<span class="required">*</span></label>
										<div class="controls">
											<input name="mainname" id="mainname" type="text" class="span6 m-wrap" value="<?php foreach($sidebarmain as $d){if($sub['sidebar_main_id']==$d['sidebar_main_id']){echo $d['sidebar_main_name'];}}?>" readonly/><span id="mainnameMsg"></span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">副選單名稱<span class="required">*</span></label>
										<div class="controls">
											<input name="subname" id="subname" type="text" class="span6 m-wrap" value="<?php echo $sub['sidebar_sub_name'];?>" /><span id="subnameMsg"></span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">圖示<span class="required">*</span></label>
										<div class="controls">
											<input name="icon" id="icon" type="text" class="span6 m-wrap" value="<?php echo $sub['sidebar_sub_icon'];?>"/><span id="iconMsg"></span>
										</div>
									</div>	
									<div class="control-group">
										<label class="control-label">代碼<span class="required">*</span></label>
										<div class="controls">
											<input name="code" id="code" type="text" class="span6 m-wrap" value="<?php echo $sub['sidebar_sub_code'];?>"/><span id="codeMsg"></span>
										</div>
									</div>		
									<div class="control-group">
										<label class="control-label">前台連結<span class="required">*</span></label>
										<div class="controls">
											<input name="frontlink" id="frontlink" type="text" class="span6 m-wrap" value="<?php echo $sub['sidebar_sub_front_link'];?>"/><span id="frontlinkMsg"></span>
										</div>
									</div>		
									<div class="control-group">
										<label class="control-label">後台連結<span class="required">*</span></label>
										<div class="controls">
											<input name="backlink" id="backlink" type="text" class="span6 m-wrap" value="<?php echo $sub['sidebar_sub_back_link'];?>"/><span id="backlinkMsg"></span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">排序<span class="required">*</span></label>
										<div class="controls">
											<input name="sort" id="sort" type="text" class="span6 m-wrap" value="<?php echo $sub['sidebar_sub_sort'];?>"/><span id="sortMsg"></span>
										</div>
									</div>		
									<div class="form-actions">
										<button type="submit" class="btn green" name="id" value="<?php echo $upid;?>">
											儲存 <div class="icon-save"></div>
										</button>
										<a href="<?php echo base_url('sidebarmenu/sublist?id=').$mainid;?>">
											<button type="button" class="btn">取消 <div class="icon-undo"></div></button>
										</a>
									</div>
								</form>
								<!-- END FORM-->
							</div>
						</div>
						<!-- END VALIDATION STATES-->
					</div>
				</div>
				<!-- END DASHBOARD STATS -->
				<div class="clearfix"></div>
			</div>
		</div>
		<!-- END PAGE CONTAINER-->    
	</div>
	<!-- END PAGE -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<div class="footer">
	<?php echo $footer; ?>
</div>
<!-- END FOOTER -->
<?php echo $script; ?>

<script src="<?php echo base_url('public/globel/plugin/main/sidebarmenu-validation.js')?>" type="text/javascript"></script>

<script>
	jQuery(document).ready(function() {   
		// initiate layout and plugins
		FormValidation.init();
	});
</script>

</body>
<!-- END BODY -->
</html>