		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">    
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3 class="page-title">
							<?php echo $title; ?> <small><?php echo $title_small;?></small>
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="<?php echo base_url('home');?>">首頁</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li>
								<a href="<?php echo base_url('sidebarmenu');?>"><?php echo $title;?></a> 
								<i class="icon-angle-right"></i>
							</li>
							<li><a href="<?php echo $this->uri->segment(2);?>"><?php echo $title_small; ?></a></li>
							<li class="pull-right no-text-shadow" style="display: none;">
								<div id="dashboard-report-range" class="dashboard-date-range tooltips no-tooltip-on-touch-device responsive" data-tablet="" data-desktop="tooltips" data-placement="top" data-original-title="Change dashboard date range">
									<i class="icon-calendar"></i>
									<span></span>
									<i class="icon-angle-down"></i>
								</div>
							</li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<div id="dashboard">
					<!-- BEGIN ALTER -->
					<?php echo $this->session->flashdata('messagediv');?>
					<!-- END ALTER -->
					<!-- BEGIN DASHBOARD STATS -->
					<!-- BEGIN PAGE CONTENT-->
					<div class="row-fluid">
						<div class="span12">
							<div class="portlet box blue" id="form_wizard_1">
								<div class="portlet-title">
									<div class="caption">
										<i class="icon-reorder"></i> 搜尋列</span>
									</div>
									<div class="tools hidden-phone">
										<a href="javascript:;" class="collapse"></a>
										<a href="#portlet-config" data-toggle="modal" class="config" style="display: none;"></a>
										<a href="javascript:;" class="reload" style="display: none;"></a>
										<a href="javascript:;" class="remove" style="display: none;"></a>
									</div>
								</div>
								<div class="portlet-body form">
									<form method="post" action="<?php echo base_url('sidebarmenu/sublist?id=').$upid?>">
										<div class="control-group">
											<label class="control-label">搜尋關鍵字：</label>
											<div class="controls">
												<input type="text" class="span8 m-wrap" name="keyword" id="keyword" placeholder="請輸入關鍵字，請勿輸入特殊字元！" />

												<button type="submit" class="btn green">
													搜尋 <i class="m-icon-swapright m-icon-white"></i>
												</button>            
											</div>
										</div>
									</form>
								</div>
							</div>

						</div>
					</div>

					<!-- END PAGE CONTENT-->

					<div class="row-fluid">
						<div class="span12">
							<!-- BEGIN EXAMPLE TABLE PORTLET-->
							<div class="portlet box light-grey">
								<div class="portlet-title">
									<div class="caption"><i class="icon-globe"></i><?php echo $title_small; ?></div>
									<div class="tools">
										<a href="javascript:;" class="collapse"></a>
										<a href="#portlet-config" data-toggle="modal" class="config" style="display: none;"></a>
										<a href="javascript:;" class="reload" style="display: none;"></a>
										<a href="javascript:;" class="remove" style="display: none;"></a>
									</div>
								</div>
								<div class="portlet-body">
									<div class="clearfix">
										<div class="btn-group">
											<a href="<?php echo base_url('sidebarmenu/createsub_form?id=').$upid;?>">
												<button id="sample_editable_1_new" class="btn green">
													新增 <i class="icon-plus"></i>
												</button>
											</a>
										</div>
										<div class="btn-group">
											<a href="<?php echo base_url('sidebarmenu');?>">
												<button type="button" class="btn">回主選單 <div class="icon-undo"></div></button>
											</a>
										</div>
										<div class="btn-group pull-right" style="display: none;">
											<button class="btn dropdown-toggle" data-toggle="dropdown">Tools <i class="icon-angle-down"></i>
											</button>
											<ul class="dropdown-menu pull-right">
												<li><a href="#"><i class="icon-print"></i> Print</a></li>
												<li><a href="#"><i class="icon-save"></i> Save as PDF</a></li>
												<li><a href="#"><i class="icon-save"></i> Export to Excel</a></li>
											</ul>
										</div>
									</div>
									<div style="overflow:scroll; overflow:auto;">
									<table class="table table-striped table-bordered table-hover" id="sample_1" style="min-width: 768px;">
										<thead>
											<tr>
												<th style="width:8px;">
													<input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" />
												</th>
												<th>項次</th>
												<th>副選單</th>
												<th>建立日期</th>
												<th>更新日期</th>
												<th>動作</th>
											</tr>
										</thead>
										<tbody>
											<?php $a=0 ?>
											<?php foreach ($result as $row) { ?>
											<tr class="odd" ">
												<td><input type="checkbox" class="checkboxes" value=""/></td>
												<td><?php echo $a=$a+1; ?>
												</td>
												<td><?php echo $row['sidebar_sub_name']; ?></td>
												<td><?php echo $row['sidebar_sub_created_date']; ?></td>
												<td><?php echo $row['sidebar_sub_updated_date']; ?></td>
												<td>
													<a href="<?php echo base_url('sidebarmenu/updatesub_form?id=').$row['sidebar_sub_id']."&mainid=".$upid;?>" class="btn blue mini">編輯 <i class="icon-edit"></i>
													</a>
													<?php if($row['sidebar_sub_status'] == 1){?>
													<button class="btn purple mini" class="btn purple mini" data-toggle="modal" href="#confirm_window" onclick="alert_window('<?php echo base_url('sidebarmenu/sub_unstatus?id=').$row['sidebar_sub_id'].'&mainid='.$row['sidebar_main_id'];?>','確定要下架這筆資料嗎?','確定下架')">下架 <i class="icon-eye-close"></i></button>
													<?php }?>
													<?php if($row['sidebar_sub_status'] == 0){?>
													<button class="btn green mini" class="btn green mini" data-toggle="modal" href="#confirm_window" onclick="alert_window('<?php echo base_url('sidebarmenu/sub_status?id=').$row['sidebar_sub_id'].'&mainid='.$row['sidebar_main_id'];?>','確定要上架這筆資料嗎?','確定上架')">上架 <i class="icon-eye-close"></i></button>
													<?php }?>
													<button class="btn red mini" data-toggle="modal" href="#confirm_window" onclick="alert_window('<?php echo base_url('sidebarmenu/deletesub?id=').$row['sidebar_sub_id'].'&mainid='.$row['sidebar_main_id'];?>','確定要上架這筆資料嗎?','確定上架')">刪除 <i class="icon-trash"></i></button>
												</td>
											</tr>
											<?php } ?>
										</tbody>
									</table>
								    </div>
								</div>
							</div>

							<!-- 警告視窗 -->
							<!-- BEGIN CONFIRM WINDOW -->
							<div id="confirm_window" class="modal hide fade " role="dialog" data-keyboard="false" style="top:20%">
								<div class="modal-header red">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
									<h4><div class="icon-warning-sign"></div> 警告</h4>
								</div>
								<form>
									<div class="modal-body">
										<p id='confirm_text'>警告文字</p>
									</div>
									<div class="modal-footer">
										<button type="button" data-dismiss="modal" class="btn">取消 <div class="icon-undo"></div></button>
										<button id="confirm_btn" type="button" data-dismiss="modal" class="btn red" >確認按鈕<div class="icon-check"></div></button>
									</div>
								</form>
							</div>
							<!-- END CONFIRM WINDOW -->

							<!-- END EXAMPLE TABLE PORTLET-->
						</div>
					</div>
					<!-- END DASHBOARD STATS -->
					<div class="clearfix"></div>
				</div>
			</div>
			<!-- END PAGE CONTAINER-->    
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
	<div class="footer">
		<?php echo $footer; ?>
	</div>
	<!-- END FOOTER -->
	<?php echo $script; ?>
	<script type="text/javascript" src="./public/media/js/jquery.dataTables.js"></script>
    <script type="text/javascript" src="./public/media/js/DT_bootstrap.js"></script>
</body>
<script type="text/javascript">
	//$link為欲前往連結
	//$confirm_text為警告視窗文字
	//$btn_text為確認按鈕文字
	function alert_window($link,$confirm_text,$btn_text){
		$("#confirm_text").text($confirm_text);
		$("#confirm_btn").text($btn_text);
		$("#confirm_btn").attr('onclick',"javascript:location.href='"+$link+"'");
	}
</script>
<!-- END BODY -->
</html>