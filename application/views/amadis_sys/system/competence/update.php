			<!-- BEGIN PAGE -->
			<div class="page-content">
				<!-- BEGIN PAGE CONTAINER-->
				<div class="container-fluid">
					<!-- BEGIN PAGE HEADER-->
					<div class="row-fluid">
						<div class="span12">
							<!-- BEGIN PAGE TITLE & BREADCRUMB-->
							<h3 class="page-title">
								<?php echo $title; ?> <small><?php echo $title_small; ?></small>
							</h3>
							<ul class="breadcrumb">
								<li>
									<i class="icon-home"></i>
									<a href="<?php echo base_url('home');?>">首頁</a> 
									<i class="icon-angle-right"></i>
								</li>
								<li>
									<a href="<?php echo base_url($this->router->fetch_class()); ?>"><?php echo $title; ?></a>
									<i class="icon-angle-right"></i>
								</li>
								<li><a href="#"><?php echo $title_small; ?></a></li>
							</ul>
							<!-- END PAGE TITLE & BREADCRUMB-->
						</div>
					</div>
					<!-- END PAGE HEADER-->
					<div class="row-fluid">
						<div class="span12">
							<!-- BEGIN VALIDATION STATES-->
							<div class="portlet box green">
								<div class="portlet-title">
									<div class="caption"><i class="icon-user"></i><?php echo $title_small; ?></div>
									<div class="tools">
										<a href="javascript:;" class="collapse"></a>
									<!-- <a href="#portlet-config" data-toggle="modal" class="config"></a>
									<a href="javascript:;" class="reload"></a>
									<a href="javascript:;" class="remove"></a> -->
								</div>
							</div>
							<div class="portlet-body form">
								<!-- BEGIN FORM-->
								<h3></h3>
								<?php $allcompetence = array();?>
								<?php $actionid = array();?>
								<form action="<?php echo base_url('competence/update');?>" class="form-horizontal"  method="post" name="submitform" id="submitform">
									<div class="alert alert-error hide">
										<button class="close" data-dismiss="alert"></button>
										You have some form errors. Please check below.
									</div>
									<div class="alert alert-success hide">
										<button class="close" data-dismiss="alert"></button>
										Your form validation is successful!
									</div>
									<div class="control-group">
										<label class="control-label">權限名稱<span class="required">*</span></label>
										<div class="controls">
											<input name="competencename" id="competencename" type="text" class="span6 m-wrap" value="<?php echo $result['competence_name'];?>" /><span id="competencenameMsg"></span>
										</div>
									</div>
									<?php $b=1;?>
									<?php foreach($mainbar as $m){?>
									<div class="accordion-group">
										<div class="accordion-heading">
											<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion2" href="<?php echo "#tag".$m['sidebar_main_id'];?>">
												<i class="icon-th"></i>
												<?php echo $m['sidebar_main_name']?>
											</a>
										</div>

										<div id="<?php echo "tag".$m['sidebar_main_id'];?>" class="accordion-body collapse">

											<?php foreach($subbar as $r){ if($m['sidebar_main_id'] == $r['sidebar_main_id']){?>
											<div class="accordion-inner">
												<div class="control-group">

													<label class="control-label"><?php echo $r['sidebar_sub_name'];?></label>
													<?php foreach($action as $d){if($r['sidebar_sub_id']==$d['sidebar_sub_id']){?>
													<div class="controls">
														<label class="checkbox">
															<input type="checkbox" name="CheckAll" id="CheckAll"/> 全選
														</label>
														<label class="checkbox">
															<input type="checkbox" name="<?php echo "view".$d['competence_id']."a".$d['sidebar_sub_id']?>" id="<?php echo "view".$d['competence_id']."a".$d['sidebar_sub_id']?>" value="1" <?php if($d['actions_view'] == 1){echo "checked";}?>/> 檢視
														</label>
														<label class="checkbox">
															<input type="checkbox" name="<?php echo "new".$d['competence_id']."a".$d['sidebar_sub_id']?>" id="<?php echo "new".$d['competence_id']."a".$d['sidebar_sub_id']?>" value="1" <?php if($d['actions_insert'] == 1){echo "checked";}?>/> 新增
														</label>
														<label class="checkbox">
															<input type="checkbox" name="<?php echo "enable".$d['competence_id']."a".$d['sidebar_sub_id']?>" id="<?php echo "enable".$d['competence_id']."a".$d['sidebar_sub_id']?>" value="1" <?php if($d['actions_enable'] == 1){echo "checked";}?>/> 上下架
														</label>
														<label class="checkbox">
															<input type="checkbox" name="<?php echo "update".$d['competence_id']."a".$d['sidebar_sub_id']?>" id="<?php echo "update".$d['competence_id']."a".$d['sidebar_sub_id']?>" value="1" <?php if($d['actions_update'] == 1){echo "checked";}?> /> 修改
														</label>
														<label class="checkbox">
															<input type="checkbox" name="<?php echo "delete".$d['competence_id']."a".$d['sidebar_sub_id']?>" id="<?php echo "delete".$d['competence_id']."a".$d['sidebar_sub_id']?>" value="1" <?php if($d['actions_delete'] == 1){echo "checked";}?> /> 刪除
														</label>
														<label class="checkbox">
															<input type="checkbox" name="<?php echo "export".$d['competence_id']."a".$d['sidebar_sub_id']?>" id="<?php echo "export".$d['competence_id']."a".$d['sidebar_sub_id']?>" value="1" <?php if($d['actions_export'] == 1){echo "checked";}?> /> 匯出
														</label>
													</div>
													<?php array_push($allcompetence,$d['competence_id']."a".$d['sidebar_sub_id']); array_push($actionid,$d['actions_id']);}}?>
												</div>	
											</div>
											<?php $b=$b+1;}}?>

										</div>
									</div>
									<?php }?>
									<?php $this->session->set_flashdata('allcompetence',$allcompetence);?>
									<?php $this->session->set_flashdata('actionid',$actionid);?>				
									<div class="form-actions">
										<button type="submit" class="btn green" name="id" value="<?php echo $upid;?>" onclick="check()">
											儲存 <div class="icon-save"></div>
										</button>
										<a href="<?php echo base_url('competence');?>">
											<button type="button" class="btn">取消 <div class="icon-undo"></div></button>
										</a>
									</div>
								</form>
								<!-- END FORM-->
							</div>
						</div>
						<!-- END VALIDATION STATES-->
					</div>
				</div>
				<!-- END DASHBOARD STATS -->
				<div class="clearfix"></div>
			</div>
		</div>
		<!-- END PAGE CONTAINER-->    
	</div>
	<!-- END PAGE -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<div class="footer">
	<?php echo $footer; ?>
</div>
<!-- END FOOTER -->
<?php echo $script; ?>

<script>
	$(document).ready(function() {       
		// App.init();
		// TableManaged.init();
		// FormWizard.init();
		$("#CheckAll").click(function(){
			if($("#CheckAll").prop("checked")){
				$("input[type='checkbox']").each(function(){
					$(this).prop("checked",true);
				})
			}else{
				$("input[type='checkbox']").each(function(){
					$(this).prop("checked",false);
				})
			}
		});
	});
</script>
</body>
<!-- END BODY -->
</html>