		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3 class="page-title">
							<?php echo $title; ?> <small><?php echo $title_small; ?></small>
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="<?php echo base_url('home');?>">首頁</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li>
								<a href="<?php echo base_url('sidebarmenu');?>"><?php echo $title; ?></a>
								<i class="icon-angle-right"></i>
							</li>
							<li><a href="#"><?php echo $title_small; ?></a></li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN VALIDATION STATES-->
						<div class="portlet box green">
							<div class="portlet-title">
								<div class="caption"><i class="icon-user"></i><?php echo $title_small; ?></div>
								<div class="tools">
									<a href="javascript:;" class="collapse"></a>
									<!-- <a href="#portlet-config" data-toggle="modal" class="config"></a>
									<a href="javascript:;" class="reload"></a>
									<a href="javascript:;" class="remove"></a> -->
								</div>
							</div>
							<div class="portlet-body form">
								<!-- BEGIN FORM-->
								<h3></h3>
								<form action="<?php echo base_url('sidebarmenu/update');?>" class="form-horizontal"  method="post" name="submitform" id="submitform">
									<div class="alert alert-error hide">
										<button class="close" data-dismiss="alert"></button>
										錯誤！請完成所有必填項目
									</div>
									<div class="alert alert-success hide">
										<button class="close" data-dismiss="alert"></button>
										Your form validation is successful!
									</div>
<!-- 									<div class="control-group">
										<label class="control-label">權限<span class="required">*</span></label>
										<div class="controls">
											<select class="span6 m-wrap" name="competence" id="competence">
												<?php foreach ($competence as $r) { foreach($result as $d){?>
													<option value="<?php echo $r["competence_id"];?>" <?php if($r["competence_id"]==$d["competence_id"])
													{echo "selected='selected'";}?>><?php echo $r["competence_name"];?></option>
												<?php }}?>
											</select>
											<span id="competenceMsg"></span>
										</div>
									</div> -->
									<div class="control-group">
										<label class="control-label">名稱<span class="required">*</span></label>
										<div class="controls">
											<input name="mainname" id="mainname" type="text" class="span6 m-wrap" value="<?php foreach($result as $r){echo $r['sidebar_main_name'];}?>" /><span id="mainnameMsg"></span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">圖示<span class="required">*</span></label>
										<div class="controls">
											<input name="icon" id="icon" type="text" class="span6 m-wrap" value="<?php foreach($result as $r){echo $r['sidebar_main_icon'];}?>"/><span id="iconMsg"></span>
										</div>
									</div>	
									<div class="control-group">
										<label class="control-label">代碼<span class="required">*</span></label>
										<div class="controls">
											<input name="code" id="code" type="text" class="span6 m-wrap" value="<?php foreach($result as $r){echo $r['sidebar_main_code'];}?>"/><span id="codeMsg"></span>
										</div>
									</div>		
									<div class="control-group">
										<label class="control-label">前台連結<span class="required">*</span></label>
										<div class="controls">
											<input name="frontlink" id="frontlink" type="text" class="span6 m-wrap" value="<?php foreach($result as $r){echo $r['sidebar_main_front_link'];}?>"/><span id="frontlinkMsg"></span>
										</div>
									</div>		
									<div class="control-group">
										<label class="control-label">後台連結<span class="required">*</span></label>
										<div class="controls">
											<input name="backlink" id="backlink" type="text" class="span6 m-wrap" value="<?php foreach($result as $r){echo $r['sidebar_main_back_link'];}?>"/><span id="backlinkMsg"></span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">排序<span class="required">*</span></label>
										<div class="controls">
											<input name="sort" id="sort" type="text" class="span6 m-wrap" value="<?php foreach($result as $r){echo $r['sidebar_main_sort'];}?>"/><span id="sortMsg"></span>
										</div>
									</div>					
									<div class="form-actions">
										<button type="submit" class="btn green" name="id" value="<?php echo $upid;?>">
											儲存 <div class="icon-save"></div>
										</button>
										<a href="<?php echo base_url('sidebarmenu');?>">
											<button type="button" class="btn">取消 <div class="icon-undo"></div></button>
										</a>
									</div>
								</form>
								<!-- END FORM-->
							</div>
						</div>
						<!-- END VALIDATION STATES-->
					</div>
				</div>
				<!-- END DASHBOARD STATS -->
				<div class="clearfix"></div>
			</div>
		</div>
		<!-- END PAGE CONTAINER-->    
	</div>
	<!-- END PAGE -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<div class="footer">
	<?php require_once(dirname(__FILE__).'/../common/footer.php'); ?>
</div>
<!-- END FOOTER -->
<?php require_once(dirname(__FILE__).'/../common/script.php'); ?>
<!-- BEGIN PAGE LEVEL PLUGINS -->
<!-- BEGIN RULE -->
<?php require_once(dirname(__FILE__).'/../account/rule.php'); ?>
<!-- END RULE -->
</body>
<script src="<?php echo base_url('public/globel/js/jquery.validate.min.js')?>" type="text/javascript"></script>
<script src="<?php echo base_url('public/globel/plugin/main/sidebarmenu-validation.js')?>" type="text/javascript"></script>

<script>
	jQuery(document).ready(function() {   
		// initiate layout and plugins
		FormValidation.init();
	});
</script>
<!-- END BODY -->
</html>