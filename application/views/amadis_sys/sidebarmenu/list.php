		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">    
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3 class="page-title">
							<?php echo $title; ?> <small><?php echo $title_small; ?></small>
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="<?php echo base_url('home');?>">首頁</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li><a href="<?php echo $this->router->fetch_class();?>"><?php echo $title_small; ?></a></li>
							<li class="pull-right no-text-shadow" style="display: none;">
								<div id="dashboard-report-range" class="dashboard-date-range tooltips no-tooltip-on-touch-device responsive" data-tablet="" data-desktop="tooltips" data-placement="top" data-original-title="Change dashboard date range">
									<i class="icon-calendar"></i>
									<span></span>
									<i class="icon-angle-down"></i>
								</div>
							</li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<div id="dashboard">
					<!-- BEGIN ALTER -->
					<?php echo $this->session->flashdata('messagediv');?>
					<!-- END ALTER -->
					<!-- BEGIN DASHBOARD STATS -->
					<!-- BEGIN PAGE CONTENT-->
					<div class="row-fluid">
						<div class="span12">
							<div class="portlet box blue" id="form_wizard_1">
								<div class="portlet-title">
									<div class="caption">
										<i class="icon-search"></i> 搜尋列</span>
									</div>
									<div class="tools hidden-phone">
										<a href="javascript:;" class="collapse"></a>
										<a href="#portlet-config" data-toggle="modal" class="config" style="display: none;"></a>
										<a href="javascript:;" class="reload" style="display: none;"></a>
										<a href="javascript:;" class="remove" style="display: none;"></a>
									</div>
								</div>
								<div class="portlet-body form">
									<form method="post" action="<?php echo base_url('sidebarmenu')?>">
										<div class="control-group">
											<label class="control-label">搜尋關鍵字：</label>
											<div class="controls">
												<input type="text" class="span8 m-wrap" name="fullname" id="fullname" placeholder="請輸入關鍵字 / 主選單名稱 /" />

												<button type="submit" class="btn green">
													搜尋 <i class="icon-circle-arrow-right"></i>
												</button>            
											</div>
										</div>
									</form>
								</div>
							</div>

						</div>
					</div>

					<!-- END PAGE CONTENT-->

					<div class="row-fluid">
						<div class="span12">
							<!-- BEGIN EXAMPLE TABLE PORTLET-->
							<div class="portlet box light-grey">
								<div class="portlet-title">
									<div class="caption"><i class="icon-globe"></i><?php echo $title_small; ?></div>
									<div class="tools">
										<a href="javascript:;" class="collapse"></a>
										<a href="#portlet-config" data-toggle="modal" class="config" style="display: none;"></a>
										<a href="javascript:;" class="reload" style="display: none;"></a>
										<a href="javascript:;" class="remove" style="display: none;"></a>
									</div>
								</div>
								<div class="portlet-body">
									<div class="clearfix">
										<div class="btn-group">
											<a href="<?php echo base_url('sidebarmenu/create_form');?>">
												<button id="sample_editable_1_new" class="btn green">
													新增 <i class="icon-plus"></i>
												</button>
											</a>
										</div>
										<div class="btn-group pull-right" style="display: none;">
											<button class="btn dropdown-toggle" data-toggle="dropdown">Tools <i class="icon-angle-down"></i>
											</button>
											<ul class="dropdown-menu pull-right">
												<li><a href="#"><i class="icon-print"></i> Print</a></li>
												<li><a href="#"><i class="icon-save"></i> Save as PDF</a></li>
												<li><a href="#"><i class="icon-save"></i> Export to Excel</a></li>
											</ul>
										</div>
									</div>
									<table class="table table-striped table-bordered table-hover" id="sample_1">
										<thead>
											<tr>
												<th style="width:8px;">
													<input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" />
												</th>
												<th class="hidden-480">項次</th>
												<th class="hidden-480">主選單</th>
												<th>狀態</th>
												<th>更新日期</th>
												<th class="hidden-480">動作</th>
											</tr>
										</thead>
										<tbody>
											<?php $a=0 ?>
											<?php foreach ($result as $row) { ?>
												<tr class="odd" ">
													<td><input type="checkbox" class="checkboxes" value=""/></td>
													<td class="hidden-480"><?php echo $a=$a+1; ?>
												</td>
												<td><?php echo $row['sidebar_main_name']; ?></td>
												<td><?php echo ($row['sidebar_main_status'])?'<span class="label label-success">啟用</span>':'<span class="label label-important">停用</span>'; ?></td>
												<td><?php echo $row['sidebar_main_updated_date']; ?></td>
												<td class="hidden-480">
													<a href="<?php echo base_url('sidebarmenu/update_form?id=').$row['sidebar_main_id'];?>" class="btn blue mini">編輯 <i class="icon-edit"></i>
													</a>
													<a href="<?php echo base_url('sidebarmenu/sublist?id=').$row['sidebar_main_id'];?>" class="btn blue-stripe mini">編輯副選單 <i class="icon-edit"></i>
													</a>
													<?php if($row['sidebar_main_status'] == 1){?>
														<button id="" class="btn purple mini" class="btn purple mini" data-toggle="modal" href="#invisible<?php echo $row['sidebar_main_id'] ?>">停用 <i class="icon-eye-close"></i></button>
													<?php }?>
													<?php if($row['sidebar_main_status'] == 0){?>
														<button id="" class="btn green mini" class="btn green mini" data-toggle="modal" href="#visible<?php echo $row['sidebar_main_id'] ?>">啟用 <i class="icon-eye-open"></i></button>
													<?php }?>
													<button id="" class="btn red mini" data-toggle="modal" href="#static<?php echo $row['sidebar_main_id'] ?>">刪除 <i class="icon-trash"></i></button>
												</td>
											</tr>

											<!-- BEGIN CONFIRM DELETE WINDOW -->
											<div id="static<?php echo $row['sidebar_main_id'] ?>" class="modal hide fade" tabindex="-1" data-backdrop="static" data-keyboard="false" style="top:20%">
												<div class="modal-header red">
													<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
													<h4><div class="icon-warning-sign"></div> 警告</h4>
												</div>
												<div class="modal-body">
													<p>確定要刪除這筆資料嗎?</p>
												</div>
												<div class="modal-footer">
													<button type="button" data-dismiss="modal" class="btn">取消 <div class="icon-undo"></div></button>
													<button id="delete" type="button" data-dismiss="modal" class="btn red" onclick="javascript:location.href='<?php echo base_url('sidebarmenu/delete?id=').$row['sidebar_main_id'];?>'">確定刪除 <div class="icon-trash"></div></button>
												</div>
											</div>
											<!-- END CONFIRM DELETE WINDOW -->

											<!-- BEGIN CONFIRM UNSTATUS WINDOW -->
											<div id="invisible<?php echo $row['sidebar_main_id'] ?>" class="modal hide fade" tabindex="-1" data-backdrop="static" data-keyboard="false" style="top:20%">
												<div class="modal-header red">
													<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
													<h4><div class="icon-warning-sign"></div> 警告</h4>
												</div>
												<div class="modal-body">
													<p>確定要停用這筆資料嗎?</p>
												</div>
												<div class="modal-footer">
													<button type="button" data-dismiss="modal" class="btn">取消 <div class="icon-undo"></div></button>
													<button id="unstatus" onclick=javascript:location.href='<?php echo base_url('sidebarmenu/info_invisible?id=').$row['sidebar_main_id'];?>' type="button" data-dismiss="modal" class="btn red">確定停用 <div class="icon-check"></div></button>
												</div>
											</div>
											<!-- END CONFIRM UNSTATUS WINDOW -->

											<!-- BEGIN CONFIRM ACTIVESTATUS WINDOW -->
											<div id="visible<?php echo $row['sidebar_main_id'] ?>" class="modal hide fade" tabindex="-1" data-backdrop="static" data-keyboard="false" style="top:20%">
												<div class="modal-header red">
													<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
													<h4><div class="icon-warning-sign"></div> 警告</h4>
												</div>
												<div class="modal-body">
													<p>確定要啟用這筆資料嗎?</p>
												</div>
												<div class="modal-footer">
													<button type="button" data-dismiss="modal" class="btn">取消 <div class="icon-undo"></div></button>
													<button id="activestatus" type="button" data-dismiss="modal" class="btn green" onclick="javascript:location.href='<?php echo base_url('sidebarmenu/info_visible?id=').$row['sidebar_main_id'];?>'">確定啟用 <div class="icon-check"></div></button>
												</div>
											</div>
											<!-- END CONFIRM ACTIVESTATUS WINDOW -->
										<?php } ?>
									</tbody>
								</table>
							</div>
						</div>

						<!-- END EXAMPLE TABLE PORTLET-->
					</div>
				</div>
				<!-- END DASHBOARD STATS -->
				<div class="clearfix"></div>
			</div>
		</div>
		<!-- END PAGE CONTAINER-->    
	</div>
	<!-- END PAGE -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<div class="footer">
	<?php require_once(dirname(__FILE__).'/../common/footer.php'); ?>
</div>
<!-- END FOOTER -->
<?php require_once(dirname(__FILE__).'/../common/script.php'); ?>
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="./public/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="./public/media/js/DT_bootstrap.js"></script>
</body>
<!-- END BODY -->
</html>