		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3 class="page-title">
							<?php echo $title; ?> <small><?php echo $title_small; ?></small>
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="<?php echo base_url('home');?>">首頁</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li>
								<a href="<?php echo base_url($this->router->fetch_class()); ?>"><?php echo $title; ?></a>
								<i class="icon-angle-right"></i>
							</li>
							<li><a href="#"><?php echo $title_small; ?></a></li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<form action="<?php echo base_url('rgms_report_2/excel_template_2/update');?>" class="form-horizontal"  method="post" enctype="multipart/form-data">
					<div class="row-fluid">
						<div class="span12">
							<!-- BEGIN VALIDATION STATES-->
							<div class="portlet box green">
								<div class="portlet-title">
									<div class="caption"><i class="icon-reorder"></i><?php echo $title_small; ?></div>
									<div class="tools">
										<a href="javascript:;" class="collapse"></a>
									<!-- <a href="#portlet-config" data-toggle="modal" class="config"></a>
									<a href="javascript:;" class="reload"></a>
									<a href="javascript:;" class="remove"></a> -->
								</div>
							</div>	
							<div class="portlet-body form">
								<!-- BEGIN FORM-->
								<h3></h3>
								<div class="alert alert-error hide">
									<button class="close" data-dismiss="alert"></button>
									錯誤！請完成所有必填項目
								</div>
								<div class="alert alert-success hide">
									<button class="close" data-dismiss="alert"></button>
									Your form validation is successful!
								</div>
								<div class="control-group" id="import_column">
									<label class="control-label">匯入欄位</label>
									<div class="controls" >
										<button class="btn green" type="button" name="add_column" id="add_column" class="btn" onclick="create()">增加匯入欄位 <i class="icon-plus"></i></button>
									</div>
								</div>
								<?php foreach($origKey as $index => $row){?>
									<div class="control-group" id="origImport<?php echo $index;?>">
										<!-- <label class="control-label">來源頁數<span class="required">*</span></label> -->
										<div class="controls">
											<button class="btn red icn-only" type="button" name="delegoods1" id="delegoods" class="btn" onclick="delOrigtxt(<?php echo $index;?>)"><i class="icon-remove"></i></button>
											&nbsp;&nbsp;受測者資料：
											<select class="" name="competence_data[]" id="origData<?php echo $index;?>" data-placeholder="請選擇" style="width: 30%" onchange="showOrigImg(<?php echo $index;?>)">
												<option value=""></option>
												<?php foreach($keyName as $key => $row2){?>
													<option value="<?php echo $key;?>" <?php if($row == $key){echo 'selected';}?>><?php echo $row2.':'.$value[$key];?></option>
												<?php }?>
											</select>
											<!-- <input type="text" id='dataIndex' style="display: none;"> -->
											&nbsp;&nbsp;圖片：
											<img id='selectOrigImg<?php echo $index;?>' src="<?php echo $origSrc[$row];?>" height="100px" width="100px">
											&nbsp;&nbsp;目標頁數：
											<input type="text" name="destPage[]" value="<?php echo $origPage[$index];?>" type="text" style=""/><span id="countMsg"></span>
											&nbsp;&nbsp;目標欄：
											<input type="text" name="destCol[]" value="<?php echo $origCol[$index];?>" type="text" style=""/><span id="countMsg"></span>
										</div>
									</div>
								<?php }?>
								<div class="control-group" id="import" style="display: none">
									<!-- <label class="control-label">來源頁數<span class="required">*</span></label> -->
									<div class="controls">
										<button class="btn red icn-only" type="button" name="delegoods1" id="delegoods" class="btn" onclick="deltxt(0)"><i class="icon-remove"></i></button>
										&nbsp;&nbsp;受測者資料：
										<select class="" name="data" id="competence_data" data-placeholder="請選擇" style="width: 30%">
											<option value=""></option>
											<?php foreach($keyName as $key => $row){?>
												<option value="<?php echo $key;?>"><?php echo $row.':'.$value[$key];?></option>
											<?php }?>
										</select>
										<!-- <input type="text" id='dataIndex' style="display: none;"> -->
										&nbsp;&nbsp;圖片：
										<img id='selectImg' src="" height="100px" width="100px">
										&nbsp;&nbsp;目標頁數：
										<input type="text" name="" id="destPage" type="text" style=""/><span id="countMsg"></span>
										&nbsp;&nbsp;目標欄：
										<input type="text" name="" id="destCol" type="text" style=""/><span id="countMsg"></span>
									</div>
									<div class="control-group" style='display: none;'>
										<label class="control-label"></label>
										<div class="controls">
											<input type="text" name="id" value="<?php echo $upid;?>" >
										</div>
									</div>
								</div>
								<div class="form-actions">
									<button type="submit" class="btn green">
										儲存 <div class="icon-save"></div>
									</button>
									<a href="<?php echo base_url('rgms_report_2/excel_template_2/print_data');?>">
										<button type="button" class="btn">資料結構檢視 <div class="icon-eye"></div></button>
									</a>
									<a href="<?php echo base_url('rgms_report_2/excel_template_2');?>">
										<button type="button" class="btn">取消 <div class="icon-undo"></div></button>
									</a>
								</div>
								<!-- END FORM-->
							</div>
						</div>
						<!-- END VALIDATION STATES-->
					</div>
				</div>
			</form>
			<!-- END DASHBOARD STATS -->
			<div class="clearfix"></div>
		</div>
	</div>
	<!-- END PAGE CONTAINER-->    
</div>
<!-- END PAGE -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<div class="footer">
	<?php echo $footer; ?>
</div>
<!-- END FOOTER -->
<?php echo $script; ?>
<script src="<?php echo base_url('public/globel/js/form-samples.js')?>" type="text/javascript"></script> 
<script src="<?php echo base_url('public/globel/js/select2.min.js')?>" type="text/javascript"></script>
<script src="<?php echo base_url('public/globel/js/jquery.validate.min.js')?>" type="text/javascript"></script>
<script src="<?php echo base_url('public/globel/plugin/main/excel_template-validation.js')?>" type="text/javascript"></script>
<!-- BEGIN PAGE LEVEL PLUGINS -->

<!-- BEGIN RULE -->
<!-- END RULE -->
<script>
	jQuery(document).ready(function() {    
		FormValidation.init();
		FormSamples.init();
	});
</script>

<script>
	$("select[name='competence_data[]']").select2({
		placeholder: "Search your Tool",
		allowClear: true
	});
  //set the default value
  var txtId = 0;
  // document.getElementById("test").innerHTML = ;
  count = new Array();
  var arraylength = 0;
  //add input block in showBlock
  $("#add_column").click(function(){
  	txtId ++;
  	$copy = $("#import").clone(true).attr("id","import" + txtId).attr("style","display:block");
  	$("#import_column").append($copy);
  	$copy.find("#competence_data").attr("name","competence_data[]").attr("value","").attr("id","competence_data" + txtId).attr('onchange','showImg('+txtId+')').select2({
  		placeholder: "Search your Tool",
  		allowClear: true
  	});
  	$copy.find("#destPage").attr("name","destPage[]").attr("value","").attr("id","destPage" + txtId);
  	$copy.find("#destPageName").attr("name","destPageName[]").attr("value","").attr("id","destPageName" + txtId);
  	$copy.find("#destCol").attr("name","destCol[]").attr("value","").attr("id","destCol" + txtId);
  	$copy.find("#selectImg").attr("name","selectImg"+txtId).attr("value","").attr("id","selectImg" + txtId);
  	$copy.find("#delegoods").attr("onclick","deltxt(" + txtId + ");").attr('id','delegoods'+txtId);
  });

  //remove div
  function deltxt(id) {
  	$("#import" + id).remove();
  }

  $('#other').click(function() {
  	$('#target').submit();
  });

  function delOrigtxt(id) {
  	$("#origImport" + id).remove();
  }

  $('#other').click(function() {
  	$('#target').submit();
  });

  function showImg(id){
  	selectValue = $('#competence_data'+id+' option:selected').text();
  	if(selectValue.substr(-3) == 'png' || selectValue.substr(-3) == 'jpg'){
  		src = selectValue.split(':');
  		$('#selectImg'+id).attr('src',src[1]+':'+src[2]);
  	}
  	else{
  		$('#selectImg'+id).attr('src','');
  	}
  }

  function showOrigImg(id){
  	selectValue = $('#origData'+id+' option:selected').text();

  	if(selectValue.substr(-3) == 'png' || selectValue.substr(-3) == 'jpg'){
  		src = selectValue.split(':');
  		$('#selectOrigImg'+id).attr('src',src[1]+':'+src[2]);
  	}
  	else{
  		$('#selectOrigImg'+id).attr('src','');
  	}
  }
</script>

</body>
<!-- END BODY -->
</html>