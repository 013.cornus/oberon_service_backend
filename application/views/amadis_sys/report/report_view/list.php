		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">    
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3 class="page-title">
							<?php echo $title; ?> <small><?php echo $title_small; ?></small>
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="<?php echo base_url('home'); ?>">首頁</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li><a href="<?php echo $this->router->fetch_class();?>"><?php echo $title_small; ?></a></li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<div id="dashboard">
					<!-- BEGIN ALTER -->
					<?php echo $this->session->flashdata('messagediv');?>
					<!-- END ALTER -->
					<!-- BEGIN DASHBOARD STATS -->
					<!-- BEGIN PAGE CONTENT-->
					<div class="row-fluid">
						<div class="span12">
							<div class="portlet box blue" id="form_wizard_1">
								<!-- <div class="portlet-title">
									<div class="caption">
										<i class="icon-search"></i> 搜尋列</span>
									</div>
									<div class="tools hidden-phone">
										<a href="javascript:;" class="collapse"></a>
										<a href="#portlet-config" data-toggle="modal" class="config" style="display: none;"></a>
										<a href="javascript:;" class="reload" style="display: none;"></a>
										<a href="javascript:;" class="remove" style="display: none;"></a>
									</div>
								</div> -->
								<!-- <div class="portlet-body form">
									<form method="post" action="<?php echo base_url('goods/list')?>">
										<div class="control-group">
											<label class="control-label">搜尋關鍵字：</label>
											<div class="controls">
												&nbsp;&nbsp;
												<input type="text" class="span6 m-wrap" name="fullname" id="fullname" placeholder="請輸入關鍵字 / 商品名稱 /" />

												<button type="submit" class="btn green">
													搜尋 <i class="icon-circle-arrow-right"></i>
												</button>            
											</div>
										</div>
									</form>
								</div> -->
							</div>
						</div>
					</div>
					<!-- END PAGE CONTENT-->
					<div class="row-fluid">

						<div class="span12">
							<!-- BEGIN EXAMPLE TABLE PORTLET-->
							<div class="portlet box light-grey">
								<div class="portlet-title">
									<div class="caption"><i class="icon-globe"></i><?php echo $title_small; ?></div>
									<div class="tools">
										<a href="javascript:;" class="collapse"></a>
										<a href="#portlet-config" data-toggle="modal" class="config" style="display: none;"></a>
										<a href="javascript:;" class="reload" style="display: none;"></a>
										<a href="javascript:;" class="remove" style="display: none;"></a>
									</div>
								</div>
								<div class="portlet-body">
									<div class="clearfix">
										<!-- <div class="btn-group">
											<a href="<?php echo base_url('goods/create_form');?>">
												<button id="sample_editable_1_new" class="btn green">
													新增 <i class="icon-plus"></i>
												</button>
											</a>
										</div> -->
									</div>
									<div style="overflow:scroll; overflow:auto;">
										<table class="table table-striped table-bordered table-hover" id="sample_1" style="min-width: 768px;">
											<thead>
												<tr>
													<!-- <th style="width:8px;">
														<input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" />
													</th> -->
													<th>項次</th>
													<th>受測者</th>
													<th>受測日期</th>
													<th>動作</th>
												</tr>
											</thead>
											<tbody>
												<?php $itemNum = 0; foreach ($result as $key => $row) {?>
													<tr class="odd">
														<td><?php $itemNum++; echo $itemNum;?></td>
														<td><?php echo $row['name'];?></td>
														<td><?php echo date('Y-m-d H:i:s',$row["date"]);?></td>
														<td>
															<a href="<?php echo $link[$key];?>" target='_blank' class="btn gray mini">檢視 <i class="icon-eye-open"></i>
															</a>
														</td>
													</tr>
												<?php } ?>
											</tbody>
										</table>
									</div>
									<!-- <div class="row-fluid">
										<?php echo $page_list;?>
									</div> -->

									<!-- 跳到指定頁數 -->
									<!-- <div class="row-fluid">
										<div class="control-group pull-right">						
											<form id="pagejump" action="<?php echo base_url('rgms_report_2/report_view_2/pagejump');?>" class="pull-right" method="post">
												<div class="controls">
													跳轉至<input type="text" id="pagenum" name="pagenum"  class="m-wrap" style="width: 30px">頁
													&nbsp;<button class="btn red icn-only">確認</button>
												</div>	
											</form>	
										</div>
									</div> -->
									<!-- 跳到指定頁數 -->
								</div>
							</div>

							<!-- 警告視窗 -->
							<!-- BEGIN CONFIRM WINDOW -->
							<div id="confirm_window" class="modal hide fade " role="dialog" data-keyboard="false" style="top:20%">
								<div class="modal-header red">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
									<h4><div class="icon-warning-sign"></div> 警告</h4>
								</div>
								<form>
									<div class="modal-body">
										<p id='confirm_text'>警告文字</p>
									</div>
									<div class="modal-footer">
										<button type="button" data-dismiss="modal" class="btn">取消 <div class="icon-undo"></div></button>
										<button id="confirm_btn" type="button" data-dismiss="modal" class="btn red" >確認按鈕<div class="icon-check"></div></button>
									</div>
								</form>
							</div>
							<!-- END CONFIRM WINDOW -->


							<!-- END EXAMPLE TABLE PORTLET-->
						</div>
					</div>
					<!-- END DASHBOARD STATS -->
					<div class="clearfix"></div>
				</div>
			</div>
			<!-- END PAGE CONTAINER-->    
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
	<div class="footer">
		<?php echo $footer; ?>
	</div>

	<!-- END FOOTER -->
	<?php echo $script; ?>
</body>
<script type="text/javascript">
	//$link為欲前往連結
	//$confirm_text為警告視窗文字
	//$btn_text為確認按鈕文字
	function alert_window($link,$confirm_text,$btn_text){
		$("#confirm_text").text($confirm_text);
		$("#confirm_btn").text($btn_text);
		$("#confirm_btn").attr('onclick',"javascript:location.href='"+$link+"'");
	}
</script>
<!-- END BODY -->
</html>