		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">    
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3 class="page-title">
							<?php echo $title; ?> <small><?php echo $title_small; ?></small>
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="<?php echo base_url('home');?>">首頁</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li>
								<a href="<?php echo base_url($this->router->fetch_class()); ?>"><?php echo $title; ?></a>
								<i class="icon-angle-right"></i>
							</li>
							<li><a href="#"><?php echo $title_small; ?></a></li>
							<li class="pull-right no-text-shadow" style="display: none;">
								<div id="dashboard-report-range" class="dashboard-date-range tooltips no-tooltip-on-touch-device responsive" data-tablet="" data-desktop="tooltips" data-placement="top" data-original-title="Change dashboard date range">
									<i class="icon-calendar"></i>
									<span></span>
									<i class="icon-angle-down"></i>
								</div>
							</li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<form action="<?php echo base_url('rgms_report_2/rgms_report_2/create');?>" class="form-horizontal"  method="post" enctype="multipart/form-data">
					<div class="row-fluid">
						<div class="span12">
							<!-- BEGIN VALIDATION STATES-->
							<div class="portlet box green">
								<div class="portlet-title">
									<div class="caption"><i class="icon-reorder"></i>模板選擇</div>
									<div class="tools">
										<a href="javascript:;" class="collapse"></a>
									<!-- <a href="#portlet-config" data-toggle="modal" class="config"></a>
									<a href="javascript:;" class="reload"></a>
									<a href="javascript:;" class="remove"></a> -->
								</div>
							</div>	
							<div class="portlet-body form">
								<!-- BEGIN FORM-->
								<h3></h3>
								<div class="alert alert-error hide">
									<button class="close" data-dismiss="alert"></button>
									錯誤！請完成所有必填項目
								</div>
								<div class="alert alert-success hide">
									<button class="close" data-dismiss="alert"></button>
									Your form validation is successful!
								</div>
								<div class="control-group">
									<label class="control-label">是否透過模板設定產出</label>
									<div class="controls">
										<label class="radio">
											<input type="radio" name="importByTemplate" value="1" checked />
											是
										</label>
										<label class="radio">
											<input type="radio" name="importByTemplate" value="0" />
											否
										</label>  
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">模板選擇</label>
									<div class="controls">
										<select class="span6 select2_category" name="template" id="template" data-placeholder="請選擇">
											<option value=""></option>
											<?php foreach($template as $key => $row){?>
												<option value="<?php echo $row['excel_template_id'];?>"><?php echo $row["excel_template_name"];?></option>
											<?php }?>
										</select>
									</div>
								</div>
								<!-- END FORM-->
							</div>
						</div>
						<!-- END VALIDATION STATES-->
					</div>
				</div>
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN VALIDATION STATES-->
						<div class="portlet box green">
							<div class="portlet-title">
								<div class="caption"><i class="icon-reorder"></i><?php echo $title_small; ?></div>
								<div class="tools">
									<a href="javascript:;" class="collapse"></a>
									<!-- <a href="#portlet-config" data-toggle="modal" class="config"></a>
									<a href="javascript:;" class="reload"></a>
									<a href="javascript:;" class="remove"></a> -->
								</div>
							</div>	
							<div class="portlet-body form">
								<!-- BEGIN FORM-->
								<h3></h3>
								<div class="alert alert-error hide">
									<button class="close" data-dismiss="alert"></button>
									錯誤！請完成所有必填項目
								</div>
								<div class="alert alert-success hide">
									<button class="close" data-dismiss="alert"></button>
									Your form validation is successful!
								</div>
									<div class="control-group">
										<label class="control-label">受測者</label>
										<div class="controls">
											<select class="span6 select2_category" name="source_data_id" id="source_data_id" data-placeholder="請選擇" onchange="addSelection();inputDetectionDate(this.options[this.selectedIndex].text)">
												<option value=""></option>
												<?php foreach($item as $key => $row){?>
													<option value="<?php echo $row['case'];?>"><?php echo $row["name"]."(".date('Y-m-d H:i:s',$row["date"]).")";?></option>
												<?php }?>
											</select>
										</div>
									</div>

									<div class="control-group" id="import_column">
										<label class="control-label">匯入欄位</label>
										<div class="controls" >
											<button class="btn green" type="button" name="add_column" id="add_column" class="btn" onclick="create()">增加匯入欄位 <i class="icon-plus"></i></button>
										</div>
									</div>
									<div class="control-group" id="import" style="display: none">
										<!-- <label class="control-label">來源頁數<span class="required">*</span></label> -->
										<div class="controls">
											<button class="btn red icn-only" type="button" name="delegoods1" id="delegoods" class="btn" onclick="deltxt(0)"><i class="icon-remove"></i></button>
											&nbsp;&nbsp;受測者資料：
											<select class="" name="data" id="competence_data" data-placeholder="請選擇" style="width: 30%">
												<option value=""></option>
											</select>
											<!-- <input type="text" id='dataIndex' style="display: none;"> -->
											&nbsp;&nbsp;圖片：
											<img id='selectImg' src="" height="100px" width="100px">
											&nbsp;&nbsp;目標頁數：
											<input type="text" name="" id="destPage" type="text" style=""/><span id="countMsg"></span>
											&nbsp;&nbsp;目標欄：
											<input type="text" name="" id="destCol" type="text" style=""/><span id="countMsg"></span>
										</div>
									</div>
									<input type="text" name="date" id="date" type="text" style="display: none"/>
									<div class="form-actions">
										<button type="submit" class="btn green">
											新增 <div class="icon-save"></div>
										</button>
										<a href="<?php echo base_url('rgms_report_2/rgms_report_2/print_data');?>">
											<button type="button" class="btn">資料結構檢視 <div class="icon-eye"></div></button>
										</a>
										<a href="<?php echo base_url('rgms_report_2/rgms_report_2');?>">
											<button type="button" class="btn">取消 <div class="icon-undo"></div></button>
										</a>
									</div>
									<!-- END FORM-->
								</div>
							</div>
							<!-- END VALIDATION STATES-->
						</div>
					</div>
				</form>
				<!-- END DASHBOARD STATS -->
				<div class="clearfix"></div>
			</div>
		</div>
		<!-- END PAGE CONTAINER-->    
	</div>
	<!-- END PAGE -->
</div>
<!-- END CONTAINER -->

<!-- BEGIN FOOTER -->
<div class="footer">
	<?php echo $footer; ?>
</div>
<!-- END FOOTER -->
<?php echo $script; ?>
<script src="<?php echo base_url('public/globel/js/form-samples.js')?>" type="text/javascript"></script> 
<script src="<?php echo base_url('public/globel/js/select2.min.js')?>" type="text/javascript"></script>
<script src="<?php echo base_url('public/globel/js/jquery.validate.min.js')?>" type="text/javascript"></script>
<script src="<?php echo base_url('public/globel/plugin/main/goods-validation.js')?>" type="text/javascript"></script>

<script>
	jQuery(document).ready(function() {    
		FormValidation.init();
		FormSamples.init();
	});
</script>
<!-- BEGIN RULE -->

<script>
  //set the default value
  var txtId = 0;
  // document.getElementById("test").innerHTML = ;
  count = new Array();
  var arraylength = 0;
  //add input block in showBlock
  $("#add_column").click(function(){
  	txtId ++;
  	$copy = $("#import").clone(true).attr("id","import" + txtId).attr("style","display:block");
  	$("#import_column").append($copy);
  	$copy.find("#competence_data").attr("name","competence_data[]").attr("value","").attr("id","competence_data" + txtId).attr('onchange','showImg('+txtId+')').select2({
  		placeholder: "Search your Tool",
  		allowClear: true
  	});
  	$copy.find("#destPage").attr("name","destPage[]").attr("value","").attr("id","destPage" + txtId);
  	$copy.find("#destPageName").attr("name","destPageName[]").attr("value","").attr("id","destPageName" + txtId);
  	$copy.find("#destCol").attr("name","destCol[]").attr("value","").attr("id","destCol" + txtId);
  	$copy.find("#selectImg").attr("name","selectImg"+txtId).attr("value","").attr("id","selectImg" + txtId);
  	// $copy.find("#dataTypeSelect").attr("name","dataType[]").attr("value","general").attr("id","dataType" + txtId).select2({
  	// 	placeholder: "Search your Tool",
  	// 	allowClear: true
  	// });
  	$copy.find("#delegoods").attr("onclick","deltxt(" + txtId + ");").attr('id','delegoods'+txtId);
  });

  //remove div
  function deltxt(id) {
  	$("#import" + id).remove();
  }

  $('#other').click(function() {
  	$('#target').submit();
  });

  function addSelection(){
  	// alert($("select[name='source_data_id']").val());
  	var mainselected = $("#source_data_id").val();
  	$.ajax({
  		url:"<?php echo base_url('rgms_report_2/rgms_report_2/createSelection');?>",				
  		method:"POST",
  		dataType:"json",
  		data:{
  			mainselected:mainselected
  		},					
  		success:function(res){
  			$("select[name='competence_data[]']").each(function(){
  				$(this).empty();
  			});
  			$("select[name='data']").empty();
  			var NumOfData = res['keyName'].length;
  			for (var i = 0; i < NumOfData; i++) {
  				$("select[name='competence_data[]']").each(function(){
  					$(this).append("<option value='"+res['value'][i]+"'>"+res['keyName'][i]+':'+res['value'][i]+"</option>");
  				});
  				$("select[name='data']").append("<option value='"+res['value'][i]+"'>"+res['keyName'][i]+':'+res['value'][i]+"</option>");
  			}
  		},
  		error : function(xhr, ajaxOptions, thrownError){
  			// $("portlet-body").append(xhr.status);
  			// $("body").append(xhr.responseText );

  			// alert(thrownError);
  		}
  	})
  }

  function showImg(id){
  	selectValue = $('#competence_data'+id).val();
  	if(selectValue.substr(-3) == 'png' || selectValue.substr(-3) == 'jpg'){
  		$('#selectImg'+id).attr('src',selectValue);
  	}
  	else{
  		$('#selectImg'+id).attr('src','');
  	}
  }

  function inputDetectionDate(date){
  	$("#date").val(date.substr(-20,19));
  }
</script> 

<!-- END RULE -->
</body>
<!-- END BODY -->
</html>