		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">    
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3 class="page-title">
							<?php echo $title; ?> <small><?php echo $title_small; ?></small>
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="<?php echo base_url('home');?>">首頁</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li>
								<a href="<?php echo base_url($this->router->fetch_class()); ?>"><?php echo $title; ?></a>
								<i class="icon-angle-right"></i>
							</li>
							<li><a href="#"><?php echo $title_small; ?></a></li>
							<li class="pull-right no-text-shadow" style="display: none;">
								<div id="dashboard-report-range" class="dashboard-date-range tooltips no-tooltip-on-touch-device responsive" data-tablet="" data-desktop="tooltips" data-placement="top" data-original-title="Change dashboard date range">
									<i class="icon-calendar"></i>
									<span></span>
									<i class="icon-angle-down"></i>
								</div>
							</li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN VALIDATION STATES-->
						<div class="portlet box green">
							<div class="portlet-title">
								<div class="caption"><i class="icon-reorder"></i><?php echo $title_small; ?></div>
								<div class="tools">
									<a href="javascript:;" class="collapse"></a>
									<!-- <a href="#portlet-config" data-toggle="modal" class="config"></a>
									<a href="javascript:;" class="reload"></a>
									<a href="javascript:;" class="remove"></a> -->
								</div>
							</div>
							<div class="portlet-body form">
								<!-- BEGIN FORM-->
								<h3></h3>
								<form action="<?php echo base_url('excel_create/create');?>" class="form-horizontal"  method="post" enctype="multipart/form-data">
									<div class="alert alert-error hide">
										<button class="close" data-dismiss="alert"></button>
										錯誤！請完成所有必填項目
									</div>
									<div class="alert alert-success hide">
										<button class="close" data-dismiss="alert"></button>
										Your form validation is successful!
									</div>
									<!-- <div class="control-group">
										<label class="control-label">excel匯入<span class="required">*</span></label>
										<div class="controls">
											<input name="excel" id="excel" type="file" class="span6 m-wrap"/><span id="productnameMsg"></span>
										</div>
									</div> -->
									<div class="control-group" id="goods">
										<label class="control-label">匯入欄位<span class="required">*</span></label>
										<div class="controls" >
											<button class="btn green" type="button" name="addgoods" id="addgoods" class="btn" onclick="create()">增加匯入欄位 <i class="icon-plus"></i></button>
										</div>
										<div class="control-group"></div>
									</div>
									<div class="control-group" id="import" style="display: none">
										<!-- <label class="control-label">來源頁數<span class="required">*</span></label> -->
										<div class="controls">
											<button class="btn red icn-only" type="button" name="delegoods1" id="delegoods" class="btn" onclick="deltxt(0)"><i class="icon-remove"></i></button>
											&nbsp;&nbsp;來源頁數：
											<input name="" id="origPage" type="text" class="span1 m-wrap"/><span id="productnameMsg"></span>
											&nbsp;&nbsp;來源欄(英文)：
											<input type="text" name="" id="origCol" type="text" class="span1 m-wrap" style=""/><span id="countMsg"></span>
											&nbsp;&nbsp;來源列(數字)：
											<input type="text" name="" id="origRow" type="text" class="span1 m-wrap" style=""/><span id="countMsg"></span>
											&nbsp;&nbsp;目標頁數(數字)：
											<input type="text" name="" id="destPage" type="text" class="span1 m-wrap" style=""/><span id="countMsg"></span>
											<!-- &nbsp;&nbsp;目標頁數(名稱)：
											<input type="text" name="" id="destPageName" type="text" class="span1 m-wrap" style=""/><span id="countMsg"></span> -->
											&nbsp;&nbsp;目標欄(英文)：
											<input type="text" name="" id="destCol" type="text" class="span1 m-wrap" style=""/><span id="countMsg"></span>
											&nbsp;&nbsp;目標列(數字)：
											<input type="text" name="" id="destRow" type="text" class="span1 m-wrap" style=""/><span id="countMsg"></span>
										</div>
									</div>
									<div class="form-actions">
										<button type="submit" class="btn green">
											新增 <div class="icon-save"></div>
										</button>
										<a href="<?php echo base_url('goods');?>">
											<button type="button" class="btn">取消 <div class="icon-undo"></div></button>
										</a>
									</div>
								</form>
								<!-- END FORM-->
							</div>
						</div>
						<!-- END VALIDATION STATES-->
					</div>
				</div>
				<!-- END DASHBOARD STATS -->
				<div class="clearfix"></div>
			</div>
		</div>
		<!-- END PAGE CONTAINER-->    
	</div>
	<!-- END PAGE -->
</div>
<!-- END CONTAINER -->

<!-- BEGIN FOOTER -->
<div class="footer">
	<?php echo $footer; ?>
</div>
<!-- END FOOTER -->
<?php echo $script; ?>
<script src="<?php echo base_url('public/globel/js/form-samples.js')?>" type="text/javascript"></script> 
<script src="<?php echo base_url('public/globel/js/select2.min.js')?>" type="text/javascript"></script>
<script src="<?php echo base_url('public/globel/js/jquery.validate.min.js')?>" type="text/javascript"></script>
<script src="<?php echo base_url('public/globel/plugin/main/goods-validation.js')?>" type="text/javascript"></script>

<script>
	jQuery(document).ready(function() {    
		FormValidation.init();
		FormSamples.init();
	});
</script>
<!-- BEGIN RULE -->

<script>
  //set the default value
  var txtId = 0;
  // document.getElementById("test").innerHTML = ;
  count = new Array();
  var arraylength = 0;
  //add input block in showBlock
  $("#addgoods").click(function(){

  	txtId ++;
  	$copy = $("#import").clone(true).attr("id","import" + txtId).attr("style","display:block");
  	$("#goods").append($copy);
  	$copy.find("#origPage").attr("name","origPage[]").attr("value","").attr("id","origPage" + txtId);
  	$copy.find("#origCol").attr("name","origCol[]").attr("value","").attr("id","origCol" + txtId);
  	$copy.find("#origRow").attr("name","origRow[]").attr("value","").attr("id","origRow" + txtId);
  	$copy.find("#destPage").attr("name","destPage[]").attr("value","").attr("id","destPage" + txtId);
  	$copy.find("#destPageName").attr("name","destPageName[]").attr("value","").attr("id","destPageName" + txtId);
  	$copy.find("#destCol").attr("name","destCol[]").attr("value","").attr("id","destCol" + txtId);
  	$copy.find("#destRow").attr("name","destRow[]").attr("value","").attr("id","destRow" + txtId);
  	$copy.find("#delegoods").attr("onclick","deltxt(" + txtId + ");").attr('id','delegoods'+txtId);
  });

  //remove div
  function deltxt(id) {
  	$("#import" + id).remove();
  }

  $('#other').click(function() {
  	$('#target').submit();
  });
</script> 

<!-- END RULE -->
</body>
<!-- END BODY -->
</html>