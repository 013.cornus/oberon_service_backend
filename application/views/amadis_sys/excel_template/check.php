		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3 class="page-title">
							<?php echo $title; ?> <small><?php echo $title_small; ?></small>
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="<?php echo base_url('home');?>">首頁</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li>
								<a href="<?php echo base_url($this->router->fetch_class()); ?>"><?php echo $title; ?></a>
								<i class="icon-angle-right"></i>
							</li>
							<li><a href="#"><?php echo $title_small; ?></a></li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN VALIDATION STATES-->
						<div class="portlet box yellow">
							<div class="portlet-title">
								<div class="caption"><i class="icon-search"></i><?php echo $title_small; ?></div>
								<div class="tools">
									<a href="javascript:;" class="collapse"></a>
									<!-- <a href="#portlet-config" data-toggle="modal" class="config"></a>
									<a href="javascript:;" class="reload"></a>
									<a href="javascript:;" class="remove"></a> -->
								</div>
							</div>
							<div class="portlet-body form">
								<!-- BEGIN FORM-->
								<h3 class="form-section">
									<i class="icon-th-list"></i> 商品資訊 
								</h3>
								<div class="form-horizontal form-view">
									<form action="#" class="form-horizontal form-bordered form-row-stripped">
										<div class="control-group">
											<label class="control-label">商品名稱</label>
											<div class="controls">
												<span class="text"><?php echo $result['goods_name']; ?></span>
											</div>
										</div>
										<div class="control-group">
											<label class="control-label" >商品單價</label>
											<div class="controls">    
												<span class="text"><?php echo $result['goods_a_price'];?></span>
											</div>
										</div>
										<div class="control-group">
											<label class="control-label" >業績獎金PV</label>
											<div class="controls">    
												<span class="text"><?php echo $result['goods_a_pv'];?></span>
											</div>
										</div>
										<!-- 各商品單位 -->
										
										<div class="form-actions">
											<a href="<?php echo base_url('goods');?>">
												<button type="button" class="btn">返回<div class="icon-undo"></div></button>
											</a>
										</div>
									</form>
									<!-- END FORM-->  
								</div>
							</div>

						</div>
					</div>
					<div class="portlet-body form" style="display: none;">
						<!-- BEGIN FORM-->
						<div class="form-horizontal form-view">
							<h3 class="form-section">訂單資訊</h3>
							<div class="row-fluid">
								<div class="span6 ">
									<div class="control-group">
										<label class="control-label" for="firstName">訂單編號：</label>
										<div class="controls">
											<span class="text"><?php foreach($result as $row){echo $row['order_number'];}?></span>
										</div>
									</div>
								</div>
								<!--/span-->
								<div class="span6 ">
									<div class="control-group">
										<label class="control-label" for="lastName">付款別：</label>
										<div class="controls">
											<span class="text"><?php foreach($result as $row){if($row['order_pay_type'] == 0){echo "現金";} else{echo "信用卡";}}?></span>
										</div>
									</div>
								</div>
								<!--/span-->
							</div>
							<!--/row-->
							<div class="row-fluid">
								<div class="span6 ">
									<div class="control-group">
										<label class="control-label" >客戶姓名：</label>
										<div class="controls">
											<span class="text"><</span>
										</div>
									</div>
								</div>
								<!--/span-->
								<div class="span6 ">
									<div class="control-group">
										<label class="control-label" >合約生效日：</label>
										<div class="controls">
											<span class="text bold"><?php foreach($result as $row){echo $row['order_active_date'];}?></span>
										</div>
									</div>
								</div>
								<!--/span-->
							</div>
							<!--/row-->        
							<div class="row-fluid">
								<div class="span6 ">
									<div class="control-group">
										<label class="control-label" >經辦人：</label>
										<div class="controls">
											<span class="text bold"><?php foreach($result as $row){echo $row['employee_name'];}?></span>
										</div>
									</div>
								</div>
								<!--/span-->
								<div class="span6 ">
									<div class="control-group">
										<label class="control-label" >合約到期日：</label>
										<div class="controls">   
											<span class="text bold"><?php foreach($result as $row){echo $row['order_over_date'];}?></span>
										</div>
									</div>
								</div>
								<!--/span-->
							</div>
							<!--/row-->
							<div class="row-fluid">
								<div class="span6 ">
									<div class="control-group" style="display: none">
										<label class="control-label" >:</label>
										<div class="controls">
											<span class="text bold"></span>
										</div>
									</div>
								</div>
								<!--/span-->
								<div class="span6 ">
									<div class="control-group">
										<label class="control-label" >合約簽收日：</label>
										<div class="controls">
											<span class="text bold"><?php foreach($result as $row){echo $row['order_sign_date'];}?></span>
										</div>
									</div>
								</div>
								<!--/span-->
							</div>
							<!--/row-->                   
							<h3 class="form-section">訂單商品資訊</h3>
							<?php foreach($orderinfo as $r){if($r['order_id'] == $upid){if($r['order_info_is_del'] == 0){?>
							<div class="row-fluid">
								<div class="span5">
									<div class="control-group">
										<label class="control-label" >商品項目：</label>
										<div class="controls">
											<?php foreach($goods as $d){if($r['goods_id']==$d['goods_id']){?>
											<span class="text"><?php echo $d['goods_name'];?></span>
											<?php }}?>
										</div>
									</div>
								</div>
								<!--/span-->
								<div class="span1">
									<div class="control-group">
										<label class="control-label">承購單位：</label>
										<div class="controls">
											<span class="text"><?php echo $r['order_info_count'];?></span>
										</div>
									</div>
								</div>
								<!--/span-->
								<div class="span1">
									<div class="control-group">
										<label class="control-label">規格：</label>
										<div class="controls">
											<span class="text"><?php echo $r['order_info_pair'];?></span>
										</div>
									</div>
								</div>
								<!--/span-->
								<div class="span3">
									<div class="control-group">
										<label class="control-label" >轉單：</label>
										<div class="controls">
											<span class="text"><?php if($r['order_info_change_type'] == 0){echo "無";} else if($r['order_info_change_type'] == 1){echo "單轉單";} else{echo "錢轉單";}?></span>
										</div>
									</div>
								</div>
								<!--/span-->
							</div>
							<!--/row-->    
							<?php }}}?>     
							<div class="form-actions">
								<button  class="btn yellow" data-toggle="modal" href="#pass" <?php foreach($result as $r){if($r['order_is_check'] != 0){echo "style='display:none'";}}?>>
									通過 <div class="icon-save"></div>
								</button>
								<button  class="btn red" data-toggle="modal" name="id" value="<?php echo $upid;?>"  href="#reject" <?php foreach($result as $r){if($r['order_is_check'] != 0){echo "style='display:none'";}}?>>
									否決 <div class="icon-save"></div>
								</button>
								<a href="<?php echo base_url('order');?>">
									<button type="button" class="btn"><?php foreach($result as $r){if($r['order_is_check'] != 0){echo "返回";} else{echo "取消";}}?> <div class="icon-undo"></div></button>
								</a>
							</div>
						</div>
						<!-- END FORM-->  
					</div>
					<!-- END VALIDATION STATES-->
				</div>
			</div>
			<!-- END DASHBOARD STATS -->
			<div class="clearfix"></div>
		</div>
	</div>
	<!-- BEGIN CONFIRM PASS WINDOW -->
	<div id="pass" class="modal hide fade" tabindex="-1" data-backdrop="static" data-keyboard="false" style="top:20%">
		<div class="modal-header red">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
			<h4><div class="icon-warning-sign"></div> 警告</h4>
		</div>
		<div class="modal-body">
			<p>確定要通過這筆訂單嗎?</p>
		</div>
		<div class="modal-footer">
			<button type="button" data-dismiss="modal" class="btn">取消 <div class="icon-undo"></div></button>
			<button id="delete" type="button" data-dismiss="modal" class="btn red" onclick="javascript:location.href='<?php echo base_url('order/check?id=').$upid;?>'">確定通過 <div class="icon-trash"></div></button>
		</div>
	</div>
	<!-- END CONFIRM PASS WINDOW -->
	<!-- BEGIN CONFIRM REJECT WINDOW -->
	<div id="reject" class="modal hide fade" tabindex="-1" data-backdrop="static" data-keyboard="false" style="top:20%">
		<div class="modal-header red">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
			<h4><div class="icon-warning-sign"></div> 警告</h4>
		</div>
		<div class="modal-body">
			<p>確定要否決這筆訂單嗎?</p>
		</div>
		<div class="modal-footer">
			<button type="button" data-dismiss="modal" class="btn">取消 <div class="icon-undo"></div></button>
			<button id="delete" type="button" data-dismiss="modal" class="btn red" onclick="javascript:location.href='<?php echo base_url('order/reject?id=').$upid;?>'">確定否決 <div class="icon-trash"></div></button>
		</div>
	</div>
	<!-- END CONFIRM REJECT WINDOW -->
	<!-- END PAGE CONTAINER-->    
</div>
<!-- END PAGE -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<div class="footer">
	<?php echo $footer; ?>
</div>
<!-- END FOOTER -->
<?php echo $script; ?>
<!-- BEGIN RULE -->
<!-- END RULE -->
<script>
	jQuery(document).ready(function() {       
			// App.init();
			TableManaged.init();
			FormWizard.init();
		});
	</script>
</body>
<!-- END BODY -->
</html>