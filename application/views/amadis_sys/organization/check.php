		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3 class="page-title">
							<?php echo $title; ?> <small><?php echo $title_small; ?></small>
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="<?php echo base_url('home');?>">首頁</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li>
								<a href="<?php echo base_url($this->router->fetch_class()); ?>"><?php echo $title; ?></a>
								<i class="icon-angle-right"></i>
							</li>
							<li><a href="#"><?php echo $title_small; ?></a></li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN VALIDATION STATES-->
						<div class="portlet box yellow">
							<div class="portlet-title">
								<div class="caption"><i class="icon-user"></i><?php echo $title_small; ?></div>
								<div class="tools">
									<a href="javascript:;" class="collapse"></a>
									<!-- <a href="#portlet-config" data-toggle="modal" class="config"></a>
									<a href="javascript:;" class="reload"></a>
									<a href="javascript:;" class="remove"></a> -->
								</div>
							</div>
							<div class="portlet-body form">
								<!-- BEGIN FORM-->
								<h3></h3>
								<form action="#" class="form-horizontal"  method="post" name="submitform" id="submitform">

									<div class="control-group">
										<label class="control-label">組織名稱</label>
										<div class="controls">
											<input type="text" id="organization" name="organization"  data-required="1" class="span6 m-wrap" value="<?php echo isset($result['organization_name']) ? $result['organization_name'] : ''; ?>" disabled/>
											<span id="ordernumberMsg"></span>
										</div>
									</div>	

									<div class="control-group">
										<label class="control-label">副總經理</label>
										<div class="controls">
											<select class="span6 m-wrap" name="principal" id="principal" disabled>
												<option value="">請選擇</option>
												<?php foreach ($employee as $key => $value) { ?>
												<option value="<?php echo $value['employee_id'];?>"<?php echo ($result['employee_id'] == $value['employee_id'])? 'selected':''; ?> >
													<?php echo $value['employee_name']; ?>
												</option>
												<?php } ?>
											</select>
											<span id="principalMsg"></span>
										</div>
									</div>

									<div class="control-group">
										<label class="control-label">母單位組織</label>
										<div class="controls">
											<select class="span6 m-wrap" name="parentunit" id="parentunit" disabled>
												<option value="">請選擇</option>
												<?php foreach ($organization as $key => $value) { ?>
												<option value="<?php echo $value['organization_id'];?>"<?php echo ($result['organization_parent_unit'] == $value['organization_id'])? 'selected':''; ?> >
													<?php echo $value['organization_name']; ?>
												</option>
												<?php } ?>
											</select>
										</div>
									</div>

									<div class="control-group">
										<label class="control-label">推薦人</label>
										<div class="controls">
											<select class="span6 m-wrap" name="referrer" id="referrer" disabled>
												<option value="">請選擇</option>
												<?php foreach ($organization as $key => $value) { ?>
												<option value="<?php echo $value['organization_id'];?>"<?php echo ($result['organization_referrer'] == $value['organization_id'])? 'selected':''; ?> >
													<?php echo $value['organization_name']; ?>
												</option>
												<?php } ?>
											</select>
										</div>
									</div>

									<div class="control-group">
										<label class="control-label">地址</label>
										<div class="controls">
											<select class="span3 m-wrap" name="address" disabled>
												<option value="">請選擇</option>
												<?php foreach ($address as $key => $value) { ?>
												<option value="<?php echo $value['address_id'];?>"<?php echo ($result['address_id'] == $value['address_id'])? 'selected':''; ?> >
													<?php echo $value['address_zip'].' '.$value['address_city'].$value['address_area']; ?>
												</option>
												<?php } ?>
											</select>
											<input class="span5 m-wrap" name="address_info" type="text" value="<?php echo isset($result['organization_address']) ? $result['organization_address'] : ''; ?>" disabled/>
											<span id="addressMsg"></span>
										</div>
									</div>

									<div class="control-group">
										<label class="control-label">聯絡電話</label>
										<div class="controls">
											<input name="tel" id="tel" type="text" class="span6 m-wrap" value="<?php echo isset($result['organization_tel']) ? $result['organization_tel'] : ''; ?>" disabled/>
											<span id="telMsg"></span>
										</div>
									</div>	

									<div class="control-group">
										<label class="control-label">傳真號碼</label>
										<div class="controls">
											<input name="fax" id="fax" type="text" class="span6 m-wrap" value="<?php echo isset($result['organization_fax']) ? $result['organization_fax'] : ''; ?>" disabled/>
											<span id="faxMsg"></span>
										</div>
									</div>	

									<div class="form-actions">
										<!-- <button type="submit" class="btn green" name="id" value="<?php echo $upid;?>" onclick="check()">
											儲存 <div class="icon-save"></div>
										</button> -->
										<a href="<?php echo base_url('organization');?>" class="btn">
											取消 <div class="icon-undo"></div>
										</a>
									</div>

								</form>
								<!-- END FORM-->
							</div>
						</div>
						<!-- END VALIDATION STATES-->
					</div>
				</div>
				<!-- END DASHBOARD STATS -->
				<div class="clearfix"></div>
			</div>
		</div>
		<!-- END PAGE CONTAINER-->    
	</div>
	<!-- END PAGE -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<div class="footer">
	<?php echo $footer; ?>
</div>
<!-- END FOOTER -->
<?php echo $script; ?>

<!-- BEGIN PAGE LEVEL PLUGINS -->

<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
   
<!-- END PAGE LEVEL SCRIPTS -->

<!-- BEGIN RULE -->
<!-- END RULE -->
<script>
	jQuery(document).ready(function() {       
		   // App.init();
		   TableManaged.init();
		   FormWizard.init();
		});
	</script>

</body>
<!-- END BODY -->
</html>