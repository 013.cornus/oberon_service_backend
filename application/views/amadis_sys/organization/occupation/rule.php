<script type="text/javascript">
	var rule1=/./;
	var rule2=/.@./;
	var rule3=/^[A-Z]{1}[0-9]{9}$/;
	var rule4=/^09\d{8}/;
	var rule5=/^0\d{1,2}-?(\d{6,8})(#\d{1,5}){0,1}/;
	var rule6=/[\u4e00-\u9fa5]{0,2}縣|市[\u4e00-\u9fa5]{2,4}鄉|鎮|市|區[\u4e00-\u9fa5]{2,4}路|街|大道.*\d*號./;
	//訂單編號
	$("#ordernumber").blur(function(){
		if(rule1.test($(this).val())){	
			$(this).css("border-color","green")
			ordernumberMsg.innerHTML="OK";
			ordernumberMsg.style.color="green";
		}else{
			$(this).css("border-color","red")
			ordernumberMsg.innerHTML="您的輸入不符合規則";
			ordernumberMsg.style.color="red";
		}
	})
	//客戶姓名
	$("#customername").blur(function(){
		if(rule1.test($(this).val())){	
			$(this).css("border-color","green")
			customernameMsg.innerHTML="OK";
			customernameMsg.style.color="green";
		}else{
			$(this).css("border-color","red")
			customernameMsg.innerHTML="您的輸入不符合規則";
			customernameMsg.style.color="red";
		}
	})
	//業務承攬人
	$("#employeename").blur(function(){
		if(rule1.test($(this).val())){	
			$(this).css("border-color","green")
			employeenameMsg.innerHTML="OK";
			employeenameMsg.style.color="green";
		}else{
			$(this).css("border-color","red")
			employeenameMsg.innerHTML="您的輸入不符合規則";
			employeenameMsg.style.color="red";
		}
	})
	//合約生效日
	$("#activedate").blur(function(){
		if(rule1.test($(this).val())){	
			$(this).css("border-color","green")
			activedateMsg.innerHTML="OK";
			activedateMsg.style.color="green";
		}else{
			$(this).css("border-color","red")
			activedateMsg.innerHTML="您的輸入不符合規則";
			activedateMsg.style.color="red";
		}
	})
	//合約到期日
	$("#overdate").blur(function(){
		if(rule1.test($(this).val())){	
			$(this).css("border-color","green")
			overdateMsg.innerHTML="OK";
			overdateMsg.style.color="green";
		}else{
			$(this).css("border-color","red")
			overdateMsg.innerHTML="您的輸入不符合規則";
			overdateMsg.style.color="red";
		}
	})
	//合約簽收日
	$("#signdate").blur(function(){
		if(rule1.test($(this).val())){	
			$(this).css("border-color","green")
			signdateMsg.innerHTML="OK";
			signdateMsg.style.color="green";
		}else{
			$(this).css("border-color","red")
			signdateMsg.innerHTML="您的輸入不符合規則";
			signdateMsg.style.color="red";
		}
	})
	function check() {
		if (rule1.test($("#ordernumber").val()) && rule1.test($("#customername").val()) && rule1.test($("#employeename").val()) && rule1.test($("#activedate").val()) && rule1.test($("#overdate").val()) && rule1.test($("#signdate").val())){
			document.getElementById("submitform").submit();	
		} else {
			alert("請輸入完整資訊");
		}
	}
</script>
</script>