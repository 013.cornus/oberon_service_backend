<script type="text/javascript">
	var rule1=/./;
	//姓名
	$("#name").blur(function(){
		if(rule1.test($(this).val())){	
			$(this).css("border-color","green")
			namesMsg.innerHTML="OK";
			namesMsg.style.color="green";
		}else{
			$(this).css("border-color","red")
			namesMsg.innerHTML="請輸入姓名";
			namesMsg.style.color="red";
		}
	})

	var rule3=/^[A-Z]{1}[0-9]{9}$/;
	//身分證字號
	$("#idcard").blur(function(){
		if(rule1.test($(this).val())){	
			$(this).css("border-color","green")
			idMsg.innerHTML="OK";
			idMsg.style.color="green";
		}else{
			$(this).css("border-color","red")
			idMsg.innerHTML="請輸入身分證字號";
			idMsg.style.color="red";

		}
	})

	//營業處
	$("#organization").blur(function(){
		if(rule3.test($(this).val())){	
			$(this).css("border-color","green")
			organizationMsg.innerHTML="OK";
			organizationMsg.style.color="green";
		}else{
			$(this).css("border-color","red")
			organizationMsg.innerHTML="請輸入營業處";
			organizationMsg.style.color="red";

		}
	})

    //職稱
    $("#occupation").blur(function(){
    	if(rule3.test($(this).val())){	
    		$(this).css("border-color","green")
    		occupationMsg.innerHTML="OK";
    		occupationMsg.style.color="green";
    	}else{
    		$(this).css("border-color","red")
    		occupationMsg.innerHTML="請輸入職稱";
    		occupationMsg.style.color="red";

    	}
    })

	//推薦人
	$("#referrer").blur(function(){
		if(rule1.test($(this).val())){	
			$(this).css("border-color","green")
			referrerMsg.innerHTML="OK";
			referrerMsg.style.color="green";
		}else{
			$(this).css("border-color","red")
			referrerMsg.innerHTML="請輸入推薦人";
			referrerMsg.style.color="red";
		}
	})

	//直屬經理
	$("#supervisor").blur(function(){
		if(rule1.test($(this).val())){	
			$(this).css("border-color","green")
			supervisorMsg.innerHTML="OK";
			supervisorMsg.style.color="green";
		}else{
			$(this).css("border-color","red")
			supervisorMsg.innerHTML="請輸入直屬經理	";
			supervisorMsg.style.color="red";
		}
	})

	//手機
	var rule4=/^09\d{8}/;
	$("#cellphone").blur(function(){
		if(rule1.test($(this).val())){	
			$(this).css("border-color","green")
			cellphoneMsg.innerHTML="OK";
			cellphoneMsg.style.color="green";
		}else{
			$(this).css("border-color","red")
			cellphoneMsg.innerHTML="請輸入手機";
			cellphoneMsg.style.color="red";
		}
	})


	//匯款銀行
	$("#bank").blur(function(){
		if(rule1.test($(this).val())){	
			$(this).css("border-color","green")
			bankMsg.innerHTML="OK";
			bankMsg.style.color="green";
		}else{
			$(this).css("border-color","red")
			bankMsg.innerHTML="請輸入匯款銀行";
			bankMsg.style.color="red";
		}
	})

	//分行
	$("#branch").blur(function(){
		if(rule1.test($(this).val())){	
			$(this).css("border-color","green")
			branchMsg.innerHTML="OK";
			branchMsg.style.color="green";
		}else{
			$(this).css("border-color","red")
			branchMsg.innerHTML="請輸入分行";
			branchMsg.style.color="red";
		}
	})

	//匯款帳號
	$("#account").blur(function(){
		if(rule1.test($(this).val())){	
			$(this).css("border-color","green")
			accountMsg.innerHTML="OK";
			accountMsg.style.color="green";
		}else{
			$(this).css("border-color","red")
			accountMsg.innerHTML="請輸入匯款帳號";
			accountMsg.style.color="red";
		}
	})

	function check() {
		if (rule1.test($("#name").val()) && rule1.test($("#idcard").val()) && rule1.test($("#organization").val()) && rule1.test($("#cellphone").val()) && rule1.test($("#addressdetail").val()) && rule1.test($("#referrer").val()) && rule1.test($("#supervisor").val()) && rule1.test($("#telephone").val()) && rule1.test($("#bank").val()) && rule1.test($("#branch").val()) && rule1.test($("#account").val())){
			document.getElementById("submitform").submit();	
		} else {
			alert("請輸入完整資訊");
			if(rule1.test($("#name").val()) == false){	
				$("#name").css("border-color","red")
				namesMsg.innerHTML="請輸入姓名";
				namesMsg.style.color="red";
			}

			if(rule1.test($("#idcard").val()) == false){	
				$("#idcard").css("border-color","red")
				idMsg.innerHTML="請輸入身分證字號";
				idMsg.style.color="red";
			}

			if(rule1.test($("#organization").val()) == false){	
				$("#organization").css("border-color","red")
				organizationMsg.innerHTML="請輸入營業處";
				organizationMsg.style.color="red";
			}

			if(rule1.test($("#occupation").val()) == false){	
				$("#occupation").css("border-color","red")
				occupationMsg.innerHTML="請輸入職稱";
				occupationMsg.style.color="red";
			}

			if(rule1.test($("#referrer").val()) == false){	
				$("#referrer").css("border-color","red")
				referrerMsg.innerHTML="請輸入推薦人";
				referrerMsg.style.color="red";
			}

			if(rule1.test($("#supervisor").val()) == false){	
				$("#supervisor").css("border-color","red")
				supervisorMsg.innerHTML="請輸入直屬經理	";
				supervisorMsg.style.color="red";
			}

			if(rule1.test($("#cellphone").val()) == false){	
				$("#cellphone").css("border-color","red")
				cellphoneMsg.innerHTML="請輸入手機";
				cellphoneMsg.style.color="red";
			}

			if(rule1.test($("#bank").val()) == false){	
				$("#bank").css("border-color","red")
				bankMsg.innerHTML="請輸入匯款銀行";
				bankMsg.style.color="red";
			}

			if(rule1.test($("#branch").val()) == false){	
				$("#branch").css("border-color","red")
				branchMsg.innerHTML="請輸入分行";
				branchMsg.style.color="red";
			}

			if(rule1.test($("#account").val()) == false){	
				$("#account").css("border-color","red")
				accountMsg.innerHTML="請輸入匯款帳號";
				accountMsg.style.color="red";
			}
		}
	}
</script>