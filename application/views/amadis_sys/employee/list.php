		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">    
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3 class="page-title">
							<?php echo $title; ?> <small><?php echo $title_small; ?></small>
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="<?php echo base_url('home'); ?>">首頁></a> 
								<i class="icon-angle-right"></i>
							</li>
							<li><a href="<?php echo $this->router->fetch_class();?>"><?php echo $title_small; ?></a></li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<div id="dashboard">
					<!-- BEGIN ALTER -->
					<?php echo $this->session->flashdata('messagediv');?>
					<!-- END ALTER -->
					<!-- BEGIN DASHBOARD STATS -->
					<!-- BEGIN PAGE CONTENT-->
					<div class="row-fluid">
						<div class="span12">
							<div class="portlet box blue" id="form_wizard_1">
								<div class="portlet-title">
									<div class="caption">
										<i class="icon-search"></i> 搜尋列</span>
									</div>
									<div class="tools hidden-phone">
										<a href="javascript:;" class="collapse"></a>
										<a href="#portlet-config" data-toggle="modal" class="config" style="display: none;"></a>
										<a href="javascript:;" class="reload" style="display: none;"></a>
										<a href="javascript:;" class="remove" style="display: none;"></a>
									</div>
								</div>
								<div class="portlet-body form">
									<form method="post" action="<?php echo base_url('employee/list')?>">
										<div class="control-group">
											<label class="control-label">搜尋關鍵字：</label>
											<div class="controls">
												<select class="span2 select2_category" data-placeholder="業務姓名" tabindex="1" name="employeeid" id="employeeid"">
													<option value=""></option>
													<?php  foreach ($employee as $r) {?>
														<a href="<?php echo base_url('home')?>">
															<option value="<?php echo $r["employee_id"];?>"><?php echo $r["employee_name"];?></option>
														</a>
													<?php } ?>
												</select>
											</div>
											<div class="controls" <?php echo $hide;?>>
												<select class="span2 select2_category" data-placeholder="營業處" tabindex="1" name="orgid" id="orgid">		
 													<!--<?php foreach ($organization as $r) {?>
														<option value="<?php echo $r["organization_id"];?>" <?php if($selectorg == $r['organization_id']){echo "selected='selected'";}?>><?php echo $r["organization_name"];?></option>
														<?php }?> -->
														<option value=''></option>
														<?php foreach ($organization as $r) {?>
															<option value="<?php echo $r["organization_id"];?>" <?php if($this->session->userdata('orgid') == $r['organization_id']){echo "selected='selected'";}?>><?php echo $r["organization_name"];?></option>
														<?php }?>
													</select>
												</div>
												<div class="controls">
													&nbsp;&nbsp;
													<input type="text" class="span6 m-wrap" name="fullname" id="fullname" placeholder="請輸入關鍵字 / 業務姓名 /" />

													<button type="submit" class="btn green">
														搜尋 <i class="icon-circle-arrow-right"></i>
													</button>            
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
						<!-- END PAGE CONTENT-->
						<div class="row-fluid">
							<div class="span12">
								<!-- BEGIN EXAMPLE TABLE PORTLET-->
								<div class="portlet box light-grey">
									<div class="portlet-title">
										<div class="caption"><i class="icon-globe"></i><?php echo $title_small; ?></div>
										<div class="tools">
											<a href="javascript:;" class="collapse"></a>
											<a href="#portlet-config" data-toggle="modal" class="config" style="display: none;"></a>
											<a href="javascript:;" class="reload" style="display: none;"></a>
											<a href="javascript:;" class="remove" style="display: none;"></a>
										</div>
									</div>
									<div class="portlet-body">
										<div class="clearfix">
											<div class="btn-group">
												<a href="<?php echo base_url('employee/create_form');?>">
													<button id="sample_editable_1_new" class="btn green">
														新增 <i class="icon-plus"></i>
													</button>
												</a>
											</div>
											<div class="btn-group pull-right" style="">
												<button class="btn dropdown-toggle" data-toggle="dropdown"><i class="icon-download-alt"></i> 工具列 <i class="icon-angle-down"></i>
												</button>
												<ul class="dropdown-menu pull-right">
													<li><a href="#confirm_window" data-toggle="modal" onclick="alert_window('<?php echo base_url('exportexcel/employee_export');?>','確定要匯出報表(依照頁面搜尋條件)?','確定匯出報表')"><i class="icon-save"></i> 輸出成 Excel</a></li>
												</ul>
											</div>
										</div>
										<div style="overflow:scroll; overflow:auto;" >
											<table class="table table-striped table-bordered table-hover" style="min-width:768px;" id="sample_1">
												<thead>
													<tr>
														<!-- <th>
															<input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" />
														</th> -->
														<th>項次</th>
														<th>姓名</th>
														<th>狀態</th>
														<th>所屬單位</th>
														<th>推薦人</th>
														<th>直屬經理</th>	
														<th>動作</th>
													</tr>
												</thead>
												<tbody>
													<?php foreach ($result as $key => $row) { ?>
														<tr class="odd">
															<!-- <td><input type="checkbox" class="checkboxes" value="<?php echo $row['employee_id']; ?>"/></td> -->
															<td><?php echo $key+1;?><!-- <?php echo mb_substr($row['employee_bank_branch'],0,-2);?> --></td>
															<td><?php echo $row['employee_name'];?></td>
															<td><?php echo '<span class="label label-'.$color[$key].'">'.$str[$key].'</span>'; ?></td>
															<td><?php echo $orgname[$key];?></td>
															<td><?php echo $referrername[$key];?></td>
															<td><?php echo $managername[$key];?></td>	
															<td>
																<a href="<?php echo base_url('employee/update_form?id=').$row['employee_id'];?>" class="btn blue mini">編輯 <i class="icon-edit"></i>
																</a>
																<?php if($row['employee_status'] == 1){?>
																	<button class="btn purple mini" class="btn purple mini" data-toggle="modal" href="#confirm_window" onclick="alert_window('<?php echo base_url('employee/info_invisible?id=').$row['employee_id'];?>','確定要停權這筆資料嗎?','確定停權')">停權 <i class="icon-eye-close"></i></button>
																<?php }?>
																<?php if($row['employee_status'] == 0){?>
																	<button id="" class="btn green mini" class="btn green mini" data-toggle="modal" href="#confirm_window" onclick="alert_window('<?php echo base_url('employee/info_visible?id=').$row['employee_id'];?>','確定要啟用這筆資料嗎?','確定啟用')">啟用 <i class="icon-eye-open"></i></button>
																<?php }?>
																<button id="" class="btn red mini" data-toggle="modal" href="#confirm_window" onclick="alert_window('<?php echo base_url('employee/delete?id=').$row['employee_id'];?>','確定要刪除這筆資料嗎?','確定刪除')">刪除 <i class="icon-trash"></i></button>
																<a href="<?php echo base_url('employee/check_form?id=').$row['employee_id'];?>" class="btn gray mini">檢視 <i class="icon-eye-open"></i>
																</a>
															</td>
														</tr>
													<?php } ?>
												</tbody>
											</table>
										</div>
										<div class="row-fluid">
											<?php echo $page_list;?>
										</div>

										<!-- 跳到指定頁數 -->
										<div class="row-fluid">
											<div class="control-group pull-right">						
												<form id="pagejump" action="<?php echo base_url('employee/pagejump');?>" class="pull-right" method="post">
													<div class="controls">
														跳轉至<input type="text" id="pagenum" name="pagenum"  class="m-wrap" style="width: 30px">頁
														&nbsp;<button class="btn red icn-only">確認</button>
													</div>	
												</form>	
											</div>
										</div>
										<!-- 跳到指定頁數 -->
									</div>
								</div>

								<!-- 警告視窗 -->
								<!-- BEGIN CONFIRM WINDOW -->
								<div id="confirm_window" class="modal hide fade " role="dialog" data-keyboard="false" style="top:20%">
									<div class="modal-header red">
										<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
										<h4><div class="icon-warning-sign"></div> 警告</h4>
									</div>
									<form>
										<div class="modal-body">
											<p id='confirm_text'>警告文字</p>
										</div>
										<div class="modal-footer">
											<button type="button" data-dismiss="modal" class="btn">取消 <div class="icon-undo"></div></button>
											<button id="confirm_btn" type="button" data-dismiss="modal" class="btn red" >確認按鈕<div class="icon-check"></div></button>
										</div>
									</form>
								</div>
								<!-- END CONFIRM WINDOW -->

								<!-- END EXAMPLE TABLE PORTLET-->
							</div>
						</div>
						<!-- END DASHBOARD STATS -->
						<div class="clearfix"></div>
					</div>
				</div>
				<!-- END PAGE CONTAINER-->    
			</div>
			<!-- END PAGE -->
		</div>
		<!-- END CONTAINER -->
		<!-- BEGIN FOOTER -->
		<div class="footer">
			<?php echo $footer; ?>
		</div>

		<!-- END FOOTER -->
		<?php echo $script; ?>
		<script src="<?php echo base_url('public/globel/js/form-samples.js')?>" type="text/javascript"></script> 
		<script src="<?php echo base_url('public/globel/js/select2.min.js')?>" type="text/javascript"></script>
	</body>
	<script type="text/javascript">
		//$link為欲前往連結
	    //$confirm_text為警告視窗文字
	    //$btn_text為確認按鈕文字
		function alert_window($link,$confirm_text,$btn_text){
			$("#confirm_text").text($confirm_text);
			$("#confirm_btn").text($btn_text);
			$("#confirm_btn").attr('onclick',"javascript:location.href='"+$link+"'");
		}
	</script>
	<!-- END BODY -->
	</html>