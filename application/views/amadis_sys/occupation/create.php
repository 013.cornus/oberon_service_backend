		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">    
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3 class="page-title">
							<?php echo $title; ?> <small><?php echo $title_small; ?></small>
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="<?php echo base_url('home');?>">首頁</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li>
								<a href="<?php echo base_url($this->router->fetch_class()); ?>"><?php echo $title; ?></a>
								<i class="icon-angle-right"></i>
							</li>
							<li><a href="#"><?php echo $title_small; ?></a></li>
							<li class="pull-right no-text-shadow" style="display: none;">
								<div id="dashboard-report-range" class="dashboard-date-range tooltips no-tooltip-on-touch-device responsive" data-tablet="" data-desktop="tooltips" data-placement="top" data-original-title="Change dashboard date range">
									<i class="icon-calendar"></i>
									<span></span>
									<i class="icon-angle-down"></i>
								</div>
							</li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN VALIDATION STATES-->
						<div class="portlet box green">
							<div class="portlet-title">
								<div class="caption"><i class="icon-reorder"></i><?php echo $title_small; ?></div>
								<div class="tools">
									<a href="javascript:;" class="collapse"></a>
									<!-- <a href="#portlet-config" data-toggle="modal" class="config"></a>
									<a href="javascript:;" class="reload"></a>
									<a href="javascript:;" class="remove"></a> -->
								</div>
							</div>
							<div class="portlet-body form">
								<!-- BEGIN FORM-->
								<h3></h3>
								<?php foreach($latestid as $r){$id = $r['maxid']+1;?>
								<form action="<?php echo base_url('occupation/create?id='.$id);?>" class="form-horizontal"  method="post" name="submitform" id="submitform">
									<?php } ?>
									<div class="alert alert-error hide">
										<button class="close" data-dismiss="alert"></button>
										You have some form errors. Please check below.
									</div>
									<div class="alert alert-success hide">
										<button class="close" data-dismiss="alert"></button>
										Your form validation is successful!
									</div>
									<div class="control-group">
										<label class="control-label">職級名稱<span class="required">*</span></label>
										<div class="controls">
											<input name="occupationname" id="occupationname" type="text" class="span6 m-wrap" required ><span class="word-error" id="occupationMsg"></span>
										</div>
									</div>
									<div class="control-group" style="display: none;">
										<label class="control-label">基金獎金提撥率<span class="required">*</span></label>
										<div class="controls">
											<input name="fund" id="fund" type="text" class="span6 m-wrap" /><span id="bonusrateMsg"></span>
										</div>
									</div>	
									<div class="control-group" style="display: none;">
										<label class="control-label">股權獎金提撥率<span class="required">*</span></label>
										<div class="controls">
											<input name="equity" id="equity" type="text" class="span6 m-wrap" /><span id="bonusrateMsg"></span>
										</div>
									</div>											
									<div class="form-actions">
										<button type="submit" class="btn green" id="submit_btn" onclick="check()">
											新增 <div class="icon-save"></div>
										</button>
										<a href="<?php echo base_url('occupation');?>">
											<button type="button" class="btn">取消 <div class="icon-undo"></div></button>
										</a>
									</div>

								</form>
								<!-- END FORM-->
							</div>

						</div>
						<!-- END VALIDATION STATES-->
					</div>
				</div>
				<!-- END DASHBOARD STATS -->
				<div class="clearfix"></div>
			</div>
		</div>
		<!-- END PAGE CONTAINER-->    
	</div>
	<!-- END PAGE -->
</div>
<!-- END CONTAINER -->

<!-- BEGIN FOOTER -->
<div class="footer">
	<?php require_once(dirname(__FILE__).'/../common/footer.php'); ?>
</div>
<!-- END FOOTER -->

<?php require_once(dirname(__FILE__).'/../common/script.php'); ?>

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/additional-methods.min.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url('public/plugin/main/occupation.js'); ?>" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->

</body>
<!-- END BODY -->

</html>
