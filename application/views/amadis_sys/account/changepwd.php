<body class="page-header-fixed page-full-width">
	<!-- BEGIN HEADER -->
	<?php echo $headerHtml; ?>
	<!-- END HEADER -->
	<!-- BEGIN CONTAINER -->   
	<div class="page-container row-fluid">
		<!-- BEGIN EMPTY PAGE SIDEBAR -->
		<?php echo $sidebarHtml; ?>
		<!-- END EMPTY PAGE SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div class="page-content no-min-height">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="portlet-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>portlet Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid promo-page">
				<!-- BEGIN PAGE CONTENT-->
				<br><br><br>
				<div class="row-fluid">
					<div class="span12">
						<div class="">
							<div class="container">
								<!-- BEGIN DASHBOARD STATS -->
								<div class="row-fluid"></div>
								<div class="row-fluid" style="margin: auto;">
									<div class="portlet-body form" style="">
										<!-- BEGIN FORM-->
										<h3 class="page-title">
											<i class="icon-key"></i>
											<?php echo $title; ?> <small><?php echo $title_small; ?></small>
										</h3>
										
										<form class="form-horizontal" action="<?php echo base_url('changepassword/checkpwd'); ?>" method="post" id="submitform">
											<div class="alert alert-error hide">
												<button class="close" data-dismiss="alert"></button>
												You have some form errors. Please check below.
											</div>
											<div class="alert alert-success hide">
												<button class="close" data-dismiss="alert"></button>
												Your form validation is successful!
											</div>
											<div class="alert alert-info">
												<i class="icon-warning-sign"></i> 歡迎登入！初次登入系統需<span style="color: red;">變更密碼</span>
											</div>
											<input type="hidden" name="op" value="check"/>
											<div class="control-group">
												<label class="control-label">帳號</label>
												<div class="controls">
													<input class="span6 m-wrap" type="text" value="<?php echo $result['users_account'];?>" disabled/>
													<span id="errorMsg"></span>
												</div>
											</div>
											<div class="control-group">
												<label class="control-label">新密碼<span class="required">*</span></label>
												<div class="controls">
													<input type="password" class="span6 m-wrap" name="password_1" id="password_1" />
													<span id="errorMsg"></span>
												</div>
											</div>
											<div class="control-group">
												<label class="control-label">再次確認新密碼<span class="required">*</span></label>
												<div class="controls">
													<input type="password" class="span6 m-wrap" name="password_2" id="password_2" />
													<span id="errorMsg"></span>
												</div>
											</div>
											<div class="form-actions">
												<button type="submit" class="btn green">
													確定 <div class="icon-save"></div>
												</button>
											</div>
										</form>
										<!-- END FORM-->
									</div>
								</div>
								<!-- END DASHBOARD STATS -->
								<!-- <div class="clearfix"></div> -->
								
							</div>
						</div>
						<!-- <div class="block-grey" >
						</div> -->
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
		<!-- END PAGE CONTAINER--> 
	</div>
	<!-- END PAGE --> 
	<!-- END CONTAINER -->

	<!-- BEGIN FOOTER1 -->
	<?php echo $footerHtml; ?>
	<!-- END FOOTER -->

	<!-- BEGIN JAVASCRIPTS-->
	<!-- BEGIN CORE PLUGINS -->
	<script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/additional-methods.min.js"></script>
	<!-- <script src="<?php echo base_url('public/globel/js/jquery-1.10.1.min.js');?>" type="text/javascript"></script> -->
	<script src="<?php echo base_url('public/globel/js/jquery-migrate-1.2.1.min.js');?>" type="text/javascript"></script>
	<script src="<?php echo base_url('public/globel/js/jquery-ui-1.10.1.custom.min.js');?>" type="text/javascript"></script>      
	<script src="<?php echo base_url('public/globel/js/bootstrap.min.js');?>" type="text/javascript"></script>
	<!-- <script src="<?php echo base_url('public/globel/js/jquery.slimscroll.min.js');?>" type="text/javascript"></script>
	<script src="<?php echo base_url('public/globel/js/jquery.blockui.min.js');?>" type="text/javascript"></script>  
	<script src="<?php echo base_url('public/globel/js/jquery.cookie.min.js');?>" type="text/javascript"></script> -->
	<script src="<?php echo base_url('public/globel/js/jquery.uniform.min.js');?>" type="text/javascript" ></script>
	<!-- END CORE PLUGINS -->
	<script src="<?php echo base_url('public/globel/js/app.js');?>"></script> 
	<!-- BEGIN PAGE LEVEL SCRIPTS -->

	<script src="<?php echo base_url('public/plugin/main/changepassword.js'); ?>" type="text/javascript"></script>
	<!-- END PAGE LEVEL SCRIPTS -->     
	<script>
		jQuery(document).ready(function() {    
			App.init();
			jQuery('#promo_carousel').carousel({
				interval: 10000,
				pause: 'hover'
			});
		});
	</script>
	<!-- END JAVASCRIPTS -->
</body>