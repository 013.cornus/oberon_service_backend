		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3 class="page-title">
							<?php echo $title; ?> <small><?php echo $title_small; ?></small>
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="<?php echo base_url('home');?>">首頁</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li>
								<a href="<?php echo base_url($this->router->fetch_class()); ?>"><?php echo $title; ?></a>
								<i class="icon-angle-right"></i>
							</li>
							<li><a href="#"><?php echo $title_small; ?></a></li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN VALIDATION STATES-->
						<div class="portlet box green">
							<div class="portlet-title">
								<div class="caption"><i class="icon-user"></i><?php echo $title_small; ?></div>
								<div class="tools">
									<a href="javascript:;" class="collapse"></a>
									<!-- <a href="#portlet-config" data-toggle="modal" class="config"></a>
									<a href="javascript:;" class="reload"></a>
									<a href="javascript:;" class="remove"></a> -->
								</div>
							</div>
							<div class="portlet-body form">
								<!-- BEGIN FORM-->
								<h3></h3>
								<form action="<?php echo base_url('home/dochange');?>" class="form-horizontal"  method="post" name="updateForm" id="updateForm">
									<div class="alert alert-error hide">
										<button class="close" data-dismiss="alert"></button>
										You have some form errors. Please check below.
									</div>
									<div class="alert alert-success hide">
										<button class="close" data-dismiss="alert"></button>
										Your form validation is successful!
									</div>
									<div class="control-group">
										<label class="control-label">用戶名稱<span class="required">*</span></label>
										<div class="controls">
											<input name="accountname" id="accountname" type="text" class="span6 m-wrap" value="<?php echo $username;?>" readonly/>
											<span id="errorMsg"></span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">用戶帳號<span class="required">*</span></label>
										<div class="controls">
											<input type="text" class="span6 m-wrap" value="<?php echo $account;?>" readonly>
											<span id="errorMsg"></span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">新用戶密碼<span class="required">*</span></label>
										<div class="controls">
											<input type="password" name="password_1" id="password_1" type="text" class="span6 m-wrap" />
											<span id="errorMsg"></span>
										</div>
									</div>	
									<div class="control-group">
										<label class="control-label">再次確認密碼<span class="required">*</span></label>
										<div class="controls">
											<input type="password" name="password_2" id="password_2" type="text" class="span6 m-wrap" />
											<span id="errorMsg"></span>
										</div>
									</div>				
									<div class="form-actions">
										<button type="submit" class="btn green" name="id">
											儲存 <div class="icon-save"></div>
										</button>
										<a href="<?php echo base_url('home');?>">
											<button type="button" class="btn">取消 <div class="icon-undo"></div></button>
										</a>
									</div>
								</form>
								<!-- END FORM-->
							</div>
						</div>
						<!-- END VALIDATION STATES-->
					</div>
				</div>
				<!-- END DASHBOARD STATS -->
				<div class="clearfix"></div>
			</div>
		</div>
		<!-- END PAGE CONTAINER-->    
	</div>
	<!-- END PAGE -->
</div>
<!-- END CONTAINER -->

<!-- BENIG FOOTER -->
<div class="footer">
	<?php require_once(dirname(__FILE__).'/../common/footer.php'); ?>
</div>
<!-- END FOOTER -->
<?php require_once(dirname(__FILE__).'/../common/script.php'); ?>

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/additional-methods.min.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url('public/plugin/main/account.js');?>" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->

</body>
<!-- END BODY -->
</html>