<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!--> 
<html lang="en" class="no-js"> 
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<?php echo require_once('common/head.php'); ?>
	<!-- BEGIN PAGE LEVEL STYLES -->
	<link rel="stylesheet" type="text/css" href="./public/media/css/select2_metro.css" />
	<link rel="stylesheet" href="./public/media/css/DT_bootstrap.css" />
	<link rel="stylesheet" type="text/css" href="./public/media/css/chosen.css" />
	<!-- END PAGE LEVEL STYLES -->
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-header-fixed">
	<!-- BEGIN HEADER -->
	<div class="header navbar navbar-inverse navbar-fixed-top">
		<!-- BEGIN TOP NAVIGATION BAR -->
		<?php require_once('common/header.php'); ?>
		<!-- END TOP NAVIGATION BAR -->
	</div>
	<!-- END HEADER -->
	<div class="copyrights">Collect from <a href="#" ></a></div>
	<!-- BEGIN CONTAINER -->
	<div class="page-container">
		<!-- BEGIN SIDEBAR -->
		<div class="page-sidebar nav-collapse collapse">
			<?php require_once('common/sidebar.php'); ?>
		</div>
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">    
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3 class="page-title">
							<?php echo $title; ?> <small><?php echo $title_small; ?></small>
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="index.html">首頁</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li><a href="#"><?php echo $title_small; ?></a></li>
							<li class="pull-right no-text-shadow" style="display: none;">
								<div id="dashboard-report-range" class="dashboard-date-range tooltips no-tooltip-on-touch-device responsive" data-tablet="" data-desktop="tooltips" data-placement="top" data-original-title="Change dashboard date range">
									<i class="icon-calendar"></i>
									<span></span>
									<i class="icon-angle-down"></i>
								</div>
							</li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN VALIDATION STATES-->
						<div class="portlet box green">
							<div class="portlet-title">
								<div class="caption"><i class="icon-reorder"></i><?php echo $title_small; ?></div>
								<div class="tools">
									<a href="javascript:;" class="collapse"></a>
									<!-- <a href="#portlet-config" data-toggle="modal" class="config"></a>
									<a href="javascript:;" class="reload"></a>
									<a href="javascript:;" class="remove"></a> -->
								</div>
							</div>
							<div class="portlet-body form">
								<!-- BEGIN FORM-->
								<h3></h3>
								<form action="<?php echo base_url('customers/create');?>" class="form-horizontal"  method="post" name="submitform" id="submitform">
									<div class="alert alert-error hide">
										<button class="close" data-dismiss="alert"></button>
										You have some form errors. Please check below.
									</div>
									<div class="alert alert-success hide">
										<button class="close" data-dismiss="alert"></button>
										Your form validation is successful!
									</div>
									<div class="control-group">
										<label class="control-label">姓名<span class="required">*</span></label>
										<div class="controls">
											<input type="text" id="name" name="name"  data-required="1" class="span6 m-wrap"/>
											<span id="namesMsg"></span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">身分證字號<span class="required">*</span></label>
										<div class="controls">
											<input name="idcard" id="idcard" type="text" class="span6 m-wrap" maxlength="10"/><span id="idMsg"></span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">所屬單位<span class="required">*</span></label>
										<div class="controls">
											<select class="span6 m-wrap" name="organization" id="organization">
												<?php foreach($organization as $d){?>
												<option value="<?php echo $d["organization_id"];?>"><?php echo $d["organization_name"];?></option>
											    <?php }?>
											</select>
											<span id="organizationMsg"></span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">手機<span class="required">*</span></label>
										<div class="controls">
											<input name="cellphone" id="cellphone" type="text" class="span6 m-wrap" maxlength="10" />
											<span id="cellphoneMsg"></span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">聯絡電話<span class="required">*</span></label>
										<div class="controls">
											<input name="telephone" id="telephone" type="text" class="span6 m-wrap"/>
											<span id="telephoneMsg"></span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">聯絡地址<span class="required">*</span></label>
										<div class="controls">
											<select class="span6 m-wrap" name="address" style="width: auto;">
												<?php foreach ($result as $r) { ?>
													<option value="<?php echo $r["address_id"];?>"><?php echo $r["address_city"].$r["address_area"];?></option>
												<?php }?>
											</select>
											<input name="addressdetail" id="addressdetail" type="text" class="span6 m-wrap" style="width: 30%;"/>
											<span id="addressMsg"></span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">推薦人<span class="required">*</span></label>
										<div class="controls">
											<input name="referrer" id="referrer" type="text" class="span6 m-wrap"/>
											<span id="referrerMsg"></span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">職稱<span class="required">*</span></label>
										<div class="controls">
											<select class="span6 m-wrap" name="occupation" id="occupation">
												<?php foreach ($occupation as $r) { ?>
													<option value="<?php echo $r["occupation_id"];?>"><?php echo $r["occupation_name"];?></option>
												<?php }?>
											</select>
											<span id="occupationMsg"></span>
										</div>
									</div>
									<div class="form-actions">
										<button type="button" class="btn green" onclick="check()">
											新增 <div class="icon-save"></div>
										</button>
										<a href="<?php echo base_url('customers');?>">
											<button type="button" class="btn">取消 <div class="icon-undo"></div></button>
										</a>
									</div>
								</form>
								<!-- END FORM-->
							</div>
						</div>
						<!-- END VALIDATION STATES-->
					</div>
				</div>
				<!-- END DASHBOARD STATS -->
				<div class="clearfix"></div>
			</div>
		</div>
		<!-- END PAGE CONTAINER-->    
	</div>
	<!-- END PAGE -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<div class="footer">
	<?php require_once('common/footer.php'); ?>
</div>
<!-- END FOOTER -->
<?php require_once('common/script.php'); ?>
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="./public/media/js/select2.min.js"></script>
<script type="text/javascript" src="./public/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="./public/media/js/DT_bootstrap.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="./public/media/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="./public/media/js/additional-methods.min.js"></script>
<script type="text/javascript" src="./public/media/js/jquery.bootstrap.wizard.min.js"></script>
<script type="text/javascript" src="./public/media/js/chosen.jquery.min.js"></script>
<script type="text/javascript" src="./public/media/js/select2.min.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="./public/media/js/app.js"></script>
<script src="./public/media/js/form-wizard.js"></script>     
<!-- END PAGE LEVEL SCRIPTS -->
<script src="./public/media/js/app.js"></script>
<script src="./public/media/js/table-managed.js"></script> 
<script type="text/javascript">
	var rule1=/./;
	//姓名
	$("#name").blur(function(){
		if(rule1.test($(this).val())){	
			$(this).css("border-color","green")
			namesMsg.innerHTML="OK";
			namesMsg.style.color="green";
		}else{
			$(this).css("border-color","red")
			namesMsg.innerHTML="您的輸入不符合規則";
			namesMsg.style.color="red";
		}
	})

	var rule3=/^[A-Z]{1}[0-9]{9}$/;
	//身分證字號
	$("#idcard").blur(function(){
		if(rule3.test($(this).val())){	
			$(this).css("border-color","green")
			idMsg.innerHTML="OK";
			idMsg.style.color="green";
		}else{
			$(this).css("border-color","red")
			idMsg.innerHTML="您的輸入不符合規則";
			idMsg.style.color="red";

		}
	})
	//所屬單位
	$("#organization").blur(function(){
		if(rule1.test($(this).val())){	
			$(this).css("border-color","green")
			organizationMsg.innerHTML="OK";
			organizationMsg.style.color="green";
		}else{
			$(this).css("border-color","red")
			organizationMsg.innerHTML="您的輸入不符合規則";
			organizationMsg.style.color="red";
		}
	})
	//手機
	var rule4=/^09\d{8}/;
	$("#cellphone").blur(function(){
		if(rule4.test($(this).val())){	
			$(this).css("border-color","green")
			cellphoneMsg.innerHTML="OK";
			cellphoneMsg.style.color="green";
		}else{
			$(this).css("border-color","red")
			cellphoneMsg.innerHTML="您的輸入不符合規則";
			cellphoneMsg.style.color="red";
		}
	})

	var rule5=/^0\d{1,2}-?(\d{6,8})(#\d{1,5}){0,1}/;
	//連絡電話
	$("#telephone").blur(function(){
		if(rule5.test($(this).val())){	
			$(this).css("border-color","green")
			telephoneMsg.innerHTML="OK";
			telephoneMsg.style.color="green";
		}else{
			$(this).css("border-color","red")
			telephoneMsg.innerHTML="您的輸入不符合規則";
			telephoneMsg.style.color="red";
		}
	})
	rule6=/[\u4e00-\u9fa5]{0,2}縣|市[\u4e00-\u9fa5]{2,4}鄉|鎮|市|區[\u4e00-\u9fa5]{2,4}路|街|大道.*\d*號./;
	//聯絡地址
	$("#addressdetail").blur(function(){
		if(rule1.test($(this).val())){	
			$(this).css("border-color","green")
			addressMsg.innerHTML="OK";
			addressMsg.style.color="green";
		}else{
			$(this).css("border-color","red")
			addressMsg.innerHTML="您的輸入不符合規則";
			addressMsg.style.color="red";
		}
	})
	//推薦人
	$("#referrer").blur(function(){
		if(rule1.test($(this).val())){	
			$(this).css("border-color","green")
			referrerMsg.innerHTML="OK";
			referrerMsg.style.color="green";
		}else{
			$(this).css("border-color","red")
			referrerMsg.innerHTML="您的輸入不符合規則";
			referrerMsg.style.color="red";
		}
	})
	//職稱
	$("#occupation").blur(function(){
		if(rule1.test($(this).val())){	
			$(this).css("border-color","green")
			occupationMsg.innerHTML="OK";
			occupationMsg.style.color="green";
		}else{
			$(this).css("border-color","red")
			occupationMsg.innerHTML="您的輸入不符合規則";
			occupationMsg.style.color="red";
		}
	})

	function check() {
		if (rule1.test($("#name").val()) && rule3.test($("#idcard").val()) && rule1.test($("#organization").val()) && rule4.test($("#cellphone").val()) && rule1.test($("#addressdetail").val()) && rule1.test($("#referrer").val()) && rule1.test($("#occupation").val()) && rule5.test($("#telephone").val())){
			document.getElementById("submitform").submit();	
		} else {
			alert("請輸入完整資訊");
		}
	}
</script>
<!-- END rule1 -->
<script>
	jQuery(document).ready(function() {       
		   // App.init();
		   TableManaged.init();
		   FormWizard.init();
		   FormValidation.init();
		   App.init();
		});
</script>
</body>
<!-- END BODY -->
</html>