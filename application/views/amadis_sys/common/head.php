<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!--> 
<html lang="en" class="no-js"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<meta charset="utf-8" />
	<title><?php echo HEAD_TITLE; ?></title>
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<!-- BEGIN GLOBAL MANDATORY STYLES -->
	<link href="<?php echo base_url('public/globel/css/bootstrap.min.css');?>" rel="stylesheet" type="text/css"/>
	<link href="<?php echo base_url('public/globel/css/bootstrap-responsive.min.css');?>" rel="stylesheet" type="text/css"/>
	<link href="<?php echo base_url('public/globel/css/font-awesome.min.css');?>" rel="stylesheet" type="text/css"/>
	<link href="<?php echo base_url('public/globel/css/style-metro.css');?>" rel="stylesheet" type="text/css"/>
	<link href="<?php echo base_url('public/globel/css/style.css');?>" rel="stylesheet" type="text/css"/>
	<link href="<?php echo base_url('public/globel/css/style-responsive.css');?>" rel="stylesheet" type="text/css"/>
	<link href="<?php echo base_url('public/globel/css/default.css');?>" rel="stylesheet" type="text/css" id="style_color"/>
	<link href="<?php echo base_url('public/globel/css/uniform.default.css');?>" rel="stylesheet" type="text/css"/>
	<!-- END GLOBAL MANDATORY STYLES -->
	<!-- BEGIN PAGE LEVEL STYLES --> 
	<link href="<?php echo base_url('public/globel/css/jquery.gritter.css');?>" rel="stylesheet" type="text/css"/>
	<link href="<?php echo base_url('public/globel/css/daterangepicker.cs');?>" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url('public/globel/css/fullcalendar.css');?>" rel="stylesheet" type="text/css"/>
	<link href="<?php echo base_url('public/globel/css/jqvmap.css');?>" rel="stylesheet" type="text/css" media="screen"/>
	<link href="<?php echo base_url('public/globel/css/jquery.easy-pie-chart.css');?>" rel="stylesheet" type="text/css" media="screen"/>
	<!-- END PAGE LEVEL STYLES -->
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<script src="<?php echo base_url('public/globel/js/app.js');?>" type="text/javascript"></script>
	<script src="<?php echo base_url('public/globel/js/index.js');?>" type="text/javascript"></script>        
	<!-- END PAGE LEVEL SCRIPTS -->  

	<!-- BEGIN GLOBAL ICON IMAGE -->
	<!-- <link rel="shortcut icon" href="<?php echo base_url('public/globel/image/logo/favicon.ico');?>" /> -->
	<link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url('public/globel/image/logo/earphone_connect.png');?>" />
	<!-- END GLOBAL ICON IMAGE -->

	<!-- BEGIN PAGE LEVEL STYLES -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/globel/css/select2_metro.css');?>" />
	<link rel="stylesheet" href="<?php echo base_url('public/globel/css/DT_bootstrap.css');?>"/>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/globel/css/chosen.css');?>" />
	<!-- END PAGE LEVEL STYLES -->
	<link href="<?php echo base_url('public/globel/css/datetimepicker.css');?>" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/globel/css/datepicker.css');?>" />
	<link href="<?php echo base_url('public/globel/css/jquery.fancybox.css');?>" rel="stylesheet" />
	<link href="<?php echo base_url('public/globel/css/search.css');?>" rel="stylesheet" type="text/css"/>

	<!-- BEGIN PAGE ICON STYLES -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<!-- END PAGE ICON STYLES -->

	<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/globel/css/bootstrap-toggle-buttons.css');?>" />
	<link href="<?php echo base_url('public/globel/css/bootstrap-modal.css" rel="stylesheet" type="text/css');?>
	?>" />

	<!-- BEGIN USERS PAGE STYLES -->
	<link href="<?php echo base_url('public/globel/css/promo.css');?>" rel="stylesheet" type="text/css"/>
	<link href="<?php echo base_url('public/globel/css/animate.css');?>" rel="stylesheet" type="text/css"/>
	<!-- END USERS PAGE STYLES -->
	
	<?php if (isset($includeCss) && strstr($includeCss, 'select2')) { ?>
	<!-- BEGIN PAGE LEVEL STYLES -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/globel/css/select2_metro.css');?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/globel/css/chosen.css');?>" />
	<!-- END PAGE LEVEL STYLES -->
	<?php } ?>

	<style>
		.error { border-color: red !important }
		.word-error { color: red; }
	</style>
</head>
<!-- END HEAD -->