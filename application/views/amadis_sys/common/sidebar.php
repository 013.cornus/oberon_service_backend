<!-- BEGIN SIDEBAR -->
<div class="page-sidebar nav-collapse collapse">
	<!-- BEGIN SIDEBAR MENU -->        
	<ul class="page-sidebar-menu">
		<li>
			<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
			<div class="sidebar-toggler hidden-phone"></div>
			<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
		</li>
		<li>
			<!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
			<form class="sidebar-search" style="display: none;">
				<div class="input-box">
					<a href="javascript:;" class="remove"></a>
					<input type="text" placeholder="Search..." />
					<input type="button" class="submit" value=" " />
				</div>
			</form>
			<!-- END RESPONSIVE QUICK SEARCH FORM -->
		</li>
		<?php 
		$select = $this->session->flashdata('sidebarselected');
		$mainsidebar = $this->session->flashdata('mainsidebar');
		$competence_id = $this->session->userdata('competence_id');
		?>

		<?php foreach($mainbar as $main){if($main['sidebar_main_id'] == 1){ ?>
		<li class="start <?php if($select == $main['sidebar_main_code']){ echo "active"; } else{echo "";}?>" <?php foreach($view as $v){if($v['sidebar_main_id'] == $main['sidebar_main_id']){if($v['actions_view'] == 0){echo "style='display:none'";}}}?>>
			<a href="<?php echo base_url($main['sidebar_main_back_link']);?>">
				<i class="<?php echo $main['sidebar_main_icon'];?>"></i>
				<span class="title"><?php echo $main['sidebar_main_name'];?></span>
				<span class="selected"></span>
			</a>
		</li>
		<?php }else{ ?>
		<li class="<?php foreach($subbar as $sub){if($main['sidebar_main_id'] == $sub['sidebar_main_id']){if($select == $sub['sidebar_sub_code']){echo "active"; } else{ echo "";}}}?>" <?php $a=0; foreach($subbar as $sub){if($main['sidebar_main_id'] == $sub['sidebar_main_id']){foreach($view as $v){if($v['sidebar_sub_id'] == $sub['sidebar_sub_id']){if($v['actions_view'] == 1){$a=$a+1;}}}}}?> <?php if($a == 0 && $main['sidebar_main_back_link'] == ''){echo "style='display:none'";}?>>
			<a href="javascript:;">
				<i class="<?php echo $main['sidebar_main_icon'];?>"></i> 
				<span class="title"><?php echo $main['sidebar_main_name'];?></span>
				<span <?php foreach($subbar as $sub){if($main['sidebar_main_id'] == $sub['sidebar_main_id']){if($mainsidebar == $main['sidebar_main_code']){echo "class='selected'";} else{echo "class='arrow'";}}}?>></span>
			</a>
			<ul class="sub-menu">
				<?php foreach($subbar as $sub){if($main['sidebar_main_id'] == $sub['sidebar_main_id']){?>
				<li class="<?php if($select == $sub['sidebar_sub_code']){ echo "active"; } else{ echo "";}?>" <?php foreach($view as $v){if($v['sidebar_sub_id'] == $sub['sidebar_sub_id']){if($v['actions_view'] == 0){echo "style='display:none'";}}}?>>
					<a href="<?php echo ($sub['sidebar_sub_front_link'] != '#') ? base_url($sub['sidebar_sub_front_link']) : '#';?>">
						<i class="<?php echo $sub['sidebar_sub_icon'];?>"></i> 
						<?php echo $sub['sidebar_sub_name'];?>
					</a>
				</li>
				<?php }}?>
			</ul>
		</li>
		<?php }}?>
	</ul>
	<!-- END SIDEBAR MENU -->
</div>
<!-- END SIDEBAR -->