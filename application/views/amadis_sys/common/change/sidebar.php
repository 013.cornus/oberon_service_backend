<!-- BEGIN EMPTY PAGE SIDEBAR -->
<div class="page-sidebar nav-collapse collapse visible-phone visible-tablet">
	<ul class="page-sidebar-menu">
		<li class="visible-phone visible-tablet">
			<!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
			<form class="sidebar-search">
				<div class="input-box">
					<a href="javascript:;" class="remove"></a>
					<input type="text" placeholder="Search..." />            
					<input type="button" class="submit" value=" " />
				</div>
			</form>
			<!-- END RESPONSIVE QUICK SEARCH FORM -->
		</li>
		<li>
			<a class="active" href="#">
				Dashboard                        
			</a>
		</li>
		<li  class="active">
			<a href="javascript:;">
				Layouts
				<span class="arrow open"></span>   
				<span class="selected"></span>   
			</a>
			<ul class="sub-menu">
				<li >
					<a href="#">
					Blank Page</a>
				</li>
				<li >
					<a href="layout_boxed_page.html">Boxed Page</a>
				</li>
				<li >
					<a href="layout_boxed_not_responsive.html">
					Non-Responsive Boxed Layout</a>
				</li>
				<li >
					<a href="layout_blank_page.html">
					Blank Page</a>
				</li>
				<li>
					<a href="javascript:;">
						More options
						<span class="arrow"></span>
					</a>
					<ul class="sub-menu">
						<li><a href="#">Second level link</a></li>
						<li>
							<a href="javascript:;">More options<span class="arrow"></span></a>
							<ul class="sub-menu">
								<li><a href="#">Third level link</a></li>
								<li><a href="#">Third level link</a></li>
								<li><a href="#">Third level link</a></li>
								<li><a href="#">Third level link</a></li>
								<li><a href="#">Third level link</a></li>
							</ul>
						</li>
						<li><a href="#">Second level link</a></li>
						<li><a href="#">Second level link</a></li>
						<li><a href="#">Second level link</a></li>
					</ul>
				</li>
			</ul>
		</li>
		<li>
			<a href="">Tables</a>
		</li>
		<li>
			<a href="">Extra
				<span class="arrow"></span>
			</a>
			<ul class="sub-menu">
				<li><a href="index.html">About Us</a></li>
				<li><a href="index.html">Services</a></li>
				<li><a href="index.html">Pricing</a></li>
				<li><a href="index.html">FAQs</a></li>
				<li><a href="index.html">Gallery</a></li>
				<li><a href="index.html">Registration</a></li>
				<li><a href="index.html">2 Columns (Left)</a></li>
				<li><a href="index.html">2 Columns (Right)</a></li>
			</ul>
		</li>
	</ul>
</div>
<!-- END EMPTY PAGE SIDEBAR -->