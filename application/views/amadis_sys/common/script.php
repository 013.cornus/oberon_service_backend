<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<script src="<?php echo base_url('public/globel/js/jquery-1.10.1.min.js');?>" type="text/javascript"></script>
<script src="<?php echo base_url('public/globel/js/jquery-migrate-1.2.1.min.js');?>" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui-1.10.1.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="<?php echo base_url('public/globel/js/jquery-ui-1.10.1.custom.min.js');?>" type="text/javascript"></script>      
<script src="<?php echo base_url('public/globel/js/bootstrap.min.js');?>" type="text/javascript"></script>

<script src="<?php echo base_url('public/globel/js/jquery.slimscroll.min.js');?>" type="text/javascript"></script>
<script src="<?php echo base_url('public/globel/js/jquery.blockui.min.js');?>" type="text/javascript"></script>  
<script src="<?php echo base_url('public/globel/js/jquery.cookie.min.js');?>" type="text/javascript"></script>
<script src="<?php echo base_url('public/globel/js/jquery.uniform.min.js');?>" type="text/javascript" ></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<!-- <script src="<?php echo base_url('public/globel/js/chosen.jquery.min.js')?>" type="text/javascript"></script>
<script src="<?php echo base_url('public/globel/js/jquery.vmap.js');?>" type="text/javascript"></script>   
<script src="<?php echo base_url('public/globel/js/jquery.vmap.russia.js');?>" type="text/javascript"></script>
<script src="<?php echo base_url('public/globel/js/jquery.vmap.world.js');?>" type="text/javascript"></script>
<script src="<?php echo base_url('public/globel/js/jquery.vmap.europe.js');?>" type="text/javascript"></script>
<script src="<?php echo base_url('public/globel/js/jquery.vmap.germany.js');?>" type="text/javascript"></script>
<script src="<?php echo base_url('public/globel/js/jquery.vmap.usa.js');?>" type="text/javascript"></script>
<script src="<?php echo base_url('public/globel/js/jquery.vmap.sampledata.js');?>" type="text/javascript"></script>  
<script src="<?php echo base_url('public/globel/js/jquery.flot.js');?>" type="text/javascript"></script>
<script src="<?php echo base_url('public/globel/js/jquery.flot.resize.js');?>" type="text/javascript"></script>
<script src="<?php echo base_url('public/globel/js/jquery.pulsate.min.js');?>" type="text/javascript"></script> -->

<script src="<?php echo base_url('public/globel/js/date.js');?>" type="text/javascript"></script>
<script src="<?php echo base_url('public/globel/js/daterangepicker.js');?>" type="text/javascript"></script>     
<script src="<?php echo base_url('public/globel/js/jquery.gritter.js');?>" type="text/javascript"></script>

<!-- <script src="<?php echo base_url('public/globel/js/jquery.dataTables.js');?>" type="text/javascript"></script>
<script src="<?php echo base_url('public/globel/js/DT_bootstrap.js');?>" type="text/javascript"></script>
<script src="<?php echo base_url('public/globel/js/jquery.bootstrap.wizard.min.js');?>" type="text/javascript"></script>
<script src="<?php echo base_url('public/globel/js/fullcalendar.min.js');?>" type="text/javascript"></script>
<script src="<?php echo base_url('public/globel/js/jquery.easy-pie-chart.js');?>" type="text/javascript"></script>
<script src="<?php echo base_url('public/globel/js/jquery.sparkline.min.js');?>" type="text/javascript"></script>  --> 
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<!-- <script src="<?php echo base_url('public/globel/js/bootstrap-datetimepicker.js')?>" type="text/javascript"></script> -->
<script src="<?php echo base_url('public/globel/js/bootstrap-datepicker.js')?>" type="text/javascript"></script>
<script src="<?php echo base_url('public/globel/js/bootstrap-modalmanager.js');?>" type="text/javascript" ></script>
<script src="<?php echo base_url('public/globel/js/bootstrap-modal.js" type="text/javascript');?>" ></script>
<script src="<?php echo base_url('public/globel/js/bootstrap-timepicker.js');?>" type="text/javascript"></script>
<script src="<?php echo base_url('public/globel/js/bootstrap-colorpicker.js');?>" type="text/javascript" ></script>
<script src="<?php echo base_url('public/globel/js/form-components.js')?>" type="text/javascript"></script>  
<script src="<?php echo base_url('public/globel/js/form-wizard.js')?>" type="text/javascript"></script> 
<script src="<?php echo base_url('public/globel/js/table-managed.js')?>" type="text/javascript"></script>
<script src="<?php echo base_url('public/globel/js/form-samples.js')?>" type="text/javascript"></script>
<script src="<?php echo base_url('public/globel/js/jquery.validate.min.js')?>" type="text/javascript"></script> 
<script src="<?php echo base_url('public/globel/js/select2.min.js')?>" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->  
<script>
	jQuery(document).ready(function() {    
		// initlayout and core plugins
		App.init();
		TableManaged.init();
		// FormComponents.init();
		// FormWizard.init();
		FormSamples.init();
		// FormValidation.init();

		// Index.init();
		// Index.initJQVMAP();
		// Index.initCalendar();
		// Index.initCharts();
		// Index.initChat();
		// Index.initMiniCharts();
		// Index.initDashboardDaterange();
		// Index.initIntro();
	});
</script>
<!-- END JAVASCRIPTS -->