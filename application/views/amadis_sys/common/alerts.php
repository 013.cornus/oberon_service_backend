<?php $alert = $this->session->flashdata('alert');?>
<div class='<?php echo $alert['color'];?>'>
	<button class="close" data-dismiss="alert"></button>
	<div class="<?php echo $alert['sign'];?>">
		<?php echo $alert['text'];?>
	</div>
</div>