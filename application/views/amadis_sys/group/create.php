		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">    
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3 class="page-title">
							<?php echo $title; ?> <small><?php echo $title_small; ?></small>
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="<?php echo base_url('home');?>">首頁</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li>
								<a href="<?php echo base_url($this->router->fetch_class()); ?>"><?php echo $title; ?></a>
								<i class="icon-angle-right"></i>
							</li>
							<li><a href="#"><?php echo $title_small; ?></a></li>
							<li class="pull-right no-text-shadow" style="display: none;">
								<div id="dashboard-report-range" class="dashboard-date-range tooltips no-tooltip-on-touch-device responsive" data-tablet="" data-desktop="tooltips" data-placement="top" data-original-title="Change dashboard date range">
									<i class="icon-calendar"></i>
									<span></span>
									<i class="icon-angle-down"></i>
								</div>
							</li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN VALIDATION STATES-->
						<div class="portlet box green">
							<div class="portlet-title">
								<div class="caption"><i class="icon-reorder"></i><?php echo $title_small; ?></div>
								<div class="tools">
									<a href="javascript:;" class="collapse"></a>
									<!-- <a href="#portlet-config" data-toggle="modal" class="config"></a>
									<a href="javascript:;" class="reload"></a>
									<a href="javascript:;" class="remove"></a> -->
								</div>
							</div>
							<div class="portlet-body form">
								<!-- BEGIN FORM-->
								<h3></h3>
								<form action="<?php echo base_url('group/create');?>" class="form-horizontal"  method="post" name="submitform" id="submitform" onsubmit="return check_account();" enctype="multipart/form-data">
									<div class="alert alert-error hide">
										<button class="close" data-dismiss="alert"></button>
										錯誤！請完成所有必填項目
									</div>
									<div class="alert alert-success hide">
										<button class="close" data-dismiss="alert"></button>
										Your form validation is successful!
									</div>
									<div class="control-group">
										<label class="control-label">群組類別<span class="required">*</span></label>
										<div class="controls chzn-controls">
											<select name="group_type" id="form_3_chosen" class="span6 select2_category" data-with-diselect="1" name="options1" data-placeholder="請選擇權限" tabindex="1" disabled>
												<?php foreach($groupList as $key => $row){?>
													<option value="<?php echo $row['group_info_type']?>" <?php echo $selected[$key];?>><?php echo $row['group_info_name']?></option>
												<?php }?>
											</select>
											<span id="errorMsg"></span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">序號<span class="required">*</span></label>
										<div class="controls chzn-controls">
											<select name="register_key_id" id="register_key" class="span6 select2_category" required>
												<?php foreach($register_key_list as $key => $row){?>
													<option value="<?php echo $row['register_key_id']?>"><?php echo $row['register_key']?></option>
												<?php }?>
											</select>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">帳號<span class="required">*</span></label>
										<div class="controls">
											<input name="group_account" id="group_account" type="text" class="span6 m-wrap" required/><span id="bonusrateMsg"></span>
										</div>
									</div>			
									<div class="control-group">
										<label class="control-label">輸入密碼<span class="required">*</span></label>
										<div class="controls">
											<input name="group_password" id="group_password" type="password" class="span6 m-wrap" required/><span id="bonusrateMsg"></span>
										</div>
									</div>		
									<div class="control-group">
										<label class="control-label">再輸入一次密碼<span class="required">*</span></label>
										<div class="controls">
											<input name="group_password2" id="group_password2" type="password" class="span6 m-wrap" required/><span id="bonusrateMsg"></span>
										</div>
									</div>		
									<div class="form-actions">
										<button type="submit" class="btn green">
											新增 <div class="icon-save"></div>
										</button>
										<a href="<?php echo base_url('group');?>">
											<button type="button" class="btn">取消 <div class="icon-undo"></div></button>
										</a>
									</div>

								</form>
								<!-- END FORM-->
							</div>
						</div>
						<!-- END VALIDATION STATES-->
					</div>
				</div>
				<!-- END DASHBOARD STATS -->
				<div class="clearfix"></div>
			</div>
		</div>
		<!-- END PAGE CONTAINER-->    
	</div>
	<!-- END PAGE -->
</div>
<!-- END CONTAINER -->

<!-- BEGIN FOOTER -->
<div class="footer">
	<?php echo $footer; ?>
</div>
<!-- END FOOTER -->
<?php echo $script; ?>
<script src="<?php echo base_url('public/globel/js/form-samples.js')?>" type="text/javascript"></script> 
<script src="<?php echo base_url('public/globel/js/select2.min.js')?>" type="text/javascript"></script>
<script src="<?php echo base_url('public/globel/js/jquery.validate.min.js')?>" type="text/javascript"></script>
<script src="<?php echo base_url('public/globel/plugin/main/group-validation.js')?>" type="text/javascript"></script>

<script>
	jQuery(document).ready(function() {    
		FormValidation.init();
		FormSamples.init();
	});
</script>

<script type="text/javascript">
	function check_account(){
		var duplicate = 0;
		var account = $("#group_account").val();
		$.ajax({
			url:"<?php echo base_url('group/check_duplicate');?>",				
			method:"POST",
			dataType:"json",
			async:false,
			data:{
				account:account
			},					
			success:function(res){
				duplicate = res;
			},
			error : function(xhr, ajaxOptions, thrownError){
				$("portlet-body").append(xhr.status);
				$("body").append(xhr.responseText );

				alert(thrownError);
			}
		})

		if(duplicate == 1){
			alert('該帳號已被使用,請輸入新的帳號');
			return false;
		}
		else{
			return true;
		}
	}
</script>
<!-- BEGIN RULE -->

<!-- END RULE -->
</body>
<!-- END BODY -->
</html>