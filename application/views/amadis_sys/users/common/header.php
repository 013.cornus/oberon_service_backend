<div class="header navbar navbar-inverse navbar-fixed-top">
	<!-- BEGIN TOP NAVIGATION BAR -->
	<div class="navbar-inner">
		<div class="container">
			<!-- BEGIN LOGO -->
			<a class="brand" href="<?php echo base_url('users');?>">
				<div style="width: 170px;margin-top: -23px;">
					<img src="<?php echo base_url('public/globel/image/logo/system-logo-h.png');?>" alt="logo"/>
				</div>
			</a>
			<!-- END LOGO -->
			<!-- BEGIN HORIZANTAL MENU -->
			<div class="navbar hor-menu hidden-phone hidden-tablet">
				<div class="navbar-inner">
					<ul class="nav">
						<li class="visible-phone visible-tablet">
							<!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
							<form class="sidebar-search">
								<div class="input-box">
									<a href="javascript:;" class="remove"></a>
									<input type="text" placeholder="Search..." />            
									<input type="button" class="submit" value=" " />
								</div>
							</form>
							<!-- END RESPONSIVE QUICK SEARCH FORM -->
						</li>
						<li class="active">
							<a href="#"><i class="icon-home"></i> 首頁</a>
						</li>
						<li class="">
							<a data-toggle="dropdown" class="dropdown-toggle" href="javascript:;">
								<span class="selected"></span>
								<i class="icon-list-alt"></i> 訂單管理
								<span class="arrow"></span>     
							</a>
							<ul class="dropdown-menu">
								<li >
									<a href="#"><i class="icon-th-large"></i> 訂單列表</a>
								</li>
								<li >
									<a href="#"><i class="icon-th-large"></i> 商品列表</a>
								</li>
								<li >
									<a href="#"><i class="icon-th-large"></i> 合約列表</a>
								</li>
								<li >
									<a href="#"><i class="icon-th-large"></i> 購買紀錄</a>
								</li>
								<li class="dropdown-submenu">
									<a tabindex="-1" href="javascript:;">
										More options
										<span class="arrow"></span>
									</a>
									<ul class="dropdown-menu">
										<li><a tabindex="-1" href="#">Second level link</a></li>
											<!-- <li class="dropdown-submenu">
												<a tabindex="-1" href="javascript:;">More options<span class="arrow"></span></a>
												<ul class="dropdown-menu">
													<li><a tabindex="-1" href="#">Third level link</a></li>
													<li><a tabindex="-1" href="#">Third level link</a></li>
													<li><a tabindex="-1" href="#">Third level link</a></li>
													<li><a tabindex="-1" href="#">Third level link</a></li>
													<li><a tabindex="-1" href="#">Third level link</a></li>
												</ul>
											</li> -->
											<li><a tabindex="-1" href="#">Second level link</a></li>
											<li><a tabindex="-1" href="#">Second level link</a></li>
											<li><a tabindex="-1" href="#">Second level link</a></li>
										</ul>
									</li>
								</ul>
								<b class="caret-out"></b>                        
							</li>
							<li>
								<a data-toggle="dropdown" class="dropdown-toggle" href=""><i class="icon-bell"></i> 最新消息
									<span class="arrow"></span>
								</a>
								<ul class="dropdown-menu">
									<li><a href="#">產品資訊</a></li>
									<li><a href="#">方案推薦</a></li>
									<li><a href="#">股價資訊</a></li>
									<li><a href="#">常見問題Ｑ＆Ａ</a></li>
								</ul>
								<b class="caret-out"></b>    
							</li>
							<li style="display: none;">
								<a data-toggle="dropdown" class="dropdown-toggle" href=""><i class="icon-cogs"></i> 設定
									<span class="arrow"></span>
								</a>
								<ul class="dropdown-menu">
									<li><a href="#">About Us</a></li>
									<li><a href="#">Services</a></li>
									<li><a href="#">Pricing</a></li>
									<li><a href="#">FAQs</a></li>
									<li><a href="#">Gallery</a></li>
									<li><a href="#">Registration</a></li>
									<li><a href="#">2 Columns (Left)</a></li>
									<li><a href="#">2 Columns (Right)</a></li>
								</ul>
								<b class="caret-out"></b>                        
							</li>
							<li>
								<span class="hor-menu-search-form-toggler">&nbsp;</span>
								<div class="search-form hidden-phone hidden-tablet">
									<form class="form-search">
										<div class="input-append">
											<input type="text" placeholder="關鍵字..." class="m-wrap">
											<button type="button" class="btn"></button>
										</div>
									</form>
								</div>
							</li>
						</ul>
					</div>
				</div>
				<!-- END HORIZANTAL MENU -->
				<!-- BEGIN RESPONSIVE MENU TOGGLER -->
				<a href="javascript:;" class="btn-navbar collapsed" data-toggle="collapse" data-target=".nav-collapse">
					<img src="<?php echo base_url('public/globel/image/menu-toggler.png');?>" alt="" />
				</a>          
				<!-- END RESPONSIVE MENU TOGGLER -->            
				<!-- BEGIN TOP NAVIGATION MENU -->              
				<ul class="nav pull-right">
					<!-- BEGIN NOTIFICATION DROPDOWN -->   
					<li class="dropdown" id="header_notification_bar">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<i class="icon-warning-sign"></i>
							<span class="badge">6</span>
						</a>
						<ul class="dropdown-menu extended notification">
							<li>
								<p>You have 14 new notifications</p>
							</li>
							<li>
								<a href="#">
									<span class="label label-success"><i class="icon-plus"></i></span>
									New user registered. 
									<span class="time">Just now</span>
								</a>
							</li>
							<li>
								<a href="#">
									<span class="label label-important"><i class="icon-bolt"></i></span>
									Server #12 overloaded. 
									<span class="time">15 mins</span>
								</a>
							</li>
							<li>
								<a href="#">
									<span class="label label-warning"><i class="icon-bell"></i></span>
									Server #2 not respoding.
									<span class="time">22 mins</span>
								</a>
							</li>
							<li>
								<a href="#">
									<span class="label label-info"><i class="icon-bullhorn"></i></span>
									Application error.
									<span class="time">40 mins</span>
								</a>
							</li>
							<li>
								<a href="#">
									<span class="label label-important"><i class="icon-bolt"></i></span>
									Database overloaded 68%. 
									<span class="time">2 hrs</span>
								</a>
							</li>
							<li>
								<a href="#">
									<span class="label label-important"><i class="icon-bolt"></i></span>
									2 user IP blocked.
									<span class="time">5 hrs</span>
								</a>
							</li>
							<li class="external">
								<a href="#">See all notifications <i class="m-icon-swapright"></i></a>
							</li>
						</ul>
					</li>
					<!-- END NOTIFICATION DROPDOWN -->
					<!-- BEGIN INBOX DROPDOWN -->
					<li class="dropdown" id="header_inbox_bar">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<i class="icon-envelope"></i>
							<span class="badge">5</span>
						</a>
						<ul class="dropdown-menu extended inbox">
							<li>
								<p>You have 12 new messages</p>
							</li>
							<li>
								<a href="inbox.html?a=view">
									<span class="photo"><img src="media/image/avatar2.jpg" alt="" /></span>
									<span class="subject">
										<span class="from">Lisa Wong</span>
										<span class="time">Just Now</span>
									</span>
									<span class="message">
										Vivamus sed auctor nibh congue nibh. auctor nibh
										auctor nibh...
									</span>  
								</a>
							</li>
							<li>
								<a href="inbox.html?a=view">
									<span class="photo"><img src="media/image/avatar3.jpg" alt="" /></span>
									<span class="subject">
										<span class="from">Richard Doe</span>
										<span class="time">16 mins</span>
									</span>
									<span class="message">
										Vivamus sed congue nibh auctor nibh congue nibh. auctor nibh
										auctor nibh...
									</span>  
								</a>
							</li>
							<li>
								<a href="inbox.html?a=view">
									<span class="photo"><img src="media/image/avatar1.jpg" alt="" /></span>
									<span class="subject">
										<span class="from">Bob Nilson</span>
										<span class="time">2 hrs</span>
									</span>
									<span class="message">
										Vivamus sed nibh auctor nibh congue nibh. auctor nibh
										auctor nibh...
									</span>  
								</a>
							</li>
							<li class="external">
								<a href="inbox.html">See all messages <i class="m-icon-swapright"></i></a>
							</li>
						</ul>
					</li>
					<!-- END INBOX DROPDOWN -->
					<!-- BEGIN TODO DROPDOWN -->
					<li class="dropdown" id="header_task_bar">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<i class="icon-tasks"></i>
							<span class="badge">5</span>
						</a>
						<ul class="dropdown-menu extended tasks">
							<li>
								<p>You have 12 pending tasks</p>
							</li>
							<li>
								<a href="#">
									<span class="task">
										<span class="desc">New release v1.2</span>
										<span class="percent">30%</span>
									</span>
									<span class="progress progress-success ">
										<span style="width: 30%;" class="bar"></span>
									</span>
								</a>
							</li>
							<li>
								<a href="#">
									<span class="task">
										<span class="desc">Application deployment</span>
										<span class="percent">65%</span>
									</span>
									<span class="progress progress-danger progress-striped active">
										<span style="width: 65%;" class="bar"></span>
									</span>
								</a>
							</li>
							<li>
								<a href="#">
									<span class="task">
										<span class="desc">Mobile app release</span>
										<span class="percent">98%</span>
									</span>
									<span class="progress progress-success">
										<span style="width: 98%;" class="bar"></span>
									</span>
								</a>
							</li>
							<li>
								<a href="#">
									<span class="task">
										<span class="desc">Database migration</span>
										<span class="percent">10%</span>
									</span>
									<span class="progress progress-warning progress-striped">
										<span style="width: 10%;" class="bar"></span>
									</span>
								</a>
							</li>
							<li>
								<a href="#">
									<span class="task">
										<span class="desc">Web server upgrade</span>
										<span class="percent">58%</span>
									</span>
									<span class="progress progress-info">
										<span style="width: 58%;" class="bar"></span>
									</span>
								</a>
							</li>
							<li>
								<a href="#">
									<span class="task">
										<span class="desc">Mobile development</span>
										<span class="percent">85%</span>
									</span>
									<span class="progress progress-success">
										<span style="width: 85%;" class="bar"></span>
									</span>
								</a>
							</li>
							<li class="external">
								<a href="#">See all tasks <i class="m-icon-swapright"></i></a>
							</li>
						</ul>
					</li>
					<!-- END TODO DROPDOWN -->
					<!-- BEGIN USER LOGIN DROPDOWN -->
					<li class="dropdown user">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<!-- <img alt="" src="<?php echo base_url('public/globel/image/avatar1_small.jpg');?>" /> -->
							<div style="padding:4px; ">
								<i class="icon-user"></i>
								<!-- <i class="fa fa-camera-retro" style="font-size:24px"></i> -->
								<span class="username"><?php echo $this->session->userdata('users_name');?></span>
								<i class="icon-angle-down"></i>
							</div>
						</a>
						<ul class="dropdown-menu">
							<li><a href="<?php echo base_url('home/logout'); ?>"><i class="icon-key"></i> 登出</a></li>
						</ul>
					</li>
					<!-- END USER LOGIN DROPDOWN -->
				</ul>
				<!-- END TOP NAVIGATION MENU --> 
			</div>
		</div>
		<!-- END TOP NAVIGATION BAR -->
	</div>