<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<?php echo require_once('common/head.php'); ?>
	<!-- BEGIN PAGE LEVEL STYLES -->
	<link rel="stylesheet" type="text/css" href="./public/media/css/select2_metro.css" />
	<link rel="stylesheet" href="./public/media/css/DT_bootstrap.css" />
	<link rel="stylesheet" type="text/css" href="./public/media/css/chosen.css" />
	<!-- END PAGE LEVEL STYLES -->
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-header-fixed">
	<!-- BEGIN HEADER -->
	<div class="header navbar navbar-inverse navbar-fixed-top">
		<!-- BEGIN TOP NAVIGATION BAR -->
		<?php require_once('common/header.php'); ?>
		<!-- END TOP NAVIGATION BAR -->
	</div>
	<!-- END HEADER -->
	<div class="copyrights">Collect from <a href="#" ></a></div>
	<!-- BEGIN CONTAINER -->
	<div class="page-container">
		<!-- BEGIN SIDEBAR -->
		<div class="page-sidebar nav-collapse collapse">
			<?php require_once('common/sidebar.php'); ?>
		</div>
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3 class="page-title">
							<?php echo $title; ?> <small><?php echo $title_small; ?></small>
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="<?php echo base_url('home');?>">首頁</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li>
								<a href="<?php echo base_url('customers');?>"><?php echo $title; ?></a>
								<i class="icon-angle-right"></i>
							</li>
							<li><a href="#"><?php echo $title_small; ?></a></li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN VALIDATION STATES-->
						<div class="portlet box green">
							<div class="portlet-title">
								<div class="caption"><i class="icon-user"></i><?php echo $title_small; ?></div>
								<div class="tools">
									<a href="javascript:;" class="collapse"></a>
									<!-- <a href="#portlet-config" data-toggle="modal" class="config"></a>
									<a href="javascript:;" class="reload"></a>
									<a href="javascript:;" class="remove"></a> -->
								</div>
							</div>
							<div class="portlet-body form">
								<!-- BEGIN FORM-->
								<h3></h3>
								<form action="<?php echo base_url('customers/update');?>" name="submitform" id="submitform" class="form-horizontal"  method="post">
									<div class="alert alert-error hide">
										<button class="close" data-dismiss="alert"></button>
										You have some form errors. Please check below.
									</div>
									<div class="alert alert-success hide">
										<button class="close" data-dismiss="alert"></button>
										Your form validation is successful!
									</div>
									<div class="control-group">
										<label class="control-label">姓名<span class="required">*</span></label>
										<div class="controls">
											<input type="text" name="name" id="name" data-required="1" class="span6 m-wrap" value="<?php foreach($result as $row){echo $row['employee_name'];}?>"/>
											<span id="namesMsg"></span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">身分證字號<span class="required">*</span></label>
										<div class="controls">
											<input name="idcard" id="idcard" type="text" class="span6 m-wrap" value="<?php foreach($result as $row){echo $row['employee_id_card'];}?>"/><span id="idMsg"></span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">所屬單位<span class="required">*</span></label>
										<div class="controls">
											<select class="span6 m-wrap" name="organization" id="organization">
												<?php foreach ($organization as $r) { foreach($result as $d){?>
												<option value="<?php echo $r["organization_id"];?>" <?php if($r["organization_id"]==$d["organization_id"])
												{echo "selected='selected'";}?>><?php echo $r["organization_name"];?></option>
											    <?php }}?>
											</select>
											<span id="organizationMsg"></span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">聯絡手機<span class="required">*</span></label>
										<div class="controls">
											<input name="cellphone" id="cellphone" type="text" class="span6 m-wrap" value="<?php foreach($result as $row){echo $row['employee_phone'];}?>"/>
											<span id="cellphoneMsg"></span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">聯絡電話<span class="required">*</span></label>
										<div class="controls">
											<input name="telephone" id="telephone" type="text" class="span6 m-wrap" value="<?php foreach($result as $row){echo $row['employee_tel_1'];}?>"/>
											<span id="telephoneMsg"></span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">聯絡地址<span class="required">*</span></label>
										<div class="controls">
											<select class="span3 m-wrap" name="address" id="address" style="width: auto;" >
												<?php foreach ($address as $r) { foreach($result as $d){?>
												<option value="<?php echo $r["address_id"];?>" <?php if($r["address_id"]==$d["address_id"])
												{echo "selected='selected'";}?>><?php echo $r["address_city"].'&nbsp;'.$r["address_area"];?></option>
											    <?php }}?>
											</select>
											<input name="addressdetail" type="text" class="span4 m-wrap" value="<?php foreach($result as $row){echo $row['employee_address'];}?>"/>
											<span id="addressMsg"></span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">推薦人<span class="required">*</span></label>
										<div class="controls">
											<input name="referrer" id="referrer" type="text" class="span6 m-wrap" value="<?php foreach($result as $row){echo $row['users_id'];}?>"/>
											<span id="referrerMsg"></span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">職稱<span class="required">*</span></label>
										<div class="controls">
											<select class="span6 m-wrap" name="occupation" id="occupation">
												<?php foreach ($occupation as $r) { foreach($result as $d){?>
												<option value="<?php echo $r["occupation_id"];?>" <?php if($r["occupation_id"]==$d["occupation_id"])
												{echo "selected='selected'";}?>><?php echo $r["occupation_name"];?></option>
											    <?php }}?>
											</select>
											<span id="occupationMsg"></span>
										</div>
									</div>
									<div class="form-actions">
										<button type="submit" class="btn green" name="id" value="<?php echo $upid;?>" onclick="check()">儲存 <div class="icon-save"></div></button>
										<a href="<?php echo base_url('customers');?>">
											<button type="button" class="btn">返回 <div class="icon-undo"></div></button>
										</a>
									</div>
								</form>
								<!-- END FORM-->
							</div>
						</div>
						<!-- END VALIDATION STATES-->
					</div>
				</div>
				<!-- END DASHBOARD STATS -->
				<div class="clearfix"></div>
			</div>
		</div>
		<!-- END PAGE CONTAINER-->    
	</div>
	<!-- END PAGE -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<div class="footer">
	<?php require_once('common/footer.php'); ?>
</div>
<!-- END FOOTER -->
<?php require_once('common/script.php'); ?>
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="./public/media/js/select2.min.js"></script>
<script type="text/javascript" src="./public/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="./public/media/js/DT_bootstrap.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="./public/media/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="./public/media/js/additional-methods.min.js"></script>
<script type="text/javascript" src="./public/media/js/jquery.bootstrap.wizard.min.js"></script>
<script type="text/javascript" src="./public/media/js/chosen.jquery.min.js"></script>
<script type="text/javascript" src="./public/media/js/select2.min.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="./public/media/js/app.js"></script>
<script src="./public/media/js/form-wizard.js"></script>     
<!-- END PAGE LEVEL SCRIPTS -->
<script src="./public/media/js/app.js"></script>
<script src="./public/media/js/table-managed.js"></script> 
<?php require_once('employee/rule.php'); ?>    
<script>
	jQuery(document).ready(function() {       
		   // App.init();
		   TableManaged.init();
		   FormWizard.init();
		});
	</script>

</body>
<!-- END BODY -->
</html>