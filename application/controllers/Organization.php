<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Organization extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('amadis_sys/organization_model');
		$this->load->model('amadis_sys/competence_model');
		$this->load->model('amadis_sys/sidebarmenu_model');
		$this->load->model('amadis_sys/address_model');
		$this->load->model('amadis_sys/employee_model');
		$this->load->model('amadis_sys/pagintion_model');
		$this->unitName = '營業處管理';

		$admin_id = $this->session->userdata('bms_users_id');
		$username = $this->session->userdata('bms_users_name');
		$this->competence_id = $this->session->userdata('bms_competence_id');
		$this->rightid = $this->competence_model->getaction($this->competence_id);

		$this->session->set_flashdata('sidebarselected','organization');
		$this->session->set_flashdata('mainsidebar','organization');
		
		if(!$admin_id || $this->competence_id >= 4){
			redirect('home/login');
		}
	}

    //導入設定
	public function index(){
		redirect('organization/list');
		exit;	
	}

	//營業處列表
	public function list($page=''){
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 12) {
				if($r['actions_view'] == 0) {
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div> 您沒有瀏覽的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('home');
				}
			}
		}

		$keyword = $this->input->post('fullname');
		$data = [];
		$data['title'] = $this->unitName;
		$data['title_small'] = '營業處列表';

		$result = $this->organization_model->getList($keyword,[]);

		$pager = $this->pagintion_model->setPager($result,$page);
		$data['page_list'] = $pager['page'];
		$data['result'] = $this->organization_model->getList($keyword,$pager['list']);

		foreach ($data['result'] as $key => $value) {
			$data['employee'][$key] = $this->employee_model->getidData($value['employee_id']);
		}
		
		$data['mainbar'] = $this->sidebarmenu_model->getsidebarmain();
		$data['subbar'] = $this->sidebarmenu_model->getsidebarsub();
		$data['view'] = $this->competence_model->getaction($this->competence_id);

		$data['footer'] = $this->load->view('amadis_sys/common/footer','', TRUE);
		$data['script'] = $this->load->view('amadis_sys/common/script','', TRUE);
		$this->load->view('amadis_sys/common/head');
		$this->load->view('amadis_sys/common/header');
		$this->load->view('amadis_sys/common/sidebar',$data);
		$this->load->view('amadis_sys/organization/list',$data);	
	}

	//跳到指定頁面
	public function pagejump(){
		if($this->input->post('pagenum') == null || $this->input->post('pagenum') == 0){
			redirect('organization/list');
		}
		else{
			redirect('organization/list/'.$this->input->post('pagenum').'');
		}
	}

    // 新增營業處表單
	public function create_form(){
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 12) {
				if($r['actions_insert'] == 0) {
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div> 您沒有新增的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('organization/list');
				}
			}
		}

		$data = [];
		$data['title'] = $this->unitName;
		$data['title_small'] = '新增營業處';

		$selection['status'] = 1;
		$data['organization'] = $this->organization_model->getList($selection);
		$keyword['status'] = 1;
		$data['employee'] = $this->employee_model->getList($keyword);
		$data['address'] = $this->address_model->getList();

		$data['mainbar'] = $this->sidebarmenu_model->getsidebarmain();
		$data['subbar'] = $this->sidebarmenu_model->getsidebarsub();
		$data['view'] = $this->competence_model->getaction($this->competence_id);

		$data['footer'] = $this->load->view('amadis_sys/common/footer','', TRUE);
		$data['script'] = $this->load->view('amadis_sys/common/script','', TRUE);
		$this->load->view('amadis_sys/common/head');
		$this->load->view('amadis_sys/common/header');
		$this->load->view('amadis_sys/common/sidebar',$data);
		$this->load->view('amadis_sys/organization/create',$data);
	}

	// 新增營業處執行
	public function create(){
		$datetime = date ("Y-m-d H:i:s") ; 
		$organization = array(
			'organization_name'         => $this->input->post('organization'),
			'employee_id'               => $this->input->post('principal'),
			'organization_parent_unit'  => $this->input->post('parentunit'),
			'organization_referrer'     => $this->input->post('referrer'),
			'organization_address'      => $this->input->post('address'),
			'organization_tel'          => $this->input->post('tel'),
			'organization_fax'          => $this->input->post('fax'),
			'organization_created_date' => $datetime,
			'organization_created_user' => $this->session->userdata('bms_users_id')
		);

		$this->organization_model->add_organization($organization);
		$messagediv = "<div class='alert alert-success'>
		<button class='close' data-dismiss='alert'></button>
		<div class='icon-ok'></div> 成功！資料已新增。
		</div>";
		$this->session->set_flashdata('messagediv',$messagediv);

		redirect('organization/list');
	}

	// 刪除營業處執行
	public function delete(){
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 12)
			{
				if($r['actions_delete'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div> 您沒有刪除的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('organization/list');
				}
			}
		}
		$id = $this->input->get('id');
		$datetime = date("Y-m-d H:i:s"); 
		$user_id = $this->session->userdata('bms_users_id');
		$organizationupdate = array(
			'organization_is_del'       => 1,
			'organization_updated_date' => $datetime,
			'organization_updated_user' => $user_id,
		);

		$this->organization_model->update_organization($organizationupdate,$id);
		$messagediv = "<div class='alert alert-success'>
		<button class='close' data-dismiss='alert'></button>
		<div class='icon-ok'></div> 成功！該筆資料已刪除。
		</div>";
		$this->session->set_flashdata('messagediv',$messagediv);
		redirect('organization/list');
	}

	// 下架營業處執行
	public function info_invisible(){
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 12)
			{
				if($r['actions_enable'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div> 您沒有上下架的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('organization/list');
				}
			}
		}
		$id = $this->input->get('id');
		$datetime = date("Y-m-d H:i:s"); 
		$user_id = $this->session->userdata('bms_users_id');
		$organizationupdate = array(
			'organization_status'       => 0,
			'organization_updated_date' => $datetime,
			'organization_updated_user' => $user_id,
		);

		$this->organization_model->update_organization($organizationupdate,$id);

		$messagediv = "<div class='alert alert-success'>
		<button class='close' data-dismiss='alert'></button>
		<div class='icon-ok'></div> 成功！該筆營業處已停用。
		</div>";

		$this->session->set_flashdata('messagediv',$messagediv);
		redirect('organization/list');
	}

	// 上架營業處執行
	public function info_visible(){
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 12)
			{
				if($r['actions_enable'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div> 您沒有上下架的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('organization/list');
				}
			}
		}
		$id = $this->input->get('id');
		$datetime = date("Y-m-d H:i:s"); 
		$user_id = $this->session->userdata('bms_users_id');
		$organizationupdate = array(
			'organization_status'       => 1,
			'organization_updated_date' => $datetime,
			'organization_updated_user' => $user_id,
		);

		$this->organization_model->update_organization($organizationupdate,$id);
		$messagediv = "<div class='alert alert-success'>
		<button class='close' data-dismiss='alert'></button>
		<div class='icon-ok'></div> 成功！該筆營業處已啟用。
		</div>";
		$this->session->set_flashdata('messagediv',$messagediv);
		redirect('organization/list');
	}

	// 檢視營業處表單
	public function check_form(){
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 12)
			{
				if($r['actions_view'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div> 您沒有修改的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('organization/list');
				}
			}
		}
		$data = [];
		$id = $this->input->get('id');

		$data['result'] = $this->organization_model->getData($id);

		$selection['status'] = 1;
		$data['organization'] = $this->organization_model->getList($selection);
		$keyword['status'] = 1;
		$data['employee'] = $this->employee_model->getList($keyword);
		$data['address'] = $this->address_model->getList();

		$data['mainbar'] = $this->sidebarmenu_model->getsidebarmain();
		$data['subbar'] = $this->sidebarmenu_model->getsidebarsub();
		$data['view'] = $this->competence_model->getaction($this->competence_id);
		$data['upid'] = $this->input->get('id');
		$data['title'] = $this->unitName;
		$data['title_small'] = '檢視營業處';
		$data['active'] = 'goods';

		$data['footer'] = $this->load->view('amadis_sys/common/footer','', TRUE);
		$data['script'] = $this->load->view('amadis_sys/common/script','', TRUE);
		$this->load->view('amadis_sys/common/head');
		$this->load->view('amadis_sys/common/header');
		$this->load->view('amadis_sys/common/sidebar',$data);
		$this->load->view('amadis_sys/organization/check',$data);
	}

	// 修改營業處表單
	public function update_form(){
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 12) {
				if($r['actions_update'] == 0) {
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div> 您沒有修改的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('organization/list');
				}
			}
		}
		$data = [];
		$id = $this->input->get('id');

		$data['result'] = $this->organization_model->getData($id);
		$selection['status'] = 1;
		$data['organization'] = $this->organization_model->getList($selection);
		$keyword['status'] = 1;
		$data['employee'] = $this->employee_model->getList($keyword);
		$data['address'] = $this->address_model->getList();

		$data['mainbar'] = $this->sidebarmenu_model->getsidebarmain();
		$data['subbar'] = $this->sidebarmenu_model->getsidebarsub();
		$data['view'] = $this->competence_model->getaction($this->competence_id);

		$data['upid'] = $this->input->get('id');
		$data['title'] = $this->unitName;
		$data['title_small'] = '編輯營業處';
		$data['active'] = 'goods';

		$data['footer'] = $this->load->view('amadis_sys/common/footer','', TRUE);
		$data['script'] = $this->load->view('amadis_sys/common/script','', TRUE);
		$this->load->view('amadis_sys/common/head');
		$this->load->view('amadis_sys/common/header');
		$this->load->view('amadis_sys/common/sidebar',$data);
		$this->load->view('amadis_sys/organization/update',$data);
	}

    // 修改營業處執行
	public function update(){
		$id = $this->input->post('id');
		$datetime = date ("Y-m-d H:i:s") ; 
		$organizationupdate = array(
			'organization_name'         => $this->input->post('organization'),
			'employee_id'               => $this->input->post('principal'),
			'organization_parent_unit'  => $this->input->post('parentunit'),
			'organization_referrer'     => $this->input->post('referrer'),
			'address_id'                => $this->input->post('address'),
			'organization_address'      => $this->input->post('address_info'),
			'organization_tel'          => $this->input->post('tel'),
			'organization_fax'          => $this->input->post('fax'),
			'organization_updated_date' => $datetime,
			'organization_updated_user' => $this->session->userdata('bms_users_id'),
		);

		$this->organization_model->update_organization($organizationupdate,$id);
		$messagediv = "<div class='alert alert-success'>
		<button class='close' data-dismiss='alert'></button>
		<div class='icon-ok'></div> 成功！此筆資料已更新。
		</div>";
		$this->session->set_flashdata('messagediv',$messagediv);
		redirect('organization/list');
	}

}
