<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Unittest extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('amadis_sys/account_model');
		$this->load->model('amadis_sys/competence_model');
		$this->load->model('amadis_sys/employee_model');
		$this->load->model('amadis_sys/order_model');
		$this->load->model('amadis_sys/order_info_model');
		$this->load->model('amadis_sys/organization_model');
		$this->load->model('amadis_sys/goods_model');
		// $this->load->model('amadis_sys/bouns_model');
		$this->load->model('amadis_sys/sidebarmenu_model');

		$this->unitName = '';
		$admin_id = $this->session->userdata('users_id');
		$username = $this->session->userdata('users_name');
		$competence_id = $this->session->userdata('competence_id');

		$this->session->set_flashdata('sidebarselected','account');
		$this->session->set_flashdata('mainsidebar','system');

		if(!$admin_id || $competence_id >= 4){
			redirect('home/login');
		}
	}

	public function index(){
		$data = [];
		$data['title'] = $this->unitName;
		$data['title_small'] = '';
	}

	public function ordertest(){
		$data = [];
		$data['title'] = $this->unitName;
		$data['title_small'] = '';
		$keyword = [];

		$result = $this->order_model->getList($keyword,[]);
		$result1 = $this->order_model->getList1($keyword,[]);
		$count = 0;
		$count_error = 0;

		foreach ($result as $key => $value) {
			foreach ($result1 as $key => $val) {
				if ($value['order_id'] == $val['order_id']) {
					if ($value['order_buy_date'] == $val['order_created_date'] && $value['order_active_date'] == $val['order_active_date']) {
						echo $value['order_id'].'===合約生效日==>'.$value['order_buy_date'].'=== 承購日期==>'.$value['order_active_date'].'<br>';
					} else {
						echo $value['contract_id'].'===<b style="color:red;">'.$value['order_id'].'===合約生效日==>'.$value['order_buy_date'].'=== 承購日期==>'.$value['order_active_date'].'</b><br>';
						$count_error++;
					}
				}
			}
			$count++;
		}
		echo 'TOTAL：'.$count.'<br>';
		echo 'ERROR：'.$count_error;
		// echo '<pre>';
		// print_r($result);
		// echo '<pre>';
	}

}
