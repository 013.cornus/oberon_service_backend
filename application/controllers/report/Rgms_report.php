<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rgms_report extends RGMS_Core {

	public function __construct(){
		parent::__construct();
		$this->load->model('amadis_sys/competence_model');
		$this->load->model('amadis_sys/report_template_setting_model');
		$this->load->model('amadis_sys/excel_template_model');
		$this->load->model('amadis_sys/sidebarmenu_model');
		$this->load->model('amadis_sys/pagintion_model');
		$this->unitName = '產品管理';
		$this->competence_id = $this->session->userdata('competence_id');
		$this->rightid = $this->competence_model->getaction($this->competence_id);
		

		$admin_id = $this->session->userdata('users_id');
		$username = $this->session->userdata('users_name');
		$this->session->set_flashdata('sidebarselected','rgms_report_2');
		$this->session->set_flashdata('mainsidebar','rgms_report_2');
		$this->load->library('excel');
		$this->load->library('apiconnection');
		if(!$admin_id){
			redirect('home/login');
		}
	}

    //產品列表
	public function index(){
		redirect('rgms_report_2/rgms_report_2/list');
		exit;
	}
    //產品列表
	public function list($page=''){
		$organization_id = $this->session->userdata('organization_id');
		$manager_id = $this->session->userdata('manager_id');
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 7)
			{
				if($r['actions_view'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div> 您沒有瀏覽的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('home');
				}
			}
		}
		$data = [];
		$data['title'] = $this->unitName;
		$data['title_small'] = '匯出報表列表';
		$data['active'] = 'employee';

		$keyword['fullname'] = $this->input->post('fullname');
		$data['competence'] = $this->competence_model->getdata();

		//受測者資料
		$data['item'] = json_decode($this->apiconnection->apiConnect('','http://34.80.127.28/web/tw/api/v2/detection','post'),true)['Data'];

		//模板資料
		$data['template'] = $this->excel_template_model->getList();

		//選單
		$data['mainbar'] = $this->sidebarmenu_model->getsidebarmain();
		$data['subbar'] = $this->sidebarmenu_model->getsidebarsub();
		$data['view'] = $this->competence_model->getaction($this->competence_id);

		$data['footer'] = $this->load->view('amadis_sys/common/footer','', TRUE);
		$data['script'] = $this->load->view('amadis_sys/common/script','', TRUE);
		$this->load->view('amadis_sys/common/head');
		$this->load->view('amadis_sys/common/header');
		$this->load->view('amadis_sys/common/sidebar',$data);
		$this->load->view('amadis_sys/rgms_report_2/rgms_report_2/create',$data);
	}

	// 匯出pdf執行
	public function create(){
		$post = $this->input->post();

		$file_path = 'C:\xampp\htdocs\bms_model\public\report\34.80.127.28/'.$post['source_data_id'].'/';
		if(!file_exists($file_path)){
			mkdir($file_path);
		}

		//讀取來源檔
		// $objPHPExcel = PHPExcel_IOFactory::load('C:/xampp/htdocs/bms_model/public/uploads/excel_model/Report_data.xls');

		//查詢模板資料
		$template = $this->excel_template_model->getidData($post['template']);

		//設定excel樣板
		$srcfilename = 'C:/xampp/htdocs/bms_model/public/uploads/excel_model/'.$template['excel_template_name'];
		//設定匯出路徑與名稱
		$destfilename = 'C:\Users\user\Desktop\pdf_save\.sssspdf';
		$datetime = date('YmdHis');
		try {

			if(!file_exists($srcfilename)){

				return;

			}

			$excel = new \COM("excel.application") or die("Unable to instantiate excel");
			$excel->DisplayAlerts = 0;
			$excel->ScreenUpdating  = 0;
			$excel->DisplayStatusBar = 0;
			$excel->EnableEvents = 0;
			$workbook = $excel->Workbooks->Open($srcfilename, null, false, null, "1", "1", true);
			if($post['importByTemplate'] == 1){
				//查詢模板設定
				$seletion['excel_template_id'] = $post['template'];
				$templateSetting = json_decode($this->report_template_setting_model->getDataBySelection($seletion)['report_template_setting_content'],true);

				//查詢受測者資料
				$value = $this->sourceDataToArray($post['source_data_id'],'http://34.80.127.28/web/tw/api/v2/report')['value'];

				$pageValue = 0;
				foreach($templateSetting as $key => $row){
					$target = explode(',',$row);
					$sheets = $workbook->Worksheets((int)$target[0]);
					// $result = iconv('UTF-8', 'BIG5', $dataValue);

					$result = mb_convert_encoding($value[$key], 'BIG5');;

					//判斷是否為圖片
					if(substr($result,-3) == 'jpg' || substr($result,-3) == 'png'){
						//判斷圖片網址是否失效
						if(fopen($result,'r')){
							$sheets->Shapes->AddPicture($result,False,True,$sheets->Range($target[1])->MergeArea->left,$sheets->Range($target[1])->MergeArea->top,$sheets->Range($target[1])->MergeArea->Width,$sheets->Range($target[1])->MergeArea->Height);
						}
					}
					else{
						$col = substr($target[1],0,1);
						$row = substr($target[1],1);
						$sheets->cells($row,$col)->value = $result;
						// $sheets->Range($target[1])->value = $result;
					}
				}
			}
			else{
				foreach($post['destPage'] as $key => $row){
					$sheets = $workbook->Worksheets((int)$row);
					$result = iconv('UTF-8', 'BIG5', $post['competence_data'][$key]);
					//判斷是否為圖片
					if(substr($result,-3) == 'jpg' || substr($result,-3) == 'png'){
						//判斷圖片網址是否失效
						if(fopen($result,'r')){
							$sheets->Shapes->AddPicture($result,False,True,$sheets->Range($post['destCol'][$key])->MergeArea->left,$sheets->Range($post['destCol'][$key])->MergeArea->top,$sheets->Range($post['destCol'][$key])->MergeArea->Width,$sheets->Range($post['destCol'][$key])->MergeArea->Height);
						}
					}
					else{
						// $col = substr($post['destCol'][$key],0,1);
						// $row = substr($post['destCol'][$key],1);
						// $sheets->cells($row,$col)->value = $result;
						$sheets->Range($post['destCol'][$key])->value = $result;
					}
				}
			}

			$excel->ScreenUpdating  = 1;
			$excel->DisplayStatusBar = 1;
			$excel->EnableEvents = 1;
			$workbook->SaveAs($file_path.$post['source_data_id'].'_'.$datetime.'.xlsx',51);
			$workbook->Close();
			$excel->Quit();
			unset($excel);

		} catch (\Exception $e) {

			echo ("src:$srcfilename catch exception:" . $e->__toString());

			if (method_exists($excel, "Quit")){

				$excel->Quit();

			}

			return;

		}

		//設定excel樣板
		$srcfilename = $file_path.$post['source_data_id'].'_'.$datetime.'.xlsx';
		//設定匯出路徑與名稱
		$destfilename = $file_path.$post['source_data_id'].'_'.$datetime.'.pdf';

		try {

			if(!file_exists($srcfilename)){

				return;

			}

			$excel = new \COM("excel.application") or die("Unable to instantiate excel");
			$excel->DisplayAlerts = 0;
			$workbook2 = $excel->Workbooks->Open($srcfilename, null, false, null, "1", "1", true);
			$workbook2->ExportAsFixedFormat(0, $destfilename,0,0,0,1);
			$workbook2->Close();
			$excel->Quit();
			unset($excel);

		} catch (\Exception $e) {

			echo ("src:$srcfilename catch exception:" . $e->__toString());

			if (method_exists($excel, "Quit")){

				$excel->Quit();

			}

			return;

		}

		header('Content-disposition: attachment; filename=test.pdf');
		header("Content-type:application/pdf");
		readfile($file_path.$post['source_data_id'].'_'.$datetime.'.pdf');

		redirect('rgms_report_2/rgms_report_2');
	}
}
