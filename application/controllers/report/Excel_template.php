<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class excel_template extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('amadis_sys/report_template_setting_model');
		$this->load->model('amadis_sys/excel_template_model');
		$this->load->model('amadis_sys/competence_model');
		$this->load->model('amadis_sys/sidebarmenu_model');
		$this->load->model('amadis_sys/pagintion_model');
		$this->unitName = '模板設定';
		$this->competence_id = $this->session->userdata('competence_id');
		$this->rightid = $this->competence_model->getaction($this->competence_id);
		

		$admin_id = $this->session->userdata('users_id');
		$username = $this->session->userdata('users_name');
		$this->session->set_flashdata('sidebarselected','excel_template_2');
		$this->session->set_flashdata('mainsidebar','excel_template_2');
		$this->load->library('apiconnection');

		if(!$admin_id){
			redirect('home/login');
		}
	}

    //Excel模板管理列表
	public function index(){
		redirect('rgms_report_2/excel_template_2/list');
		exit;
	}
    //Excel模板管理列表
	public function list($page=''){
		$organization_id = $this->session->userdata('organization_id');
		$manager_id = $this->session->userdata('manager_id');
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 55)
			{
				if($r['actions_view'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div> 您沒有瀏覽的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('home');
				}
			}
		}
		$data = [];
		$data['title'] = $this->unitName;
		$data['title_small'] = 'Excel模板管理列表';
		$data['active'] = 'employee';

		$keyword['fullname'] = $this->input->post('fullname');
		
		$result = $this->excel_template_model->getList($keyword);		
		$pager = $this->pagintion_model->setPager($result,$page);
		$data['page_list'] = $pager['page'];
		$data['result'] = $this->excel_template_model->getList($keyword,$pager['list']);
		
		//選單
		$data['mainbar'] = $this->sidebarmenu_model->getsidebarmain();
		$data['subbar'] = $this->sidebarmenu_model->getsidebarsub();
		$data['view'] = $this->competence_model->getaction($this->competence_id);
		
		$data['footer'] = $this->load->view('amadis_sys/common/footer','', TRUE);
		$data['script'] = $this->load->view('amadis_sys/common/script','', TRUE);
		$this->load->view('amadis_sys/common/head');
		$this->load->view('amadis_sys/common/header');
		$this->load->view('amadis_sys/common/sidebar',$data);
		$this->load->view('amadis_sys/rgms_report_2/excel_template_2/list',$data);	
	}

	//跳到指定頁面
	public function pagejump(){
		if($this->input->post('pagenum') == null || $this->input->post('pagenum') == 0){
			redirect('rgms_report_2/excel_template_2/list');
		}
		else{
			redirect('rgms_report_2/excel_template_2/list/'.$this->input->post('pagenum').'');
		}
	}

    // 新增表單
	public function create_form(){
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 55) {
				if($r['actions_insert'] == 0) {
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div><div class=' icon-warning-sign'></div> 您沒有新增的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('rgms_report_2/excel_template_2');
				}
			}
		}
		$data = [];
		$data['title'] = $this->unitName;
		$data['title_small'] = '新增Excel模板管理';

		$data['mainbar'] = $this->sidebarmenu_model->getsidebarmain();
		$data['subbar'] = $this->sidebarmenu_model->getsidebarsub();
		$data['view'] = $this->competence_model->getaction($this->competence_id);
		$this->session->set_flashdata('sidebarselected','excel_template_2');
		
		$data['footer'] = $this->load->view('amadis_sys/common/footer','', TRUE);
		$data['script'] = $this->load->view('amadis_sys/common/script','', TRUE);
		$this->load->view('amadis_sys/common/head');
		$this->load->view('amadis_sys/common/header');
		$this->load->view('amadis_sys/common/sidebar',$data);
		$this->load->view('amadis_sys/rgms_report_2/excel_template_2/create',$data);
	}

	// 新增Excel模板管理執行
	public function create(){
		$datetime = date("Y-m-d H:i:s",time());
		$config['upload_path'] = 'public/uploads/excel_model/';
		$config['allowed_types'] = 'xlsx|csv|xls';
		$config['overwrite'] = true;

		$this->load->library('upload', $config);
		$this->upload->initialize($config);

		if (!$this->upload->do_upload('excel_model'))
		{
			$error = $this->upload->display_errors();
			print_r($error);
		}
		else
		{
			$data = array('upload_data' => $this->upload->data());
		}

		$keyword['fileName'] = $data['upload_data']['file_name'];
		$fileNameCheck = $this->excel_template_model->getList($keyword);

		if(empty($fileNameCheck)){
			$excel_template = array(
				'excel_template_name'         => $data['upload_data']['file_name'],
				'excel_template_sub_name'     => $data['upload_data']['file_ext'],
				'excel_template_created_date' => $datetime,
				'excel_template_created_user' => $this->session->userdata('users_id')
			);
			$this->excel_template_model->add_excel_template($excel_template);
		}
		else{
			$excel_template = array(
				'excel_template_updated_date' => $datetime,
				'excel_template_updated_user' => $this->session->userdata('users_id')
			);
			$this->excel_template_model->update_excel_template($excel_template,$fileNameCheck[0]['excel_template_id']);
		}

		$messagediv = "<div class='alert alert-success'>
		<button class='close' data-dismiss='alert'></button>
		<div class='icon-ok'></div> 成功！Excel模板管理已新增完成。
		</div>";
		$this->session->set_flashdata('messagediv',$messagediv);
		redirect('rgms_report_2/excel_template_2');
	}

	// 刪除執行
	public function delete(){
		$id = $this->input->get('id');
		$user_id = $this->session->userdata('users_id');
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 55) {
				if($r['actions_delete'] == 0) {
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div><div class=' icon-warning-sign'></div> 您沒有刪除的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('rgms_report_2/excel_template_2');
				}
			}
		}
		
		$datetime = date("Y-m-d H:i:s"); 
		$excel_template = array(
			'excel_template_is_del'       => 1,
			'excel_template_updated_date' => date("Y-m-d H:i:s",time()),
			'excel_template_updated_user' => $this->session->userdata('users_id')
		);

		$this->excel_template_model->update_excel_template($excel_template,$id);

		$messagediv = "<div class='alert alert-success'>
		<button class='close' data-dismiss='alert'></button>
		<div class='icon-ok'></div> 成功！該筆Excel模板管理已刪除。
		</div>";
		$this->session->set_flashdata('messagediv',$messagediv);
		redirect('rgms_report_2/excel_template_2');
	}

	// 下架執行
	public function excel_template_invisible(){
		$id = $this->input->get('id');
		$user_id = $this->session->userdata('users_id');
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 55) {
				if($r['actions_enable'] == 0) {
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div><div class=' icon-warning-sign'></div> 您沒有下架的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('rgms_report_2/excel_template_2');
				}
			}
		}
		
		$datetime = date("Y-m-d H:i:s"); 
		$excel_template = array(
			'excel_template_status'       => 0,
			'excel_template_updated_date' => date("Y-m-d H:i:s",time()),
			'excel_template_updated_user' => $this->session->userdata('users_id')
		);

		$this->excel_template_model->update_excel_template($excel_template,$id);

		$messagediv = "<div class='alert alert-success'>
		<button class='close' data-dismiss='alert'></button>
		<div class='icon-ok'></div> 成功！該筆Excel模板管理已停用。
		</div>";
		$this->session->set_flashdata('messagediv',$messagediv);
		redirect('rgms_report_2/excel_template_2');
	}

	// 上架執行
	public function excel_template_visible(){
		$id = $this->input->get('id');
		$user_id = $this->session->userdata('users_id');
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 55)
			{
				if($r['actions_enable'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div><div class=' icon-warning-sign'></div> 您沒有上架的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('rgms_report_2/excel_template_2');
				}
			}
		}
		
		$datetime = date("Y-m-d H:i:s"); 

		$excel_template = array(
			'excel_template_status'       => 1,
			'excel_template_updated_date' => date("Y-m-d H:i:s",time()),
			'excel_template_updated_user' => $this->session->userdata('users_id')
		);

		$this->excel_template_model->update_excel_template($excel_template,$id);

		$messagediv = "<div class='alert alert-success'>
		<button class='close' data-dismiss='alert'></button>
		<div class='icon-ok'></div> 成功！該筆Excel模板管理已啟用。
		</div>";
		$this->session->set_flashdata('messagediv',$messagediv);
		redirect('rgms_report_2/excel_template_2');
	}

	// 編輯表單
	public function update_form(){
		$id = $this->input->get('id');
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 55)
			{
				if($r['actions_update'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div><div class=' icon-warning-sign'></div> 您沒有編輯的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('rgms_report_2/excel_template_2');
				}
			}
		}
		$data = [];
		$data['title'] = $this->unitName;
		$data['title_small'] = '編輯Excel模板管理';

		//受測者資料
		$data['item'] = json_decode($this->apiconnection->apiConnect('','http://34.80.127.28/web/tw/api/detection','post'),true)['Data'];

		$selection['excel_template_id'] = $id;
		$templateSetting = json_decode($this->report_template_setting_model->getDataBySelection($selection)['report_template_setting_content'],true);
		$data['origKey'] = array();
		$data['origPage'] = array();
		$data['origCol'] = array();
		if(!empty($templateSetting)){
			foreach($templateSetting as $key => $row){
				$target = explode(',',$row);
				array_push($data['origKey'],$key);
				array_push($data['origPage'],$target[0]);
				array_push($data['origCol'],$target[1]);
			}
		}

		$model_data = $this->model_data();
		$data['keyName'] = $model_data['keyName'];
		$data['value'] = $model_data['value'];
		foreach($data['value'] as $key => $row){
			$data['origSrc'][$key] = '';
			if(substr($row, -3) == 'png' || substr($row, -3) == 'jpg'){
				$data['origSrc'][$key] = $row;
			}
		}
		
		$data['result'] = $this->excel_template_model->getidData($id);

		$data['mainbar'] = $this->sidebarmenu_model->getsidebarmain();
		$data['subbar'] = $this->sidebarmenu_model->getsidebarsub();
		$data['view'] = $this->competence_model->getaction($this->competence_id);
		$data['upid'] = $this->input->get('id');
		
		$data['footer'] = $this->load->view('amadis_sys/common/footer','', TRUE);
		$data['script'] = $this->load->view('amadis_sys/common/script','', TRUE);
		$this->load->view('amadis_sys/common/head');
		$this->load->view('amadis_sys/common/header');
		$this->load->view('amadis_sys/common/sidebar',$data);
		$this->load->view('amadis_sys/rgms_report_2/excel_template_2/update',$data);
	}

    // 編輯執行
	public function update(){
		$post = $this->input->post();
		// $id = $
		$id = $post['id'];

		$content = array();
		foreach($post['destPage'] as $key => $row){
			$content[$post['competence_data'][$key]] = $row.','.$post['destCol'][$key];
		}

		$selection['excel_template_id'] = $id;
		$check_setting_exist = $this->report_template_setting_model->getDataBySelection($selection);

		if(!empty($check_setting_exist)){
			$excel_setting = array(
				'report_template_setting_content'      => json_encode($content),
				'report_template_setting_updated_date' => date("Y-m-d H:i:s",time()),
				'report_template_setting_updated_user' => $this->session->userdata('users_id')
			);

			$this->report_template_setting_model->update_report_template_setting($excel_setting,$check_setting_exist['report_template_setting_id']);
		}
		else{
			$excel_setting = array(
				'excel_template_id'                    => $id,
				'report_template_setting_content'      => json_encode($content),
				'report_template_setting_created_date' => date("Y-m-d H:i:s",time()),
				'report_template_setting_created_user' => $this->session->userdata('users_id')
			);

			$this->report_template_setting_model->add_report_template_setting($excel_setting);
		}

		$messagediv = "<div class='alert alert-success'>
		<button class='close' data-dismiss='alert'></button>
		<div class='icon-ok'></div> 成功！此筆Excel模板管理已更新完成。
		</div>";
		$this->session->set_flashdata('messagediv',$messagediv);
		redirect('rgms_report_2/excel_template_2');
	}

	// 編輯表單
	public function check_form(){
		$id = $this->input->get('id');
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 55)
			{
				if($r['actions_view'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div><div class=' icon-warning-sign'></div> 您沒有編輯的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('rgms_report_2/excel_template_2');
				}
			}
		}
		$data = [];
		$data['title'] = $this->unitName;
		$data['title_small'] = '編輯Excel模板管理';
		
		$data['result'] = $this->excel_template_model->getidData($id);
		
		$data['mainbar'] = $this->sidebarmenu_model->getsidebarmain();
		$data['subbar'] = $this->sidebarmenu_model->getsidebarsub();
		$data['view'] = $this->competence_model->getaction($this->competence_id);
		$data['upid'] = $this->input->get('id');
		
		$data['footer'] = $this->load->view('amadis_sys/common/footer','', TRUE);
		$data['script'] = $this->load->view('amadis_sys/common/script','', TRUE);
		$this->load->view('amadis_sys/common/head');
		$this->load->view('amadis_sys/common/header');
		$this->load->view('amadis_sys/common/sidebar',$data);
		$this->load->view('amadis_sys/rgms_report_2/excel_template_2/check',$data);
	}

	//模板受測者
	public function model_data(){
		$keyName = array();
		$value = array();
		$returnData = json_decode($this->apiconnection->apiConnect('','http://34.80.127.28/web/tw/api/v2/report?item=906','get'),true)['Data'];

		$keyName = array();
		$value = array();

		foreach($returnData as $key => $row){
			if(is_array($row)){
				foreach($row as $key2 => $row2){
					if(is_array($row2)){
						foreach($row2 as $key3 => $row3){
							if(is_array($row3)){
								foreach($row3 as $key4 => $row4){
									if(is_array($row4)){
										foreach($row4 as $key5 => $row5){
											if(is_array($row5)){
												foreach($row5 as $key6 => $row6){
													array_push($keyName,$key6);
													array_push($value,$row6);
												}
											}
											else{
												array_push($keyName,$key5);
												array_push($value,$row5);
											}
										}
									}
									else{
										array_push($keyName,$key4);
										array_push($value,$row4);
									}
								}
							}
							else{
								array_push($keyName,$key3);
								array_push($value,$row3);
							}
						}
					}
					else{
						array_push($keyName,$key2);
						array_push($value,$row2);
					}
				}
			}
			else{
				array_push($keyName,$key);
				array_push($value,$row);
			}
		}

		$data = array(
			'keyName' => $keyName,
			'value' => $value
		);

		return $data;
	}

	//列出資料結構
	public function print_data(){
		$keyName = array();
		$value = array();
		$returnData = json_decode($this->apiconnection->apiConnect('','http://34.80.127.28/web/tw/api/v2/report?item=906','get'),true)['Data'];

		$keyName = array();
		$value = array();

		foreach($returnData as $key => $row){
			if(is_array($row)){
				foreach($row as $key2 => $row2){
					if(is_array($row2)){
						foreach($row2 as $key3 => $row3){
							if(is_array($row3)){
								foreach($row3 as $key4 => $row4){
									if(is_array($row4)){
										foreach($row4 as $key5 => $row5){
											if(is_array($row5)){
												foreach($row5 as $key6 => $row6){
													array_push($keyName,$key6);
													array_push($value,$row6);
												}
											}
											else{
												array_push($keyName,$key5);
												array_push($value,$row5);
											}
										}
									}
									else{
										array_push($keyName,$key4);
										array_push($value,$row4);
									}
								}
							}
							else{
								array_push($keyName,$key3);
								array_push($value,$row3);
							}
						}
					}
					else{
						array_push($keyName,$key2);
						array_push($value,$row2);
					}
				}
			}
			else{
				array_push($keyName,$key);
				array_push($value,$row);
			}
		}

		foreach($keyName as $key => $row){
			if(substr($value[$key], -3) == 'png'){
				echo '欄位名:'.$row.',值:'.'<a href='.$value[$key].'>';
				echo $value[$key].'<br>';
				echo '</a>';
			}
			else{
				echo '欄位名:'.$row.',值:'.$value[$key].'<br>';
			}
		}
		exit();
	}
}
