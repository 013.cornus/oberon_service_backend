<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Atest extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->library('excel');
		$this->load->model('amadis_sys/bonuscalculation_model');
		$this->load->model('amadis_sys/competence_model');
		$this->load->model('amadis_sys/organization_model');
		$this->load->model('amadis_sys/employee_model');
		$this->load->model('amadis_sys/occupation_model');
		$this->load->model('amadis_sys/order_model');
		$this->load->model('amadis_sys/stock_model');
		$this->load->model('amadis_sys/profit_model');
		$this->load->model('amadis_sys/bank_model');
		$this->load->model('amadis_sys/branch_model');
		$this->load->model('amadis_sys/post_model');
		$this->load->model('amadis_sys/order_info_model');
		$this->load->model('amadis_sys/customers_model');
		$this->load->model('amadis_sys/bonus_model');
		$this->load->model('amadis_sys/users_bonus_model');
		$this->load->model('amadis_sys/users_bonus_extra_model');
		$this->load->model('amadis_sys/goods_model');

		$this->load->model('amadis_sys/api/exportexcel_model');

		$this->unitName = '獎金報表管理';

		$this->session->set_flashdata('sidebarselected','bonussummary');
		$this->session->set_flashdata('mainsidebar','bounsreport');

		$this->competenceID = $this->session->userdata('competence_id');
		$admin_id = $this->session->userdata('users_id');
		$username = $this->session->userdata('users_name');
		
		if(!$admin_id){
			redirect('home/login');
		}
	}

	//客戶報表輸出
	public function index(){
		redirect('Atest/info');
		exit;
		$competence_id = $this->session->userdata('competence_id');
		$rightid = $this->competence_model->getaction($competence_id);
		foreach($rightid as $r){
			if($r['sidebar_sub_id'] == 3)
			{
				if($r['actions_export'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div> 您沒有匯出的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('employee/list');
				}
			}
		}

		$data = [];

		if($this->session->userdata('orgid')){
			$keyword['organization_id'] = $this->session->userdata('orgid');
		}
		else{
			$keyword['organization_id'] = $this->input->post('orgid');
		}

		switch($competence_id){
			case '3':
			$keyword['organization_id'] = $this->session->userdata('manager_id');
			break;
			case '4':
			$keyword['emplyee_id'] = $this->session->userdata('emplyee_id');
			break;
			default:
			break;
		}

		$data['customerlist'] = $this->customers_model->getList($keyword);

		// $rowcount = 3;
		foreach($data['customerlist'] as $key => $customer){
			//營業處資料
			$organization = $this->organization_model->getData($customer['organization_id']);
			$data['customerlist'][$key]['organization_name'] = $organization['organization_name'];
			//經辦人員
			$employee = $this->employee_model->getidData($customer['employee_id']);
			$data['customerlist'][$key]['employee_name'] = $employee['employee_name'];

			$data['bank'] = '';
			$data['branch'] = '';
			//帳戶資料
			if($customer['customer_bank'] == 700){
				$data['customerlist'][$key]['bank'] = '中華郵政';
				$branchinfo = $this->post_model->getidData($customer['customer_bank_branch']);
				$data['customerlist'][$key]['branch'] = $branchinfo['post_name'];
				$branchcode = '7000021';
			}
			else{
				$bankinfo = $this->bank_model->getidData($customer['customer_bank']);
				$data['customerlist'][$key]['bank'] = $bankinfo['bank_name'];
				$branchinfo = $this->branch_model->getidData($customer['customer_bank_branch']);
				$data['customerlist'][$key]['branch'] = $branchinfo['bank_branch_name'];
				$branchcode = $branchinfo['bank_branch_code'];
			}
			// $rowcount++;
		}

		// echo '<pre>';
		// print_r($data['customerlist']);
		// echo '</pre>';
		// exit;

		// $this->customers($data['customerlist']);
		// redirect('amadis_sys/Apiexportexcel/customers/'.$data['customerlist']);

		$this->exportexcel_model->customers($data['customerlist']);

	}

	public function info(){
		$data = [];
		echo '匯出Excel模板設定：'.'<br>';
		echo '調用模組：Exportexcel_model'.'<br>';
		echo '導入參數請參照下列格式，參數型態為：<code>array</code>'.'<br>';

		$data['客戶報表輸出（Exportexcel_model/customers）'] = array(
			//姓名
			'customer_name' => '姓名',
        	//出生年月日
			'customer_birthday' => '出生年月日',
        	//身份證字號
			'customer_id_card' => '身份證字號',
        	//通訊地址
			'customer_permanent_address' => '通訊地址',
        	//戶籍地址
			'customer_residential_address' => '戶籍地址',
        	//手機
			'customer_phone' => '手機',
        	//聯絡電話(公)
			'customer_company_tel' => '聯絡電話(公)',
        	//聯絡電話(宅)
			'customer_residential_tel' => '聯絡電話(宅)',
        	//電子郵件信箱
			'customer_email' => '電子郵件信箱',
        	//戶名
			'customer_account_name' => '戶名',
        	//匯款銀行
			'branch' => '匯款銀行',
        	//匯款帳號
			'customer_account' => '匯款帳號',
        	//經辦人員
			'employee_name' => '經辦人員',
        	//營業處
			'organization_name' => '營業處',
        	//備註
			'customer_note' => '備註',
		);
		
		$data['業務承攬人報表輸出（Exportexcel_model/employee）'] = array(
			//姓名
			'employee_name' => '姓名',
            //身分證字號
			'employee_id_card' => '身分證字號',
            //營業處
			'organization_name' => '營業處',
            //職稱
			'occupation_name' => '職稱',
            //直屬經理
			'employee_name' => '直屬經理',
            //推薦人
			'employee_name' => '推薦人',
            //戶名
			'employee_account_name' => '戶名',
            //銀行名稱
			'bank' => '銀行名稱',
            //分行代號
			'bank_branch_code' => '分行代號',
            //匯款帳號
			'employee_bank_account' => '匯款帳號',
            //備註
			'employee_note' => '備註',
		);

		$data['獎金異動報表輸出（Exportexcel_model/bonusChange）'] = array(
			//獎金發放狀態
			'users_bonus_is_check' => '獎金發放狀態',
            //合約書編號
			'contract_id' => '合約書編號',
            //業務承攬人
			'employee_name' => '業務承攬人',
            //所屬單位
			'organization_name' => '所屬單位',
            //職稱
			'occupation_name' => '職稱',
            //獎金名稱
			'bonus_type_name' => '獎金名稱',
            //獎金金額
			'users_bonus_money' => '獎金金額',
            //合約生效日
			'order_active_date' => '合約生效日',
		);

		$data['股權待售報表輸出（Exportexcel_model/stocksold）'] = array(
			//合約書編號
			'contract_id' => '合約書編號',
            //客戶姓名
			'customer_name' => '客戶姓名',
            //承購時間
			'order_active_date' => '承購時間',
            //承購股數
			'order_stock' => '承購股數',
            //本月股價
			'stock_value' => '本月股價',
            //狀態
			'order_active_date' => '狀態',
            //經辦人員
			'employee_name' => '經辦人員',
            //營業處
			'organization_name' => '營業處',
		);

		$data['訂單報表輸出（Exportexcel_model/order）'] = array(
			//訂單審核狀態
			'order_is_check' => '訂單審核狀態',
            //承購日期
			'order_created_date' => '承購日期',
            //生效日
			'order_active_date' => '生效日',
            //合約書編號
			'contract_id' => '合約書編號',
            //承購人姓名
			'customer_name' => '承購人姓名',
            //基金
			'product_name' => '基金',
            //基金
			'order_info_count' => '基金',
            //股權
			'order_info_count_1' => '股權',
            //合計金額
			'totalprice' => '合計金額',
            //付款別
			'order_pay_type' => '付款別',
            //到期日
            // $order['order_over_date' => '到期日',
            //簽收日
			'order_sign_date' => '簽收日',
            //經辦人員
			'employee_name' => '經辦人員',
            //直屬經理
			'employee_name' => '直屬經理',
            //備註1
			'order_note_1' => '備註1',
            //備註2
			'order_note_2' => '備註2',
		);

		$data['利得發放報表輸出（Exportexcel_model/interest）'] = array(
            //合約書編號
            'contract_id' => '合約書編號',
            //生效日
            'order_active_date' => '生效日',
            //到期日
            // order_over_date' => '生效日',
            //客戶姓名
            'customer_name' => '客戶姓名',
            //阿瑪迪斯教育成長基金(一年期)
            'productnum1' => '阿瑪迪斯教育成長基金(一年期)',
            //阿瑪迪斯教育成長基金(二年期)
            'productnum2' => '阿瑪迪斯教育成長基金(二年期)',
            //阿瑪迪斯教育成長基金(三年期)
            'productnum3' => '阿瑪迪斯教育成長基金(三年期)',
            //茂云實業投資有限公司股權
            'productnum4' => '茂云實業投資有限公司股權',
            //總單位
            'total' => '總單位',
            //建置期利息補貼
            'interest' => '建置期利息補貼',
            //季利得
            'qinterest' => '季利得',
            //業務姓名
            'employee_name' => '業務姓名',
            //直屬經理
            'manager_name' => '直屬經理',
            //營業處
            'organization_name' => '營業處',
            //分行
            'branch' => '分行',
            //銀行代號
            'bank_branch_code' => '銀行代號',
            //帳號
            'customer_account' => '帳號',
		);

		$data['業務人員獎金總表輸出（Exportexcel_model/employeeBouns）'] = array(
			//營業處名稱
           'orgname' => '營業處名稱',
            //業務姓名
           'employeename' => '業務姓名',
            //一年期基金單位
           'fund1num' => '一年期基金單位',
            //二年期基金單位
           'fund2num' => '二年期基金單位',
            //三年期基金單位
           'fund3num' => '三年期基金單位',
            //股權單位
           'equitynum' => '股權單位',
            //總單位
           'totalpdnum' => '總單位',
            //業績獎金
           'count' => '業績獎金',
            //差額獎金
           'allowance' => '差額獎金',
            //同階獎金
           'samelvallowance' => '同階獎金',
            //服務津貼
           'totalallowance' => '服務津貼',
            //其他加項
           'addmoney' => '其他加項',
            //說明
           'addtxt' => '說明',
            //其他減項
           'lessmoney' => '其他減項',
            //說明
           'lesstxt' => '說明',
		);

		$data['營業處總獎金總表輸出（Exportexcel_model/organizationBouns）'] = array(
			'organization_name' => '',
            'fund1num' => '各商品銷售單位',
            'fund2num' => '各商品銷售單位',
            'fund3num' => '各商品銷售單位',
            'equitynum' => '各商品銷售單位',
            //總單位
            'ordernum' => '總單位',
            //總PV
            'orgpv' => '總PV',
            //業績獎金
            'count' => '業績獎金',
            //同階獎金
            'smaelvallowance' => '同階獎金',
            //全國分紅
            // 'dividend' => '全國分紅',
            //衍生營業處津貼
            'boAllowance' => '衍生營業處津貼',
            //服務津貼
            'totalallowance' => '服務津貼',
            //行政津貼
            'subsidypv' => '行政津貼',
            //差額獎金
            'allowance' => '差額獎金',
            //其他加項
            'addmoney' => '其他加項',
            //加項說明
            'addtxt' => '加項說明',
            //其他減項
            'lessmoney' => '其他減項',
            //減項說明
            'lesstxt' => '減項說明',
            //合計
            'totalbonusnum' => '合計',
		);	

		echo '<pre>';
		print_r($data);
		echo '</pre>';
		exit;
	}

}
