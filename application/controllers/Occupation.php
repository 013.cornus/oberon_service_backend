<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Occupation extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('amadis_sys/occupation_model');
		$this->load->model('amadis_sys/competence_model');
		$this->load->model('amadis_sys/sidebarmenu_model');
		$this->load->model('amadis_sys/bonus_model');
		$this->load->model('amadis_sys/pagintion_model');
		$this->unitName = '職級管理';

		$admin_id = $this->session->userdata('bms_users_id');
		$username = $this->session->userdata('bms_users_name');

		$this->competence_id = $this->session->userdata('bms_competence_id');
		$this->rightid = $this->competence_model->getaction($this->competence_id);
		
		$this->session->set_flashdata('sidebarselected','occupation');
		$this->session->set_flashdata('mainsidebar','organization');
		
		if(!$admin_id || $this->competence_id >= 4){
			redirect('home/login');
		}
	}

    //導入設定
	public function index(){
		redirect('occupation/list');
		exit;
	}

	public function list($page=''){
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 13)
			{
				if($r['actions_view'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div> 您沒有瀏覽的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('home');
				}
			}
		}
		$keyword['fullname'] = $this->input->post('fullname');
		$data = [];
		$data['title'] = $this->unitName;
		$data['title_small'] = '職級列表';

		$result = $this->occupation_model->getList($keyword);
		$pager = $this->pagintion_model->setPager($result,$page);
		$data['page_list'] = $pager['page'];
		$data['result'] = $this->occupation_model->getList($keyword,$pager['list']);
		$data['hide'] = "";
		if($this->competence_id != 1){
			$data['hide'] = "style='display:none'";
		}

		$data['mainbar'] = $this->sidebarmenu_model->getsidebarmain();
		$data['subbar'] = $this->sidebarmenu_model->getsidebarsub();
		$data['view'] = $this->competence_model->getaction($this->competence_id);

		$data['footer'] = $this->load->view('amadis_sys/common/footer','', TRUE);
		$data['script'] = $this->load->view('amadis_sys/common/script','', TRUE);
		$this->load->view('amadis_sys/common/head');
		$this->load->view('amadis_sys/common/header');
		$this->load->view('amadis_sys/common/sidebar',$data);
		$this->load->view('amadis_sys/organization/occupation/list',$data);	
	}

	//跳到指定頁面
	public function pagejump(){
		if($this->input->post('pagenum') == null || $this->input->post('pagenum') == 0){
			redirect('occupation/list');
		}
		else{
			redirect('occupation/list/'.$this->input->post('pagenum').'');
		}
	}

    // 新增會員表單
	public function create_form(){
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 13)
			{
				if($r['actions_insert'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div> 您沒有新增的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('occupation');
				}
			}
		}
		$datetime = date("Y-m-d");
		$data = [];
		$data['title'] = $this->unitName;
		$data['title_small'] = '新增職級';

		$data['latestid'] = $this->occupation_model->getlatestid();

		$data['mainbar'] = $this->sidebarmenu_model->getsidebarmain();
		$data['subbar'] = $this->sidebarmenu_model->getsidebarsub();
		$data['view'] = $this->competence_model->getaction($this->competence_id);

		$data['footer'] = $this->load->view('amadis_sys/common/footer','', TRUE);
		$data['script'] = $this->load->view('amadis_sys/common/script','', TRUE);
		$this->load->view('amadis_sys/common/head');
		$this->load->view('amadis_sys/common/header');
		$this->load->view('amadis_sys/common/sidebar',$data);
		$this->load->view('amadis_sys/organization/occupation/create',$data);
	}

	// 新增職級執行
	public function create(){
		$datetime = date ("Y-m-d H:i:s") ; 
		$occupation = array(
			'occupation_name'         => $this->input->post('occupationname'),
			'occupation_created_date' => $datetime,
			'occupation_created_user' => $this->session->userdata('bms_users_id')
		);
		$bonus = array(
			'occupation_id'      => $this->input->get('id'),
			'bouns_created_date' => $datetime,
			'bouns_created_user' => $this->session->userdata('bms_users_id')
		);
		$this->bonus_model->add_bonus($bonus);
		$this->occupation_model->add_occupation($occupation);
		$messagediv = "<div class='alert alert-success'>
		<button class='close' data-dismiss='alert'></button>
		<div class='icon-ok'></div> 成功！資料已新增。
		</div>";
		$this->session->set_flashdata('messagediv',$messagediv);

		redirect('occupation');
	}

	// 刪除會員執行
	public function delete(){
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 13)
			{
				if($r['actions_delete'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div> 您沒有刪除的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('occupation');
				}
			}
		}
		$id = $this->input->get('id');
		$datetime = date("Y-m-d H:i:s"); 
		$user_id = $this->session->userdata('bms_users_id');
		$occupation = array(
			'occupation_is_del'       => 1,
			'occupation_updated_date' => $datetime,
			'occupation_updated_user' => $this->session->userdata('bms_users_id')
		);
		$this->occupation_model->update_occupation($occupation,$id);

		$messagediv = "<div class='alert alert-success'>
		<button class='close' data-dismiss='alert'></button>
		<div class='icon-ok'></div> 成功！該筆資料已刪除。
		</div>";
		$this->session->set_flashdata('messagediv',$messagediv);
		redirect('occupation');
	}

	// 下架會員執行
	public function info_invisible(){
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 13)
			{
				if($r['actions_enable'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div> 您沒有上下架的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('occupation');
				}
			}
		}
		$id = $this->input->get('id');
		$datetime = date("Y-m-d H:i:s"); 
		$user_id = $this->session->userdata('bms_users_id');
		$occupation = array(
			'occupation_status'       => 0,
			'occupation_updated_date' => $datetime,
			'occupation_updated_user' => $this->session->userdata('bms_users_id')
		);

		$this->occupation_model->update_occupation($occupation,$id);
		$messagediv = "<div class='alert alert-success'>
		<button class='close' data-dismiss='alert'></button>
		<div class='icon-ok'></div> 成功！該筆職級已停用。
		</div>";
		$this->session->set_flashdata('messagediv',$messagediv);
		redirect('occupation');
	}

	// 上架會員執行
	public function info_visible(){
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 13)
			{
				if($r['actions_enable'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div> 您沒有上下架的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('occupation');
				}
			}
		}
		$id = $this->input->get('id');
		$datetime = date("Y-m-d H:i:s"); 
		$user_id = $this->session->userdata('bms_users_id');
		$occupation = array(
			'occupation_status'       => 1,
			'occupation_updated_date' => $datetime,
			'occupation_updated_user' => $this->session->userdata('bms_users_id')
		);

		$this->occupation_model->update_occupation($occupation,$id);
		$messagediv = "<div class='alert alert-success'>
		<button class='close' data-dismiss='alert'></button>
		<div class='icon-ok'></div> 成功！該筆職級已啟用。
		</div>";
		$this->session->set_flashdata('messagediv',$messagediv);
		redirect('occupation');
	}

	// 修改會員表單
	public function update_form(){
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 13)
			{
				if($r['actions_update'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div> 您沒有修改的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('occupation');
				}
			}
		}
		$data = [];
		$id = $this->input->get('id');
		$data['title'] = $this->unitName;
		$data['title_small'] = '編輯職級';
		
		$data['result'] = $this->occupation_model->getidData($id);
		// $selection['occupation'] = $id;
		// $data['bonus'] = $this->bonus_model->getList($selection);

		$data['mainbar'] = $this->sidebarmenu_model->getsidebarmain();
		$data['subbar'] = $this->sidebarmenu_model->getsidebarsub();
		$data['view'] = $this->competence_model->getaction($this->competence_id);
		$data['upid'] = $this->input->get('id');

		$data['footer'] = $this->load->view('amadis_sys/common/footer','', TRUE);
		$data['script'] = $this->load->view('amadis_sys/common/script','', TRUE);
		$this->load->view('amadis_sys/common/head');
		$this->load->view('amadis_sys/common/header');
		$this->load->view('amadis_sys/common/sidebar',$data);
		$this->load->view('amadis_sys/organization/occupation/update',$data);
	}

    // 修改會員執行
	public function update(){
		$id = $this->input->post('id');
		$datetime = date ("Y-m-d H:i:s") ; 
		$occupation = array(
			'occupation_name'         => $this->input->post('occupationname'),
			'occupation_updated_date' => $datetime,
			'occupation_updated_user' => $this->session->userdata('bms_users_id')
		);

		$this->occupation_model->update_occupation($occupation,$id);
		$messagediv = "<div class='alert alert-success'>
		<button class='close' data-dismiss='alert'></button>
		<div class='icon-ok'></div> 成功！此筆資料已更新。
		</div>";
		$this->session->set_flashdata('messagediv',$messagediv);
		redirect('occupation');
	}
}
