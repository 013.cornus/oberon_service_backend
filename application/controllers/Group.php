<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Group extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('amadis_sys/ks/group_model');
		$this->load->model('amadis_sys/ks/register_key_model');
		$this->load->model('amadis_sys/ks/users_info_model');
		$this->load->model('amadis_sys/ks/ks_users_model');
		$this->load->model('amadis_sys/system/group_info_model');
		$this->load->model('amadis_sys/competence_model');
		$this->load->model('amadis_sys/sidebarmenu_model');
		$this->load->model('amadis_sys/pagintion_model');
		$this->unitName = '登入帳號管理';
		$this->competence_id = $this->session->userdata('bms_competence_id');
		$this->rightid = $this->competence_model->getaction($this->competence_id);
		

		$admin_id = $this->session->userdata('bms_users_id');
		$username = $this->session->userdata('bms_users_name');
		$this->session->set_flashdata('sidebarselected','group');
		$this->session->set_flashdata('mainsidebar','excel_create');
		$this->load->library('apiconnection');
		if(!$admin_id){
			redirect('home/login');
		}
	}

    //群組列表
	public function index(){
		redirect('group/list');
		exit;
	}
    //群組列表
	public function list($page=''){
		$organization_id = $this->session->userdata('bms_organization_id');
		$manager_id = $this->session->userdata('bms_manager_id');
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 7)
			{
				if($r['actions_view'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div> 您沒有瀏覽的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('home');
				}
			}
		}
		$data = [];
		$data['title'] = $this->unitName;
		$data['title_small'] = '登入帳號設定列表';
		$data['active'] = 'employee';

		$keyword['fullname'] = $this->input->post('fullname');
		
		$keyword['type'] = $this->session->userdata('bms_group_type');
		$result = $this->group_model->getList($keyword);		
		$pager = $this->pagintion_model->setPager($result,$page);
		$data['page_list'] = $pager['page'];
		$data['result'] = $this->group_model->getList($keyword,$pager['list']);
		
		//選單
		$data['mainbar'] = $this->sidebarmenu_model->getsidebarmain();
		$data['subbar'] = $this->sidebarmenu_model->getsidebarsub();
		$data['view'] = $this->competence_model->getaction($this->competence_id);
		
		$data['footer'] = $this->load->view('amadis_sys/common/footer','', TRUE);
		$data['script'] = $this->load->view('amadis_sys/common/script','', TRUE);
		$this->load->view('amadis_sys/common/head');
		$this->load->view('amadis_sys/common/header');
		$this->load->view('amadis_sys/common/sidebar',$data);
		$this->load->view('amadis_sys/group/list',$data);	
	}

	//跳到指定頁面
	public function pagejump(){
		if($this->input->post('pagenum') == null || $this->input->post('pagenum') == 0){
			redirect('group/list');
		}
		else{
			redirect('group/list/'.$this->input->post('pagenum').'');
		}
	}

    // 新增表單
	public function create_form(){
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 7) {
				if($r['actions_insert'] == 0) {
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div><div class=' icon-warning-sign'></div> 您沒有新增的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('group');
				}
			}
		}
		$keyword['group_type'] = $this->session->userdata('bms_group_type');
		$register_key_list = $this->register_key_model->getList($keyword);

		$data['register_key_list'] = [];
		$index = 0;
		foreach ($register_key_list as $key => $row) {
			if ($row['register_key_use'] == 0) {
				$data['register_key_list'][$index] = array(
					'register_key_id' => $row['register_key_id'],
					'register_key' => $row['register_key_name'],
				);
				$index++;
			}
		}

		$data['title'] = $this->unitName;
		$data['title_small'] = '新增群組';
		$data['mainbar'] = $this->sidebarmenu_model->getsidebarmain();
		$data['subbar'] = $this->sidebarmenu_model->getsidebarsub();
		$data['view'] = $this->competence_model->getaction($this->competence_id);
		$data['groupList'] = $this->group_info_model->getList($keyword);
		foreach($data['groupList'] as $key => $row){
			$data['selected'][$key] = '';
			if($row['group_info_type'] == $this->session->userdata('bms_group_type')){
				$data['selected'][$key] = 'selected';
			}
		}
		$this->session->set_flashdata('sidebarselected','group');
		
		$data['footer'] = $this->load->view('amadis_sys/common/footer','', TRUE);
		$data['script'] = $this->load->view('amadis_sys/common/script','', TRUE);
		$this->load->view('amadis_sys/common/head');
		$this->load->view('amadis_sys/common/header');
		$this->load->view('amadis_sys/common/sidebar',$data);
		$this->load->view('amadis_sys/group/create',$data);
	}

	// 新增群組執行
	public function create(){
		$post = $this->input->post();
		$datetime = date("Y-m-d H:i:s",time());
		$group = array(
			'group_name'           => $post['group_account'],
			'group_phone'          => $post['group_account'],
			'group_type'           => $this->session->userdata('bms_group_type'),
			'group_password'       => md5($post['group_password']),
			'group_phone_is_check' => 1,
			'group_created_date'   => $datetime,
			'group_created_user'   => $this->session->userdata('bms_users_id')
		);
		$group_id = $this->group_model->add_group($group);

		//update register key
		$key_check = $this->register_key_model->getidData($post['register_key_id']);
		if(!$key_check){
			$messagediv = "<div class='alert'>
			<button class='close' data-dismiss='alert'></button>
			<div></div><div class=' icon-warning-sign'></div> 缺少序號
			</div>";
			$this->session->set_flashdata('messagediv',$messagediv);
			redirect('group');
		}
		
		$register_key_data = array(
			'register_key_use' => 1,
		);

		$this->register_key_model->update_register_key($post['register_key_id'], $register_key_data);

		$user_info = array(
			'users_info_name'           => '測試用戶',
			'users_info_sex'            => 1,
			'users_info_birthday'       => '2020-01-01',
			'users_info_blood_type'     => 1,
			'users_info_phone_is_check' => 1,
			'users_info_created_date'   => date('Y-m-d H:i:s',time())
		);
		$users_info_id = $this->users_info_model->add_users_info($user_info);

		$user = array(
			'group_id'           => $group_id,
			'users_info_id'      => $users_info_id,
			'users_main'         => 1,
			'users_created_date' => date('Y-m-d H:i:s',time())
		);
		$this->ks_users_model->add_users($user);

		$messagediv = "<div class='alert alert-success'>
		<button class='close' data-dismiss='alert'></button>
		<div class='icon-ok'></div> 成功！群組已新增完成。
		</div>";
		$this->session->set_flashdata('messagediv',$messagediv);
		redirect('group');
	}

	// 刪除執行
	public function delete(){
		$id = $this->input->get('id');
		$user_id = $this->session->userdata('bms_users_id');
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 7) {
				if($r['actions_delete'] == 0) {
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div><div class=' icon-warning-sign'></div> 您沒有刪除的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('group');
				}
			}
		}
		
		$datetime = date("Y-m-d H:i:s"); 
		$group = array(
			'group_is_del'       => 1,
			'group_updated_date' => date("Y-m-d H:i:s",time()),
			'group_updated_user' => $this->session->userdata('bms_users_id')
		);

		$this->group_model->update_group($group,$id);

		$messagediv = "<div class='alert alert-success'>
		<button class='close' data-dismiss='alert'></button>
		<div class='icon-ok'></div> 成功！該筆群組已刪除。
		</div>";
		$this->session->set_flashdata('messagediv',$messagediv);
		redirect('group');
	}

	// 下架執行
	public function group_invisible(){
		$id = $this->input->get('id');
		$user_id = $this->session->userdata('bms_users_id');
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 7) {
				if($r['actions_enable'] == 0) {
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div><div class=' icon-warning-sign'></div> 您沒有下架的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('group');
				}
			}
		}
		
		$datetime = date("Y-m-d H:i:s"); 
		$group = array(
			'group_status'       => 0,
			'group_updated_date' => date("Y-m-d H:i:s",time()),
			'group_updated_user' => $this->session->userdata('bms_users_id')
		);

		$this->group_model->update_group($group,$id);

		$messagediv = "<div class='alert alert-success'>
		<button class='close' data-dismiss='alert'></button>
		<div class='icon-ok'></div> 成功！該筆群組已停用。
		</div>";
		$this->session->set_flashdata('messagediv',$messagediv);
		redirect('group');
	}

	// 上架執行
	public function group_visible(){
		$id = $this->input->get('id');
		$user_id = $this->session->userdata('bms_users_id');
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 7)
			{
				if($r['actions_enable'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div><div class=' icon-warning-sign'></div> 您沒有上架的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('group');
				}
			}
		}
		
		$datetime = date("Y-m-d H:i:s"); 

		$group = array(
			'group_status'       => 1,
			'group_updated_date' => date("Y-m-d H:i:s",time()),
			'group_updated_user' => $this->session->userdata('bms_users_id')
		);

		$this->group_model->update_group($group,$id);

		$messagediv = "<div class='alert alert-success'>
		<button class='close' data-dismiss='alert'></button>
		<div class='icon-ok'></div> 成功！該筆群組已啟用。
		</div>";
		$this->session->set_flashdata('messagediv',$messagediv);
		redirect('group');
	}

	// 編輯表單
	public function update_form(){
		$id = $this->input->get('id');
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 7)
			{
				if($r['actions_update'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div><div class=' icon-warning-sign'></div> 您沒有編輯的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('group');
				}
			}
		}
		$data = [];
		$data['title'] = $this->unitName;
		$data['title_small'] = '編輯群組';
		
		$data['result'] = $this->group_model->getidData($id);
		$data['group_info'] = $this->group_info_model->getDataByType($data['result']['group_type']);
		
		$data['mainbar'] = $this->sidebarmenu_model->getsidebarmain();
		$data['subbar'] = $this->sidebarmenu_model->getsidebarsub();
		$data['view'] = $this->competence_model->getaction($this->competence_id);
		$data['upid'] = $this->input->get('id');
		
		$data['footer'] = $this->load->view('amadis_sys/common/footer','', TRUE);
		$data['script'] = $this->load->view('amadis_sys/common/script','', TRUE);
		$this->load->view('amadis_sys/common/head');
		$this->load->view('amadis_sys/common/header');
		$this->load->view('amadis_sys/common/sidebar',$data);
		$this->load->view('amadis_sys/group/update',$data);
	}

    // 編輯執行
	public function update(){
		$id = $this->input->post('id');

		$group = array(
			'group_name'         => $this->input->post('group_name'),
			'group_updated_date' => date("Y-m-d H:i:s",time()),
			'group_updated_user' => $this->session->userdata('bms_users_id')
		);

		$this->group_model->update_group($group,$id);

		$messagediv = "<div class='alert alert-success'>
		<button class='close' data-dismiss='alert'></button>
		<div class='icon-ok'></div> 成功！此筆群組已更新完成。
		</div>";
		$this->session->set_flashdata('messagediv',$messagediv);
		redirect('group');
	}

	// 編輯表單
	public function check_form(){
		$id = $this->input->get('id');
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 7)
			{
				if($r['actions_view'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div><div class=' icon-warning-sign'></div> 您沒有編輯的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('group');
				}
			}
		}
		$data = [];
		$data['title'] = $this->unitName;
		$data['title_small'] = '編輯群組';
		
		$data['result'] = $this->group_model->getidData($id);
		
		$data['mainbar'] = $this->sidebarmenu_model->getsidebarmain();
		$data['subbar'] = $this->sidebarmenu_model->getsidebarsub();
		$data['view'] = $this->competence_model->getaction($this->competence_id);
		$data['upid'] = $this->input->get('id');
		
		$data['footer'] = $this->load->view('amadis_sys/common/footer','', TRUE);
		$data['script'] = $this->load->view('amadis_sys/common/script','', TRUE);
		$this->load->view('amadis_sys/common/head');
		$this->load->view('amadis_sys/common/header');
		$this->load->view('amadis_sys/common/sidebar',$data);
		$this->load->view('amadis_sys/group/check',$data);
	}

	public function check_duplicate(){
		$post = $this->input->post();
		$account = $post['account'];
		$check = $this->group_model->getKeyData('group_phone',$account);
		if($check){
			echo 1;
		}
		else{
			echo 0;
		}
	}
}
