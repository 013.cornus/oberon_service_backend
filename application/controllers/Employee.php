<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Employee extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('amadis_sys/employee_model');
		$this->load->model('amadis_sys/competence_model');
		$this->load->model('amadis_sys/organization_model');
		$this->load->model('amadis_sys/customers_model');
		$this->load->model('amadis_sys/occupation_model');
		$this->load->model('amadis_sys/sidebarmenu_model');
		$this->load->model('amadis_sys/address_model');
		$this->load->model('amadis_sys/order_info_model');
		$this->load->model('amadis_sys/order_model');
		$this->load->model('amadis_sys/users_model');
		$this->load->model('amadis_sys/account_model');
		$this->load->model('amadis_sys/pagintion_model');
		$this->unitName = '人事管理';

		$this->session->set_flashdata('sidebarselected','employee');
		$this->session->set_flashdata('mainsidebar','employee');

		$this->competence_id = $this->session->userdata('competence_id');
		$this->rightid = $this->competence_model->getaction($this->competence_id);

		$admin_id = $this->session->userdata('users_id');
		$username = $this->session->userdata('users_name');

		if(!$admin_id){
			redirect('home/login');
		}
	}
	public function index(){
		$this->session->unset_userdata('orgid');
		$this->session->unset_userdata('employeename');
		$this->session->unset_userdata('promote');
		redirect('employee/list');
		exit;
	}
    //業務列表
	public function list($page=''){
		$organization_id = $this->session->userdata('organization_id');
		$manager_id = $this->session->userdata('manager_id');
		// $this->bonus_count->goal_bonus();
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 1)
			{
				if($r['actions_view'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div> 您沒有瀏覽的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('home');
				}
			}
		}
		$data = [];
		$data['title'] = $this->unitName;
		$data['title_small'] = '業務列表';
		$data['active'] = 'employee';
		$data['hide'] = '';

		if($this->input->post('orgid') != ''){
			$this->session->set_userdata('orgid',$this->input->post('orgid'));
		}

		if($this->input->post('fullname') != ''){
			$this->session->set_userdata('employeename',$this->input->post('fullname'));
		}

		$selection['status'] = 1;
		switch ($this->competence_id) {
			case '3':
			$keyword['orgid'] = $this->input->post('orgid');
			$keyword['emplyee_id'] = $this->input->post('employeeid');
			$keyword['organization_id'] = trim($manager_id);
			$data['hide'] = "style='display:none'";
			$selection['organization_id'] = $organization_id;
			$data['employee'] = $this->employee_model->getList($selection);
			// $keyword['organization_id'] = trim($organization_id);
			break;
			default:
			if($this->session->userdata('orgid')){
				$keyword['orgid'] = $this->session->userdata('orgid');
			}
			else{
				$keyword['orgid'] = $this->input->post('orgid');
			}
			$keyword['emplyee_id'] = $this->input->post('employeeid');
			$data['employee'] = $this->employee_model->getList($selection);
			break;
		}

		if($this->session->userdata('employeename')){
			$keyword['employee_name'] = $this->session->userdata('employeename');
		}
		else{
			$keyword['employee_name'] = $this->input->post('fullname');
		}
		
		$result = $this->employee_model->getList($keyword,[]);
		$pager = $this->pagintion_model->setPager($result,$page);
		unset($result);
		$data['page_list'] = $pager['page'];
		$data['result'] = $this->employee_model->getList($keyword,$pager['list']);
		foreach($data['result'] as $key => $value){
			$organization = $this->organization_model->getData($value['organization_id']);
			$data['orgname'][$key] = $organization['organization_name'];
			$referrer = $this->employee_model->getidData($value['users_id']);
			$data['referrername'][$key] = $referrer['employee_name'];
			$manager = $this->employee_model->getidData($value['employee_supervisor']);
			$data['managername'][$key] = $manager['employee_name'];
			if($value['employee_status'] == 1){
				$str = '啟用';
				$color = 'success'; 
			}
			else{
				$str = '停用';
				$color = 'important';
			}
			$data['str'][$key] = $str;
			$data['color'][$key] = $color;	
		}
		
		$data['organization'] = $this->organization_model->getList($selection);
		$data['occupation'] = $this->occupation_model->getList($selection);
		
		//選單
		$data['mainbar'] = $this->sidebarmenu_model->getsidebarmain();
		$data['subbar'] = $this->sidebarmenu_model->getsidebarsub();
		$data['view'] = $this->competence_model->getaction($this->competence_id);
		
		$data['footer'] = $this->load->view('amadis_sys/common/footer','', TRUE);
		$data['script'] = $this->load->view('amadis_sys/common/script','', TRUE);
		$this->load->view('amadis_sys/common/head');
		$this->load->view('amadis_sys/common/header');
		$this->load->view('amadis_sys/common/sidebar',$data);
		$this->load->view('amadis_sys/employee/list',$data);	
	}

	//跳到指定頁面
	public function pagejump(){
		if($this->input->post('pagenum') == null || $this->input->post('pagenum') == 0){
			redirect('employee/list');
		}
		else{
			redirect('employee/list/'.$this->input->post('pagenum').'');
		}
	}

    // 新增會員表單
	public function create_form(){
		$manager_id = $this->session->userdata('manager_id');
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 1)
			{
				if($r['actions_insert'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div> 您沒有新增的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('employee/list');
				}
			}
		}
		$data = [];
		$data['view'] = $this->competence_model->getaction($this->competence_id);
		$data['title'] = $this->unitName;
		$data['title_small'] = '新增業務承攬人';
		$data['active'] = 'employee';
		$this->session->set_flashdata('sidebarselected','new_employee');

		$selection['status'] = 1;
		$data['result'] = $this->address_model->getList();
		$data['occupation'] = $this->occupation_model->getList($selection);
		$data['right'] = 0;
		if($this->competence_id == 3){
			$selection['organization_id'] = $manager_id;
			$data['organization'] = $this->organization_model->getList($selection);
			$data['employee'] = $this->employee_model->getList($selection);
		}
		else{
			$data['organization'] = $this->organization_model->getList($selection);
			$data['employee'] = $this->employee_model->getList($selection);
			$data['right'] = 1;
		}
		
		//選單
		$data['mainbar'] = $this->sidebarmenu_model->getsidebarmain();
		$data['subbar'] = $this->sidebarmenu_model->getsidebarsub();
		$data['view'] = $this->competence_model->getaction($this->competence_id);

		$data['footer'] = $this->load->view('amadis_sys/common/footer','', TRUE);
		$data['script'] = $this->load->view('amadis_sys/common/script','', TRUE);
		$this->load->view('amadis_sys/common/head');
		$this->load->view('amadis_sys/common/header');
		$this->load->view('amadis_sys/common/sidebar',$data);
		$this->load->view('amadis_sys/employee/create',$data);
	}

	// 新增會員執行
	public function create(){
		$datetime = date ("Y-m-d H:i:s") ;
		if($this->input->post('bank') == null || $this->input->post('bank') == ''){
			$bank = 0;
		}
		else{
			$bank = $this->input->post('bank');
		}

		if($this->input->post('branch') == null || $this->input->post('branch') == ''){
			$branch = 0;
		}
		else{
			$branch = $this->input->post('branch');
		}

		if($this->input->post('optionsRadios1') == null || $this->input->post('optionsRadios1') == ''){
			$banktype = 0;
		}
		else{
			$banktype = $this->input->post('optionsRadios1');
		}

		$employee = array(
			'employee_name'           => $this->input->post('name'),
			'employee_id_card'        => $this->input->post('idcard'),
			'organization_id'         => $this->input->post('organization'),
			'occupation_id'           => $this->input->post('occupation'),
			'employee_supervisor'     => $this->input->post('supervisor'),
			'employee_phone'          => $this->input->post('cellphone'),
			'employee_tel_1'          => $this->input->post('telephone'),
			'address_id'              => $this->input->post('address'),
			'employee_address'        => $this->input->post('addressdetail'),
			'employee_bank_type'      => $banktype,
			'users_id'                => $this->input->post('referrer'),
			'employee_bank_name'      => $bank,
			'employee_bank_branch	' => $branch,
			'employee_account_name	' => $this->input->post('account_name'),
			'employee_bank_account'   => $this->input->post('account'),
			'employee_note'           => $this->input->post('note'),
			'employee_created_date'   => $datetime,
			'employee_created_user'   => $this->session->userdata('users_id')
		);
		$this->employee_model->add_employee($employee);

		$maxid = $this->employee_model->get_last_id($employee);
		$eid = $maxid['maxid'];

		$account = array(
			'competence_id'      => 4,
			'employee_id'        => $eid,
			'organization_id'    => $this->input->post('organization'),
			'customer_id'        => 0,
			'users_name'         => $this->input->post('name'),
			'users_account'      => $this->input->post('cellphone'),
			'users_password'     => hash('sha256','000000'),
			'users_created_date' => date("Y-m-d H:i:s",time()),
			'users_created_user' => $this->session->userdata('users_id')
		);
		$this->account_model->add_account($account);

		$messagediv = "<div class='alert alert-success'>
		<button class='close' data-dismiss='alert'></button>
		<div class='icon-ok'></div> 成功！資料已新增。
		</div>";
		$this->session->set_flashdata('messagediv',$messagediv);

		redirect('employee/list');
	}

	// 刪除會員執行
	public function delete(){
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 1)
			{
				if($r['actions_delete'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div> 您沒有刪除的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('employee/list');
				}
			}
		}
		$id = $this->input->get('id');
		$datetime = date("Y-m-d H:i:s"); 
		$user_id = $this->session->userdata('users_id');

		$employeeupdate = array(
			'employee_is_del'       => 1,
			'employee_updated_date' => $datetime,
			'employee_updated_user' => $user_id
		);
		$this->employee_model->update_employee($employeeupdate,$id);

		$messagediv = "<div class='alert alert-success'>
		<button class='close' data-dismiss='alert'></button>
		<div class='icon-ok'></div> 成功！該筆資料已刪除。
		</div>";
		$this->session->set_flashdata('messagediv',$messagediv);
		redirect('employee/list');
	}

	// 下架會員執行
	public function info_invisible(){
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 1)
			{
				if($r['actions_enable'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div> 您沒有上下架的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('employee/list');
				}
			}
		}
		$id = $this->input->get('id');
		$datetime = date("Y-m-d H:i:s"); 
		$user_id = $this->session->userdata('users_id');

		$employeeupdate = array(
			'employee_status'       => 0,
			'employee_updated_date' => $datetime,
			'employee_updated_user' => $user_id
		);
		$this->employee_model->update_employee($employeeupdate,$id);

		$messagediv = "<div class='alert alert-success'>
		<button class='close' data-dismiss='alert'></button>
		<div class='icon-ok'></div> 成功！該業務承攬人已停用。
		</div>";
		$this->session->set_flashdata('messagediv',$messagediv);
		redirect('employee/list');
	}

	// 上架會員執行
	public function info_visible(){
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 1)
			{
				if($r['actions_enable'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div> 您沒有上下架的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('employee/list');
				}
			}
		}
		$id = $this->input->get('id');
		$datetime = date("Y-m-d H:i:s"); 
		$user_id = $this->session->userdata('users_id');
		$employeeupdate = array(
			'employee_status'       => 1,
			'employee_updated_date' => $datetime,
			'employee_updated_user' => $user_id
		);
		$this->employee_model->update_employee($employeeupdate,$id);

		$messagediv = "<div class='alert alert-success'>
		<button class='close' data-dismiss='alert'></button>
		<div class='icon-ok'></div> 成功！該業務承攬人已啟用。
		</div>";
		$this->session->set_flashdata('messagediv',$messagediv);
		redirect('employee/list');
	}

	// 檢視業務承攬人表單
	public function check_form(){
		$org_id = $this->session->userdata('manager_id');
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 1)
			{
				if($r['actions_view'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div> 您沒有修改的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('employee/list');
				}
			}
		}
		$data = [];
		$id = $this->input->get('id');
		$selection['status'] = 1;
		$data['result'] = $this->employee_model->getidData($id);
		$organization_info = $this->organization_model->getData($data['result']['organization_id']);
		$data['organization_name'] = $organization_info['organization_name'];
		$occupation_info = $this->occupation_model->getidData($data['result']['occupation_id']);
		$data['occupation_name'] = $occupation_info['occupation_name'];
		if($data['result']['employee_bank_name'] != 700){
			$bankinfo = $this->bank_model->getidData($data['result']['employee_bank_name']);
			$branchinfo = $this->branch_model->getidData($data['result']['employee_bank_branch']);
			$data['bankname'] = $bankinfo['bank_name'];
			$data['branchname'] = $branchinfo['bank_branch_name'];
		}
		else{
			$data['bankname'] = '中華郵政';
			$branchinfo = $this->post_model->getidData($data['result']['employee_bank_branch']);
			$data['branchname'] = $branchinfo['post_name'];	
		}

		$data['address'] = $this->address_model->getList();
		$data['occupation'] = $this->occupation_model->getList($selection);
		$data['organization'] = $this->organization_model->getList($selection);
		$data['referrer'] = $this->employee_model->getidData($data['result']['users_id']);
		$data['supervisor'] = $this->employee_model->getidData($data['result']['employee_supervisor']);

		$idinfo = $this->employee_model->getidData($id);
		if($this->competence_id == 3 && $idinfo['organization_id'] != $org_id){
			redirect('employee/list');
		}
		
		//選單
		$data['mainbar'] = $this->sidebarmenu_model->getsidebarmain();
		$data['subbar'] = $this->sidebarmenu_model->getsidebarsub();
		$data['view'] = $this->competence_model->getaction($this->competence_id);
		$data['upid'] = $this->input->get('id');
		$data['title'] = $this->unitName;
		$data['title_small'] = '檢視-業務承攬人';
		$data['active'] = 'employee';

		$data['footer'] = $this->load->view('amadis_sys/common/footer','', TRUE);
		$data['script'] = $this->load->view('amadis_sys/common/script','', TRUE);
		$this->load->view('amadis_sys/common/head');
		$this->load->view('amadis_sys/common/header');
		$this->load->view('amadis_sys/common/sidebar',$data);
		$this->load->view('amadis_sys/employee/check',$data);
	}

	// 修改會員表單
	public function update_form(){
		$organization_id = $this->session->userdata('organization_id');
		$org_id = $this->session->userdata('manager_id');
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 1)
			{
				if($r['actions_update'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div> 您沒有修改的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('employee/list');
				}
			}
		}
		$data = [];
		$id = $this->input->get('id');
		$selection['status'] = 1;
		$data['result'] = $this->employee_model->getidData($id);

		$data['address'] = $this->address_model->getList();
		$data['occupation'] = $this->occupation_model->getList($selection);
		$data['right'] = 0;
		$data['disabled'] = 0;
		if($this->competence_id == 3){
			$selection['organization_id'] = $org_id;
			$data['organization'] = $this->organization_model->getList($selection);
			$data['employee'] = $this->employee_model->getList($selection);
			$data['disabled'] = 1;
		}
		else{
			$data['organization'] = $this->organization_model->getList($selection);
			$data['employee'] = $this->employee_model->getList($selection);
			$data['right'] = 1;
		}
		
		$idinfo = $this->employee_model->getidData($id);
		if($this->competence_id == 3 && $idinfo['organization_id'] != $org_id){
			redirect('employee/list');
		}

		//選單
		$data['mainbar'] = $this->sidebarmenu_model->getsidebarmain();
		$data['subbar'] = $this->sidebarmenu_model->getsidebarsub();
		$data['view'] = $this->competence_model->getaction($this->competence_id);
		$data['upid'] = $this->input->get('id');
		$data['title'] = $this->unitName;
		$data['title_small'] = '編輯-業務承攬人';
		$data['active'] = 'employee';

		$data['footer'] = $this->load->view('amadis_sys/common/footer','', TRUE);
		$data['script'] = $this->load->view('amadis_sys/common/script','', TRUE);
		$this->load->view('amadis_sys/common/head');
		$this->load->view('amadis_sys/common/header');
		$this->load->view('amadis_sys/common/sidebar',$data);
		$this->load->view('amadis_sys/employee/update',$data);
	}

    // 修改會員執行
	public function update(){
		$id = $this->input->post('id');
		$datetime = date ("Y-m-d H:i:s",time()); 
		$employeeupdate = array(
			'employee_name'           => $this->input->post('name'),
			'employee_id_card'        => $this->input->post('idcard'),
			'organization_id'         => $this->input->post('organization'),
			'occupation_id'           => $this->input->post('occupation'),
			'employee_supervisor'     => $this->input->post('supervisor'),
			'employee_phone'          => $this->input->post('cellphone'),
			'employee_tel_1'          => $this->input->post('telephone'),
			'address_id'              => $this->input->post('address'),
			'employee_address'        => $this->input->post('addressdetail'),
			'employee_bank_type'      => $this->input->post('optionsRadios1'),
			'users_id'                => $this->input->post('referrer'),
			'employee_bank_name'      => $this->input->post('bank'),
			'employee_bank_branch	' => $this->input->post('branch'),
			'employee_account_name	' => $this->input->post('account_name'),
			'employee_bank_account'   => $this->input->post('account'),
			'employee_note'           => $this->input->post('note'),
			'employee_updated_date'   => $datetime,
			'employee_updated_user'   => $this->session->userdata('users_id')
		);
		$this->employee_model->update_employee($employeeupdate,$id);

		$accountinfo = $this->users_model->get_employee_account($id);
		
		if($accountinfo['employee_id'] == null){
			if($this->input->post('cellphone') != '' && $this->input->post('cellphone') != 0 && strlen($this->input->post('cellphone')) == 10){
				$account = array(
					'competence_id'      => 4,
					'employee_id'        => $this->input->post('id'),
					'organization_id'    => $this->input->post('organization'),
					'customer_id'        => 0,
					'users_name'         => $this->input->post('name'),
					'users_account'      => $this->input->post('cellphone'),
					'users_password'     => hash('sha256','000000'),
					'users_created_date' => date("Y-m-d H:i:s",time()),
					'users_created_user' => $this->session->userdata('users_id')
				);
				$this->account_model->add_account($account);
			}
		}
		$messagediv = "<div class='alert alert-success'>
		<button class='close' data-dismiss='alert'></button>
		<div class='icon-ok'></div> 成功！此筆資料已更新。
		</div>";
		$this->session->set_flashdata('messagediv',$messagediv);
		redirect('employee/list');
	}

	//批次新增帳號
	public function create_account(){
		$employeelist = $this->employee_model->getList();
		foreach($employeelist as $employee){
			$accountinfo = $this->users_model->get_employee_account($employee['employee_id']);

			if($accountinfo['employee_id'] == null){
				if($employee['employee_phone'] != '' && $employee['employee_phone'] != 0 && strlen($employee['employee_phone']) == 10){
					$account = array(
						'competence_id'      => 4,
						'employee_id'        => $employee['employee_id'],
						'organization_id'    => $employee['organization_id'],
						'customer_id'        => 0,
						'users_name'         => $employee['employee_name'],
						'users_account'      => $employee['employee_phone'],
						'users_password'     => hash('sha256','000000'),
						'users_created_date' => date("Y-m-d H:i:s",time()),
						'users_created_user' => $this->session->userdata('users_id')
					);
					$this->account_model->add_account($account);
				}
			}
		}

		$messagediv = "<div class='alert alert-success'>
		<button class='close' data-dismiss='alert'></button>
		<div class='icon-ok'></div> 成功！帳號已批次建立。
		</div>";
		$this->session->set_flashdata('messagediv',$messagediv);
		redirect('employee/list');	
	}

	//批次更新電話
	public function update_phone(){
		$employeelist = $this->employee_model->getList();
		foreach($employeelist as $employee){
			$employeeupdate = array(
				'employee_phone'        => str_replace('-','',$employee['employee_phone']),
				'employee_updated_date' => date('Y-m-d',time()),
				'employee_updated_user' => $this->session->userdata('users_id')
			);
			$this->employee_model->update_employee($employeeupdate,$employee['employee_id']);
		}
	}
}
