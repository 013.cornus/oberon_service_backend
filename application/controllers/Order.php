<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('amadis_sys/order_model');
		$this->load->model('amadis_sys/order_count_model');
		$this->load->model('amadis_sys/competence_model');
		$this->load->model('amadis_sys/goods_model');
		$this->load->model('amadis_sys/product_model');
		$this->load->model('amadis_sys/product_info_model');
		$this->load->model('amadis_sys/customers_model');
		$this->load->model('amadis_sys/employee_model');
		$this->load->model('amadis_sys/sidebarmenu_model');
		$this->load->model('amadis_sys/pagintion_model');
		$this->load->model('amadis_sys/organization_model');
		$this->load->model('amadis_sys/order_info_model');
		$this->load->model('amadis_sys/bonus_model');
		$this->load->model('amadis_sys/bonus_rate_model');
		$this->load->model('amadis_sys/users_bonus_model');
		$this->load->model('amadis_sys/profit_model');
		$this->unitName = '訂單管理';
		$this->competence_id = $this->session->userdata('bms_competence_id');
		$this->rightid = $this->competence_model->getaction($this->competence_id);

		$admin_id = $this->session->userdata('bms_users_id');
		$username = $this->session->userdata('bms_users_name');

		$this->session->set_flashdata('sidebarselected','order');
		$this->session->set_flashdata('mainsidebar','order');

		if(!$admin_id){
			redirect('home/login');
		}

	}

	 //訂單列表
	public function index(){
		$this->session->unset_userdata('bms_orgid');
		$this->session->unset_userdata('bms_emid');
		$this->session->unset_userdata('bms_year');
		$this->session->unset_userdata('bms_month');
		$this->session->unset_userdata('bms_customid');
		$this->session->unset_userdata('bms_contract_id');
		$this->session->unset_userdata('bms_check_status');
		redirect('order/list');
		exit;
	}

    // 訂單列表
	public function list($page=''){
		$admin_id = $this->session->userdata('bms_users_id');
		$customerid = $this->session->userdata('bms_customer_id');
		$certainemid = $this->session->userdata('bms_employee_id');
		$organization_id = $this->session->userdata('bms_organization_id');
		$manager_id = $this->session->userdata('bms_manager_id');

		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 10) {
				if($r['actions_view'] == 0) {
					$alert['color'] = 'alert';
					$alert['sign'] = '';
					$alert['text'] = '您沒有瀏覽的權限。';
					$this->session->set_flashdata('alert',$alert);
					redirect('home');
				}
			}
		}
		$data = [];
		$data['title'] = $this->unitName;
		$data['title_small'] = '訂單列表';
		$data['pagenum'] = $page;

		if($this->input->post('orgid') != ''){
			$this->session->set_userdata('bms_orgid',$this->input->post('orgid'));
		}

		if($this->input->post('employeeid') != ''){
			$this->session->set_userdata('bms_emid',$this->input->post('employeeid'));
		}

		if($this->input->post('year') != ''){
			$this->session->set_userdata('bms_year',$this->input->post('year'));
		}

		if($this->input->post('month') != ''){
			$this->session->set_userdata('bms_month',$this->input->post('month'));
		}

		if($this->input->post('customid') != ''){
			$this->session->set_userdata('bms_customid',$this->input->post('customid'));
		}

		if($this->input->post('check_status') != ''){
			$this->session->set_userdata('bms_check_status',$this->input->post('check_status'));
		}

		if($this->input->post('fullname') != ''){
			$this->session->set_userdata('bms_contract_id',$this->input->post('fullname'));
		}

		$data['hide'] = '';
		$data['emselect'] = '';
		$data['customselect'] = '';
		$data['orgselect'] = '';

		if($this->session->userdata('bms_customid')){
			$keyword['customerid'] = $this->session->userdata('bms_customid');
		}
		else{
			$keyword['customerid'] = $this->input->post('customid');
		}

		switch ($this->competence_id) {
			case '3':
			$keyword['organization_id'] = trim($manager_id);

			$selection['organization_id'] = $organization_id;
			$selection['status'] = 1;
			$data['orgem'] = $this->employee_model->getList($selection);

			$data['ordercustomid'] = $this->order_model->getorgcustomer($organization_id);

			//撈出所有客戶名
			foreach($data['ordercustomid'] as $key => $value){
				$data['customername'][$key] = $this->customers_model->getiddata($value['customer_id']);
			}
			
			$data['hide'] = "style='display:none'";
			$data['orgselect'] = "style='display:none'";
			break;

			case '4':

			//業務本身及下線ID
			$everyid = array();
			array_push($everyid,$certainemid);
			for($a=0;$a<=10;$a++)
			{
				foreach($everyid as $parent){
					$sonlist = $this->employee_model->getdownline($parent);
					foreach($sonlist as $son){
						if(!in_array($son['employee_id'],$everyid)){
							array_push($everyid,$son['employee_id']);
						}
					}
				}
			}
			$keyword['employee_list'] = $everyid;

			//業務本身及下線的所有客戶ID
			$customeridlist = array();
			foreach($everyid as $downline){
				$selection['status'] = 1;
				$selection['employee_id'] = $downline;
				$orderlist = $this->order_model->getList($selection);
				foreach($orderlist as $orderinfo){
					if(!in_array($orderinfo['customer_id'],$customeridlist)){
						array_push($customeridlist,$orderinfo['customer_id']);
					}
				}
			}

			$data['ordercustomid'] = $customeridlist;		
			//撈出所有客戶名
			foreach($data['ordercustomid'] as $key => $value){
				$data['customername'][$key] = $this->customers_model->getiddata($value);
			}

			$data['emselect'] = "style='display:none'";
			$data['hide'] = "style='display:none'";
			$data['orgselect'] = "style='display:none'";
			break;
			case '5':
			$keyword['customer_id'] = trim($customerid);

			$data['hide'] = "style='display:none'";
			$data['emselect'] = "style='display:none'";
			$data['customselect'] = "style='display:none'";
			$data['orgselect'] = "style='display:none'";
			break;
			default:
			$keyword['status'] = 1;
			$data['orgem'] = $this->employee_model->getList($keyword);
			$data['ordercustomid'] = $this->order_model->getordercustomer();
			$selection['status'] = 1;
			$data['organization'] = $this->organization_model->getList($selection);
			foreach($data['ordercustomid'] as $key => $value){
				$data['customername'][$key] = $this->customers_model->getiddata($value['customer_id']);
			}

			break;
		}

		if($this->session->userdata('bms_emid')){
			$keyword['employeeid'] = $this->session->userdata('bms_emid');
		}
		else{
			$keyword['employeeid'] = $this->input->post('employeeid');
		}

		if($this->session->userdata('bms_year')){
			$keyword['year'] = $this->session->userdata('bms_year');
		}
		else{
			$keyword['year'] = $this->input->post('year');
		}

		if($this->session->userdata('bms_month')){
			$keyword['month'] = $this->session->userdata('bms_month');
		}
		else{
			$keyword['month'] = $this->input->post('month');
		}

		if($this->session->userdata('bms_contract_id')){
			$keyword['contract_id'] = $this->session->userdata('bms_contract_id');
		}
		else{
			$keyword['contract_id'] = $this->input->post('fullname');
		}

		if($this->session->userdata('bms_check_status')){
			$keyword['check_status'] = $this->session->userdata('bms_check_status');
		}
		else{
			$keyword['check_status'] = $this->input->post('check_status');
		}

		$result = $this->order_model->getList($keyword,[]);
		$pager = $this->pagintion_model->setPager($result,$page);
		$data['page_list'] = $pager['page'];
		$data['result'] = $this->order_model->getList($keyword,$pager['list']);

		foreach ($data['result'] as $key => $value) {
			$customer = $this->customers_model->getiddata($value['customer_id']);
			$sales = $this->employee_model->getidData($value['employee_id']);
			$organization = $this->organization_model->getData($value['organization_id']);
			$data['sales'][$key] = $sales['employee_name'];
			$data['customer'][$key] = $customer['customer_name'];
			$data['organizationname'][$key] = $organization['organization_name'];

			switch ($value['order_is_check']) {
				case '1':
				$str = '審核通過';
				$color = 'success';
				// $icon = 'check';
				break;
				case '2':
				$str = '審核否決';
				$color = 'important';
				// $icon = 'remove';
				break;
				case '3':
				$str = '訂單作廢';
				$color = 'important';
				// $icon = 'remove';
				break;
				default:
				$str = '未審核';
				$color = 'default';
				// $icon = 'exclamation-sign';
				break;
			}
			$data['str'][$key] = $str;
			$data['color'][$key] = $color;
			// $data['icon'][$key] = $icon;
		}

		$data['competence'] = $this->session->userdata('bms_competence_id');
		$data['mainbar'] = $this->sidebarmenu_model->getsidebarmain();
		$data['subbar'] = $this->sidebarmenu_model->getsidebarsub();
		$data['view'] = $this->competence_model->getaction($this->competence_id);
		$data['alert'] = '';

		if($this->session->flashdata('alert')){
			$alert = $this->session->flashdata('alert');
			$this->session->set_flashdata('alert',$alert);
			$data['alert'] = $this->load->view('amadis_sys/common/alerts','',TRUE);
		}

		$data['footer'] = $this->load->view('amadis_sys/common/footer','', TRUE);
		$data['script'] = $this->load->view('amadis_sys/common/script','', TRUE);

		$this->load->view('amadis_sys/common/head');
		$this->load->view('amadis_sys/common/header');
		$this->load->view('amadis_sys/common/sidebar',$data);
		$this->load->view('amadis_sys/order/list',$data);
	}

    // 新增訂單表單
	public function create_form(){
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 10) {
				if($r['actions_insert'] == 0) {
					$alert['color'] = 'alert';
					$alert['sign'] = '';
					$alert['text'] = '您沒有新增的權限。';
					$this->session->set_flashdata('alert',$alert);
					redirect('order/list');
				}
			}
		}
		$this->session->set_flashdata('sidebarselected','order_new');
		$datetime = date("Y-m-d");
		$data = [];
		$data['title'] = $this->unitName;
		$data['title_small'] = '新增訂單';

		$data['orginfo'] = $this->organization_model->getData($this->session->userdata('bms_manager_id'));
		$data['compentence'] = $this->session->userdata('bms_competence_id');
		$selection['status'] = 1;

		if($this->competence_id == 3){
			$selection['organization_id'] = $this->session->userdata('bms_manager_id');
			$data['employee'] = $this->employee_model->getList($selection);

			$keyword['organization_id'] = $this->session->userdata('bms_manager_id');
			$data['customer'] = $this->customers_model->getList($keyword);
		}
		else{
			$data['employee'] = $this->employee_model->getList($selection);
			$data['customer'] = $this->customers_model->getList();
		}
		$data['product'] = $this->product_model->getList($selection);
		$data['organization'] = $this->organization_model->getList($selection);

		$data['date'] = $this->order_model->getlatest();
		$data['latestid'] = $this->order_model->getlatestid();
		$data['ordertail'] = substr(date("Ymd"),2,8).str_pad($this->session->userdata('bms_users_id'),2,'0',STR_PAD_LEFT).rand(0,9999);

		$data['mainbar'] = $this->sidebarmenu_model->getsidebarmain();
		$data['subbar'] = $this->sidebarmenu_model->getsidebarsub();
		$data['view'] = $this->competence_model->getaction($this->competence_id);

		$data['footer'] = $this->load->view('amadis_sys/common/footer','', TRUE);
		$data['script'] = $this->load->view('amadis_sys/common/script','', TRUE);

		$this->load->view('amadis_sys/common/head');
		$this->load->view('amadis_sys/common/header');
		$this->load->view('amadis_sys/common/sidebar',$data);
		$this->load->view('amadis_sys/order/create',$data);
	}

	// 新增訂單執行
	public function create(){
		$orderfundpv = 0;
		$orderequitypv = 0;
		$datetime = date("Y-m-d H:i:s",time());
		$activedate = $this->input->post('activedate');
		$order = array(
			'customer_id'        => $this->input->post('customername'),
			'employee_id'        => $this->input->post('employeename'),
			'contract_id'        => $this->input->post('contractnum'),
			'order_pay_type'     => $this->input->post('paytype'),
			'order_buy_date'     => $this->input->post('buydate'),
			'order_active_date'  => $this->input->post('activedate'),
			'order_sign_date'    => $this->input->post('signdate'),
			'order_note'         => $this->input->post('note'),
			'order_created_date' => $datetime,
			'order_created_user' => $this->session->userdata('bms_users_id')
		);
		$this->order_model->add_order($order);

		$id = $this->order_model->getlatestid();

		$order_info = array(
			'order_id'                => $id['maxid'],
			'order_info_num'          => $this->input->post('contractnum'),
			'product_id'              => $this->input->post('product_id'),
			'order_info_count'        => $this->input->post('count'),
			'order_info_created_date' => $datetime,
			'order_info_created_user' => $this->session->userdata('bms_users_id')
		);
		$this->order_info_model->add_order_info($order_info);

		$alert['color'] = 'alert alert-success';
		$alert['sign'] = 'icon-ok';
		$alert['text'] = '成功！資料已新增。';
		$this->session->set_flashdata('alert',$alert);

		redirect('order/list');
	}

	//刪除訂單品項
	public function deleteinfo(){
		$id = $this->input->get('id');
		$orderid=$this->input->get('orderid');
		$datetime = date("Y-m-d H:i:s",time()); 
		$user_id = $this->session->userdata('bms_users_id');
		$this->order_info_model->delete_orderinfo($id,$datetime,$user_id);
		redirect('order/update_form?id='.$orderid);
	}

	// 刪除訂單執行
	public function delete(){
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 10)
			{
				if($r['actions_delete'] == 0)
				{
					$alert['color'] = 'alert';
					$alert['sign'] = '';
					$alert['text'] = '您沒有刪除的權限。';
					$this->session->set_flashdata('alert',$alert);
					redirect('order/list');
				}
			}
		}
		$id = $this->input->get('id');
		$datetime = date("Y-m-d H:i:s",time()); 
		$user_id = $this->session->userdata('bms_users_id');
		$orderupdate = array(
			'order_is_del'       => 1,
			'order_updated_date' => $datetime,
			'order_updated_user' => $user_id
		);
		$this->order_model->update_order($orderupdate,$id);

		$allinterest = $this->profit_model->certainprofit($id);
		foreach($allinterest as $interest){
			$paidinterest = array(
				'profit_is_del'        => 1,
				'profit_updated_date' => $datetime,
				'profit_updated_user' => $user_id
			);
			$this->profit_model->update_profit($paidinterest,$interest['profit_id']);
		}

		$orderinfolist = $this->order_info_model->getcertaininfo($id);
		foreach($orderinfolist as $orderinfo){
			$this->order_info_model->delete_orderinfo($orderinfo['order_info_id'],$datetime,$user_id);
		}

		$orderbonus = $this->users_bonus_model->getOrderBonus($id);
		foreach($orderbonus as $orderbonuslist){
			$this->users_bonus_model->delete_usersbonus($orderbonuslist['users_bonus_id'],$datetime,$user_id);
		}

		$alert['color'] = 'alert alert-success';
		$alert['sign'] = 'icon-ok';
		$alert['text'] = '成功！該筆資料已刪除。';
		$this->session->set_flashdata('alert',$alert);
		redirect('order/list/'.$this->input->get('page').'');
	}

	// 下架訂單執行
	public function info_invisible(){
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 10) {
				if($r['actions_enable'] == 0) {
					$alert['color'] = 'alert';
					$alert['sign'] = '';
					$alert['text'] = '您沒有上下架的權限。';
					$this->session->set_flashdata('alert',$alert);
					redirect('order/list');
				}
			}
		}
		$id = $this->input->get('id');
		$datetime = date("Y-m-d H:i:s"); 
		$user_id = $this->session->userdata('bms_users_id');

		$orderupdate = array(
			'order_status'       => 0,
			'order_updated_date' => $datetime,
			'order_updated_user' => $user_id
		);
		$this->order_model->update_order($orderupdate,$id);

		$alert['color'] = 'alert alert-success';
		$alert['sign'] = 'icon-ok';
		$alert['text'] = '成功！該筆訂單已停用。';
		$this->session->set_flashdata('alert',$alert);
		redirect('order/list/'.$this->input->get('page').'');
	}

	// 上架訂單執行
	public function info_visible(){
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 10) {
				if($r['actions_enable'] == 0) {
					$alert['color'] = 'alert';
					$alert['sign'] = '';
					$alert['text'] = '您沒有上下架的權限。';
					$this->session->set_flashdata('alert',$alert);
					redirect('order/list');
				}
			}
		}
		$id = $this->input->get('id');
		$datetime = date("Y-m-d H:i:s"); 
		$user_id = $this->session->userdata('bms_users_id');
		$orderupdate = array(
			'order_status'       => 1,
			'order_updated_date' => $datetime,
			'order_updated_user' => $user_id
		);
		$this->order_model->update_order($orderupdate,$id);

		$alert['color'] = 'alert alert-success';
		$alert['sign'] = 'icon-ok';
		$alert['text'] = '成功！該筆訂單已啟用。';
		$this->session->set_flashdata('alert',$alert);
		redirect('order/list/'.$this->input->get('page').'');
	}

	// 修改訂單表單
	public function update_form(){
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 10) {
				if($r['actions_update'] == 0) {
					$alert['color'] = 'alert';
					$alert['sign'] = '';
					$alert['text'] = '您沒有修改的權限。';
					$this->session->set_flashdata('alert',$alert);
					redirect('order/list');
				}
			}
		}
		$id = $this->input->get('id');
		$ordercheck = $this->order_model->getidData($id);
		if ($this->competence_id != 1) {
			if($ordercheck['order_is_check'] == 1 || $ordercheck['order_is_check'] == 2) {
				$alert['color'] = 'alert';
				$alert['sign'] = '';
				$alert['text'] = '此訂單已審核完畢,無法再次編輯。';
				$this->session->set_flashdata('alert',$alert);
				redirect('order/list');
			}
		}

		$keyword['fullname'] = $this->input->post('fullname');
		$keyword['organization_id'] = $this->session->userdata('bms_manager_id');
		$selection['status'] = 1;
		$data = [];
		$data['hide'] = '';
		$data['updateright'] = 'readonly';

		$data['result'] = $this->order_model->getidData($id);

		if($this->competence_id == 3){
			$selection['organization_id'] = $this->session->userdata('bms_manager_id');
			$data['employee'] = $this->employee_model->getList($selection);
			$data['customer'] = $this->customers_model->getList($keyword);
		}
		else{
			$data['employee'] = $this->employee_model->getList($selection);
			$data['customer'] = $this->customers_model->getList();
		}
		
		if($data['result']['order_is_check'] != 2 || $this->competence_id != 3){
			$data['hide'] = "style='display:none'";
		}

		if($this->competence_id < 3){
			$data['updateright'] = '';
		}

		$data['orginfo'] = $this->organization_model->getData($this->session->userdata('bms_manager_id'));
		$data['compentence'] = $this->session->userdata('bms_competence_id');
		$data['product'] = $this->product_model->getList($selection);
		$data['infonum'] = 0;
		$data['orderinfo'] = $this->order_info_model->getcertaininfo($id);
		$data['order_info'] = $this->order_info_model->getOrderIdInfo($id);
		foreach($data['orderinfo'] as $row){
			$data['infonum']++;
		}
		$selection['status'] = 1;
		$data['organization'] = $this->organization_model->getList($selection);
		$data['pagenum'] = $this->input->get('page');

		$data['mainbar'] = $this->sidebarmenu_model->getsidebarmain();
		$data['subbar'] = $this->sidebarmenu_model->getsidebarsub();
		$data['view'] = $this->competence_model->getaction($this->competence_id);
		$data['upid'] = $this->input->get('id');

		$data['title'] = $this->unitName;
		$data['title_small'] = '編輯訂單';

		$data['footer'] = $this->load->view('amadis_sys/common/footer','', TRUE);
		$data['script'] = $this->load->view('amadis_sys/common/script','', TRUE);
		$this->load->view('amadis_sys/common/head');
		$this->load->view('amadis_sys/common/header');
		$this->load->view('amadis_sys/common/sidebar',$data);
		$this->load->view('amadis_sys/order/update',$data);
	}

	// 修改訂單執行
	public function update(){
		$id = $this->input->post('id');
		$datetime = date("Y-m-d H:i:s",time());
		$user_id = $this->session->userdata('bms_users_id');
		$activedate = $this->input->post('activedate');

		$orderupdate = array(
			'customer_id'        => $this->input->post('customername'),
			'employee_id'        => $this->input->post('employeename'),
			'contract_id'        => $this->input->post('contractnum'),
			'order_pay_type'     => $this->input->post('paytype'),
			'order_buy_date'     => $this->input->post('buydate'),
			'order_active_date'  => $this->input->post('activedate'),
			'order_sign_date'    => $this->input->post('signdate'),
			'order_note'         => $this->input->post('note'),
			'order_updated_date' => $datetime,
			'order_updated_user' => $this->session->userdata('bms_users_id')
		);

		$this->order_model->update_order($orderupdate,$id);
		$order_info = $this->order_info_model->getOrderIdInfo($id);

		$orderinfoupdate = array(
			'product_id'              => $this->input->post('product_id'),
			'order_info_count'        => $this->input->post('count'),
			'order_info_created_date' => $activedate,
			'order_info_updated_date' => $datetime,
			'order_info_updated_user' => $this->session->userdata('bms_users_id')
		);
		$this->order_info_model->update_orderinfo($orderinfoupdate,$order_info['order_info_id']);

		if($this->input->post('recheckwill') == 1){
			$this->order_model->recheck_order($id,$datetime,$user_id);
		}

		$order = $this->order_model->getidData($id);

		$alert['color'] = 'alert alert-success';
		$alert['sign'] = 'icon-ok';
		$alert['text'] = '成功！此筆資料已更新。';
		$this->session->set_flashdata('alert',$alert);
		redirect('order/list/'.$this->input->post('formerPage').'');
	}

	// 審核訂單表單
	public function check_form(){
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 10) {
				if($r['actions_view'] == 0) {
					$alert['color'] = 'alert';
					$alert['sign'] = '';
					$alert['text'] = '您沒有檢視的權限。';
					$this->session->set_flashdata('alert',$alert);
					redirect('order/list');
				}
			}
		}
		$data = [];
		$id = $this->input->get('id');
		$data['upid'] = $id;
		$data['title'] = $this->unitName;
		$data['title_small'] = '訂單審核';
		$selection['status'] = 1;

		$data['result'] = $this->order_model->getidData($id);
		$data['orderinfo'] = $this->order_info_model->getOrderIdInfo($id);
		$data['product'] = $this->product_model->getidData($data['orderinfo']['product_id']);
		$selection['product_id'] = $data['orderinfo']['product_id'];
		$data['product_info'] = $this->product_info_model->getList($selection);
		foreach($data['product_info'] as $key => $row){
			$goods = $this->goods_model->getidData($row['goods_id']);
			$data['goods_name'][$key] = $goods['goods_name'];
		}
		$data['pagenum'] = $this->input->get('page');

		$data['competence'] = $this->session->userdata('bms_competence_id');
		$data['mainbar'] = $this->sidebarmenu_model->getsidebarmain();
		$data['subbar'] = $this->sidebarmenu_model->getsidebarsub();
		$data['view'] = $this->competence_model->getaction($this->competence_id);
		
		$data['total'] = 0;

		$customer = $this->customers_model->getiddata($data['result']['customer_id']);
		$employee = $this->employee_model->getidData($data['result']['employee_id']);
		if($employee['employee_supervisor'] != 0){
			$supervisor = $this->employee_model->getidData($employee['employee_supervisor']);
			$data['supervisor'] = $supervisor['employee_name'];
		}
		else{
			$data['supervisor'] = $employee['employee_name'];
		}
		$data['customer'] = "<a href='".base_url('customers/check_form?id=').$data['result']['customer_id']."' target='_blank'>".$customer['customer_name']."</a>";

		$data['employee'] = "<a href='".base_url('employee/check_form?id=').$data['result']['employee_id']."' target='_blank'>".$employee['employee_name']."</a>";


		$organization = $this->organization_model->getData($data['result']['organization_id']);
		$data['organization'] = $organization['organization_name'];

		switch ($data['result']['order_is_check']) {
			case '1':
			$str = '審核通過';
			$color = 'green';
			$icon = 'check';
			break;
			case '2':
			$str = '審核否決';
			$color = 'red';
			$icon = 'remove';
			break;
			default:
			$str = '未審核';
			$color = '';
			$icon = 'exclamation-sign';
			break;
		}
		$data['str'] = $str;
		$data['color'] = $color;
		$data['icon'] = $icon;

		$data['footer'] = $this->load->view('amadis_sys/common/footer','', TRUE);
		$data['script'] = $this->load->view('amadis_sys/common/script','', TRUE);
		$this->load->view('amadis_sys/common/head');
		$this->load->view('amadis_sys/common/header');
		$this->load->view('amadis_sys/common/sidebar',$data);
		$this->load->view('amadis_sys/order/check',$data);
	}

	// 審核訂單
	public function check(){
		foreach($this->rightid as $r){
			if($this->competence_id > 2) {
				$alert['color'] = 'alert';
				$alert['sign'] = '';
				$alert['text'] = '您沒有審核的權限。';
				$this->session->set_flashdata('alert',$alert);
				redirect('order/list');
			}
		}
		$id = $this->input->get('id');
		$user_id = $this->session->userdata('bms_users_id');
		$datetime = date("Y-m-d H:i:s",time()); 

		$ordercheck = $this->order_model->getidData($id);
		if($ordercheck['order_is_check'] == 1 || $ordercheck['order_is_check'] == 2 || $ordercheck['order_is_check'] == 3) {
			$alert['color'] = 'alert';
			$alert['sign'] = '';
			$alert['text'] = '此訂單已審核完畢,無法再次編輯。';
			$this->session->set_flashdata('alert',$alert);
			redirect('order/list');
		}

		$orderupdate = array(
			'order_is_check'     => 1,
			'order_updated_date' => $datetime,
			'order_updated_user' => $user_id
		);
		$this->order_model->update_order($orderupdate,$id);

		$alert['color'] = 'alert alert-success';
		$alert['sign']  = 'icon-ok';
		$alert['text']  = '成功！該筆訂單已通過。';
		$this->session->set_flashdata('alert',$alert);
		redirect('order/list');
	}

	// 批次審核訂單
	public function group_check(){
		foreach($this->rightid as $r){
			if($this->competence_id > 2) {
				$alert['color'] = 'alert';
				$alert['sign'] = '';
				$alert['text'] = '您沒有審核的權限。';
				$this->session->set_flashdata('alert',$alert);
				redirect('order/list');
			}
		}

		$keyword['organization_id'] = $this->session->userdata('bms_orgid');
		$keyword['employee_id'] = $this->session->userdata('bms_emid');
		$keyword['year'] = $this->session->userdata('bms_year');
		$keyword['month'] = $this->session->userdata('bms_month');
		$keyword['customer_id'] = $this->session->userdata('bms_customid');
		$keyword['contract_id'] = $this->session->userdata('bms_contract_id');
		$keyword['check_status'] = $this->session->userdata('bms_check_status');
		$user_id = $this->session->userdata('bms_users_id');
		$datetime = date("Y-m-d H:i:s",time());

		$orderlist = $this->order_model->getList($keyword);

		foreach($orderlist as $order){
			if($order['order_is_check'] == 0){
				$orderupdate = array(
					'order_is_check'     => 1,
					'order_updated_date' => $datetime,
					'order_updated_user' => $user_id
				);
				$this->order_model->update_order($orderupdate,$order['order_id']);
			}
		}

		$alert['color'] = 'alert alert-success';
		$alert['sign']  = 'icon-ok';
		$alert['text']  = '成功！訂單已批次通過。';
		$this->session->set_flashdata('alert',$alert);
		redirect('order/list');
	}

	// 否決訂單
	public function reject(){
		foreach($this->rightid as $r){
			if($this->competence_id > 2) {
				$alert['color'] = 'alert';
				$alert['sign'] = '';
				$alert['text'] = '您沒有審核的權限。';
				$this->session->set_flashdata('alert',$alert);
				redirect('order/list');
			}
		}
		$id = $this->input->get('id');
		$user_id = $this->session->userdata('bms_users_id');
		$datetime = date("Y-m-d H:i:s"); 

		$ordercheck = $this->order_model->getidData($id);
		if($ordercheck['order_is_check'] == 1 || $ordercheck['order_is_check'] == 2 || $ordercheck['order_is_check'] == 3)
		{
			$alert['color'] = 'alert';
			$alert['sign'] = '';
			$alert['text'] = '此訂單已審核完畢,無法再次編輯。';
			$this->session->set_flashdata('alert',$alert);
			redirect('order/list');
		}
		
		$orderupdate = array(
			'order_is_check'     => 2,
			'order_updated_date' => $datetime,
			'order_updated_user' => $user_id
		);
		$this->order_model->update_order($orderupdate,$id);

		$messagediv = "<div class='alert alert-success'>
		<button class='close' data-dismiss='alert'></button>
		<div class='icon-ok'></div> 成功！該筆訂單已否決。
		</div>";
		$this->session->set_flashdata('messagediv',$messagediv);
		redirect('order/list');
	}

	// 重審訂單
	public function recheck(){
		foreach($this->rightid as $r){
			if($this->competence_id > 3) {
				$alert['color'] = 'alert';
				$alert['sign'] = '';
				$alert['text'] = '您沒有重新送審核的權限。';
				$this->session->set_flashdata('alert',$alert);
				redirect('order/list');
			}
		}
		$id = $this->input->get('id');
		$user_id = $this->session->userdata('bms_users_id');
		$datetime = date("Y-m-d H:i:s"); 

		$ordercheck = $this->order_model->getidData($id);
		if($ordercheck['order_is_check'] == 1 || $ordercheck['order_is_check'] == 3)
		{	
			$alert['color'] = 'alert';
			$alert['sign']  = '';
			$alert['text']  = '此訂單已審核,無法再次編輯。';
			$this->session->set_flashdata('alert',$alert);
			redirect('order/list');
		}
		
		$orderupdate = array(
			'order_is_check'     => 0,
			'order_updated_date' => $datetime,
			'order_updated_user' => $user_id
		);
		$this->order_model->update_order($orderupdate,$id);

		$alert['color'] = 'alert alert-success';
		$alert['sign']  = 'icon-ok';
		$alert['text']  = '成功！該筆訂單已回到待審核狀態。';
		$this->session->set_flashdata('alert',$alert);
		redirect('order/list');
	}

	// 作廢訂單
	public function invalid(){
		foreach($this->rightid as $r){
			if($this->competence_id > 2) {
				$alert['color'] = 'alert';
				$alert['sign'] = '';
				$alert['text'] = '您沒有審核的權限。';
				$this->session->set_flashdata('alert',$alert);
				redirect('order/list');
			}
		}
		$id = $this->input->get('id');
		$user_id = $this->session->userdata('bms_users_id');
		$datetime = date("Y-m-d H:i:s"); 

		$ordercheck = $this->order_model->getidData($id);
		if($ordercheck['order_is_check'] == 1 || $ordercheck['order_is_check'] == 2 || $ordercheck['order_is_check'] == 3)
		{
			$alert['color'] = 'alert';
			$alert['sign']  = '';
			$alert['text']  = '此訂單已審核完畢,無法再次編輯。';
			$this->session->set_flashdata('alert',$alert);
			redirect('order/list');
		}
		
		$orderupdate = array(
			'order_is_check'     => 3,
			'order_updated_date' => $datetime,
			'order_updated_user' => $user_id
		);
		$this->order_model->update_order($orderupdate,$id);

		$alert['color'] = 'alert alert-success';
		$alert['sign']  = 'icon-ok';
		$alert['text']  = '成功！該筆訂單已作廢。';
		$this->session->set_flashdata('alert',$alert);
		redirect('order/list');
	}

	//跳到指定頁面
	public function pagejump(){
		if($this->input->post('pagenum') == null || $this->input->post('pagenum') == 0){
			redirect('order/list');
		}
		else{
			redirect('order/list/'.$this->input->post('pagenum').'');
		}
	}
}
