<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Competence extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('amadis_sys/competence_model');
		$this->load->model('amadis_sys/sidebarmenu_model');
		$this->unitName = '權限管理';
		$admin_id = $this->session->userdata('bms_users_id');
		$username = $this->session->userdata('bms_users_name');
		$this->competence_id = $this->session->userdata('bms_competence_id');
		$this->rightid = $this->competence_model->getaction($this->competence_id);
		
		$this->session->set_flashdata('sidebarselected','competence');
		$this->session->set_flashdata('mainsidebar','system');
		if(!$admin_id || $this->competence_id >= 4){
			redirect('home/login');
		}
	}

    //顧客列表
	public function index(){
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 24)
			{
				if($r['actions_view'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div> 您沒有瀏覽的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('home');
				}
			}
		}
		$keyword = $this->input->post('fullname');
		$data = [];
		$data['title'] = $this->unitName;
		$data['title_small'] = '權限列表';
		$data['active'] = 'goods';
		$data['result'] = $this->competence_model->getdata($keyword);

		$data['mainbar'] = $this->sidebarmenu_model->getsidebarmain();
		$data['subbar'] = $this->sidebarmenu_model->getsidebarsub();
		$data['view'] = $this->competence_model->getaction($this->competence_id);

		$data['footer'] = $this->load->view('amadis_sys/common/footer','', TRUE);
		$data['script'] = $this->load->view('amadis_sys/common/script','', TRUE);
		$this->load->view('amadis_sys/common/head');
		$this->load->view('amadis_sys/common/header');
		$this->load->view('amadis_sys/common/sidebar',$data);
		$this->load->view('amadis_sys/system/competence/list',$data);	
	}

    // 新增權限表單
	public function create_form(){
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 24)
			{
				if($r['actions_insert'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div> 您沒有新增的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('competence');
				}
			}
		}
		$data = [];
		$data['title'] = $this->unitName;
		$data['title_small'] = '新增權限';
		$data['active'] = 'goods';

		$data['maxid'] = $this->competence_model->getlatest();
		$data['view'] = $this->competence_model->getaction($this->competence_id);
		
		$data['mainbar'] = $this->sidebarmenu_model->getsidebarmain();
		$data['subbar'] = $this->sidebarmenu_model->getsidebarsub();

		$data['footer'] = $this->load->view('amadis_sys/common/footer','', TRUE);
		$data['script'] = $this->load->view('amadis_sys/common/script','', TRUE);
		$this->load->view('amadis_sys/common/head');
		$this->load->view('amadis_sys/common/header');
		$this->load->view('amadis_sys/common/sidebar',$data);
		$this->load->view('amadis_sys/system/competence/create',$data);
	}

	// 新增權限執行
	public function create(){
		$datetime = date ("Y-m-d H:i:s") ; 
		$competence = array(
			'competence_name'         => $this->input->post('competencename'),
			'competence_created_date' => $datetime,
			'competence_created_user' => $this->session->userdata('bms_users_id')
		);
		$data['mainbar'] = $this->sidebarmenu_model->getsidebarmain();
		$data['subbar'] = $this->sidebarmenu_model->getsidebarsub();
		$i = 1;
		foreach($data['subbar'] as $r){
			foreach($data['mainbar'] as $m){
				if($r['sidebar_main_id'] == $m['sidebar_main_id'] && $m['sidebar_main_is_del'] == 0){
					${'view'.$i} = $this->input->post('view'.$i);
					${'insert'.$i} = $this->input->post('insert'.$i);
					${'update'.$i} = $this->input->post('update'.$i);
					${'delete'.$i} = $this->input->post('delete'.$i);
					${'export'.$i} = $this->input->post('export'.$i);

					if(isset(${'view'.$i})){
						${'view'.$i} = 1;
					}
					else{
						${'view'.$i} = 0;
					}

					if(isset(${'insert'.$i})){
						${'insert'.$i} = 1;
					}
					else{
						${'insert'.$i} = 0;
					}

					if(isset(${'update'.$i})){
						${'update'.$i} = 1;
					}
					else{
						${'update'.$i} = 0;
					}

					if(isset(${'enable'.$i})){
						${'enable'.$i} = 1;
					}
					else{
						${'enable'.$i} = 0;
					}

					if(isset(${'delete'.$i})){
						${'delete'.$i} = 1;
					}
					else{
						${'delete'.$i} = 0;
					}

					if(isset(${'export'.$i})){
						${'export'.$i} = 1;
					}
					else{
						${'export'.$i} = 0;
					}
					$newright = array(
						'competence_id'        => $this->input->get('id'),
						'sidebar_main_id'      => $r['sidebar_main_id'],
						'sidebar_sub_id'       => $r['sidebar_sub_id'],
						'actions_view'         => ${'view'.$i},
						'actions_insert'       => ${'insert'.$i},
						'actions_update'       => ${'update'.$i},
						'actions_enable'       => ${'enable'.$i},
						'actions_delete'       => ${'delete'.$i},
						'actions_export'       => ${'export'.$i},
						'actions_created_date' => $datetime,
						'actions_created_user' => $this->session->userdata('bms_users_id')
					);
					$this->competence_model->add_actions($newright);
				}
			}
			$i = $i + 1;
		}
		// for($a=1;$a<=$i;$a++)
		// {
		// 	$newright = array(
		// 		'competence_id'        =>$this->input->get('id'),
		// 		'sidebar_sub_id'      => $a,
		// 		'actions_view'         => $this->input->post('view'.$a),
		// 		'actions_insert'       => $this->input->post('insert'.$a),
		// 		'actions_update'       => $this->input->post('update'.$a),
		// 		'actions_delete'       => $this->input->post('delete'.$a),
		// 		'actions_export'       => $this->input->post('export'.$a),
		// 		'actions_created_date' => $datetime,
		// 		'actions_created_user' => $this->session->userdata('bms_users_id')
		// 	);
		// 	$this->competence_model->add_actions($newright);
		// }
		$this->competence_model->add_competence($competence);
		$messagediv = "<div class='alert alert-success'>
		<button class='close' data-dismiss='alert'></button>
		<div class='icon-ok'></div> 成功！資料已新增。
		</div>";
		$this->session->set_flashdata('messagediv',$messagediv);

		redirect('competence');
	}

	// 刪除權限執行
	public function delete(){
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 24)
			{
				if($r['actions_delete'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div> 您沒有刪除的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('competence');
				}
			}
		}
		$id = $this->input->get('id');
		$datetime = date("Y-m-d H:i:s"); 
		$user_id = $this->session->userdata('bms_users_id');
		$competence = array(
			'competence_is_del'       => 1,
			'competence_updated_date' => $datetime,
			'competence_updated_user' => $id
		);
		$this->competence_model->update_competence($competence,$id);
		$messagediv = "<div class='alert alert-success'>
		<button class='close' data-dismiss='alert'></button>
		<div class='icon-ok'></div> 成功！該筆資料已刪除。
		</div>";
		$this->session->set_flashdata('messagediv',$messagediv);
		redirect('competence');
	}

	// 下架權限執行
	public function info_invisible(){
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 24)
			{
				if($r['actions_enable'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div> 您沒有上下架的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('competence');
				}
			}
		}
		$id = $this->input->get('id');
		$datetime = date("Y-m-d H:i:s"); 
		$user_id = $this->session->userdata('bms_users_id');
		$competence = array(
			'competence_status'       => 0,
			'competence_updated_date' => $datetime,
			'competence_updated_user' => $id
		);
		$this->competence_model->update_competence($competence,$id);
		$messagediv = "<div class='alert alert-success'>
		<button class='close' data-dismiss='alert'></button>
		<div class='icon-ok'></div> 成功！該筆權限已停用。
		</div>";
		$this->session->set_flashdata('messagediv',$messagediv);
		redirect('competence');
	}

	// 上架權限執行
	public function info_visible(){
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 24)
			{
				if($r['actions_enable'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div> 您沒有上下架的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('competence');
				}
			}
		}
		$id = $this->input->get('id');
		$datetime = date("Y-m-d H:i:s"); 
		$user_id = $this->session->userdata('bms_users_id');
		$competence = array(
			'competence_status'       => 1,
			'competence_updated_date' => $datetime,
			'competence_updated_user' => $id
		);
		$this->competence_model->update_competence($competence,$id);
		$messagediv = "<div class='alert alert-success'>
		<button class='close' data-dismiss='alert'></button>
		<div class='icon-ok'></div> 成功！該筆權限已啟用。
		</div>";
		$this->session->set_flashdata('messagediv',$messagediv);
		redirect('competence');
	}

	// 修改權限表單
	public function update_form(){
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 24)
			{
				if($r['actions_update'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div> 您沒有修改的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('competence');
				}
			}
		}
		$data = [];
		$data['view'] = $this->competence_model->getaction($this->competence_id);
		$id = $this->input->get('id');
		$this->competence_id = $this->input->get('id');
		$data['result'] = $this->competence_model->getidData($id);
		$data['action'] = $this->competence_model->getaction($this->competence_id);
		$data['upid'] = $this->input->get('id');
		$data['title']= $this->unitName;
		$data['title_small'] = '編輯權限';

		$data['mainbar'] = $this->sidebarmenu_model->getsidebarmain();
		$data['subbar'] = $this->sidebarmenu_model->getsidebarsub();
		$data['view'] = $this->competence_model->getaction($this->competence_id);

		$data['footer'] = $this->load->view('amadis_sys/common/footer','', TRUE);
		$data['script'] = $this->load->view('amadis_sys/common/script','', TRUE);
		$this->load->view('amadis_sys/common/head');
		$this->load->view('amadis_sys/common/header');
		$this->load->view('amadis_sys/common/sidebar',$data);
		$this->load->view('amadis_sys/system/competence/update',$data);
	}

    // 修改權限執行
	public function update(){
		$id            = $this->input->post('id');
		$allcompetence = $this->session->flashdata('allcompetence');
		$actionid      = $this->session->flashdata('actionid');
		$datetime      = date ("Y-m-d H:i:s") ;
		$competence = array(
			'competence_name'         => $this->input->post('competencename'),
			'competence_updated_date' => $datetime,
			'competence_updated_user' => $this->session->userdata('bms_users_id')
		);
		$a = 0;
		foreach($allcompetence as $r){
			$editright = array(
				'actions_view'         => $this->input->post('view'.$r),
				'actions_insert'       => $this->input->post('new'.$r),
				'actions_update'       => $this->input->post('update'.$r),
				'actions_enable'       => $this->input->post('enable'.$r),
				'actions_delete'       => $this->input->post('delete'.$r),
				'actions_export'       => $this->input->post('export'.$r),
				'actions_updated_date' => $datetime,
				'actions_updated_user' => $this->session->userdata('bms_users_id')
			);
			$actionsid = $actionid[$a];
			$this->competence_model->update_actions($editright,$actionsid);
			$a = $a + 1;
		}

		$this->competence_model->update_competence($competence,$id);
		$messagediv = "<div class='alert alert-success'>
		<button class='close' data-dismiss='alert'></button>
		<div class='icon-ok'></div> 成功！此筆資料已更新。
		</div>";
		$this->session->set_flashdata('messagediv',$messagediv);
		redirect('competence');
	}
}