<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('amadis_sys/product_model');
		$this->load->model('amadis_sys/goods_model');
		$this->load->model('amadis_sys/competence_model');
		$this->load->model('amadis_sys/sidebarmenu_model');
		$this->load->model('amadis_sys/bonus_rate_model');
		$this->load->model('amadis_sys/product_info_model');
		$this->load->model('amadis_sys/pagintion_model');
		$this->unitName = '商品管理';
		$this->competence_id = $this->session->userdata('bms_competence_id');
		$this->rightid = $this->competence_model->getaction($this->competence_id);
		

		$admin_id = $this->session->userdata('bms_users_id');
		$username = $this->session->userdata('bms_users_name');
		$this->session->set_flashdata('sidebarselected','product');
		$this->session->set_flashdata('mainsidebar','product');

		if(!$admin_id){
			redirect('home/login');
		}
	}

    //商品列表
	public function index(){
		redirect('product/list');
		exit;
	}
    //業務列表
	public function list($page=''){
		$organization_id = $this->session->userdata('bms_organization_id');
		$manager_id = $this->session->userdata('bms_manager_id');
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 7)
			{
				if($r['actions_view'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div> 您沒有瀏覽的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('home');
				}
			}
		}
		$data = [];
		$data['title'] = $this->unitName;
		$data['title_small'] = '商品列表';
		$data['active'] = 'employee';

		$keyword['fullname'] = $this->input->post('fullname');
		
		$result = $this->product_model->getList($keyword);		
		$pager = $this->pagintion_model->setPager($result,$page);
		$data['page_list'] = $pager['page'];
		$data['result'] = $this->product_model->getList($keyword,$pager['list']);
		
		//選單
		$data['mainbar'] = $this->sidebarmenu_model->getsidebarmain();
		$data['subbar'] = $this->sidebarmenu_model->getsidebarsub();
		$data['view'] = $this->competence_model->getaction($this->competence_id);
		
		$data['footer'] = $this->load->view('amadis_sys/common/footer','', TRUE);
		$data['script'] = $this->load->view('amadis_sys/common/script','', TRUE);
		$this->load->view('amadis_sys/common/head');
		$this->load->view('amadis_sys/common/header');
		$this->load->view('amadis_sys/common/sidebar',$data);
		$this->load->view('amadis_sys/product/list',$data);	
	}

	//跳到指定頁面
	public function pagejump(){
		if($this->input->post('pagenum') == null || $this->input->post('pagenum') == 0){
			redirect('product/list');
		}
		else{
			redirect('product/list/'.$this->input->post('pagenum').'');
		}
	}

    // 新增表單
	public function create_form(){
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 7) {
				if($r['actions_insert'] == 0) {
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div><div class=' icon-warning-sign'></div> 您沒有新增的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('product');
				}
			}
		}
		$data = [];
		$data['title'] = $this->unitName;
		$data['title_small'] = '新增商品';

		$selection['status'] = 1;
		$data['goods'] = $this->goods_model->getList($selection);
		$data['mainbar'] = $this->sidebarmenu_model->getsidebarmain();
		$data['subbar'] = $this->sidebarmenu_model->getsidebarsub();
		$data['view'] = $this->competence_model->getaction($this->competence_id);
		$this->session->set_flashdata('sidebarselected','product');
		
		$data['footer'] = $this->load->view('amadis_sys/common/footer','', TRUE);
		$data['script'] = $this->load->view('amadis_sys/common/script','', TRUE);
		$this->load->view('amadis_sys/common/head');
		$this->load->view('amadis_sys/common/header');
		$this->load->view('amadis_sys/common/sidebar',$data);
		$this->load->view('amadis_sys/product/create',$data);
	}

	// 新增商品執行
	public function create(){
		$datetime = date("Y-m-d H:i:s",time());

		$product = array(
			'product_name'         => $this->input->post('product_name'),
			'product_price'        => $this->input->post('product_price'),
			'product_code'         => $this->input->post('product_code'),
			'product_created_date' => $datetime,
			'product_created_user' => $this->session->userdata('bms_users_id')
		);
		$this->product_model->add_product($product);

		$latestproduct = $this->product_model->get_latest_id();
		$goodsidlist = $this->input->post('goodsname');
		$countlist = $this->input->post('count');

		foreach($goodsidlist as $key => $goodsid){
			$goodsinfo = $this->goods_model->getidData($goodsid);
			$goods_price = $goodsinfo['goods_a_price'];
			$product_info = array(
				'product_id'                => $latestproduct['maxid'],
				'goods_id'                  => $goodsid,
				'product_info_count'        => $countlist[$key],
				'product_info_created_date' => $datetime,
				'product_info_created_user' => $this->session->userdata('bms_users_id')
			);
			$this->product_info_model->add_product_info($product_info);
		}

		$messagediv = "<div class='alert alert-success'>
		<button class='close' data-dismiss='alert'></button>
		<div class='icon-ok'></div> 成功！商品已新增完成。
		</div>";
		$this->session->set_flashdata('messagediv',$messagediv);
		redirect('product');
	}

	// 刪除執行
	public function delete(){
		$id = $this->input->get('id');
		$user_id = $this->session->userdata('bms_users_id');
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 7) {
				if($r['actions_delete'] == 0) {
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div><div class=' icon-warning-sign'></div> 您沒有刪除的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('product');
				}
			}
		}
		
		$datetime = date("Y-m-d H:i:s"); 
		$product = array(
			'product_is_del'       => 1,
			'product_updated_date' => date("Y-m-d H:i:s",time()),
			'product_updated_user' => $this->session->userdata('bms_users_id')
		);

		$this->product_model->update_product($product,$id);

		$messagediv = "<div class='alert alert-success'>
		<button class='close' data-dismiss='alert'></button>
		<div class='icon-ok'></div> 成功！該筆商品已刪除。
		</div>";
		$this->session->set_flashdata('messagediv',$messagediv);
		redirect('product');
	}

	// 下架執行
	public function product_invisible(){
		$id = $this->input->get('id');
		$user_id = $this->session->userdata('bms_users_id');
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 7) {
				if($r['actions_enable'] == 0) {
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div><div class=' icon-warning-sign'></div> 您沒有下架的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('product');
				}
			}
		}
		
		$datetime = date("Y-m-d H:i:s"); 
		$product = array(
			'product_status'       => 0,
			'product_updated_date' => date("Y-m-d H:i:s",time()),
			'product_updated_user' => $this->session->userdata('bms_users_id')
		);

		$this->product_model->update_product($product,$id);

		$messagediv = "<div class='alert alert-success'>
		<button class='close' data-dismiss='alert'></button>
		<div class='icon-ok'></div> 成功！該筆商品已停用。
		</div>";
		$this->session->set_flashdata('messagediv',$messagediv);
		redirect('product');
	}

	// 上架執行
	public function product_visible(){
		$id = $this->input->get('id');
		$user_id = $this->session->userdata('bms_users_id');
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 7)
			{
				if($r['actions_enable'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div><div class=' icon-warning-sign'></div> 您沒有上架的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('product');
				}
			}
		}
		
		$datetime = date("Y-m-d H:i:s"); 

		$product = array(
			'product_status'       => 1,
			'product_updated_date' => date("Y-m-d H:i:s",time()),
			'product_updated_user' => $this->session->userdata('bms_users_id')
		);

		$this->product_model->update_product($product,$id);

		$messagediv = "<div class='alert alert-success'>
		<button class='close' data-dismiss='alert'></button>
		<div class='icon-ok'></div> 成功！該筆商品已啟用。
		</div>";
		$this->session->set_flashdata('messagediv',$messagediv);
		redirect('product');
	}

	// 編輯表單
	public function update_form(){
		$id = $this->input->get('id');
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 7)
			{
				if($r['actions_update'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div><div class=' icon-warning-sign'></div> 您沒有編輯的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('product');
				}
			}
		}
		$data = [];
		$data['title'] = $this->unitName;
		$data['title_small'] = '編輯商品';
		$selection['status'] = 1;
		$data['goods'] = $this->goods_model->getList($selection);
		
		$data['result'] = $this->product_model->getidData($id);
		$data['orderinfo'] = $this->product_info_model->getcertaininfo($id);
		$data['infonum'] = 0;
		foreach($data['orderinfo'] as $row){
			$data['infonum']++;
		}
		
		$data['mainbar'] = $this->sidebarmenu_model->getsidebarmain();
		$data['subbar'] = $this->sidebarmenu_model->getsidebarsub();
		$data['view'] = $this->competence_model->getaction($this->competence_id);
		$data['upid'] = $this->input->get('id');
		
		$data['footer'] = $this->load->view('amadis_sys/common/footer','', TRUE);
		$data['script'] = $this->load->view('amadis_sys/common/script','', TRUE);
		$this->load->view('amadis_sys/common/head');
		$this->load->view('amadis_sys/common/header');
		$this->load->view('amadis_sys/common/sidebar',$data);
		$this->load->view('amadis_sys/product/update',$data);
	}

    // 編輯執行
	public function update(){
		$id = $this->input->post('id');
		$datetime = date("Y-m-d H:i:s",time());
		$product_rate_info = $this->bonus_rate_model->getonedata($id);

		$product = array(
			'product_name'         => $this->input->post('product_name'),
			'product_price'        => $this->input->post('product_price'),
			'product_code'         => $this->input->post('product_code'),
			'product_updated_date' => $datetime,
			'product_updated_user' => $this->session->userdata('bms_users_id')
		);

		$this->product_model->update_product($product,$id);

		$oriInfoId = $this->session->flashdata('infoidnum');
		$origgoods = $this->input->post('origgoods');
		$origCount = $this->input->post('origCount');

		foreach($oriInfoId as $key => $infoid) {
			$info = $this->product_info_model->getidData($infoid);
			$goodsinfo = $this->goods_model->getidData($origgoods[$key]);
			if($origgoods[$key] != $info['goods_id'] || $origCount[$key] != $info['product_info_count']){
				$updaterequirement1 = true;
			}


			if($origCount[$key] == null){
				$this->product_info_model->delete_product_info($infoid,$datetime,$this->session->userdata('bms_users_id'));
				continue;
			}
			else{
				$product_info = array(
					'goods_id'                  => $origgoods[$key],
					'product_info_count'        => $origCount[$key],
					'product_info_updated_date' => $datetime,
					'product_info_updated_user' => $this->session->userdata('bms_users_id')
				);
				$this->product_info_model->update_product_info($product_info,$infoid);
			}
		}

		$goodsList = $this->input->post('goods');
		$countList = $this->input->post('goodscount');

		foreach($goodsList as $key => $goodsid){
			$goodsinfo = $this->goods_model->getidData($goodsid);
			$goods_price = $goodsinfo['goods_a_price'];
			$product_info = array(
				'product_id'                => $id,
				'goods_id'                  => $goodsid,
				'product_info_count'        => $countList[$key],
				'product_info_created_date' => $datetime,
				'product_info_created_user' => $this->session->userdata('bms_users_id')
			);
			$this->product_info_model->add_product_info($product_info);
		}

		$messagediv = "<div class='alert alert-success'>
		<button class='close' data-dismiss='alert'></button>
		<div class='icon-ok'></div> 成功！此筆商品已更新完成。
		</div>";
		$this->session->set_flashdata('messagediv',$messagediv);
		redirect('product');
	}

	// 編輯表單
	public function check_form(){
		$id = $this->input->get('id');
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 7)
			{
				if($r['actions_view'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div><div class=' icon-warning-sign'></div> 您沒有編輯的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('product');
				}
			}
		}
		$data = [];
		$data['title'] = $this->unitName;
		$data['title_small'] = '編輯商品';
		
		$data['result'] = $this->product_model->getidData($id);
		
		$data['mainbar'] = $this->sidebarmenu_model->getsidebarmain();
		$data['subbar'] = $this->sidebarmenu_model->getsidebarsub();
		$data['view'] = $this->competence_model->getaction($this->competence_id);
		$data['upid'] = $this->input->get('id');
		
		$data['footer'] = $this->load->view('amadis_sys/common/footer','', TRUE);
		$data['script'] = $this->load->view('amadis_sys/common/script','', TRUE);
		$this->load->view('amadis_sys/common/head');
		$this->load->view('amadis_sys/common/header');
		$this->load->view('amadis_sys/common/sidebar',$data);
		$this->load->view('amadis_sys/product/check',$data);
	}
}
