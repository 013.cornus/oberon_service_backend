<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// ini_set('display_errors','1');
// error_reporting(E_ALL);
class Home extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('amadis_sys/users_model');
		$this->load->model('amadis_sys/competence_model');
		$this->load->model('amadis_sys/sidebarmenu_model');
		$this->load->model('amadis_sys/users_log_model');
		$this->load->model('amadis_sys/system/group_info_model');
		$this->load->model('amadis_sys/ks/detection_model');
		$this->load->model('amadis_sys/ks/ks_users_model');
		$this->load->model('amadis_sys/ks/users_info_model');
		$this->load->model('amadis_sys/ks/group_model');
		
		$this->unitName = '首頁';
		$this->competence_id = $this->session->userdata('bms_competence_id');

		$this->session->set_flashdata('sidebarselected','homepage');
		$this->session->set_flashdata('mainsidebar','homepage');
		set_time_limit(60000);
	}

	public function index(){
		$admin_id = $this->session->userdata('bms_users_id');
		$viewid = $this->session->userdata('bms_compentece_id');
		$users_group_type = $this->session->userdata('bms_group_type');
		$check_login = $this->users_model->getuseridData($admin_id);

		if(!$admin_id){
			redirect('home/login');
		}
		else{
			if($check_login['users_check'] == 0) {
				redirect('changepassword');
			}
		}

		$data = [];
		$data['title'] = $this->unitName = '首頁';
		$data['title_small'] = '資訊總覽';
		$selection['status'] = 1;
		$data['mainbar'] = $this->sidebarmenu_model->getsidebarmain();
		$data['subbar'] = $this->sidebarmenu_model->getsidebarsub();
		$data['view'] = $this->competence_model->getaction($this->competence_id);

		$currentmonth = date('Y-m',time());
		$lastmonth = date('Y-m',strtotime("$currentmonth -1month"));

		//獲取帳號分類
		$group_type = $this->group_model->getGroupType();
		$groupInfo = $this->group_info_model->getList();
		$data['typeList'] = [];
		foreach($groupInfo as $key => $row){
			if($row['group_info_type'] == 0 || ($row['group_info_type'] != $users_group_type && $users_group_type != 0)){
				continue;
			}

			$groupList = array(
				'group_type_name' => $row['group_info_name'],
				'group_type_id'   => $row['group_info_type']
			);

			array_push($data['typeList'],$groupList);
		}

		//獲取各帳號群檢測數
		$data['status_1'] = 0;
		$data['status_2'] = 0;
		$data['status_3'] = 0;
		$data['status_4'] = 0;
		$data['status_5'] = 0;
		$data['status_6'] = 0;
		$data['groupCaseNum'] = [];
		foreach($data['typeList'] as $key => $row){
			$caseNum = 0;
			$query['type'] = $row['group_type_id'];
			$groupList = $this->group_model->getList($query);
			$data['link'.$row['group_type_id']] = base_url('rgms_report/report_view/list?group_type='.$row['group_type_id']);
			foreach($groupList as $key2 => $row2){
				$query2['group'] = $row2['group_id'];
				$usersList = $this->ks_users_model->getList($query2);
				foreach($usersList as $key3 => $row3){
					$query3['user'] = $row3['users_id'];
					$detectionList = $this->detection_model->getList($query3);
					$caseNum = $caseNum + count($detectionList);
					// foreach($detectionList as $key4 => $row4){
					// 	$allData = glob('C:\web\app\report/'.$row4['detection_case_id'].'\*_Research.JSON');
					// 	foreach($allData as $key => $row){
			  //   			$file = utf8_encode(file_get_contents($row));
			  //   			$info = json_decode($file,true);
			  //   			$data['status_1'] = $data['status_1']+$info[0]['1'];
			  //   		    $data['status_2'] = $data['status_2']+$info[0]['2'];
			  //   			$data['status_3'] = $data['status_3']+$info[0]['3'];
			  //   			$data['status_4'] = $data['status_4']+$info[0]['4'];
			  //   			$data['status_5'] = $data['status_5']+$info[0]['5'];
			  //   			$data['status_6'] = $data['status_6']+$info[0]['6'];
			  //   		}
					// }
				}
			}
			array_push($data['groupCaseNum'],$caseNum);
		}

		//get users amount
		$keyword['type'] = $this->session->userdata('bms_group_type');
		$group_id_list = $this->group_model->getList($keyword);

		$users_num = 0;
		foreach ($group_id_list as $key => $row) {
			$keyword['group'] = $row['group_id'];
			$users_num += count($this->ks_users_model->getList($keyword));
		}
		$data['users_num'] = $users_num;
		/*<---------------------------------------------------------------->*/

		$data['detectionNumber'] = 0;
		$data['detectionTotalView'] = '';
		$data['link0'] = base_url('rgms_report/report_view/list?group_type=0');
		if($users_group_type != 0){
			$data['detectionTotalView'] = "style ='display:none'";
		}
		foreach($data['groupCaseNum'] as $key => $row){
			$data['detectionNumber'] = $data['detectionNumber'] + $row;
		}

		// $data['link1'] = base_url().'';
		// $data['link2'] = base_url().'order';
		// $data['link3'] = base_url().'customers';
		// $data['link4'] = base_url().'order';
		$data['hide'] = '';
		$selection['test'] = '';

		$data['footer'] = $this->load->view('amadis_sys/common/footer','', TRUE);
		$data['script'] = $this->load->view('amadis_sys/common/script','', TRUE);
		$data['alert'] = '';
		
		if($this->session->flashdata('alert')){
			$alert = $this->session->flashdata('alert');
			$this->session->set_flashdata('alert',$alert);
			$data['alert'] = $this->load->view('amadis_sys/common/alerts','',TRUE);
		}
		$this->load->view('amadis_sys/common/head');
		$this->load->view('amadis_sys/common/header');
		$this->load->view('amadis_sys/common/sidebar',$data);
		$this->load->view('amadis_sys/home',$data);
	}

	//確認帳號身分
	public function identitycheck(){
		$user_acconut = $this->input->post('user_email');
		$password = $this->input->post('user_password');
		$account = array(
			'user_acconut' => $user_acconut,
			'password'     => $password
		);

		$data['account'] = $account;
		$chech_result = $this->users_model->check_account_num($user_acconut);
		if($chech_result == false){
			$admin_login = array(
				'users_account'  => $user_acconut,
				'users_password' => hash('sha256',$password),
			);
			$data = $this->users_model->login_user($admin_login['users_account'],$admin_login['users_password']);
			if($data){
				$this->session->set_userdata('bms_users_id',$data['users_id']);
				$this->session->set_userdata('bms_customer_id',$data['customer_id']);
				$this->session->set_userdata('bms_users_account',$data['users_account']);
				$this->session->set_userdata('bms_users_password',$data['users_password']);
				$this->session->set_userdata('bms_users_name',$data['users_name']);
				$this->session->set_userdata('bms_group_type',$data['group_type']);
				$this->session->set_userdata('bms_competence_id',$data['competence_id']);
				$this->users_model->updatelastlogin($data['users_id']);
				$this->users_log_model->insertData(1);
				$alert['color'] = 'alert alert-success';
				$alert['sign'] = 'icon-ok';
				$alert['text'] = '登入成功！';
				$this->session->set_flashdata('alert',$alert);
				redirect('home');
			}
			else{
				$alert['color'] = 'alert alert-danger';
				$alert['sign'] = 'icon-warning-sign';
				$alert['text'] = '帳號或密碼有誤，請重新輸入！';
				$this->session->set_flashdata('alert',$alert);
				redirect('home/login');
			}
		}
		else{
			$admin_login = array(
				'users_account'  => $user_acconut,
				'users_password' => hash('sha256',$password),
			);
			$account_exist = $this->users_model->login_user($admin_login['users_account'],$admin_login['users_password']);
			if($account_exist){
				$this->load->view('amadis_sys/common/head');
				$this->load->view('amadis_sys/check_identity',$data);
				$this->load->view('amadis_sys/common/script');
			}
			else{
				$alert['color'] = 'alert alert-danger';
				$alert['sign'] = 'icon-warning-sign';
				$alert['text'] = '帳號或密碼有誤，請重新輸入！';
				$this->session->set_flashdata('alert',$alert);
				redirect('home/login');
			}
		}
	}

	//確認帳號密碼
	public function logincheck(){
		$account = $this->session->flashdata('account');
		$type = $this->input->get('type');
		$user_acconut = $account['user_acconut'];
		$password = $account['password'];
		$admin_login = array(
			'users_account'  => $user_acconut,
			'users_password' => hash('sha256',$password),
		);

		if($type == 1){
			$data = $this->users_model->login_user($admin_login['users_account'],$admin_login['users_password']);
		}
		else{
			$data = $this->users_model->login_customer($admin_login['users_account'],$admin_login['users_password']);
		}

		if($data){
			$this->session->set_userdata('bms_users_id',$data['users_id']);
			$this->session->set_userdata('bms_customer_id',$data['customer_id']);
			$this->session->set_userdata('bms_users_account',$data['users_account']);
			$this->session->set_userdata('bms_users_password',$data['users_password']);
			$this->session->set_userdata('bms_users_name',$data['users_name']);
			$this->session->set_userdata('bms_group_type',$data['group_type']);
			$this->session->set_userdata('bms_competence_id',$data['competence_id']);
			$this->users_model->updatelastlogin($data['users_id']);
			$this->users_log_model->insertData(1);
			$alert['color'] = 'alert alert-success';
			$alert['sign'] = 'icon-ok';
			$alert['text'] = '登入成功！';
			$this->session->set_flashdata('alert',$alert);
			redirect('home');
		}
		else{
			$alert['color'] = 'alert alert-danger';
			$alert['sign'] = 'icon-warning-sign';
			$alert['text'] = '帳號或密碼有誤，請重新輸入！';
			$this->session->set_flashdata('alert',$alert);
			redirect('home/login');
		}
	}

	//變更密碼
	public function passwordchange(){
		$user_acconut = $this->input->post('user_email');
		$password = $this->input->post('user_password');
		$admin_login = array(
			'users_account'  => $user_acconut,
			'users_password' => hash('sha256',$password),
		);
		$data = [];
		$data['username'] = $this->session->userdata('bms_users_name');
		$data['account'] = $this->session->userdata('bms_users_account');
		$data['title'] = $this->unitName = '帳戶設定';
		$data['title_small'] = '密碼變更';

		$data['mainbar'] = $this->sidebarmenu_model->getsidebarmain();
		$data['subbar'] = $this->sidebarmenu_model->getsidebarsub();
		$data['view'] = $this->competence_model->getaction($this->competence_id);

		$this->load->view('amadis_sys/common/head');
		$this->load->view('amadis_sys/common/header');
		$this->load->view('amadis_sys/common/sidebar',$data);
		$this->load->view('amadis_sys/account/updatepwd',$data);
	}

	//變更密碼執行
	public function dochange(){
		$user_acconut = $this->input->post('user_email');
		$password = $this->input->post('user_password');
		$id = $this->session->userdata('bms_users_id');

		$newpassword = array(
			'users_password'     => hash('sha256',$this->input->post('password_2')),
			'users_updated_date' => date("Y-m-d H:i:s",time()),
			'users_updated_user' => $id,
		);
		$this->account_model->changepassword($newpassword,$id);

		$alert['color'] = 'alert alert-success';
		$alert['sign'] = 'icon-ok';
		$alert['text'] = '成功！用戶密碼已更新。';
		$this->session->set_flashdata('alert',$alert);
		redirect('home');
	}

	public function login(){
		$admin_id = $this->session->userdata('bms_users_id');
		if($admin_id){
			redirect('home');
		}
		$data = [];
		$data['title'] = 'KS | KS管理系統';
		$data['copy_text'] = 'KS Backend System';
		$data['year'] = date('Y');
		$data['alert'] = '';

		if($this->session->flashdata('alert')){
			$alert = $this->session->flashdata('alert');
			$this->session->set_flashdata('alert',$alert);
			$data['alert'] = $this->load->view('amadis_sys/common/alerts','',TRUE);
		}
		$this->load->view('amadis_sys/common/head');
		$this->load->view('amadis_sys/login',$data);
		$this->load->view('amadis_sys/common/script');
	}

	public function logout(){
		$this->users_log_model->insertData(2);
		$this->session->unset_userdata('bms_users_id');
		$this->session->unset_userdata('bms_customer_id');
		$this->session->unset_userdata('bms_users_account');
		$this->session->unset_userdata('bms_users_password');
		$this->session->unset_userdata('bms_users_name');
		$this->session->unset_userdata('bms_group_type');
		$this->session->unset_userdata('bms_competence_id');
		redirect('home/login');
	}
}
