<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Goods extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('amadis_sys/goods_model');
		$this->load->model('amadis_sys/competence_model');
		$this->load->model('amadis_sys/sidebarmenu_model');
		$this->load->model('amadis_sys/bonus_rate_model');
		$this->load->model('amadis_sys/pagintion_model');
		$this->unitName = '產品管理';
		$this->competence_id = $this->session->userdata('competence_id');
		$this->rightid = $this->competence_model->getaction($this->competence_id);
		

		$admin_id = $this->session->userdata('users_id');
		$username = $this->session->userdata('users_name');
		$this->session->set_flashdata('sidebarselected','goods');
		$this->session->set_flashdata('mainsidebar','goods');

		if(!$admin_id){
			redirect('home/login');
		}
	}

    //產品列表
	public function index(){
		redirect('goods/list');
		exit;
	}
    //產品列表
	public function list($page=''){
		$organization_id = $this->session->userdata('organization_id');
		$manager_id = $this->session->userdata('manager_id');
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 7)
			{
				if($r['actions_view'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div> 您沒有瀏覽的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('home');
				}
			}
		}
		$data = [];
		$data['title'] = $this->unitName;
		$data['title_small'] = '產品列表';
		$data['active'] = 'employee';

		$keyword['fullname'] = $this->input->post('fullname');
		
		$result = $this->goods_model->getList($keyword);		
		$pager = $this->pagintion_model->setPager($result,$page);
		$data['page_list'] = $pager['page'];
		$data['result'] = $this->goods_model->getList($keyword,$pager['list']);
		
		//選單
		$data['mainbar'] = $this->sidebarmenu_model->getsidebarmain();
		$data['subbar'] = $this->sidebarmenu_model->getsidebarsub();
		$data['view'] = $this->competence_model->getaction($this->competence_id);
		
		$data['footer'] = $this->load->view('amadis_sys/common/footer','', TRUE);
		$data['script'] = $this->load->view('amadis_sys/common/script','', TRUE);
		$this->load->view('amadis_sys/common/head');
		$this->load->view('amadis_sys/common/header');
		$this->load->view('amadis_sys/common/sidebar',$data);
		$this->load->view('amadis_sys/goods/list',$data);	
	}

	//跳到指定頁面
	public function pagejump(){
		if($this->input->post('pagenum') == null || $this->input->post('pagenum') == 0){
			redirect('goods/list');
		}
		else{
			redirect('goods/list/'.$this->input->post('pagenum').'');
		}
	}

    // 新增表單
	public function create_form(){
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 7) {
				if($r['actions_insert'] == 0) {
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div><div class=' icon-warning-sign'></div> 您沒有新增的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('goods');
				}
			}
		}
		$data = [];
		$data['title'] = $this->unitName;
		$data['title_small'] = '新增產品';

		$data['mainbar'] = $this->sidebarmenu_model->getsidebarmain();
		$data['subbar'] = $this->sidebarmenu_model->getsidebarsub();
		$data['view'] = $this->competence_model->getaction($this->competence_id);
		$this->session->set_flashdata('sidebarselected','new_goods');
		
		$data['footer'] = $this->load->view('amadis_sys/common/footer','', TRUE);
		$data['script'] = $this->load->view('amadis_sys/common/script','', TRUE);
		$this->load->view('amadis_sys/common/head');
		$this->load->view('amadis_sys/common/header');
		$this->load->view('amadis_sys/common/sidebar',$data);
		$this->load->view('amadis_sys/goods/create',$data);
	}

	// 新增產品執行
	public function create(){
		$datetime = date("Y-m-d H:i:s",time());
		$latestgoods = $this->goods_model->get_latest_id();
		$goods = array(
			'goods_name'         => $this->input->post('goodsname'),
			'goods_a_price'      => $this->input->post('tgoodsprice'),
			'goods_created_date' => $datetime,
			'goods_created_user' => $this->session->userdata('users_id')
		);
		$this->goods_model->add_goods($goods);

		$messagediv = "<div class='alert alert-success'>
		<button class='close' data-dismiss='alert'></button>
		<div class='icon-ok'></div> 成功！產品已新增完成。
		</div>";
		$this->session->set_flashdata('messagediv',$messagediv);
		redirect('goods');
	}

	// 刪除執行
	public function delete(){
		$id = $this->input->get('id');
		$user_id = $this->session->userdata('users_id');
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 7) {
				if($r['actions_delete'] == 0) {
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div><div class=' icon-warning-sign'></div> 您沒有刪除的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('goods');
				}
			}
		}
		
		$datetime = date("Y-m-d H:i:s"); 
		$goods = array(
			'goods_is_del'       => 1,
			'goods_updated_date' => date("Y-m-d H:i:s",time()),
			'goods_updated_user' => $this->session->userdata('users_id')
		);

		$this->goods_model->update_goods($goods,$id);

		$messagediv = "<div class='alert alert-success'>
		<button class='close' data-dismiss='alert'></button>
		<div class='icon-ok'></div> 成功！該筆產品已刪除。
		</div>";
		$this->session->set_flashdata('messagediv',$messagediv);
		redirect('goods');
	}

	// 下架執行
	public function goods_invisible(){
		$id = $this->input->get('id');
		$user_id = $this->session->userdata('users_id');
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 7) {
				if($r['actions_enable'] == 0) {
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div><div class=' icon-warning-sign'></div> 您沒有下架的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('goods');
				}
			}
		}
		
		$datetime = date("Y-m-d H:i:s"); 
		$goods = array(
			'goods_status'       => 0,
			'goods_updated_date' => date("Y-m-d H:i:s",time()),
			'goods_updated_user' => $this->session->userdata('users_id')
		);

		$this->goods_model->update_goods($goods,$id);

		$messagediv = "<div class='alert alert-success'>
		<button class='close' data-dismiss='alert'></button>
		<div class='icon-ok'></div> 成功！該筆產品已停用。
		</div>";
		$this->session->set_flashdata('messagediv',$messagediv);
		redirect('goods');
	}

	// 上架執行
	public function goods_visible(){
		$id = $this->input->get('id');
		$user_id = $this->session->userdata('users_id');
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 7)
			{
				if($r['actions_enable'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div><div class=' icon-warning-sign'></div> 您沒有上架的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('goods');
				}
			}
		}
		
		$datetime = date("Y-m-d H:i:s"); 

		$goods = array(
			'goods_status'       => 1,
			'goods_updated_date' => date("Y-m-d H:i:s",time()),
			'goods_updated_user' => $this->session->userdata('users_id')
		);

		$this->goods_model->update_goods($goods,$id);

		$messagediv = "<div class='alert alert-success'>
		<button class='close' data-dismiss='alert'></button>
		<div class='icon-ok'></div> 成功！該筆產品已啟用。
		</div>";
		$this->session->set_flashdata('messagediv',$messagediv);
		redirect('goods');
	}

	// 編輯表單
	public function update_form(){
		$id = $this->input->get('id');
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 7)
			{
				if($r['actions_update'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div><div class=' icon-warning-sign'></div> 您沒有編輯的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('goods');
				}
			}
		}
		$data = [];
		$data['title'] = $this->unitName;
		$data['title_small'] = '編輯產品';
		
		$data['result'] = $this->goods_model->getidData($id);
		$goods_rate_info = $this->bonus_rate_model->getonedata($id);
		
		$data['mainbar'] = $this->sidebarmenu_model->getsidebarmain();
		$data['subbar'] = $this->sidebarmenu_model->getsidebarsub();
		$data['view'] = $this->competence_model->getaction($this->competence_id);
		$data['upid'] = $this->input->get('id');
		
		$data['footer'] = $this->load->view('amadis_sys/common/footer','', TRUE);
		$data['script'] = $this->load->view('amadis_sys/common/script','', TRUE);
		$this->load->view('amadis_sys/common/head');
		$this->load->view('amadis_sys/common/header');
		$this->load->view('amadis_sys/common/sidebar',$data);
		$this->load->view('amadis_sys/goods/update',$data);
	}

    // 編輯執行
	public function update(){
		$id = $this->input->post('id');
		$goods_rate_info = $this->bonus_rate_model->getonedata($id);

		$goods = array(
			'goods_name'         => $this->input->post('goodsname'),
			'goods_a_price'      => $this->input->post('tgoodsprice'),
			'goods_updated_date' => date("Y-m-d H:i:s",time()),
			'goods_updated_user' => $this->session->userdata('users_id')
		);

		$this->goods_model->update_goods($goods,$id);

		$bonus_rate = array(
			'bonus_rate_interest_allowance' => $this->input->post('interest'),
			'bonus_rate_build_period'       => $this->input->post('build_period'),
			'bonus_rate_quarterly'          => $this->input->post('qinterest'),
			'bonus_rate_give'               => $this->input->post('times'),
			'bonus_rate_updated_date'       => date("Y-m-d H:i:s",time()),
			'bonus_rate_updated_user'       => $this->session->userdata('users_id')
		);

		$this->bonus_rate_model->update_bonus_rate($bonus_rate,$goods_rate_info['bonus_rate_id']);

		$messagediv = "<div class='alert alert-success'>
		<button class='close' data-dismiss='alert'></button>
		<div class='icon-ok'></div> 成功！此筆產品已更新完成。
		</div>";
		$this->session->set_flashdata('messagediv',$messagediv);
		redirect('goods');
	}

	// 編輯表單
	public function check_form(){
		$id = $this->input->get('id');
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 7)
			{
				if($r['actions_view'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div><div class=' icon-warning-sign'></div> 您沒有編輯的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('goods');
				}
			}
		}
		$data = [];
		$data['title'] = $this->unitName;
		$data['title_small'] = '編輯產品';
		
		$data['result'] = $this->goods_model->getidData($id);
		
		$data['mainbar'] = $this->sidebarmenu_model->getsidebarmain();
		$data['subbar'] = $this->sidebarmenu_model->getsidebarsub();
		$data['view'] = $this->competence_model->getaction($this->competence_id);
		$data['upid'] = $this->input->get('id');
		
		$data['footer'] = $this->load->view('amadis_sys/common/footer','', TRUE);
		$data['script'] = $this->load->view('amadis_sys/common/script','', TRUE);
		$this->load->view('amadis_sys/common/head');
		$this->load->view('amadis_sys/common/header');
		$this->load->view('amadis_sys/common/sidebar',$data);
		$this->load->view('amadis_sys/goods/check',$data);
	}
}
