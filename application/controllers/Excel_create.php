<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Excel_create extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('amadis_sys/competence_model');
		$this->load->model('amadis_sys/sidebarmenu_model');
		$this->load->model('amadis_sys/bonus_rate_model');
		$this->load->model('amadis_sys/pagintion_model');
		$this->unitName = '產品管理';
		$this->competence_id = $this->session->userdata('bms_competence_id');
		$this->rightid = $this->competence_model->getaction($this->competence_id);
		

		$admin_id = $this->session->userdata('bms_users_id');
		$username = $this->session->userdata('bms_users_name');
		$this->session->set_flashdata('sidebarselected','excel_create');
		$this->session->set_flashdata('mainsidebar','excel_create');
		$this->load->library('excel');

		if(!$admin_id){
			redirect('home/login');
		}
	}

    //產品列表
	public function index(){
		redirect('excel_create/list');
		exit;
	}
    //產品列表
	public function list($page=''){
		$organization_id = $this->session->userdata('bms_organization_id');
		$manager_id = $this->session->userdata('bms_manager_id');
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 7)
			{
				if($r['actions_view'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div> 您沒有瀏覽的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('home');
				}
			}
		}
		$data = [];
		$data['title'] = $this->unitName;
		$data['title_small'] = '匯出報表列表';
		$data['active'] = 'employee';

		$keyword['fullname'] = $this->input->post('fullname');

		//讀取資料測試
		$objPHPExcel = PHPExcel_IOFactory::load('C:/xampp/htdocs/bms_model/public/uploads/excel_model/Report_data.xlsx');
		$objPHPExcel->setActiveSheetIndex(0);
		$row = $objPHPExcel->getActiveSheet()->getHighestRow();
		$col = ord($objPHPExcel->getActiveSheet()->getHighestColumn());
		$value = array();
		$location = array();

		for($colNum = 65; $colNum <= $col; $colNum++){
			for($rowNum = 1; $rowNum<=$row; $rowNum++){
				if(!empty($objPHPExcel->getActiveSheet()->getCell(chr($colNum).$rowNum)->getValue())){
					array_push($value,$objPHPExcel->getActiveSheet()->getCell(chr($colNum).$rowNum)->getValue());
					array_push($location,chr($colNum).$rowNum);
				}
			}
		}
		
		// foreach($location as $key => $row){
		// 	echo $row.":".$value[$key]."<br>";
		// }
		// exit();
		//讀取資料測試

		// $txt = fopen('C:\Users\user\Desktop\source.txt','r');
		// $txtcontent = fread($txt,filesize('C:\Users\user\Desktop\source.txt'));
		// $test = preg_split("/[\s,]+/", $txtcontent);
		// print_r($test);
		// exit();

		//選單
		$data['mainbar'] = $this->sidebarmenu_model->getsidebarmain();
		$data['subbar'] = $this->sidebarmenu_model->getsidebarsub();
		$data['view'] = $this->competence_model->getaction($this->competence_id);

		$data['footer'] = $this->load->view('amadis_sys/common/footer','', TRUE);
		$data['script'] = $this->load->view('amadis_sys/common/script','', TRUE);
		$this->load->view('amadis_sys/common/head');
		$this->load->view('amadis_sys/common/header');
		$this->load->view('amadis_sys/common/sidebar',$data);
		$this->load->view('amadis_sys/excel_create/create',$data);
	}

	//跳到指定頁面
	public function pagejump(){
		if($this->input->post('pagenum') == null || $this->input->post('pagenum') == 0){
			redirect('excel_create/list');
		}
		else{
			redirect('excel_create/list/'.$this->input->post('pagenum').'');
		}
	}

    // 新增表單
	public function create_form(){
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 7) {
				if($r['actions_insert'] == 0) {
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div><div class=' icon-warning-sign'></div> 您沒有新增的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('excel_create');
				}
			}
		}
		$data = [];
		$data['title'] = $this->unitName;
		$data['title_small'] = '新增產品';

		$data['mainbar'] = $this->sidebarmenu_model->getsidebarmain();
		$data['subbar'] = $this->sidebarmenu_model->getsidebarsub();
		$data['view'] = $this->competence_model->getaction($this->competence_id);
		$this->session->set_flashdata('sidebarselected','new_excel_create');

		$data['footer'] = $this->load->view('amadis_sys/common/footer','', TRUE);
		$data['script'] = $this->load->view('amadis_sys/common/script','', TRUE);
		$this->load->view('amadis_sys/common/head');
		$this->load->view('amadis_sys/common/header');
		$this->load->view('amadis_sys/common/sidebar',$data);
		$this->load->view('amadis_sys/excel_create/create',$data);
	}

	// 新增產品執行
	public function create(){
		$post = $this->input->post();

		//讀取來源檔
		$objPHPExcel = PHPExcel_IOFactory::load('C:/xampp/htdocs/bms_model/public/uploads/excel_model/source.xlsx');

		//設定excel樣板
		$srcfilename = 'C:\Users\user\Desktop\JSTEST.xlsx';
		//設定匯出路徑與名稱
		$destfilename = 'C:\Users\user\Desktop\pdf_save\ssss.pdf';

		try {

			if(!file_exists($srcfilename)){

				return;

			}

			$excel = new \COM("excel.application") or die("Unable to instantiate excel");
			$excel->DisplayAlerts = 0;
			$workbook = $excel->Workbooks->Open($srcfilename, null, false, null, "1", "1", true);
			foreach($post['origPage'] as $key => $row){
				$objPHPExcel->setActiveSheetIndex($row-1);
				$sheets = $workbook->Worksheets((int)$post['destPage'][$key]);
				// $sheets = $workbook->Worksheets($post['destPageName'][$key]);
				$sheets->activate;
				$sheets->Cells($post['destRow'][$key],$post['destCol'][$key])->value = $objPHPExcel->getActiveSheet()->getCell($post['origCol'][$key].$post['origRow'][$key])->getValue();
				// $sheets->Cells($post['destRow'][$key],$post['destCol'][$key])->value = $objPHPExcel->getActiveSheet()->getCell($post['origCol'][$key].$post['origRow'][$key])->getValue();
			}
			$sheets = $workbook->Worksheets(4);
			$sheets->activate;
			// $sheets->Pictures->Insert('http://35.229.221.127/farxun_bms/public/uploads/photo/persontest.jpg')->ShapeRange(true,75,100);
			// echo $sheets->Cells('C56')->value = 123;
			// exit();
			$sheets->Shapes->AddPicture('http://35.229.221.127/farxun_bms/public/uploads/photo/persontest.jpg',False,True,$sheets->Range('C56:I62')->left,$sheets->Range('C56:I62')->top,$sheets->Range('C56:I62')->Width,$sheets->Range('C56:I62')->Height);
			$workbook->ExportAsFixedFormat(0, $destfilename,0,0,0,1);
			$workbook->SaveAs('C:\Users\user\Desktop\pdf_save\ssss.csv',6);
			$workbook->SaveAs('C:\Users\user\Desktop\pdf_save\ssss.xml',46);
			$workbook->SaveAs('C:\Users\user\Desktop\pdf_save\ssss.xls',56);
			$workbook->Close();
			$excel->Quit();
			unset($excel);

		} catch (\Exception $e) {

			echo ("src:$srcfilename catch exception:" . $e->__toString());

			if (method_exists($excel, "Quit")){

				$excel->Quit();

			}

			return;

		}
	}

	// 刪除執行
	public function delete(){
		$id = $this->input->get('id');
		$user_id = $this->session->userdata('bms_users_id');
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 7) {
				if($r['actions_delete'] == 0) {
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div><div class=' icon-warning-sign'></div> 您沒有刪除的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('excel_create');
				}
			}
		}

		$datetime = date("Y-m-d H:i:s"); 
		$excel_create = array(
			'excel_create_is_del'       => 1,
			'excel_create_updated_date' => date("Y-m-d H:i:s",time()),
			'excel_create_updated_user' => $this->session->userdata('bms_users_id')
		);

		$this->excel_create_model->update_excel_create($excel_create,$id);

		$messagediv = "<div class='alert alert-success'>
		<button class='close' data-dismiss='alert'></button>
		<div class='icon-ok'></div> 成功！該筆產品已刪除。
		</div>";
		$this->session->set_flashdata('messagediv',$messagediv);
		redirect('excel_create');
	}

	// 下架執行
	public function excel_create_invisible(){
		$id = $this->input->get('id');
		$user_id = $this->session->userdata('bms_users_id');
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 7) {
				if($r['actions_enable'] == 0) {
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div><div class=' icon-warning-sign'></div> 您沒有下架的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('excel_create');
				}
			}
		}

		$datetime = date("Y-m-d H:i:s"); 
		$excel_create = array(
			'excel_create_status'       => 0,
			'excel_create_updated_date' => date("Y-m-d H:i:s",time()),
			'excel_create_updated_user' => $this->session->userdata('bms_users_id')
		);

		$this->excel_create_model->update_excel_create($excel_create,$id);

		$messagediv = "<div class='alert alert-success'>
		<button class='close' data-dismiss='alert'></button>
		<div class='icon-ok'></div> 成功！該筆產品已停用。
		</div>";
		$this->session->set_flashdata('messagediv',$messagediv);
		redirect('excel_create');
	}

	// 上架執行
	public function excel_create_visible(){
		$id = $this->input->get('id');
		$user_id = $this->session->userdata('bms_users_id');
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 7)
			{
				if($r['actions_enable'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div><div class=' icon-warning-sign'></div> 您沒有上架的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('excel_create');
				}
			}
		}

		$datetime = date("Y-m-d H:i:s"); 

		$excel_create = array(
			'excel_create_status'       => 1,
			'excel_create_updated_date' => date("Y-m-d H:i:s",time()),
			'excel_create_updated_user' => $this->session->userdata('bms_users_id')
		);

		$this->excel_create_model->update_excel_create($excel_create,$id);

		$messagediv = "<div class='alert alert-success'>
		<button class='close' data-dismiss='alert'></button>
		<div class='icon-ok'></div> 成功！該筆產品已啟用。
		</div>";
		$this->session->set_flashdata('messagediv',$messagediv);
		redirect('excel_create');
	}

	// 編輯表單
	public function update_form(){
		$id = $this->input->get('id');
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 7)
			{
				if($r['actions_update'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div><div class=' icon-warning-sign'></div> 您沒有編輯的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('excel_create');
				}
			}
		}
		$data = [];
		$data['title'] = $this->unitName;
		$data['title_small'] = '編輯產品';

		$data['result'] = $this->excel_create_model->getidData($id);
		$excel_create_rate_info = $this->bonus_rate_model->getonedata($id);

		$data['mainbar'] = $this->sidebarmenu_model->getsidebarmain();
		$data['subbar'] = $this->sidebarmenu_model->getsidebarsub();
		$data['view'] = $this->competence_model->getaction($this->competence_id);
		$data['upid'] = $this->input->get('id');

		$data['footer'] = $this->load->view('amadis_sys/common/footer','', TRUE);
		$data['script'] = $this->load->view('amadis_sys/common/script','', TRUE);
		$this->load->view('amadis_sys/common/head');
		$this->load->view('amadis_sys/common/header');
		$this->load->view('amadis_sys/common/sidebar',$data);
		$this->load->view('amadis_sys/excel_create/update',$data);
	}

    // 編輯執行
	public function update(){
		$id = $this->input->post('id');
		$excel_create_rate_info = $this->bonus_rate_model->getonedata($id);

		$excel_create = array(
			'excel_create_name'         => $this->input->post('excel_createname'),
			'excel_create_a_price'      => $this->input->post('texcel_createprice'),
			'excel_create_updated_date' => date("Y-m-d H:i:s",time()),
			'excel_create_updated_user' => $this->session->userdata('bms_users_id')
		);

		$this->excel_create_model->update_excel_create($excel_create,$id);

		$bonus_rate = array(
			'bonus_rate_interest_allowance' => $this->input->post('interest'),
			'bonus_rate_build_period'       => $this->input->post('build_period'),
			'bonus_rate_quarterly'          => $this->input->post('qinterest'),
			'bonus_rate_give'               => $this->input->post('times'),
			'bonus_rate_updated_date'       => date("Y-m-d H:i:s",time()),
			'bonus_rate_updated_user'       => $this->session->userdata('bms_users_id')
		);

		$this->bonus_rate_model->update_bonus_rate($bonus_rate,$excel_create_rate_info['bonus_rate_id']);

		$messagediv = "<div class='alert alert-success'>
		<button class='close' data-dismiss='alert'></button>
		<div class='icon-ok'></div> 成功！此筆產品已更新完成。
		</div>";
		$this->session->set_flashdata('messagediv',$messagediv);
		redirect('excel_create');
	}

	// 編輯表單
	public function check_form(){
		$id = $this->input->get('id');
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 7)
			{
				if($r['actions_view'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div><div class=' icon-warning-sign'></div> 您沒有編輯的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('excel_create');
				}
			}
		}
		$data = [];
		$data['title'] = $this->unitName;
		$data['title_small'] = '編輯產品';

		$data['result'] = $this->excel_create_model->getidData($id);

		$data['mainbar'] = $this->sidebarmenu_model->getsidebarmain();
		$data['subbar'] = $this->sidebarmenu_model->getsidebarsub();
		$data['view'] = $this->competence_model->getaction($this->competence_id);
		$data['upid'] = $this->input->get('id');

		$data['footer'] = $this->load->view('amadis_sys/common/footer','', TRUE);
		$data['script'] = $this->load->view('amadis_sys/common/script','', TRUE);
		$this->load->view('amadis_sys/common/head');
		$this->load->view('amadis_sys/common/header');
		$this->load->view('amadis_sys/common/sidebar',$data);
		$this->load->view('amadis_sys/excel_create/check',$data);
	}
}
