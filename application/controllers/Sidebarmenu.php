<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sidebarmenu extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('amadis_sys/sidebarmenu_model');
		$this->load->model('amadis_sys/competence_model');
		$this->unitName = '選單管理';
		$admin_id = $this->session->userdata('bms_users_id');
		$username = $this->session->userdata('bms_users_name');
		$this->competence_id = $this->session->userdata('bms_competence_id');
		$this->rightid = $this->competence_model->getaction($this->competence_id);
		
		$this->session->set_flashdata('sidebarselected','sidebarmenu');
		$this->session->set_flashdata('mainsidebar','system');
		if(!$admin_id || $this->competence_id >= 4){
			redirect('home/login');
		}
	}

    //選單列表
	public function index(){
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 26)
			{
				if($r['actions_view'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div> 您沒有瀏覽的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('home');
				}
			}
		}
		$keyword = $this->input->post('fullname');
		$data = [];
		$data['title'] = $this->unitName;
		$data['title_small'] = '選單列表';

		$data['result'] = $this->sidebarmenu_model->getdata($keyword);
		$data['mainbar'] = $this->sidebarmenu_model->getsidebarmain();
		$data['subbar'] = $this->sidebarmenu_model->getsidebarsub();
		$data['view'] = $this->competence_model->getaction($this->competence_id);

		$data['footer'] = $this->load->view('amadis_sys/common/footer','', TRUE);
		$data['script'] = $this->load->view('amadis_sys/common/script','', TRUE);
		$this->load->view('amadis_sys/common/head');
		$this->load->view('amadis_sys/common/header');
		$this->load->view('amadis_sys/common/sidebar',$data);
		$this->load->view('amadis_sys/system/sidebarmenu/list',$data);	
	}

	//副選單列表
	public function sublist(){
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 26)
			{
				if($r['actions_insert'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div> 您沒有瀏覽的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('sidebarmenu');
				}
			}
		}
		$keyword = $this->input->post('keyword');
		$id = $this->input->get('id');
		$data = [];
		$data['upid']=$this->input->get('id');
		$data['title'] = $this->unitName;
		$data['title_small'] = '副選單列表';

		$data['result'] = $this->sidebarmenu_model->getsubdata($id,$keyword);
		$data['mainbar'] = $this->sidebarmenu_model->getsidebarmain();
		$data['subbar'] = $this->sidebarmenu_model->getsidebarsub();
		$data['view'] = $this->competence_model->getaction($this->competence_id);

		$data['footer'] = $this->load->view('amadis_sys/common/footer','', TRUE);
		$data['script'] = $this->load->view('amadis_sys/common/script','', TRUE);
		$this->load->view('amadis_sys/common/head');
		$this->load->view('amadis_sys/common/header');
		$this->load->view('amadis_sys/common/sidebar',$data);
		$this->load->view('amadis_sys/system/sidebarmenu/sublist',$data);	
	}

    // 新增主選單表單
	public function create_form(){
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 26)
			{
				if($r['actions_insert'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div> 您沒有新增的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('sidebarmenu');
				}
			}
		}
		$datetime=date("Y-m-d");
		$data = [];
		$data['title'] = $this->unitName;
		$data['title_small'] = '新增選單';

		$data['mainbar'] = $this->sidebarmenu_model->getsidebarmain();
		$data['subbar'] = $this->sidebarmenu_model->getsidebarsub();
		$data['view'] = $this->competence_model->getaction($this->competence_id);

		$data['footer'] = $this->load->view('amadis_sys/common/footer','', TRUE);
		$data['script'] = $this->load->view('amadis_sys/common/script','', TRUE);
		$this->load->view('amadis_sys/common/head');
		$this->load->view('amadis_sys/common/header');
		$this->load->view('amadis_sys/common/sidebar',$data);
		$this->load->view('amadis_sys/system/sidebarmenu/create',$data);
	}

	// 新增副選單表單
	public function createsub_form(){
		$datetime=date("Y-m-d");
		$id=$this->input->get('id');
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 26)
			{
				if($r['actions_insert']==0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div> 您沒有新增的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('sidebarmenu/sublist?id='.$id);
				}
			}
		}
		$data = [];
		$data['upid']=$this->input->get('id');
		$data['title'] = $this->unitName;
		$data['unit_title'] = '副選單列表';
		$data['title_small'] = '新增副選單';
		
		$data['sidebarmain'] = $this->sidebarmenu_model->getonedata($id);
		// $data['competence'] = $this->competence_model->getcompetence();
		$data['mainbar'] = $this->sidebarmenu_model->getsidebarmain();
		$data['subbar'] = $this->sidebarmenu_model->getsidebarsub();
		$data['view'] = $this->competence_model->getaction($this->competence_id);

		$data['footer'] = $this->load->view('amadis_sys/common/footer','', TRUE);
		$data['script'] = $this->load->view('amadis_sys/common/script','', TRUE);
		$this->load->view('amadis_sys/common/head');
		$this->load->view('amadis_sys/common/header');
		$this->load->view('amadis_sys/common/sidebar',$data);
		$this->load->view('amadis_sys/system/sidebarmenu/createsub',$data);
	}

	// 新增主選單執行
	public function create(){
		$datetime = date ("Y-m-d H:i:s") ; 
		$sidebarmain = array(
			// 'competence_id'             => $this->input->post('competence'),
			'sidebar_main_name'         => $this->input->post('mainname'),
			'sidebar_main_icon'         => $this->input->post('icon'),
			'sidebar_main_code'         => $this->input->post('code'),
			'sidebar_main_front_link'   => $this->input->post('frontlink'),
			'sidebar_main_back_link'    => $this->input->post('backlink'),
			'sidebar_main_created_date' => $datetime,
			'sidebar_main_created_user' => $this->session->userdata('bms_users_id')
		);
		$this->sidebarmenu_model->add_sidebarmain($sidebarmain);
		$messagediv = "<div class='alert alert-success'>
		<button class='close' data-dismiss='alert'></button>
		<div class='icon-ok'></div> 成功！資料已新增。
		</div>";
		$this->session->set_flashdata('messagediv',$messagediv);

		redirect('sidebarmenu');
	}

	// 新增副選單執行
	public function createsub(){
		$datetime = date ("Y-m-d H:i:s") ;
		$id = $this->input->get('id');
		$sidebarsub = array(
			// 'competence_id'            => $this->input->post('competence'),
			'sidebar_main_id'          => $this->input->get('id'),
			'sidebar_sub_name'         => $this->input->post('subname'),
			'sidebar_sub_icon'         => $this->input->post('icon'),
			'sidebar_sub_code'         => $this->input->post('code'),
			'sidebar_sub_front_link'   => $this->input->post('frontlink'),
			'sidebar_sub_back_link'    => $this->input->post('backlink'),
			'sidebar_sub_created_date' => $datetime,
			'sidebar_sub_created_user' => $this->session->userdata('bms_users_id')
		);

		$maxid = $this->sidebarmenu_model->getlatestid();
		foreach($maxid as $id){
			$newid = $id['maxid'] + 1;
		}

		$competence = $this->competence_model->getdata();
		foreach($competence as $key => $row){
			$newright = array(
				'competence_id'        => $row['competence_id'],
				'sidebar_main_id'      => $this->input->get('id'),
				'sidebar_sub_id'       => $newid,
				'actions_view'         => 1,
				'actions_insert'       => 1,
				'actions_update'       => 1,
				'actions_enable'       => 1,
				'actions_delete'       => 1,
				'actions_export'       => 1,
				'actions_updated_date' => $datetime,
				'actions_updated_user' => $this->session->userdata('bms_users_id')
			);
			$this->competence_model->add_actions($newright);
		}

		$this->sidebarmenu_model->add_sidebarsub($sidebarsub);
		$messagediv = "<div class='alert alert-success'>
		<button class='close' data-dismiss='alert'></button>
		<div class='icon-ok'></div> 成功！資料已新增。
		</div>";
		$this->session->set_flashdata('messagediv',$messagediv);

		redirect('sidebarmenu/sublist?id='.$this->input->get('id'));
	}

	// 刪除主選單執行
	public function delete(){
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 26)
			{
				if($r['actions_delete'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div> 您沒有刪除的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('sidebarmenu');
				}
			}
		}
		$id = $this->input->get('id');
		$datetime = date("Y-m-d H:i:s"); 
		$user_id = $this->session->userdata('bms_users_id');
		$this->sidebarmenu_model->delete_sidebar_main($id,$datetime,$user_id);
		$messagediv = "<div class='alert alert-success'>
		<button class='close' data-dismiss='alert'></button>
		<div class='icon-ok'></div> 成功！該筆資料已刪除。
		</div>";
		$this->session->set_flashdata('messagediv',$messagediv);
		redirect('sidebarmenu');
	}

	// 刪除副選單執行
	public function deletesub(){
		$id = $this->input->get('id');
		$mainid = $this->input->get('mainid');
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 26)
			{
				if($r['actions_delete'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div> 您沒有刪除的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('sidebarmenu/sublist?id='.$mainid);
				}
			}
		}
		$datetime = date("Y-m-d H:i:s"); 
		$user_id = $this->session->userdata('bms_users_id');
		$this->sidebarmenu_model->delete_sidebar_sub($id,$datetime,$user_id);
		$messagediv = "<div class='alert alert-success'>
		<button class='close' data-dismiss='alert'></button>
		<div class='icon-ok'></div> 成功！該筆資料已刪除。
		</div>";
		$this->session->set_flashdata('messagediv',$messagediv);
		redirect('sidebarmenu/sublist?id='.$mainid);
	}

	// 下架主選單執行
	public function info_invisible(){
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 26)
			{
				if($r['actions_enable'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div> 您沒有上下架的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('sidebarmenu');
				}
			}
		}
		$id = $this->input->get('id');
		$datetime = date("Y-m-d H:i:s"); 
		$user_id = $this->session->userdata('bms_users_id');
		$this->sidebarmenu_model->invisible_sidebar_main($id,$datetime,$user_id);
		$messagediv = "<div class='alert alert-success'>
		<button class='close' data-dismiss='alert'></button>
		<div class='icon-ok'></div> 成功！該筆選單已停用。
		</div>";
		$this->session->set_flashdata('messagediv',$messagediv);
		redirect('sidebarmenu');
	}

	// 上架主選單執行
	public function info_visible(){
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 26)
			{
				if($r['actions_enable'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div> 您沒有上下架的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('sidebarmenu');
				}
			}
		}
		$id = $this->input->get('id');
		$datetime = date("Y-m-d H:i:s"); 
		$user_id = $this->session->userdata('bms_users_id');
		$this->sidebarmenu_model->visible_sidebar_main($id,$datetime,$user_id);
		$messagediv = "<div class='alert alert-success'>
		<button class='close' data-dismiss='alert'></button>
		<div class='icon-ok'></div> 成功！該筆選單已啟用。
		</div>";
		$this->session->set_flashdata('messagediv',$messagediv);
		redirect('sidebarmenu');
	}

	// 下架副選單執行
	public function sub_unstatus(){
		$id = $this->input->get('id');
		$mainid = $this->input->get('mainid');
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 26)
			{
				if($r['actions_enable'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div> 您沒有上下架的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('sidebarmenu/sublist?id='.$mainid);
				}
			}
		}
		$datetime = date("Y-m-d H:i:s"); 
		$user_id = $this->session->userdata('bms_users_id');
		$this->sidebarmenu_model->invisible_sidebar_sub($id,$datetime,$user_id);
		$messagediv = "<div class='alert alert-success'>
		<button class='close' data-dismiss='alert'></button>
		<div class='icon-ok'></div> 成功！該筆選單已停用。
		</div>";
		$this->session->set_flashdata('messagediv',$messagediv);
		redirect('sidebarmenu/sublist?id='.$mainid);
	}

	// 上架副選單執行
	public function sub_status(){
		$id = $this->input->get('id');
		$mainid = $this->input->get('mainid');
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 26)
			{
				if($r['actions_enable'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div> 您沒有上下架的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('sidebarmenu/sublist?id='.$mainid);
				}
			}
		}
		$datetime = date("Y-m-d H:i:s"); 
		$user_id = $this->session->userdata('bms_users_id');
		$this->sidebarmenu_model->visible_sidebar_sub($id,$datetime,$user_id);
		$messagediv = "<div class='alert alert-success'>
		<button class='close' data-dismiss='alert'></button>
		<div class='icon-ok'></div> 成功！該筆選單已啟用。
		</div>";
		$this->session->set_flashdata('messagediv',$messagediv);
		redirect('sidebarmenu/sublist?id='.$mainid);
	}

	// 修改主選單表單
	public function update_form(){
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 26)
			{
				if($r['actions_update'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div> 您沒有修改的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('sidebarmenu');
				}
			}
		}
		$data = [];
		$id = $this->input->get('id');
		$data['result'] = $this->sidebarmenu_model->getidData($id);
		$data['competence'] = $this->competence_model->getdata();
		$data['mainbar'] = $this->sidebarmenu_model->getsidebarmain();
		$data['subbar'] = $this->sidebarmenu_model->getsidebarsub();
		$data['view'] = $this->competence_model->getaction($this->competence_id);
		$data['upid'] = $this->input->get('id');
		$data['title'] = $this->unitName;
		$data['title_small'] = '編輯選單';
		$data['active'] = 'goods';

		$data['footer'] = $this->load->view('amadis_sys/common/footer','', TRUE);
		$data['script'] = $this->load->view('amadis_sys/common/script','', TRUE);
		$this->load->view('amadis_sys/common/head');
		$this->load->view('amadis_sys/common/header');
		$this->load->view('amadis_sys/common/sidebar',$data);
		$this->load->view('amadis_sys/system/sidebarmenu/update',$data);
	}

    // 修改主選單執行
	public function update(){
		$id = $this->input->post('id');
		$datetime = date ("Y-m-d H:i:s") ; 
		$sidebarmain = array(
			// 'competence_id'             => $this->input->post('competence'),
			'sidebar_main_name'         => $this->input->post('mainname'),
			'sidebar_main_icon'         => $this->input->post('icon'),
			'sidebar_main_code'         => $this->input->post('code'),
			'sidebar_main_front_link'   => $this->input->post('frontlink'),
			'sidebar_main_back_link'    => $this->input->post('backlink'),
			'sidebar_main_sort'         => $this->input->post('sort'),
			'sidebar_main_updated_date' => $datetime,
			'sidebar_main_updated_user' => $this->session->userdata('bms_users_id')
		);

		$this->sidebarmenu_model->update_sidebarmain($sidebarmain,$id);
		$messagediv = "<div class='alert alert-success'>
		<button class='close' data-dismiss='alert'></button>
		<div class='icon-ok'></div> 成功！此筆資料已更新。
		</div>";
		$this->session->set_flashdata('messagediv',$messagediv);
		redirect('sidebarmenu');
	}

	// 修改副選單表單
	public function updatesub_form(){
		$data = [];
		$id = $this->input->get('id');
		$mainid = $this->input->get('mainid');
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 26)
			{
				if($r['actions_update'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div> 您沒有修改的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('sidebarmenu/sublist?id='.$mainid);
				}
			}
		}
		$data['result'] = $this->sidebarmenu_model->getidData($mainid);
		$data['sub'] = $this->sidebarmenu_model->getsubidData($id);
		// $data['competence'] = $this->competence_model->getcompetence();
		$data['sidebarmain'] = $this->sidebarmenu_model->getsidebarmain();
		$data['mainbar'] = $this->sidebarmenu_model->getsidebarmain();
		$data['subbar'] = $this->sidebarmenu_model->getsidebarsub();
		$data['view'] = $this->competence_model->getaction($this->competence_id);
		$data['upid'] = $this->input->get('id');
		$data['title'] = $this->unitName;
		$data['unit_title'] = '副選單列表';
		$data['title_small'] = '編輯副選單';
		$data['active'] = 'goods';

		$data['footer'] = $this->load->view('amadis_sys/common/footer','', TRUE);
		$data['script'] = $this->load->view('amadis_sys/common/script','', TRUE);
		$this->load->view('amadis_sys/common/head');
		$this->load->view('amadis_sys/common/header');
		$this->load->view('amadis_sys/common/sidebar',$data);
		$this->load->view('amadis_sys/system/sidebarmenu/updatesub',$data);
	}

	// 修改副選單執行
	public function updatesub(){
		$id = $this->input->get('id');
		$mainid = $this->input->get('mainid');
		$datetime = date ("Y-m-d H:i:s");
		$sidebarsub = array(
			// 'competence_id'            => $this->input->post('competence'),
			'sidebar_main_id'          => $this->input->get('mainid'),
			'sidebar_sub_name'         => $this->input->post('subname'),
			'sidebar_sub_icon'         => $this->input->post('icon'),
			'sidebar_sub_code'         => $this->input->post('code'),
			'sidebar_sub_front_link'   => $this->input->post('frontlink'),
			'sidebar_sub_back_link'    => $this->input->post('backlink'),
			'sidebar_sub_sort'         => $this->input->post('sort'),
			'sidebar_sub_created_date' => $datetime,
			'sidebar_sub_created_user' => $this->session->userdata('bms_users_id')
		);

		$this->sidebarmenu_model->update_sidebarsub($sidebarsub,$id);
		$messagediv = "<div class='alert alert-success'>
		<button class='close' data-dismiss='alert'></button>
		<div class='icon-ok'></div> 成功！此筆資料已更新。
		</div>";
		$this->session->set_flashdata('messagediv',$messagediv);
		redirect('sidebarmenu/sublist?id='.$mainid);
	}
}
