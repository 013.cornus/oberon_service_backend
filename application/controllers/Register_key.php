<?php
ini_set('display_errors','1');
error_reporting(E_ALL);
defined('BASEPATH') OR exit('No direct script access allowed');

class Register_key extends CI_controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('amadis_sys/competence_model');
		$this->load->model('amadis_sys/sidebarmenu_model');
		$this->load->model('amadis_sys/ks/register_key_model');

		$this->unitName = '激活序號管理';	
		$this->competence_id = $this->session->userdata('bms_competence_id');
		$this->right_id = $this->competence_model->getaction($this->competence_id);

		$admin_id = $this->session->userdata('bms_users_id');
		$this->session->set_flashdata('sidebarselected','register_key');
		$this->session->set_flashdata('mainsidebar','group');
		if (!$admin_id) {
			redirect('home/login');
		}
	}

	public function index(){
		redirect('register_key/list');
		exit();
	}

	public function list($page=''){
		$keyword['group_type'] = $this->session->userdata('bms_group_type');
		
		$data = [];
		$data['title'] = $this->unitName;
		$data['title_small'] = '激活序號列表';
		$data['register_key_list'] = $this->register_key_model->getList($keyword);

		$data['mainbar'] = $this->sidebarmenu_model->getsidebarmain();
		$data['subbar'] = $this->sidebarmenu_model->getsidebarsub();
		$data['view'] = $this->competence_model->getaction($this->competence_id);

		$data['footer'] = $this->load->view('amadis_sys/common/footer','', TRUE);
		$data['script'] = $this->load->view('amadis_sys/common/script','', TRUE);

		$this->load->view('amadis_sys/common/head');
		$this->load->view('amadis_sys/common/header');
		$this->load->view('amadis_sys/common/sidebar', $data);
		$this->load->view('amadis_sys/register_key/list', $data);
	}
}