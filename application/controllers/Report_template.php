<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report_template extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('amadis_sys/report_template_model');
		$this->load->model('amadis_sys/system/group_template_setting_model');
		$this->load->model('amadis_sys/competence_model');
		$this->load->model('amadis_sys/sidebarmenu_model');
		$this->load->model('amadis_sys/pagintion_model');
		$this->unitName = '指定報告模板';
		$this->competence_id = $this->session->userdata('bms_competence_id');
		$this->rightid = $this->competence_model->getaction($this->competence_id);
		$this->type = $this->session->userdata('bms_group_type');
		

		$admin_id = $this->session->userdata('bms_users_id');
		$username = $this->session->userdata('bms_users_name');
		$this->session->set_flashdata('sidebarselected','report_template');
		$this->session->set_flashdata('mainsidebar','report_manage');

		if(!$admin_id){
			redirect('home/login');
		}
	}

    //Excel模板管理列表
	public function index(){
		redirect('report_template/update_form');
		exit;
	}

	// 編輯表單
	public function update_form(){
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 7)
			{
				if($r['actions_update'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div><div class=' icon-warning-sign'></div> 您沒有編輯的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('report_template');
				}
			}
		}
		$data = [];
		$data['title'] = $this->unitName;
		$data['title_small'] = '指定報告模板';

		$data['result'] = $this->report_template_model->getKeyData('group_type',$this->type);
		$template = $this->group_template_setting_model->getDataByType($this->type);
		$data['template_list'] = [];
		if(isset($template['template_list'])){
			$data['template_list'] = json_decode($template['template_list']);
			foreach($data['template_list'] as $key => $row){
				$template_info = json_decode(file_get_contents('http://34.80.38.39/rgms/api/template?id='.$row),true)['Data'];
				if(!$template_info){
					unset($data['template_list'][$key]);
					continue;
				}
				else{
					$data['template_name'][$key] = $template_info[0]['excel_template_name'];
					$data['selected'][$key] = '';
					if($row == $data['result']['template_id']){
						$data['selected'][$key] = 'selected';
					}
				}
			}
		}
		
		$data['mainbar'] = $this->sidebarmenu_model->getsidebarmain();
		$data['subbar'] = $this->sidebarmenu_model->getsidebarsub();
		$data['view'] = $this->competence_model->getaction($this->competence_id);
		$data['upid'] = $this->input->get('id');
		
		$data['footer'] = $this->load->view('amadis_sys/common/footer','', TRUE);
		$data['script'] = $this->load->view('amadis_sys/common/script','', TRUE);
		$this->load->view('amadis_sys/common/head');
		$this->load->view('amadis_sys/common/header');
		$this->load->view('amadis_sys/common/sidebar',$data);
		$this->load->view('amadis_sys/report_template/update',$data);
	}

    // 編輯執行
	public function update(){
		$post = $this->input->post();

		$template_id = $post['template_selected'];
		$report_template_check = $this->report_template_model->getKeyData('group_type',$this->type);
		if(isset($report_template_check)){
			$report_template = array(
				'group_type'                   => $this->type,
				'template_id'                  => $template_id,
				'report_template_updated_date' => date("Y-m-d H:i:s",time()),
				'report_template_updated_user' => $this->session->userdata('bms_users_id')
			);

			$this->report_template_model->update_report_template($report_template,$report_template_check['report_template_id']);
		}
		else{
			$report_template = array(
				'group_type'                   => $this->type,
				'template_id'                  => $template_id,
				'report_template_created_date' => date("Y-m-d H:i:s",time()),
				'report_template_created_user' => $this->session->userdata('bms_users_id')
			);

			$this->report_template_model->add_report_template($report_template);
		}

		$messagediv = "<div class='alert alert-success'>
		<button class='close' data-dismiss='alert'></button>
		<div class='icon-ok'></div> 成功！指定報告模板已更新完成。
		</div>";
		$this->session->set_flashdata('messagediv',$messagediv);
		redirect('report/report_view');
	}

	// 編輯表單
	public function check_form(){
		$id = $this->input->get('id');
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 7)
			{
				if($r['actions_view'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div><div class=' icon-warning-sign'></div> 您沒有編輯的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('report_template');
				}
			}
		}
		$data = [];
		$data['title'] = $this->unitName;
		$data['title_small'] = '編輯Excel模板管理';
		
		$data['result'] = $this->report_template_model->getidData($id);
		
		$data['mainbar'] = $this->sidebarmenu_model->getsidebarmain();
		$data['subbar'] = $this->sidebarmenu_model->getsidebarsub();
		$data['view'] = $this->competence_model->getaction($this->competence_id);
		$data['upid'] = $this->input->get('id');
		
		$data['footer'] = $this->load->view('amadis_sys/common/footer','', TRUE);
		$data['script'] = $this->load->view('amadis_sys/common/script','', TRUE);
		$this->load->view('amadis_sys/common/head');
		$this->load->view('amadis_sys/common/header');
		$this->load->view('amadis_sys/common/sidebar',$data);
		$this->load->view('amadis_sys/report_template/check',$data);
	}
}
