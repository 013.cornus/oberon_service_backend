<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customers extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('amadis_sys/customers_model');
		$this->load->model('amadis_sys/competence_model');
		$this->load->model('amadis_sys/sidebarmenu_model');
		$this->load->model('amadis_sys/employee_model');
		$this->load->model('amadis_sys/bank_model');
		$this->load->model('amadis_sys/order_model');
		$this->load->model('amadis_sys/order_info_model');
		$this->load->model('amadis_sys/goods_model');
		$this->load->model('amadis_sys/address_model');
		$this->load->model('amadis_sys/branch_model');
		$this->load->model('amadis_sys/account_model');
		$this->load->model('amadis_sys/users_model');
		$this->load->model('amadis_sys/organization_model');
		$this->load->model('amadis_sys/post_model');
		$this->load->model('amadis_sys/pagintion_model');
		$this->unitName = '客戶管理';
		$this->competence_id = $this->session->userdata('competence_id');
		$this->rightid = $this->competence_model->getaction($this->competence_id);
		
		$admin_id = $this->session->userdata('users_id');
		$username = $this->session->userdata('users_name');

		$this->session->set_flashdata('sidebarselected','customers');
		$this->session->set_flashdata('mainsidebar','customer');

		if(!$admin_id){
			redirect('home/login');
		}
	}

    //導入設定
	public function index(){
		$this->session->unset_userdata('orgid');
		$this->session->unset_userdata('customername');
		redirect('customers/list');
		exit;
	}

	//顧客列表
	public function list($page=''){
		$srcfilename = 'C:\Users\user\Desktop\JS_001.xlsx';

		$destfilename = 'C:\Users\user\Desktop\test.pdf';

		try {

			if(!file_exists($srcfilename)){

				return;

			}

			$excel = new \COM("excel.application") or die("Unable to instantiate excel");

			$workbook = $excel->Workbooks->Open($srcfilename, null, false, null, "1", "1", true);
			$sheets = $workbook->Worksheets(1);
			$sheets->activate; 
			$sheets->Cells(1, 'A')->value = 1000;
			// $excel->ActiveWorkbook->SaveAs('C:\Users\user\Desktop\excel_save\123.xlsx');  

			$workbook->ExportAsFixedFormat(0, $destfilename);
			$workbook->Close();
			$excel->Quit();

		} catch (\Exception $e) {

			echo ("src:$srcfilename catch exception:" . $e->__toString());

			if (method_exists($excel, "Quit")){

				$excel->Quit();

			}

			return;

		}


		$certainemid = $this->session->userdata('employee_id');
		$organization_id = $this->session->userdata('organization_id');
		$manager_id = $this->session->userdata('manager_id');
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 3)
			{
				if($r['actions_view'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div> 您沒有瀏覽的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('home');
				}
			}
		}

		$data = [];
		$data['title'] = $this->unitName;
		$data['title_small'] = '客戶列表';
		$data['hide'] = '';

		if($this->input->post('orgid') != ''){
			$this->session->set_userdata('orgid',$this->input->post('orgid'));
		}

		if($this->input->post('fullname') != ''){
			$this->session->set_userdata('customername',$this->input->post('fullname'));
		}

		switch ($this->competence_id) {
			case '1':
			if($this->session->userdata('orgid')){
				$keyword['organization_id'] = $this->session->userdata('orgid');
			}
			else{
				$keyword['organization_id'] = $this->input->post('orgid');
			}
			break;
			case '2':
			if($this->session->userdata('orgid')){
				$keyword['organization_id'] = $this->session->userdata('orgid');
			}
			else{
				$keyword['organization_id'] = $this->input->post('orgid');
			}
			break;
			case '3':
			$keyword['organization_id'] = $this->session->userdata('manager_id');
			$data['hide'] = "style='display:none'";
			break;	
			default:
			$keyword['employee_id'] = trim($certainemid);
			$data['hide'] = "style='display:none'";
			break;
		}

		if($this->session->userdata('customername')){
			$keyword['customer_name'] = $this->session->userdata('customername');
		}
		else{
			$keyword['customer_name'] = $this->input->post('fullname');
		}

		$result = $this->customers_model->getList($keyword,[]);
		$pager = $this->pagintion_model->setPager($result,$page);
		$data['page_list'] = $pager['page'];
		$data['result'] = $this->customers_model->getList($keyword,$pager['list']);

		foreach($data['result'] as $key => $value){
			if($value['customer_status'] == 1){
				$str = '啟用';
				$color = 'success'; 
			}
			else{
				$str = '停用';
				$color = 'important';
			}
			$data['str'][$key] = $str;
			$data['color'][$key] = $color;
		}
		$selection['status'] = 1;
		$data['organization'] = $this->organization_model->getList($selection);
		$data['mainbar'] = $this->sidebarmenu_model->getsidebarmain();
		$data['subbar'] = $this->sidebarmenu_model->getsidebarsub();
		$data['view'] = $this->competence_model->getaction($this->competence_id);

		$data['footer'] = $this->load->view('amadis_sys/common/footer','', TRUE);
		$data['script'] = $this->load->view('amadis_sys/common/script','', TRUE);
		$this->load->view('amadis_sys/common/head');
		$this->load->view('amadis_sys/common/header');
		$this->load->view('amadis_sys/common/sidebar',$data);
		$this->load->view('amadis_sys/customers/list',$data);
	}

	//跳到指定頁面
	public function pagejump(){
		if($this->input->post('pagenum') == null || $this->input->post('pagenum') == 0){
			redirect('customers/list');
		}
		else{
			redirect('customers/list/'.$this->input->post('pagenum').'');
		}
	}

    // 新增客戶表單
	public function create_form(){
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 3)
			{
				if($r['actions_insert'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div> 您沒有新增的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('customers/list');
				}
			}
		}
		$data = [];
		$data['title'] = $this->unitName;
		$data['title_small'] = '新增客戶';
		$data['active'] = 'goods';
		$this->session->set_flashdata('sidebarselected','new_customers');

		$data['result'] = $this->address_model->getcity();
		if($this->competence_id == 3){
			$keyword['organization_id'] = $this->session->userdata('manager_id');
			$data['organization'] = $this->organization_model->getList($keyword);
			$data['employee'] = $this->employee_model->getList($keyword);
		}
		else{
			$selection['status'] = 1;
			$data['organization'] = $this->organization_model->getList($selection);
			$data['employee'] = $this->employee_model->getList($selection);
		}

		//選單
		$data['mainbar'] = $this->sidebarmenu_model->getsidebarmain();
		$data['subbar'] = $this->sidebarmenu_model->getsidebarsub();
		$data['view'] = $this->competence_model->getaction($this->competence_id);

		$data['footer'] = $this->load->view('amadis_sys/common/footer','', TRUE);
		$data['script'] = $this->load->view('amadis_sys/common/script','', TRUE);
		$this->load->view('amadis_sys/common/head');
		$this->load->view('amadis_sys/common/header');
		$this->load->view('amadis_sys/common/sidebar',$data);
		$this->load->view('amadis_sys/customers/create',$data);
	}


	// 新增客戶執行
	public function create(){
		$datetime = date("Y-m-d H:i:s",time()) ;
		$cell = $this->input->post('cellphone');
		echo $cell;
		exit();

		$customers = array(
			'employee_id'                  => $this->input->post('employeename'),
			'customer_name'                => $this->input->post('name'),
			'customer_sex'                 => $this->input->post('optionsRadios1'),
			'customer_id_card'             => $this->input->post('idcard'),
			'address_id_2'                 => $this->input->post('residentialaddress'),
			'customer_residential_address' => $this->input->post('raddressdetail'),
			'customer_phone'               => $cell,
			'customer_created_date'        => $datetime,
			'customer_created_user'        => $this->session->userdata('users_id')
		);

		$this->customers_model->add_customers($customers);

		$id = '';
		$customerinfo = $this->customers_model->getlatestid(); 
		foreach($customerinfo as $row){
			$id = $row['maxid'];
		}

		if($cell != '' && $cell != null){
			$account = array(
				'competence_id'      => 5,
				'employee_id'        => 0,
				'customer_id'        => $id,
				'users_name'         => $this->input->post('name'),
				'users_account'      => $cellarea.$cell,
				'users_password'     => hash('sha256','000000'),
				'users_created_date' => date("Y-m-d H:i:s",time()),
				'users_created_user' => $this->session->userdata('users_id')
			);
			$this->account_model->add_account($account);
		}

		$messagediv = "<div class='alert alert-success'>
		<button class='close' data-dismiss='alert'></button>
		<div class='icon-ok'></div> 成功！資料已新增。
		</div>";
		$this->session->set_flashdata('messagediv',$messagediv);

		redirect('customers/list');
	}

	// 刪除客戶執行
	public function delete(){
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 3)
			{
				if($r['actions_delete'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div> 您沒有刪除的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('customers/list');
				}
			}
		}
		$id = $this->input->get('id');
		$datetime = date("Y-m-d H:i:s"); 
		$user_id = $this->session->userdata('users_id');
		$customersupdate = array(
			'customer_is_del'       => 1,
			'customer_updated_date' => $datetime,
			'customer_updated_user' => $user_id
		);

		$this->customers_model->update_customers($customersupdate,$id);

		$messagediv = "<div class='alert alert-success'>
		<button class='close' data-dismiss='alert'></button>
		<div class='icon-ok'></div> 成功！該筆資料已刪除。
		</div>";
		$this->session->set_flashdata('messagediv',$messagediv);
		redirect('customers/list');
	}

	// 下架客戶執行
	public function info_invisible(){
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 3)
			{
				if($r['actions_enable'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div> 您沒有修改的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('customers/list');
				}
			}
		}
		$id = $this->input->get('id');
		$datetime = date("Y-m-d H:i:s"); 
		$user_id = $this->session->userdata('users_id');
		$customersupdate = array(
			'customer_status'       => 0,
			'customer_updated_date' => $datetime,
			'customer_updated_user' => $user_id
		);

		$this->customers_model->update_customers($customersupdate,$id);

		$messagediv = "<div class='alert alert-success'>
		<button class='close' data-dismiss='alert'></button>
		<div class='icon-ok'></div> 成功！該筆資料已下架。
		</div>";
		$this->session->set_flashdata('messagediv',$messagediv);
		redirect('customers/list');
	}

	// 上架客戶執行
	public function info_visible(){
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 3)
			{
				if($r['actions_enable'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div> 您沒有修改的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('customers/list');
				}
			}
		}
		$id = $this->input->get('id');
		$datetime = date("Y-m-d H:i:s"); 
		$user_id = $this->session->userdata('users_id');
		$customersupdate = array(
			'customer_status'       => 1,
			'customer_updated_date' => $datetime,
			'customer_updated_user' => $user_id
		);

		$this->customers_model->update_customers($customersupdate,$id);
		
		$messagediv = "<div class='alert alert-success'>
		<button class='close' data-dismiss='alert'></button>
		<div class='icon-ok'></div> 成功！該筆資料已上架。
		</div>";
		$this->session->set_flashdata('messagediv',$messagediv);
		redirect('customers/list');
	}

	// 檢視客戶表單
	public function check_form(){
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 3)
			{
				if($r['actions_view'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div> 您沒有修改的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('customers/list');
				}
			}
		}
		$data = [];
		$id = $this->input->get('id');
		$data['result'] = $this->customers_model->getiddata($id);

		$orginfo = $this->organization_model->getData($data['result']['organization_id']);
		if($orginfo['organization_name'] != null){
			$data['orgname'] = $orginfo['organization_name'];
		}
		else{
			$data['orgname'] = '';
		}

		$eminfo = $this->employee_model->getidData($data['result']['employee_id']);
		if($eminfo['employee_name'] != null){
			$data['employeename'] = $eminfo['employee_name'];
		}
		else{
			$data['employeename'] = '';
		}

		if($data['result']['customer_bank'] != 700){
			$bankinfo = $this->bank_model->getidData($data['result']['customer_bank']);
			$branchinfo = $this->branch_model->getidData($data['result']['customer_bank_branch']);
			$data['bankname'] = $bankinfo['bank_name'];
			$data['branchname'] = $branchinfo['bank_branch_name'];
		}
		else{
			$data['bankname'] = '中華郵政';
			$branchinfo = $this->post_model->getidData($data['result']['customer_bank_branch']);
			$data['branchname'] = $branchinfo['post_name'];
		}

		if($data['result']['customer_bank'] == 0){
			$data['bankname'] = '';
			$data['branchname'] = '';
		}

		$data['address'] = $this->address_model->getcity();

		//選單
		$data['mainbar'] = $this->sidebarmenu_model->getsidebarmain();
		$data['subbar'] = $this->sidebarmenu_model->getsidebarsub();
		$data['view'] = $this->competence_model->getaction($this->competence_id);

		$data['upid'] = $this->input->get('id');
		$data['title'] = $this->unitName;
		$data['title_small'] = '檢視客戶';
		$data['active'] = 'goods';

		$data['footer'] = $this->load->view('amadis_sys/common/footer','', TRUE);
		$data['script'] = $this->load->view('amadis_sys/common/script','', TRUE);
		$this->load->view('amadis_sys/common/head');
		$this->load->view('amadis_sys/common/header');
		$this->load->view('amadis_sys/common/sidebar',$data);
		$this->load->view('amadis_sys/customers/check',$data);
	}
	
	// 修改會員表單
	public function update_form(){
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 3)
			{
				if($r['actions_update'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div> 您沒有修改的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('customers/list');
				}
			}
		}
		$data = [];
		$id = $this->input->get('id');
		$data['result'] = $this->customers_model->getiddata($id);
		$data['address'] = $this->address_model->getcity();

		//選單
		$data['mainbar'] = $this->sidebarmenu_model->getsidebarmain();
		$data['subbar'] = $this->sidebarmenu_model->getsidebarsub();
		$data['view'] = $this->competence_model->getaction($this->competence_id);
		if($this->competence_id == 3){
			$selection['organization_id'] = $this->session->userdata('manager_id');
			$selection['status'] = 1;
			$data['organization'] = $this->organization_model->getList($selection);
			$data['employee'] = $this->employee_model->getList($selection);
		}
		else{
			$selection['status'] = 1;
			$data['organization'] = $this->organization_model->getList($selection);
			$data['employee'] = $this->employee_model->getList($selection);
		}

		$data['upid'] = $this->input->get('id');
		$data['title'] = $this->unitName;
		$data['title_small'] = '編輯客戶';
		$data['active'] = 'goods';

		$data['footer'] = $this->load->view('amadis_sys/common/footer','', TRUE);
		$data['script'] = $this->load->view('amadis_sys/common/script','', TRUE);
		$this->load->view('amadis_sys/common/head');
		$this->load->view('amadis_sys/common/header');
		$this->load->view('amadis_sys/common/sidebar',$data);
		$this->load->view('amadis_sys/customers/update',$data);
	}

    // 修改會員執行
	public function update(){
		$id       = $this->input->post('id');
		$datetime = date ("Y-m-d H:i:s",time()) ; 
		$cell     = $this->input->post('cellphone');
		$customersupdate = array(
			'employee_id'                  => $this->input->post('employeename'),
			'customer_name'                => $this->input->post('name'),
			'customer_sex'                 => $this->input->post('optionsRadios1'),
			'customer_id_card'             => $this->input->post('idcard'),
			'address_id_2'                 => $this->input->post('residentialaddress'),
			'customer_residential_address' => $this->input->post('raddressdetail'),
			'customer_phone'               => $cell,
			'customer_updated_date'        => $datetime,
			'customer_updated_user'        => $this->session->userdata('users_id')
		);

		$this->customers_model->update_customers($customersupdate,$id);

		$accountinfo = $this->users_model->get_customers_account($id);
		
		if($accountinfo['customer_id'] == null){
			if($cell != '' && $cell != null){
				$account = array(
					'competence_id'      => 5,
					'employee_id'        => 0,
					'customer_id'        => $this->input->post('id'),
					'users_name'         => $this->input->post('name'),
					'users_account'      => $cell,
					'users_password'     => hash('sha256','000000'),
					'users_created_date' => date("Y-m-d H:i:s",time()),
					'users_created_user' => $this->session->userdata('users_id')
				);
				$this->account_model->add_account($account);
			}
		}

		$messagediv = "<div class='alert alert-success'>
		<button class='close' data-dismiss='alert'></button>
		<div class='icon-ok'></div> 成功！此筆資料已更新。
		</div>";
		$this->session->set_flashdata('messagediv',$messagediv);
		redirect('customers/list');
	}
}
