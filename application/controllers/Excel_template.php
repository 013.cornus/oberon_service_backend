<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Excel_template extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('amadis_sys/excel_template_model');
		$this->load->model('amadis_sys/competence_model');
		$this->load->model('amadis_sys/sidebarmenu_model');
		$this->load->model('amadis_sys/bonus_rate_model');
		$this->load->model('amadis_sys/pagintion_model');
		$this->unitName = 'PDF產出';
		$this->competence_id = $this->session->userdata('bms_competence_id');
		$this->rightid = $this->competence_model->getaction($this->competence_id);
		

		$admin_id = $this->session->userdata('bms_users_id');
		$username = $this->session->userdata('bms_users_name');
		$this->session->set_flashdata('sidebarselected','excel_template');
		$this->session->set_flashdata('mainsidebar','excel_create');

		if(!$admin_id){
			redirect('home/login');
		}
	}

    //Excel模板管理列表
	public function index(){
		redirect('excel_template/list');
		exit;
	}
    //Excel模板管理列表
	public function list($page=''){
		$organization_id = $this->session->userdata('bms_organization_id');
		$manager_id = $this->session->userdata('bms_manager_id');
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 7)
			{
				if($r['actions_view'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div> 您沒有瀏覽的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('home');
				}
			}
		}
		$data = [];
		$data['title'] = $this->unitName;
		$data['title_small'] = 'Excel模板管理列表';
		$data['active'] = 'employee';

		$keyword['fullname'] = $this->input->post('fullname');
		
		$result = $this->excel_template_model->getList($keyword);		
		$pager = $this->pagintion_model->setPager($result,$page);
		$data['page_list'] = $pager['page'];
		$data['result'] = $this->excel_template_model->getList($keyword,$pager['list']);
		
		//選單
		$data['mainbar'] = $this->sidebarmenu_model->getsidebarmain();
		$data['subbar'] = $this->sidebarmenu_model->getsidebarsub();
		$data['view'] = $this->competence_model->getaction($this->competence_id);
		
		$data['footer'] = $this->load->view('amadis_sys/common/footer','', TRUE);
		$data['script'] = $this->load->view('amadis_sys/common/script','', TRUE);
		$this->load->view('amadis_sys/common/head');
		$this->load->view('amadis_sys/common/header');
		$this->load->view('amadis_sys/common/sidebar',$data);
		$this->load->view('amadis_sys/excel_template/list',$data);	
	}

	//跳到指定頁面
	public function pagejump(){
		if($this->input->post('pagenum') == null || $this->input->post('pagenum') == 0){
			redirect('excel_template/list');
		}
		else{
			redirect('excel_template/list/'.$this->input->post('pagenum').'');
		}
	}

    // 新增表單
	public function create_form(){
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 7) {
				if($r['actions_insert'] == 0) {
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div><div class=' icon-warning-sign'></div> 您沒有新增的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('excel_template');
				}
			}
		}
		$data = [];
		$data['title'] = $this->unitName;
		$data['title_small'] = '新增Excel模板管理';

		$data['mainbar'] = $this->sidebarmenu_model->getsidebarmain();
		$data['subbar'] = $this->sidebarmenu_model->getsidebarsub();
		$data['view'] = $this->competence_model->getaction($this->competence_id);
		$this->session->set_flashdata('sidebarselected','excel_template');
		
		$data['footer'] = $this->load->view('amadis_sys/common/footer','', TRUE);
		$data['script'] = $this->load->view('amadis_sys/common/script','', TRUE);
		$this->load->view('amadis_sys/common/head');
		$this->load->view('amadis_sys/common/header');
		$this->load->view('amadis_sys/common/sidebar',$data);
		$this->load->view('amadis_sys/excel_template/create',$data);
	}

	// 新增Excel模板管理執行
	public function create(){
		$datetime = date("Y-m-d H:i:s",time());
		$config['upload_path'] = 'public/uploads/excel_model/';
		$config['allowed_types'] = 'xlsx|xls';

		$this->load->library('upload', $config);
		$this->upload->initialize($config);

		if (!$this->upload->do_upload('excel_model'))
		{
			$error = $this->upload->display_errors();
			print_r($error);
		}
		else
		{
			$data = array('upload_data' => $this->upload->data());
		}

		$excel_template = array(
			'excel_template_name'         => $data['upload_data']['file_name'],
			'excel_template_sub_name'     => $data['upload_data']['file_ext'],
			'excel_template_created_date' => $datetime,
			'excel_template_created_user' => $this->session->userdata('bms_users_id')
		);
		$this->excel_template_model->add_excel_template($excel_template);

		$messagediv = "<div class='alert alert-success'>
		<button class='close' data-dismiss='alert'></button>
		<div class='icon-ok'></div> 成功！Excel模板管理已新增完成。
		</div>";
		$this->session->set_flashdata('messagediv',$messagediv);
		redirect('excel_template');
	}

	// 刪除執行
	public function delete(){
		$id = $this->input->get('id');
		$user_id = $this->session->userdata('bms_users_id');
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 7) {
				if($r['actions_delete'] == 0) {
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div><div class=' icon-warning-sign'></div> 您沒有刪除的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('excel_template');
				}
			}
		}
		
		$datetime = date("Y-m-d H:i:s"); 
		$excel_template = array(
			'excel_template_is_del'       => 1,
			'excel_template_updated_date' => date("Y-m-d H:i:s",time()),
			'excel_template_updated_user' => $this->session->userdata('bms_users_id')
		);

		$this->excel_template_model->update_excel_template($excel_template,$id);

		$messagediv = "<div class='alert alert-success'>
		<button class='close' data-dismiss='alert'></button>
		<div class='icon-ok'></div> 成功！該筆Excel模板管理已刪除。
		</div>";
		$this->session->set_flashdata('messagediv',$messagediv);
		redirect('excel_template');
	}

	// 下架執行
	public function excel_template_invisible(){
		$id = $this->input->get('id');
		$user_id = $this->session->userdata('bms_users_id');
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 7) {
				if($r['actions_enable'] == 0) {
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div><div class=' icon-warning-sign'></div> 您沒有下架的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('excel_template');
				}
			}
		}
		
		$datetime = date("Y-m-d H:i:s"); 
		$excel_template = array(
			'excel_template_status'       => 0,
			'excel_template_updated_date' => date("Y-m-d H:i:s",time()),
			'excel_template_updated_user' => $this->session->userdata('bms_users_id')
		);

		$this->excel_template_model->update_excel_template($excel_template,$id);

		$messagediv = "<div class='alert alert-success'>
		<button class='close' data-dismiss='alert'></button>
		<div class='icon-ok'></div> 成功！該筆Excel模板管理已停用。
		</div>";
		$this->session->set_flashdata('messagediv',$messagediv);
		redirect('excel_template');
	}

	// 上架執行
	public function excel_template_visible(){
		$id = $this->input->get('id');
		$user_id = $this->session->userdata('bms_users_id');
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 7)
			{
				if($r['actions_enable'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div><div class=' icon-warning-sign'></div> 您沒有上架的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('excel_template');
				}
			}
		}
		
		$datetime = date("Y-m-d H:i:s"); 

		$excel_template = array(
			'excel_template_status'       => 1,
			'excel_template_updated_date' => date("Y-m-d H:i:s",time()),
			'excel_template_updated_user' => $this->session->userdata('bms_users_id')
		);

		$this->excel_template_model->update_excel_template($excel_template,$id);

		$messagediv = "<div class='alert alert-success'>
		<button class='close' data-dismiss='alert'></button>
		<div class='icon-ok'></div> 成功！該筆Excel模板管理已啟用。
		</div>";
		$this->session->set_flashdata('messagediv',$messagediv);
		redirect('excel_template');
	}

	// 編輯表單
	public function update_form(){
		$id = $this->input->get('id');
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 7)
			{
				if($r['actions_update'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div><div class=' icon-warning-sign'></div> 您沒有編輯的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('excel_template');
				}
			}
		}
		$data = [];
		$data['title'] = $this->unitName;
		$data['title_small'] = '編輯Excel模板管理';
		
		$data['result'] = $this->excel_template_model->getidData($id);
		$excel_template_rate_info = $this->bonus_rate_model->getonedata($id);
		
		$data['mainbar'] = $this->sidebarmenu_model->getsidebarmain();
		$data['subbar'] = $this->sidebarmenu_model->getsidebarsub();
		$data['view'] = $this->competence_model->getaction($this->competence_id);
		$data['upid'] = $this->input->get('id');
		
		$data['footer'] = $this->load->view('amadis_sys/common/footer','', TRUE);
		$data['script'] = $this->load->view('amadis_sys/common/script','', TRUE);
		$this->load->view('amadis_sys/common/head');
		$this->load->view('amadis_sys/common/header');
		$this->load->view('amadis_sys/common/sidebar',$data);
		$this->load->view('amadis_sys/excel_template/update',$data);
	}

    // 編輯執行
	public function update(){
		$id = $this->input->post('id');
		$excel_template_rate_info = $this->bonus_rate_model->getonedata($id);

		$excel_template = array(
			'excel_template_name'         => $this->input->post('excel_templatename'),
			'excel_template_a_price'      => $this->input->post('texcel_templateprice'),
			'excel_template_updated_date' => date("Y-m-d H:i:s",time()),
			'excel_template_updated_user' => $this->session->userdata('bms_users_id')
		);

		$this->excel_template_model->update_excel_template($excel_template,$id);

		$bonus_rate = array(
			'bonus_rate_interest_allowance' => $this->input->post('interest'),
			'bonus_rate_build_period'       => $this->input->post('build_period'),
			'bonus_rate_quarterly'          => $this->input->post('qinterest'),
			'bonus_rate_give'               => $this->input->post('times'),
			'bonus_rate_updated_date'       => date("Y-m-d H:i:s",time()),
			'bonus_rate_updated_user'       => $this->session->userdata('bms_users_id')
		);

		$this->bonus_rate_model->update_bonus_rate($bonus_rate,$excel_template_rate_info['bonus_rate_id']);

		$messagediv = "<div class='alert alert-success'>
		<button class='close' data-dismiss='alert'></button>
		<div class='icon-ok'></div> 成功！此筆Excel模板管理已更新完成。
		</div>";
		$this->session->set_flashdata('messagediv',$messagediv);
		redirect('excel_template');
	}

	// 編輯表單
	public function check_form(){
		$id = $this->input->get('id');
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 7)
			{
				if($r['actions_view'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div><div class=' icon-warning-sign'></div> 您沒有編輯的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('excel_template');
				}
			}
		}
		$data = [];
		$data['title'] = $this->unitName;
		$data['title_small'] = '編輯Excel模板管理';
		
		$data['result'] = $this->excel_template_model->getidData($id);
		
		$data['mainbar'] = $this->sidebarmenu_model->getsidebarmain();
		$data['subbar'] = $this->sidebarmenu_model->getsidebarsub();
		$data['view'] = $this->competence_model->getaction($this->competence_id);
		$data['upid'] = $this->input->get('id');
		
		$data['footer'] = $this->load->view('amadis_sys/common/footer','', TRUE);
		$data['script'] = $this->load->view('amadis_sys/common/script','', TRUE);
		$this->load->view('amadis_sys/common/head');
		$this->load->view('amadis_sys/common/header');
		$this->load->view('amadis_sys/common/sidebar',$data);
		$this->load->view('amadis_sys/excel_template/check',$data);
	}
}
