<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Changepassword extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('amadis_sys/users_model');
		$this->load->model('amadis_sys/competence_model');
		$this->unitName = '密碼變更';
		$this->loginusers = $this->session->userdata('bms_users_id');
		if(!$this->loginusers){
			redirect('home/login');
		}
	}

	public function index(){
		$data = [];
		$data['title'] = $this->unitName;
		$data['title_small'] = '設定密碼';
		$data['result'] = $this->users_model->getuseridData($this->loginusers);
		if ($data['result']['users_check'] != 0) {
			redirect('home');
		}
		$this->load->view('amadis_sys/common/head');
		$data['headerHtml'] = $this->load->view('amadis_sys/common/change/header',[],true);
		$data['sidebarHtml'] = $this->load->view('amadis_sys/common/change/sidebar',[],true);
		$data['footerHtml'] = $this->load->view('amadis_sys/common/change/footer',[],true);
		$this->load->view('amadis_sys/account/changepwd',$data);	
	}

	public function checkpwd(){
		$post = $this->input->post();
		$op = $this->input->post('op',true);
		if ($op === 'check') {
			$updatedata = array(
				'users_id'           => $this->loginusers,
				'users_check'        => 1,
				'users_password'     => hash('sha256',$post['password_2']),
				'users_updated_date' => date("Y-m-d H:i:s"),
				'users_updated_user' => $this->loginusers,
			);
			$this->users_model->updatepassword($updatedata);

			$success_msg = "<div class='alert alert-success'><button class='close' data-dismiss='alert'></button><i class='icon-ok'></i> 歡迎登入！密碼變更成功</div>";
			$this->session->set_flashdata('success_msg', $success_msg);
		}
		redirect('home');
	}

}
