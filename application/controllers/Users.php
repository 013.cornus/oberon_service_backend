<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('amadis_sys/account_model');
		$this->load->model('amadis_sys/competence_model');
		$this->load->model('amadis_sys/employee_model');
		$this->load->model('amadis_sys/sidebarmenu_model');
		$this->load->model('amadis_sys/order_model');
		$this->load->model('amadis_sys/customers_model');
		$this->load->model('amadis_sys/group_info_model');
		$this->load->model('amadis_sys/pagintion_model');
		$this->load->model('amadis_sys/ks/detection_model');
		$this->load->model('amadis_sys/organization_model');

		$this->unitName = '帳號管理';
		$admin_id = $this->session->userdata('bms_users_id');
		$username = $this->session->userdata('bms_users_name');
		$this->competence_id = $this->session->userdata('bms_competence_id');
		$this->rightid = $this->competence_model->getaction($this->competence_id);

		$this->session->set_flashdata('sidebarselected','users');
		$this->session->set_flashdata('mainsidebar','users');

		if(!$admin_id || $this->competence_id >= 4){
			redirect('home/login');
		}
	}

	public function index(){
		$this->session->unset_userdata('bms_competence');
		redirect('users/list');
		exit;
	}

    //帳號列表
	public function list($page=''){
		$certainemid = $this->session->userdata('bms_employee_id');
		$manager_id = $this->session->userdata('bms_manager_id');
		$list = $this->detection_model->getList();
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 41) {
				if($r['actions_view'] == 0) {
					//alert
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div> 您沒有瀏覽的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('home');
				}
			}
		}
		// $keyword = $this->input->post('fullname');
		$data = [];
		$data['title'] = $this->unitName;
		$data['title_small'] = '用戶列表';
		$data['hide'] = '';
		// $data['detection'] = $this->detection_model->getList();
		// print_r($data['detection']);
		// exit();

		if($this->input->post('competence') != ''){
			$this->session->set_userdata('bms_competence',$this->input->post('competence'));
		}
		
		switch ($this->competence_id) {
			case '2':
			$keyword['users_name'] = $this->input->post('fullname');
			$keyword['competence_id'] = 1;
			break;
			case '3':
			$keyword['users_name'] = $this->input->post('fullname');
			$keyword['organization_id'] = trim($manager_id);
			$data['hide'] = "style='display:none;'";
			$keyword['competence_id'] = 3;
			break;
			case '4':
			redirect('home');
			exit;
			break;
			case '5':
			redirect('home');
			exit;
			break;
			default:
			$keyword['users_name'] = $this->input->post('fullname');
			// $keyword['organization_id'] = trim($certainemid);
			// $data['hide'] = "style='display:none'";
			break;
		}

		if($this->session->userdata('bms_competence')){
			$keyword['competence'] = $this->session->userdata('bms_competence');
		}
		else{
			$keyword['competence'] = $this->input->post('competence');
		}

		$result = $this->account_model->getList($keyword,[]);
		$pager = $this->pagintion_model->setPager($result,$page);
		unset($result);
		$data['page_list'] = $pager['page'];
		$data['result'] = $this->account_model->getList($keyword,$pager['list']);
		foreach($data['result'] as $key => $row){
			$groupData = $this->group_info_model->getDataByType($row['group_type']);
			$groupStr[$key] = $groupData['group_info_name'];
			$data['users_type'][$key] = "<span class='label label-info'>".$groupStr[$key]."</span> ";
		}

		$data['competence'] = $this->competence_model->get_certain_competence();
		$data['mainbar'] = $this->sidebarmenu_model->getsidebarmain();
		$data['subbar'] = $this->sidebarmenu_model->getsidebarsub();
		$data['view'] = $this->competence_model->getaction($this->competence_id);

		$data['footer'] = $this->load->view('amadis_sys/common/footer','', TRUE);
		$data['script'] = $this->load->view('amadis_sys/common/script','', TRUE);
		$this->load->view('amadis_sys/common/head');
		$this->load->view('amadis_sys/common/header');
		$this->load->view('amadis_sys/common/sidebar',$data);
		$this->load->view('amadis_sys/account/users/list',$data);	
	}

	//跳到指定頁面
	public function pagejump(){
		if($this->input->post('pagenum') == null || $this->input->post('pagenum') == 0){
			redirect('users/list');
		}
		else{
			redirect('users/list/'.$this->input->post('pagenum').'');
		}
	}

    // 新增帳號
	public function create_form(){
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 41) {
				if($r['actions_insert'] == 0) {
					//alert
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div> 您沒有新增的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('users/list');
				}
			}
		}

		$data = [];
		$data['title'] = $this->unitName;
		$data['title_small'] = '新增帳號';

		$selection['status'] = 1;
		if($this->competence_id == 3){
			$selection['organization_id'] = $this->session->userdata('bms_manager_id');
			$data['organization'] = $this->organization_model->getList($selection);
			$data['employee'] = $this->employee_model->getList($selection);
		}
		else{
			$data['organization'] = $this->organization_model->getList($selection);
			$data['employee'] = $this->employee_model->getList($selection);
		}
		$competence = '';
		switch($this->competence_id){
			case '2':
			$competence = 2;
			break;
			case '3':
			$competence = 3;
			break;
			case '4':
			$competence = 4;
			break;
			case '5':
			$competence = 5;
			break;
			default:
			break;
		}
		$data['competence'] = $this->competence_model->EditableCompetence($competence);
		$data['groupList'] = $this->group_info_model->getList();

		$data['mainbar'] = $this->sidebarmenu_model->getsidebarmain();
		$data['subbar'] = $this->sidebarmenu_model->getsidebarsub();
		$data['view'] = $this->competence_model->getaction($this->competence_id);

		$data['footer'] = $this->load->view('amadis_sys/common/footer','', TRUE);
		$data['script'] = $this->load->view('amadis_sys/common/script','', TRUE);
		$this->load->view('amadis_sys/common/head',$data);
		$this->load->view('amadis_sys/common/header');
		$this->load->view('amadis_sys/common/sidebar',$data);
		$this->load->view('amadis_sys/account/users/create',$data);
	}

	// 新增帳號執行
	public function create(){
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 41) {
				if($r['actions_insert'] == 0) {
					//alert
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div> 您沒有新增的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('users/list');
				}
			}
		}
		$eid = $this->input->post('employee');

		$account = array(
			'competence_id'      => $this->input->post('competence'),
			'group_type'         => $this->input->post('group_type'),
			'employee_id'        => 0,
			'customer_id'        => 0,
			'organization_id'    => $this->input->post('occupation'),
			'users_name'         => $this->input->post('users_name'),
			'users_account'      => $this->input->post('account'),
			'users_password'     => hash('sha256','000000'),
			'users_created_date' => date("Y-m-d H:i:s",time()),
			'users_created_user' => $this->session->userdata('bms_users_id')
		);
		$this->account_model->add_account($account);

		//alert
		$messagediv = "<div class='alert alert-success'><button class='close' data-dismiss='alert'></button><div class='icon-ok'></div> 成功！資料已新增。</div>";
		$this->session->set_flashdata('messagediv',$messagediv);

		redirect('users/list');
	}

	// 刪除帳號執行
	public function delete(){
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 41) {
				if($r['actions_delete'] == 0) {
					//alert
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div> 您沒有刪除的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('users/list');
				}
			}
		}
		$id = $this->input->get('id');
		$datetime = date("Y-m-d H:i:s"); 
		$user_id = $this->session->userdata('bms_users_id');

		$account = array(
			'users_is_del'       => 1,
			'users_updated_date' => date("Y-m-d H:i:s"),
			'users_updated_user' => $this->session->userdata('bms_users_id'),
		);

		$this->account_model->update_account($account,$id);
		
		//alert
		$messagediv = "<div class='alert alert-success'>
		<button class='close' data-dismiss='alert'></button>
		<div class='icon-ok'></div> 成功！該筆資料已刪除。
		</div>";
		$this->session->set_flashdata('messagediv',$messagediv);
		redirect('users/list');
	}

	// 下架帳號執行
	public function info_invisible(){
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 41) {
				if($r['actions_enable'] == 0) {
					//alert
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div> 您沒有上下架的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('users/list');
				}
			}
		}
		$id = $this->input->get('id');
		$datetime = date("Y-m-d H:i:s"); 
		$user_id = $this->session->userdata('bms_users_id');

		$account = array(
			'users_status'       => 0,
			'users_updated_date' => date("Y-m-d H:i:s"),
			'users_updated_user' => $this->session->userdata('bms_users_id'),
		);

		$this->account_model->update_account($account,$id);

		//alert
		$messagediv = "<div class='alert alert-success'>
		<button class='close' data-dismiss='alert'></button>
		<div class='icon-ok'></div> 成功！該筆帳號已停用。
		</div>";
		$this->session->set_flashdata('messagediv',$messagediv);

		redirect('users/list');
	}

	// 上架帳號執行
	public function info_visible(){
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 41){
				if($r['actions_enable'] == 0){
					//alert
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div> 您沒有上下架的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('users/list');
				}
			}
		}
		$id = $this->input->get('id');
		$datetime = date("Y-m-d H:i:s"); 
		$user_id = $this->session->userdata('bms_users_id');

		$account = array(
			'users_status'       => 1,
			'users_updated_date' => date("Y-m-d H:i:s"),
			'users_updated_user' => $this->session->userdata('bms_users_id'),
		);

		$this->account_model->update_account($account,$id);

		//alert
		$messagediv = "<div class='alert alert-success'>
		<button class='close' data-dismiss='alert'></button>
		<div class='icon-ok'></div> 成功！該筆帳號已啟用。
		</div>";
		$this->session->set_flashdata('messagediv',$messagediv);

		redirect('users/list');
	}

	// 修改帳號表單
	public function update_form(){
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 41) {
				if($r['actions_update'] == 0) {
					//alert
					$messagediv = "<div class='alert alert-warning'>
					<button class='close' data-dismiss='alert'></button>
					<div></div><i class='icon-warning-sign'></i> 您沒有修改的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('users/list');
				}
			}
		}
		$data = [];
		$keyword = $this->input->post('fullname');
		$id = $this->input->get('id');

		if ($id == 1 && $this->competence_id != 1) {
			redirect('users/list');
			exit;
		}
		
		$data['upid'] = $id;
		$data['title'] = $this->unitName;
		$data['title_small'] = '編輯帳號';
		$selection['status'] = 1;
		if($this->competence_id == 3){
			$selection['organization_id'] = $this->session->userdata('bms_manager_id');
			$data['organization'] = $this->organization_model->getList($selection);
			$data['employee'] = $this->employee_model->getList($selection);
		}
		else{
			$data['organization'] = $this->organization_model->getList($selection);
			$data['employee'] = $this->employee_model->getList($selection);
		}
		$data['result'] = $this->account_model->getonedata($id);
		// print_r($data['result']);
		// exit();
		$competence = '';
		switch($this->competence_id){
			case '2':
			$competence = 2;
			break;
			case '3':
			$competence = 3;
			break;
			case '4':
			$competence = 4;
			break;
			case '5':
			$competence = 5;
			break;
			default:
			break;
		}
		$data['competence'] = $this->competence_model->EditableCompetence($competence);
		$data['groupList'] = $this->group_info_model->getList();
		
		$data['mainbar'] = $this->sidebarmenu_model->getsidebarmain();
		$data['subbar'] = $this->sidebarmenu_model->getsidebarsub();
		$data['view'] = $this->competence_model->getaction($this->competence_id);
		
		$data['footer'] = $this->load->view('amadis_sys/common/footer','', TRUE);
		$data['script'] = $this->load->view('amadis_sys/common/script','', TRUE);
		$this->load->view('amadis_sys/common/head');
		$this->load->view('amadis_sys/common/header');
		$this->load->view('amadis_sys/common/sidebar',$data);
		$this->load->view('amadis_sys/account/users/update',$data);
	}

    // 修改帳號執行
	public function update(){
		$id = $this->input->post('id');
		$account = array(
			'competence_id'      => $this->input->post('competence'),
			'group_type'         => $this->input->post('group_type'),
			'employee_id'        => 0,
			'customer_id'        => 0,
			'organization_id'    => $this->input->post('occupation'),
			'users_name'         => $this->input->post('users_name'),
			'users_password'     => hash('sha256',$this->input->post('password_2')),
			'users_updated_date' => date("Y-m-d H:i:s"),
			'users_updated_user' => $this->session->userdata('bms_users_id'),
		);

		$this->account_model->update_account($account,$id);
		//alert
		$messagediv = "<div class='alert alert-success'>
		<button class='close' data-dismiss='alert'></button>
		<div class='icon-ok'></div> 成功！此筆資料已更新。
		</div>";
		$this->session->set_flashdata('messagediv',$messagediv);
		redirect('users/list');
	}

	//業務承攬人選單
	public function getemployee(){
		if($this->session->userdata('bms_competence_id') == 3){
			$selection['organization_id'] = $this->session->userdata('bms_manager_id');
			$data['employee'] = $this->employee_model->getList($selection);
		}
		else{
			$employeelist = $this->employee_model->getList();
		}
		echo json_encode($employeelist);
	}

	//客戶選單
	public function getcustomer(){
		if($this->session->userdata('bms_competence_id') == 3){
			$keyword['organization_id'] = $this->session->userdata('bms_manager_id');
			$customerlist = $this->customers_model->getList($keyword);
		}
		else{
			$customerlist = $this->customers_model->getList();
		}

		echo json_encode($customerlist);
	}

	//組織選單
	public function getorganization(){
		$selection['status'] = 1;
		if($this->session->userdata('bms_competence_id') == 3){
			$selection['organization_id'] = $this->session->userdata('bms_manager_id');
			$orglist = $this->organization_model->getList($selection);
		}
		else{
			$orglist = $this->organization_model->getList($selection);
		}

		echo json_encode($orglist);
	}

	//權限選單
	public function getcompetence(){
		$competencelist = $this->competence_model->getsuitableright($this->competence_id);

		echo json_encode($competencelist);
	}
}
