<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('amadis_sys/account_model');
		$this->load->model('amadis_sys/competence_model');
		$this->load->model('amadis_sys/pagintion_model');
		$this->load->model('amadis_sys/sidebarmenu_model');

		$this->unitName = '帳號管理';
		$admin_id = $this->session->userdata('bms_users_id');
		$username = $this->session->userdata('bms_users_name');
		$this->competence_id = $this->session->userdata('bms_competence_id');
		$this->rightid = $this->competence_model->getaction($this->competence_id);

		$this->session->set_flashdata('sidebarselected','account');
		$this->session->set_flashdata('mainsidebar','system');

		if(!$admin_id || $this->competence_id >= 4){
			redirect('home/login');
		}
	}

	public function index(){
		redirect('account/list');
		exit;
	}

    //帳號列表
	public function list($page = ''){
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 25) {
				if($r['actions_view'] == 0) {
					$alert['color'] = 'alert';
					$alert['sign'] = '';
					$alert['text'] = '您沒有瀏覽的權限。';
					$this->session->set_flashdata('alert',$alert);
					redirect('home');
				}
			}
		}
		$keyword = $this->input->post('fullname');
		$data = [];
		$data['title'] = $this->unitName;
		$data['title_small'] = '帳號列表';
		
		$result = $this->account_model->getList($keyword,[]);
		$pager = $this->pagintion_model->setPager($result,$page);
		unset($result);
		$data['page_list'] = $pager['page'];
		$data['result'] = $this->account_model->getList($keyword,$pager['list']);

		$data['mainbar'] = $this->sidebarmenu_model->getsidebarmain();
		$data['subbar'] = $this->sidebarmenu_model->getsidebarsub();
		$data['view'] = $this->competence_model->getaction($this->competence_id);
		$data['alert'] = '';

		if($this->session->flashdata('alert')){
			$alert = $this->session->flashdata('alert');
			$this->session->set_flashdata('alert',$alert);
			$data['alert'] = $this->load->view('amadis_sys/common/alerts','',TRUE);
		}

		$data['footer'] = $this->load->view('amadis_sys/common/footer','', TRUE);
		$data['script'] = $this->load->view('amadis_sys/common/script','', TRUE);
		$this->load->view('amadis_sys/common/head');
		$this->load->view('amadis_sys/common/header');
		$this->load->view('amadis_sys/common/sidebar',$data);
		$this->load->view('amadis_sys/account/list',$data);	
	}

    // 新增帳號
	public function create_form(){
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 25) {
				if($r['actions_insert'] == 0) {
					$alert['color'] = 'alert';
					$alert['sign'] = '';
					$alert['text'] = '您沒有新增的權限。';
					$this->session->set_flashdata('alert',$alert);
					redirect('account');
				}
			}
		}
		$keyword = $this->input->post('fullname');
		$data = [];
		$data['title'] = $this->unitName;
		$data['title_small'] = '新增帳號';
		
		$data['competence'] = $this->competence_model->getdata();
		$data['employee'] = $this->employee_model->getList($keyword,[]);

		$data['includeCss'] = 'select2';
		$data['mainbar'] = $this->sidebarmenu_model->getsidebarmain();
		$data['subbar'] = $this->sidebarmenu_model->getsidebarsub();
		$data['view'] = $this->competence_model->getaction($this->competence_id);

		$this->load->view('amadis_sys/common/head',$data);
		$this->load->view('amadis_sys/common/header');
		$this->load->view('amadis_sys/common/sidebar',$data);
		$this->load->view('amadis_sys/account/create',$data);
	}

	// 新增帳號執行
	public function create(){
		$eid = $this->input->post('employee');
		$employee = $this->employee_model->getidData($eid);

		$account = array(
			'competence_id'      => $this->input->post('competence'),
			'employee_id'        => $eid,
			'users_name'         => $employee['employee_name'],
			'users_account'      => $this->input->post('account'),
			'users_password'     => hash('sha256','000000'),
			'users_created_date' => date("Y-m-d H:i:s"),
			'users_created_user' => $this->session->userdata('bms_users_id')
		);
		$this->account_model->add_account($account);

		$alert['color'] = 'alert alert-success';
		$alert['sign'] = 'icon-ok';
		$alert['text'] = '成功！資料已新增。';
		$this->session->set_flashdata('alert',$alert);
		redirect('account');
	}

	// 刪除帳號執行
	public function delete(){
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 25) {
				if($r['actions_delete'] == 0) {
					$alert['color'] = 'alert';
					$alert['sign'] = '';
					$alert['text'] = '您沒有刪除的權限。';
					$this->session->set_flashdata('alert',$alert);
					redirect('account');
				}
			}
		}
		$id = $this->input->get('id');
		$datetime = date("Y-m-d H:i:s"); 
		$user_id = $this->session->userdata('bms_users_id');
		$this->account_model->delete_users($id,$datetime,$user_id);
		$alert['color'] = 'alert alert-success';
		$alert['sign'] = 'icon-ok';
		$alert['text'] = '成功！該筆資料已刪除。';
		$this->session->set_flashdata('alert',$alert);
		redirect('account');
	}

	//跳到指定頁面
	public function pagejump(){
		if($this->input->post('pagenum') == null || $this->input->post('pagenum') == 0){
			redirect('account/list');
		}
		else{
			redirect('users/list/'.$this->input->post('pagenum').'');
		}
	}

	// 下架帳號執行
	public function info_invisible(){
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 25) {
				if($r['actions_enable'] == 0) {
					$alert['color'] = 'alert';
					$alert['sign'] = '';
					$alert['text'] = '您沒有上下架的權限。';
					$this->session->set_flashdata('alert',$alert);
					redirect('account');
				}
			}
		}
		$id = $this->input->get('id');
		$datetime = date("Y-m-d H:i:s"); 
		$user_id = $this->session->userdata('bms_users_id');
		$this->account_model->invisible_users($id,$datetime,$user_id);
		$alert['color'] = 'alert alert-success';
		$alert['sign'] = 'icon-ok';
		$alert['text'] = '成功！該筆帳號已停用。';
		$this->session->set_flashdata('alert',$alert);

		redirect('account');
	}

	// 上架帳號執行
	public function info_visible(){
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 25){
				if($r['actions_enable'] == 0){
					$alert['color'] = 'alert';
					$alert['sign'] = '';
					$alert['text'] = '您沒有上下架的權限。';
					$this->session->set_flashdata('alert',$alert);
					redirect('account');
				}
			}
		}
		$id = $this->input->get('id');
		$datetime = date("Y-m-d H:i:s"); 
		$user_id = $this->session->userdata('bms_users_id');
		$this->account_model->visible_users($id,$datetime,$user_id);
		$alert['color'] = 'alert alert-success';
		$alert['sign'] = 'icon-ok';
		$alert['text'] = '成功！該筆帳號已啟用。';
		$this->session->set_flashdata('alert',$alert);
		redirect('account');
	}

	// 修改帳號表單
	public function update_form(){
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 25) {
				if($r['actions_update'] == 0) {
					$alert['color'] = 'alert alert-warning';
					$alert['sign'] = 'icon-warning-sign';
					$alert['text'] = '您沒有修改的權限。';
					$this->session->set_flashdata('alert',$alert);
					redirect('account');
				}
			}
		}
		$data = [];
		$keyword = $this->input->post('fullname');
		$id = $this->input->get('id');

		$data['result'] = $this->account_model->getonedata($id);
		$data['customer'] = $this->customers_model->getList();
		$data['competence'] = $this->competence_model->getdata();
		$data['employee'] = $this->employee_model->getList($keyword,[]);

		$data['upid'] = $this->input->get('id');
		$data['title'] = $this->unitName;
		$data['title_small'] = '編輯帳號';
		
		$data['mainbar'] = $this->sidebarmenu_model->getsidebarmain();
		$data['subbar'] = $this->sidebarmenu_model->getsidebarsub();
		$data['view'] = $this->competence_model->getaction($this->competence_id);
		
		$this->load->view('amadis_sys/common/head');
		$this->load->view('amadis_sys/common/header');
		$this->load->view('amadis_sys/common/sidebar',$data);
		$this->load->view('amadis_sys/account/update',$data);
	}

    // 修改帳號執行
	public function update(){
		$id = $this->input->post('id');
		$employee = $this->employee_model->getidData($this->input->post('users_number'));
		$account = array(
			'competence_id'      => $this->input->post('competence'),
			'employee_id'        => $this->input->post('users_number'),
			'organization_id'    => $employee['organization_id'],
			'users_password'     => hash('sha256',$this->input->post('password_2')),
			'users_updated_date' => date("Y-m-d H:i:s"),
			'users_updated_user' => $this->session->userdata('bms_users_id'),
		);

		$this->account_model->update_account($account,$id);
		$alert['color'] = 'alert alert-success';
		$alert['sign'] = 'icon-ok';
		$alert['text'] = '成功！此筆資料已更新。';
		$this->session->set_flashdata('alert',$alert);
		redirect('account');
	}

	public function test(){
		redirect('home');
		show_404();
		exit;
		$data = [];
		$data['mainbar'] = $this->sidebarmenu_model->getsidebarmain();
		$data['subbar'] = $this->sidebarmenu_model->getsidebarsub();
		$data['view'] = $this->competence_model->getaction($this->competence_id);
		// $this->load->view('amadis_sys/common/head');
		// $this->load->view('amadis_sys/common/header');
		$data['headerHtml'] = $this->load->view('amadis_sys/common/header',$data,true);
		$data['sidebarHtml'] = $this->load->view('amadis_sys/common/sidebar',$data,true);
		$data['scriptsHtml'] = $this->load->view('amadis_sys/common/script',$data,true);
		$data['footerHtml'] = $this->load->view('amadis_sys/common/footer',[],true);
		$this->load->view('amadis_sys/account/test',$data);	
	}
}
