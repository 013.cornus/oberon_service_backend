<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report_create extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('amadis_sys/competence_model');
		$this->load->model('amadis_sys/sidebarmenu_model');
		$this->load->model('amadis_sys/pagintion_model');
		$this->load->model('amadis_sys/report_template_model');
		$this->load->model('amadis_sys/excel_template_model');
		$this->unitName = '產生報告';
		$this->competence_id = $this->session->userdata('bms_competence_id');
		$this->rightid = $this->competence_model->getaction($this->competence_id);
		

		$admin_id = $this->session->userdata('bms_users_id');
		$username = $this->session->userdata('bms_users_name');
		$this->session->set_flashdata('sidebarselected','report_create');
		$this->session->set_flashdata('mainsidebar','report');
		$this->load->library('excel');
		$this->load->library('apiconnection');

		if(!$admin_id){
			redirect('home/login');
		}
	}

    //報告列表
	public function index(){
		redirect('report_create/list');
		exit;
	}
    //報告列表
	public function list($page=''){
		$organization_id = $this->session->userdata('organization_id');
		$manager_id = $this->session->userdata('manager_id');
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 60)
			{
				if($r['actions_view'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div> 您沒有瀏覽的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('home');
				}
			}
		}
		$data = [];
		$data['title'] = $this->unitName;
		$data['title_small'] = '報告列表';
		$data['active'] = 'employee';

		$keyword['fullname'] = $this->input->post('fullname');
		$dataArr = array(
			'group_type' => $this->session->userdata('bms_group_type')
		);
		$url = 'http://34.80.127.28/web/tw/api/v2/detection/selectedData?'.http_build_query($dataArr);

		$data['result'] = [];
		if(isset(json_decode($this->apiconnection->apiConnect('',$url,'get'),true)['Data'])){
			$data['result'] = json_decode($this->apiconnection->apiConnect('',$url,'get'),true)['Data'];
		}

		$template_selected = $this->report_template_model->getKeyData('group_type',$this->session->userdata('bms_group_type'));

		$template_info = json_decode(file_get_contents('http://34.80.38.39/rgms/api/template?id='.$template_selected['template_id']),true);

		if($template_info['Data']){
			$data['template'] = $template_info['Data'][0]['excel_template_name'];
			foreach($data['result'] as $key => $row){
				$data['link'][$key] = 'http://34.80.38.39/rgms/api/report_view_2/check_form?caseId='.$row['case'].'&date='.date('YmdHis',$row["date"]).'&userId='.$row['id'].'&model='.$template_info['Data'][0]['excel_template_name'];
			}
		}

		else{
			$messagediv = "<div class='alert'>
			<button class='close' data-dismiss='alert'></button>
			<div></div> 模板未設定或已被下架,請重新選擇模板。<a href=".base_url('report_template').">指定報告模板</a>
			</div>";
			$this->session->set_flashdata('messagediv',$messagediv);
			foreach($data['result'] as $key => $row){
				$data['link'][$key] = '#';
			}
		}

		//選單
		$data['mainbar'] = $this->sidebarmenu_model->getsidebarmain();
		$data['subbar'] = $this->sidebarmenu_model->getsidebarsub();
		$data['view'] = $this->competence_model->getaction($this->competence_id);
		
		$data['footer'] = $this->load->view('amadis_sys/common/footer','', TRUE);
		$data['script'] = $this->load->view('amadis_sys/common/script','', TRUE);
		$this->load->view('amadis_sys/common/head');
		$this->load->view('amadis_sys/common/header');
		$this->load->view('amadis_sys/common/sidebar',$data);
		$this->load->view('amadis_sys/report_create/list',$data);	
	}

	//跳到指定頁面
	public function pagejump(){
		if($this->input->post('pagenum') == null || $this->input->post('pagenum') == 0){
			redirect('report_create/list');
		}
		else{
			redirect('report_create/list/'.$this->input->post('pagenum').'');
		}
	}

	//batch create report
	public function batch_create(){
		$case_id = $this->input->get();

		$url = 'http://34.80.127.28/web/tw/api/v2/detection/selectedData?'.http_build_query($dataArr);
		if(isset(json_decode($this->apiconnection->apiConnect('',$url,'get'),true)['Data'])){
			$data = json_decode($this->apiconnection->apiConnect('',$url,'get'),true)['Data'];
		}
		$split = explode(',', $case_id['case_list']);

		$date = [];
		$userId = [];
		foreach ($split as $value) {
			foreach ($data as $key => $row) {
				if ($value == $row['case']) {
					array_push($date, $row['date']);
					array_push($userId, $row['id']);
				}
			}
		}
		$date = implode(',', $date);
		$userId = implode(',', $userId);

		$template_selected = $this->report_template_model->getKeyData('group_type',$this->session->userdata('group_type'));

		$template_info = json_decode(file_get_contents('http://34.80.38.39/rgms/api/template?id='.$template_selected['template_id']),true);


		$url = 'http://34.80.38.39/api/report_view_2/check_form';
		$case_list['case_list'] = array(
			'caseId' => $case_id['case_list'],
			'date'   => $date,
			'userId' => $userId,
			'model'  => $template_info['Data'][0]['excel_template_name'],
		);

		$this->apiconnection->apiConnect($case_list, $url, 'get');
	}
}
