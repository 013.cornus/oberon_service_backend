<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Group_info extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('amadis_sys/group_info_model');
		$this->load->model('amadis_sys/competence_model');
		$this->load->model('amadis_sys/sidebarmenu_model');
		$this->load->model('amadis_sys/pagintion_model');
		$this->unitName = '客戶管理';
		$this->competence_id = $this->session->userdata('bms_competence_id');
		$this->rightid = $this->competence_model->getaction($this->competence_id);
		

		$admin_id = $this->session->userdata('bms_users_id');
		$username = $this->session->userdata('bms_users_name');
		$this->session->set_flashdata('sidebarselected','group_info');
		$this->session->set_flashdata('mainsidebar','excel_create');
		$this->load->library('apiconnection');
		if(!$admin_id){
			redirect('home/login');
		}
	}

    //群組模板設定列表
	public function index(){
		redirect('group_info/list');
		exit;
	}
    //群組模板設定列表
	public function list($page=''){
		$organization_id = $this->session->userdata('bms_organization_id');
		$manager_id = $this->session->userdata('bms_manager_id');
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 7)
			{
				if($r['actions_view'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div> 您沒有瀏覽的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('home');
				}
			}
		}
		$data = [];
		$data['title'] = $this->unitName;
		$data['title_small'] = '客戶設定列表';
		$data['active'] = 'employee';

		$keyword['fullname'] = $this->input->post('fullname');
		
		$result = $this->group_info_model->getList($keyword);		
		$pager = $this->pagintion_model->setPager($result,$page);
		$data['page_list'] = $pager['page'];
		$data['result'] = $this->group_info_model->getList($keyword,$pager['list']);
		
		//選單
		$data['mainbar'] = $this->sidebarmenu_model->getsidebarmain();
		$data['subbar'] = $this->sidebarmenu_model->getsidebarsub();
		$data['view'] = $this->competence_model->getaction($this->competence_id);
		
		$data['footer'] = $this->load->view('amadis_sys/common/footer','', TRUE);
		$data['script'] = $this->load->view('amadis_sys/common/script','', TRUE);
		$this->load->view('amadis_sys/common/head');
		$this->load->view('amadis_sys/common/header');
		$this->load->view('amadis_sys/common/sidebar',$data);
		$this->load->view('amadis_sys/group_info/list',$data);	
	}

	//跳到指定頁面
	public function pagejump(){
		if($this->input->post('pagenum') == null || $this->input->post('pagenum') == 0){
			redirect('group_info/list');
		}
		else{
			redirect('group_info/list/'.$this->input->post('pagenum').'');
		}
	}

    // 新增表單
	public function create_form(){
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 7) {
				if($r['actions_insert'] == 0) {
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div><div class=' icon-warning-sign'></div> 您沒有新增的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('group_info');
				}
			}
		}
		$data = [];
		$data['title'] = $this->unitName;
		$data['title_small'] = '新增群組模板設定';

		$data['template'] = json_decode($this->apiconnection->apiConnect('','http://34.80.38.39/api/report_view/getTemplateList','post'),true);
		$data['mainbar'] = $this->sidebarmenu_model->getsidebarmain();
		$data['subbar'] = $this->sidebarmenu_model->getsidebarsub();
		$data['view'] = $this->competence_model->getaction($this->competence_id);
		$this->session->set_flashdata('sidebarselected','group_info');
		
		$data['footer'] = $this->load->view('amadis_sys/common/footer','', TRUE);
		$data['script'] = $this->load->view('amadis_sys/common/script','', TRUE);
		$this->load->view('amadis_sys/common/head');
		$this->load->view('amadis_sys/common/header');
		$this->load->view('amadis_sys/common/sidebar',$data);
		$this->load->view('amadis_sys/group_info/create',$data);
	}

	// 新增群組模板設定執行
	public function create(){
		$datetime = date("Y-m-d H:i:s",time());
		$config['upload_path'] = 'public/uploads/excel_model/';
		$config['allowed_types'] = 'xlsx|xls';

		$this->load->library('upload', $config);
		$this->upload->initialize($config);

		if (!$this->upload->do_upload('excel_model'))
		{
			$error = $this->upload->display_errors();
			print_r($error);
		}
		else
		{
			$data = array('upload_data' => $this->upload->data());
		}

		$group_info = array(
			'group_info_name'         => $data['upload_data']['file_name'],
			'group_info_sub_name'     => $data['upload_data']['file_ext'],
			'group_info_created_date' => $datetime,
			'group_info_created_user' => $this->session->userdata('bms_users_id')
		);
		$this->group_info_model->add_group_info($group_info);

		$messagediv = "<div class='alert alert-success'>
		<button class='close' data-dismiss='alert'></button>
		<div class='icon-ok'></div> 成功！群組模板設定已新增完成。
		</div>";
		$this->session->set_flashdata('messagediv',$messagediv);
		redirect('group_info');
	}

	// 刪除執行
	public function delete(){
		$id = $this->input->get('id');
		$user_id = $this->session->userdata('bms_users_id');
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 7) {
				if($r['actions_delete'] == 0) {
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div><div class=' icon-warning-sign'></div> 您沒有刪除的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('group_info');
				}
			}
		}
		
		$datetime = date("Y-m-d H:i:s"); 
		$group_info = array(
			'group_info_is_del'       => 1,
			'group_info_updated_date' => date("Y-m-d H:i:s",time()),
			'group_info_updated_user' => $this->session->userdata('bms_users_id')
		);

		$this->group_info_model->update_group_info($group_info,$id);

		$messagediv = "<div class='alert alert-success'>
		<button class='close' data-dismiss='alert'></button>
		<div class='icon-ok'></div> 成功！該筆群組已刪除。
		</div>";
		$this->session->set_flashdata('messagediv',$messagediv);
		redirect('group_info');
	}

	// 下架執行
	public function group_info_invisible(){
		$id = $this->input->get('id');
		$user_id = $this->session->userdata('bms_users_id');
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 7) {
				if($r['actions_enable'] == 0) {
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div><div class=' icon-warning-sign'></div> 您沒有下架的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('group_info');
				}
			}
		}
		
		$datetime = date("Y-m-d H:i:s"); 
		$group_info = array(
			'group_info_status'       => 0,
			'group_info_updated_date' => date("Y-m-d H:i:s",time()),
			'group_info_updated_user' => $this->session->userdata('bms_users_id')
		);

		$this->group_info_model->update_group_info($group_info,$id);

		$messagediv = "<div class='alert alert-success'>
		<button class='close' data-dismiss='alert'></button>
		<div class='icon-ok'></div> 成功！該筆群組模板設定已停用。
		</div>";
		$this->session->set_flashdata('messagediv',$messagediv);
		redirect('group_info');
	}

	// 上架執行
	public function group_info_visible(){
		$id = $this->input->get('id');
		$user_id = $this->session->userdata('bms_users_id');
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 7)
			{
				if($r['actions_enable'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div><div class=' icon-warning-sign'></div> 您沒有上架的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('group_info');
				}
			}
		}
		
		$datetime = date("Y-m-d H:i:s"); 

		$group_info = array(
			'group_info_status'       => 1,
			'group_info_updated_date' => date("Y-m-d H:i:s",time()),
			'group_info_updated_user' => $this->session->userdata('bms_users_id')
		);

		$this->group_info_model->update_group_info($group_info,$id);

		$messagediv = "<div class='alert alert-success'>
		<button class='close' data-dismiss='alert'></button>
		<div class='icon-ok'></div> 成功！該筆群組模板設定已啟用。
		</div>";
		$this->session->set_flashdata('messagediv',$messagediv);
		redirect('group_info');
	}

	// 編輯表單
	public function update_form(){
		$id = $this->input->get('id');
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 7)
			{
				if($r['actions_update'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div><div class=' icon-warning-sign'></div> 您沒有編輯的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('group_info');
				}
			}
		}
		$data = [];
		$data['title'] = $this->unitName;
		$data['title_small'] = '編輯群組模板設定';
		
		$data['result'] = $this->group_info_model->getidData($id);
		$data['template'] = json_decode($this->apiconnection->apiConnect('','http://34.80.38.39/api/report_view/getTemplateList','post'),true);
		
		$data['mainbar'] = $this->sidebarmenu_model->getsidebarmain();
		$data['subbar'] = $this->sidebarmenu_model->getsidebarsub();
		$data['view'] = $this->competence_model->getaction($this->competence_id);
		$data['upid'] = $this->input->get('id');
		
		$data['footer'] = $this->load->view('amadis_sys/common/footer','', TRUE);
		$data['script'] = $this->load->view('amadis_sys/common/script','', TRUE);
		$this->load->view('amadis_sys/common/head');
		$this->load->view('amadis_sys/common/header');
		$this->load->view('amadis_sys/common/sidebar',$data);
		$this->load->view('amadis_sys/group_info/update',$data);
	}

    // 編輯執行
	public function update(){
		$id = $this->input->post('id');

		$group_info = array(
			'group_info_name'         => $this->input->post('group_info_name'),
			'excel_template'      => $this->input->post('template'),
			'group_info_updated_date' => date("Y-m-d H:i:s",time()),
			'group_info_updated_user' => $this->session->userdata('bms_users_id')
		);

		$this->group_info_model->update_group_info($group_info,$id);

		$messagediv = "<div class='alert alert-success'>
		<button class='close' data-dismiss='alert'></button>
		<div class='icon-ok'></div> 成功！此筆群組模板設定已更新完成。
		</div>";
		$this->session->set_flashdata('messagediv',$messagediv);
		redirect('group_info');
	}

	// 編輯表單
	public function check_form(){
		$id = $this->input->get('id');
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 7)
			{
				if($r['actions_view'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div><div class=' icon-warning-sign'></div> 您沒有編輯的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('group_info');
				}
			}
		}
		$data = [];
		$data['title'] = $this->unitName;
		$data['title_small'] = '編輯群組模板設定';
		
		$data['result'] = $this->group_info_model->getidData($id);
		
		$data['mainbar'] = $this->sidebarmenu_model->getsidebarmain();
		$data['subbar'] = $this->sidebarmenu_model->getsidebarsub();
		$data['view'] = $this->competence_model->getaction($this->competence_id);
		$data['upid'] = $this->input->get('id');
		
		$data['footer'] = $this->load->view('amadis_sys/common/footer','', TRUE);
		$data['script'] = $this->load->view('amadis_sys/common/script','', TRUE);
		$this->load->view('amadis_sys/common/head');
		$this->load->view('amadis_sys/common/header');
		$this->load->view('amadis_sys/common/sidebar',$data);
		$this->load->view('amadis_sys/group_info/check',$data);
	}
}
