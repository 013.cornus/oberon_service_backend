<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Apiexportexcel extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->library('excel');
		$this->load->model('amadis_sys/bonuscalculation_model');
		$this->load->model('amadis_sys/competence_model');
		$this->load->model('amadis_sys/organization_model');
		$this->load->model('amadis_sys/employee_model');
		$this->load->model('amadis_sys/occupation_model');
		$this->load->model('amadis_sys/order_model');
		$this->load->model('amadis_sys/stock_model');
		$this->load->model('amadis_sys/profit_model');
		$this->load->model('amadis_sys/bank_model');
		$this->load->model('amadis_sys/branch_model');
		$this->load->model('amadis_sys/post_model');
		$this->load->model('amadis_sys/order_info_model');
		$this->load->model('amadis_sys/customers_model');
		$this->load->model('amadis_sys/bonus_model');
		$this->load->model('amadis_sys/users_bonus_model');
		$this->load->model('amadis_sys/users_bonus_extra_model');
		$this->load->model('amadis_sys/goods_model');
		$this->unitName = '報表匯出';

		$this->competenceID = $this->session->userdata('competence_id');
		$this->adminID = $this->session->userdata('users_id');
		$this->username = $this->session->userdata('users_name');
		
		if(!$admin_id){
			redirect('home/login');
		}
	}

	//客戶報表輸出
	public function customers(){

	}
}
