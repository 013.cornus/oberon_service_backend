<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customers extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('amadis_sys/member_model');
		// $this->load->library('session');
		$this->unitName = '會員管理';
	}

    //顧客清單
	public function home(){
		$result         = $this->member_model->getdata();
		$data['result'] = $result;
		$data = [];
		$data['title'] = $this->unitName;
		$data['title_small'] = '顧客清單';
		$data['active'] = 'goods';   
		$this->load->view('amadis_sys/customers',$data);
	}
    //新增會員表單
	public function create_form(){
		$data = [];
		$data['title'] = $this->unitName;
		$data['title_small'] = '顧客清單';
		$data['active'] = 'goods';
		$this->load->view('amadis_sys/customers',$data);
	}

    // 新增會員執行
	public function create(){
		$member = array(
			'staff_name'                =>$this->input->post('name'),
			'staff_id_card'             =>$this->input->post('idcard'),
			'staff_affiliation'         =>$this->input->post('affiliation'),
			'staff_position'            =>$this->input->post('position'),
			'staff_cellphone'           =>$this->input->post('cellphone'),
			'staff_telephone'           =>$this->input->post('telephone'),
			'staff_residential_address' =>$this->input->post('address'),
			'staff_referrer'            =>$this->input->post('referrer')
		);

		$this->member_model->add_member($member);

		redirect('member/user_membermanage','refresh');
	}

}
