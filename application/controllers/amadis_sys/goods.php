<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Goods extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->unitName = '商品管理';
	}

	public function index(){
		$data = [];
		$data['title'] = $this->unitName = '股權';
		$data['title_small'] = '資訊總覽';
		$this->load->view('amadis_sys/goods',$data);
	}

}
