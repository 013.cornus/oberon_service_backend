<?php   
defined('BASEPATH') OR exit('No direct script access allowed');

class ApiConnection {
	public function __construct(){
		$CI = & get_instance();
	}

	//連結api
	/**
	* 連結api
	* @param dataArr 資料陣列
	* @param url 目標網址
	*/
	public function apiConnect($dataArr = '',$url = '',$method = 'post'){
		$options = array(
			'http' => array(
				'header'        => array(
					"Content-type: application/json; charset=utf-8",
				),
				'method'        => $method,
				'content'       => json_encode($dataArr,JSON_UNESCAPED_UNICODE),
				'ignore_errors' => true
			)
		);

		$context  = stream_context_create($options);
		$result = file_get_contents($url, false, $context);
		return $result;
	}

	public function apiConnectOuterV2($dataArr = '',$url = '',$method){
		$options = array(
			'http' => array(
				'header'        => "Content-Type: application/x-www-form-urlencoded; charset=utf-8",
				'method'        => $method,
				'content'       => http_build_query($dataArr),
			)
		);

		if($dataArr == ''){
			$options = array(
				'http' => array(
					'header'        => "Content-Type: application/x-www-form-urlencoded; charset=utf-8",
					'method'        => $method,
				)
			);
		}

		$context  = stream_context_create($options);
		$result = file_get_contents($url, false, $context);
		return $result;
	}

	public function apiConnectOuter($dataArr = '',$url = ''){
		$result = file_get_contents($url.'?'.http_build_query($dataArr));
		return $result;
	}
}   
?>		