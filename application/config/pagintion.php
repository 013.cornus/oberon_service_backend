<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// 分頁設置
$config['full_tag_open']        = '<div class="pagination" style="float: right;"><ul>';
$config['full_tag_close']       = '</ul></div>';

$config['query_string_segment'] = 'page';
$config['prev_link']            = ' <i class="fa fa-angle-left"></i> 上頁 ';
$config['prev_tag_open']        = '<li class="prev">';
$config['prev_tag_close']       = '</li>';
$config['next_link']            = '下頁 <i class="fa fa-angle-right"></i> ';
$config['next_tag_open']        = '<li class="next">';
$config['next_tag_close']       = '</li>';
$config['cur_tag_open']         = '<li class="active"><a>';
$config['cur_tag_close']        = '</a></li>';
$config['num_tag_open']         = '<li>';
$config['num_tag_close']        = '</li>';

$config['first_tag_open']       = '<li>';
$config['first_link']           = '<i class="fa fa-angle-double-left"></i> 第一頁';
$config['first_tag_close']      = '</li>';
$config['last_tag_open']        = '<li>';
$config['last_link']            = '最後頁 <i class="fa fa-angle-double-right"></i>';
$config['last_tag_close']       = '</li>';

//url位址
// $config['base_url']             = site_url($this->router->class.'/list/'); 
// $config['first_url']            = site_url($this->router->class.'/list/');
// $config['total_rows']           = count($data_arr);
// $config['num_links']            = 2;            // 顯示分頁數量
// $config['per_page']             = 10;           // 每頁顯示數量
// $config['use_page_numbers']     = TRUE;         // 使用頁碼方式而非偏移量方式傳值
